cmake_minimum_required(VERSION 2.8)
if(UNIX)
  set(CMAKE_CXX_FLAGS  "-pipe -Wall -march=broadwell")
  set(CMAKE_BUILD_TYPE "Debug" CACHE STRING "the build target")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -pg -O2 ")
  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -Wextra -Wunused -ggdb")
  set(CMAKE_EXE_LINKER_FLAGS "-O0")
  set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-pg")
  set(CMAKE_EXE_COMPILER g++ CACHE STRING "the c++ compiler")


  set(qig_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}" CACHE PATH "target directory for the executable")
  set(qig_BUILD_DIRECTORY "./build" CACHE PATH "where to build")


  set(BIN_INSTALL_DIRECTORY "${CMAKE_INSTALL_PREFIX}/bin/" CACHE PATH "where to install binaries")
  set(LIB_INSTALL_DIRECTORY "${CMAKE_INSTALL_PREFIX}/lib/" CACHE PATH "where to install libraries")
  set(HDR_INSTALL_DIRECTORY "${CMAKE_INSTALL_PREFIX}/include/" CACHE PATH "where to install header")
elseif(WIN32)
endif()
