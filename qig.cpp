// vim: fdm=marker

#include <stdio.h>
#include <git2.h>

#include "src/Application.h"



int main(int argc, char **argv)
{
  ::qig::qigApplication app(argc, argv);
  return app.exec();
}
