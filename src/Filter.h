#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include "enums.h"


namespace qig
{
  /**
   * @ingroup Application
   * @class qigFilter
   * @brief this class is not intended to be created directly but serves as a base class for fall filter.
   * each filter must have a specific purpose (a specific field to match or a specific oid to start, hide)
   * and a specific type (whether to filter a specifc object type or to apply on a traverse)
   *
   * on the other hand. all filter classes a simple containers. The object may decide on there own how to do a filtering
   */
  class qigFilter
  {
    public:
      /** creates a new filter with type type for field field
       * @param fields the field which to apply the filter at.
       * @param is_empty flag that will be returned in isEmpty();
       * @param type this determines to which class the filter can be cast. Instead of using type using a dynamic cast would also server this purpose
       */
      qigFilter(logFilter::fields fields, bool is_empty, logFilter::type type): _m_fields(fields), _m_type(type), _m_is_empty(is_empty) {}
      virtual ~qigFilter(){}

      /** returns the filter type
       * @return the filter type
       */
      logFilter::type type()const {return _m_type;}
      /** returns the filter field
       * @return the filter field
       */
      logFilter::fields fields()const {return _m_fields;}
      /** returns true if this is an empty filter
       */
      bool isEmpty() const {return _m_is_empty;}
    private:
      const logFilter::fields _m_fields;
      const logFilter::type _m_type;
      bool _m_is_empty;
  };
}
