#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include "gitTypes.h"

typedef struct git_oid git_oid;

namespace qig
{
  class qigBase;
  class qigOid;

  /**@class qigGitOidHolder
   * @brief data holder for git_oid 
   *
   * any class that inherit form this class should reimplement reset() and reloadFromId()
   *
   * reset() is emited if the current object become invalid. for example when loading another object
   * reloadFromId() is emited if another object came in scope. for example after loading another object
   */
  class qigGitOidHolder
  {

    public:
      qigGitOidHolder();
      /** constructs a new object from an git_oid.
       * this constructor does not steal the ownership of id
       * but copies the data to an internal buffer;
       * no object passed by the user will be freed
       */
      qigGitOidHolder(const git_oid* id, git_repository*);
      qigGitOidHolder(const qigGitOidHolder& id);
      qigGitOidHolder(qigGitOidHolder&& id);
      ~qigGitOidHolder();

      // returning a nonconst oid may render the state invalid because nobody emits reload
      //git_oid *oid(){return _m_oid;};
      /** returns the wraped oid;
       */
      const git_oid *oid()const{return _m_oid;};

      /** loads a new id from @a id. this function copies id to an internal buffer.
       * the @a id and @a p still belongs to the user
       *
       * @param id the new id to load
       * @param p the repository to which the id belongs
       */
      void load(const git_oid* id, git_repository* p);
      void load(const qigGitOidHolder& oid){load(oid._m_oid, oid._m_repo);}

      /**
       * does the same as load except load from data except of an git_oid
       * @param data contains the 20 bytes of the SHA1
       * @param p the repository to which the id belongs
       */
      void fromRaw(const unsigned char*data,  git_repository*p);

      /** returns the git_repository to which this oid belongs
       */
      git_repository* owner()const;

      /** resets internal state.
       * implementation should reset the internal state
       */
      virtual void reset()=0;

      /** returns pointer to member
       * do not free the returned pointer
       */
      //operator git_oid*() { return _m_oid; }
      //operator const git_oid*() const { return _m_oid; }
      const git_oid* rawValue() const { return _m_oid; }
      /** assignment operators. calls virtual methods reset and reloadFromId
       * but assignment also requiers git_repository 
       * @sa load
       */
      qigGitOidHolder& operator=(const git_oid& id)=delete;
      qigGitOidHolder& operator=(const qigGitOidHolder& oid){load(oid); return *this;}
      // also define move assignment operator
      qigGitOidHolder& operator=(qigGitOidHolder&& oid);


      /** returns true if current oid is a valid oid
       */
      bool isValid()const {return _m_repo != 0;}
      
      /** returns true if the current oid is loaded.
       */
      bool operator==(const git_oid& id)const;
      bool operator==(const qigGitOidHolder& oid)const{return this == &oid ? true :*this == *oid._m_oid;}

      /** loads p_oid into oid.
       * TODO doesn't really fit here. should be part of qigOid.
       */
      static void assign(qigOid& oid, const git_oid *p_oid, git_repository *p_repo) {
	reinterpret_cast<qigGitOidHolder&>(oid).load(p_oid, p_repo);
      }
    private:
      /// clear internal buffer unconditionally (except it it 0)
      void _clear();
      /// assign o to internal buffer unconditionally. But ensure to free the buffer before assign new value
      void set(const git_oid* p, git_repository* r);
      /** called when another oid is loaded.
       * class that inherit from this class must reimplement reload.
       * the new oid can be queried by resolve(type)
       * @sa resolve
       */
      virtual void reloadFromId()=0;

      /// the oid that is wrapped
      git_oid* _m_oid;
      /// the repo to which the oid belongs
      git_repository* _m_repo;
  };
}
