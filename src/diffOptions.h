
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include <string.h>
#include "Str.h"
#include "Oid.h"
#include "enums.h"

namespace qig
{
  struct diffOptions
  {
    enum sides { oldSide=0, newSide=1};
    diffOptions():
      target(0)
    {
      memset(tree_source, 0, sizeof(tree_source));
      memset(object_type, 0, sizeof(object_type));
    }

    static diffOptions diffTreeToTree(qigOid&& old_tree, qigOid&& new_tree);
    static diffOptions diffTreeToTree(const qigOid& old_tree, const qigOid& new_tree)
    {
       return diffTreeToTree(qigOid(old_tree), qigOid(new_tree));
    }
    static diffOptions diffBufferToBlob(qigString&& path, qigOid&& old_tree, objectType type);
    static diffOptions diffBufferToIndex(qigString&& path);

    /**
     * defines the source of the diff; value is value from treeSource
     */
    int tree_source[2];

    /**
     * the oid that is used in as source. depending on the tree_source
     * the oid might be a oid for commit, tree or blob.
     *
     * might be empty if tree_source is either treeSourceWD or treeSourceIndex
     */
    qigOid oid[2];
    /** 
     * the type of the oid;
     */
    objectType object_type[2];
    /// the file for the diff. might be empty;
    qigString name;

    /**
     * whether target is diff between a blob or diff between 2 tree
     */
    int target;
    inline bool isValid() const;
    private:
    inline bool test(const qigString & , const qigOid &, int type) const;
  };

  inline bool diffOptions::isValid() const
  {
    // do not test type???
    return test(name, oid[0], tree_source[0]) && test(name, oid[1], tree_source[1]);
  }

  bool diffOptions::test(const qigString& str, const qigOid& oid, int type) const
  {
    switch(type)
    {
      case treeSourceOid:
      case treeSourceRef:
	return oid.isValid();
      case treeSourceWD:
      case treeSourceIndex:
	return ! str.empty();
      default:
	return false;
    }
  }
}
