// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once

#include <QAbstractItemModel>
#include <memory>
#include <deque>
#include <QString>

#include "Oid.h"
class QFileIconProvider;
namespace qig
{
  class qigTree;
  class treeEntry2;
  class qigStatusTree;
  class qigFileIconProvider;
  /**
   * @class qigTreeModel
   * model for a git tree. a model used to show a git tree
   * to the user. this model is not used to show the current state
   * of a workdir
   */
  class qigTreeModel: public QAbstractItemModel
  {
    typedef std::unique_ptr<treeEntry2> treeEntryPtr;
    typedef std::unique_ptr<qigFileIconProvider> fileIconProviderPtr;
    typedef std::deque<treeEntryPtr> fileMap;
    Q_OBJECT
    public:
      qigTreeModel(QObject* parent);
      ~qigTreeModel();

      /** set tree to operate on.
       * method will fail if tree is already loaded or tree is invalid.
       * @return error code
       */
      int setTree(const qigTree &tree, const qigStatusTree *st);
      /** set oid to operate on.
       * method will fail if tree is already loaded or oid is invalid or not a tree.
       * oid must be an oid to a tree object
       * @return error code
       */
      int setOid(const qigOid &oid);
      /** prefix to the root of the git repository
       * the prefix is appended to the local path while queriing for the file path
       * @param prefix a '/' terminated string to the repositories working dir
       */
      void setPrefix(const QString& p){m_working_dir = p;}

      /** resets and clear internal data.
       */
      void repositoryUnloaded() { resetInternalData();}

      /** reimplementation for QAbstractItemModel
       */
      virtual QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const override;
      //virtual void resetInternalData(){reset();} override;
      virtual int rowCount(const QModelIndex& parent = QModelIndex()) const override;
      virtual int columnCount(const QModelIndex& parent = QModelIndex()) const override;
      virtual bool hasChildren(const QModelIndex &parent = QModelIndex()) const override;
      virtual QModelIndex index(int row, int col, const QModelIndex& parent) const override;
      virtual QModelIndex parent(const QModelIndex& child) const override;
      virtual Qt::ItemFlags flags(const QModelIndex &index) const override;

    protected:
      void resetInternalData();

    private:
      QVariant	    dataDisplay(const QModelIndex&) const;
      QVariant 	    dataDecoration(const QModelIndex&)const;
      QVariant 	    dataOid(const QModelIndex&)     const;
      bool 	    dataIsDir(const QModelIndex&)     const;
      unsigned int  dataFileState(const QModelIndex&)     const;
      QString	    dataFilePathReleative(const QModelIndex&)     const;
      QString	    dataFilePath(const QModelIndex&)     const;
      inline const treeEntry2* toTreeItem(const QModelIndex& index) const;
      const QModelIndex toModelIndex(const treeEntry2* item, int column=0) const;


      void buildTree(fileMap& m_files, const qigTree& t, treeEntry2* parent, const qigStatusTree* st, QString prefix = QString());
      fileMap m_files;
      treeEntryPtr m_root;
      fileIconProviderPtr m_fileIconProvider;

      /** chached oid of currenlty loaded tree
       */
      qigOid m_tree_oid;

      QString m_working_dir;
  };

  inline const treeEntry2* qigTreeModel::toTreeItem(const QModelIndex& index) const
  {
    void* internal = index.internalPointer();
    return internal ? 
      reinterpret_cast<const treeEntry2*>(internal):
      m_root.get();
  }
}
