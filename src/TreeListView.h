#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */

#include <QTableView>
#include "TreeView.h"

namespace qig
{
  //typedef  QListView QView;
  typedef QTableView QView;
  class qigOid;
  /**@class qigTreeListView
   * @brief view to show a git tree or work dir
   * @internal uses Qt::ActionsMenuPolicy. QActions are added by the qigMainWindow
   */
  class qigTreeListView: public qigTreeView<QView>
  {
    Q_OBJECT
    public:
      qigTreeListView(QWidget* p);
      /** override from QAbstractItemView
       */
      virtual void selectionChanged(const QItemSelection& selected, const QItemSelection& deselected) override;

    Q_SIGNALS:
      // emitted when the selection in the tree view changes.
      //        
      void selectionChanged();

    protected:

      virtual void dataChanged(const QModelIndex& topleft, const QModelIndex& bottomRight, const QVector<int>& roles) override;
      /** override from QWidget
       */
      virtual void keyPressEvent(QKeyEvent *p) override;

    private Q_SLOTS:
      void onActivated(const QModelIndex& index){return qigTreeView::onActivated(index);}
    private:
      //updates view to show content of parent of current selection;
      //does nothing if parent of current selection is invalid;
      void moveUp()override;


  };
  //typedef QListView qigTreeListView;
  //TODO implement signal to go one level up in filesystem
}
