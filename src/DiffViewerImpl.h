// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include <QString>
#include <QGlobalStatic>

#include <memory>
#include <deque>

#include "treeEntry.h"
#include "gitTypes.h"
#include "IDiffViewer.h"

namespace qig
{

  struct DiffBinary;
  struct DiffFileModelItem;
  struct DiffHunkModelItem;
  struct DiffLineModelItem;
  struct DiffDeltaModelItem;

  /**
   * @ingroup Application
   * @class DiffViewerBuiltinModelItem
   * @brief base class for all model entries.
   * 
   * the class defines an api that must be implemented.
   * {{{
   */
  class DiffViewerBuiltinModelItem: public treeEntry
  {
    public:
      // do not add the structure to the tree when constructed but when finalise is called
      DiffViewerBuiltinModelItem(int type):
	treeEntry(type)
      {
      }
      DiffViewerBuiltinModelItem(DiffViewerBuiltinModelItem *parent, int type):
	treeEntry(type)
      {
	setParent(parent);
      }
      /// non virtual override of base impl to return our base type 
      inline const DiffViewerBuiltinModelItem* parent()const;
      /// override base impl with non const version.
      inline DiffViewerBuiltinModelItem* childByIndex(unsigned int index);

      /// returns the string representation
      virtual QString dataDisplay() const {throw 1;};
      /// returns the color
      virtual Qt::GlobalColor dataForeground()const{return Qt::black;};

      unsigned int height(){return type() - 2;}

      inline void addToTreeStructure();
  };
  //}}}

  /**
   * @class qigDiffViewerImpl
   * @brief implementation parts of the IDiffer. 
   *
   * the implementation stores the files, hunks and lines in an tree structure. The tree has a virtual root @sa m_root which is parent of all files available.
   * The implementation does not cover the pure virtual function `finalise`. specialisation may access the stored
   * diff data by querying the root
   */
  class qigDiffViewerImpl: public IDiffer
  {
    typedef std::unique_ptr<DiffViewerBuiltinModelItem> DiffViewerBuiltinModelItemPtr;
    typedef std::unique_ptr<DiffDeltaModelItem> DiffDeltaModelItemPtr;
    typedef std::deque<DiffViewerBuiltinModelItemPtr> modelEntries;
    typedef DiffViewerBuiltinModelItem root;
    protected:
      virtual ~qigDiffViewerImpl(){}
    public:
      qigDiffViewerImpl();
      qigDiffViewerImpl(const qigDiffViewerImpl&)=delete;

      /** override of IDiffer 
       * default is to continue but ignore the delta otherwise
       * @sa IDiffer::accept
       */
      virtual int addDelta( IDiffDelta **delta) override;
      /** function sets *file to an allocated IDiffFile object that
       * @param file [out] a pointer to a pointer of IDiffFile
       * @return 0 on success
       */
      virtual int addFile(IDiffFile**file) override;
      //TODO
      void addBinary(DiffBinary **hunk);
      /** called by qig to inform user about a new hunk is available
       *  might be called several times for a single a file
       *
       *  user must set *hunk to an empty, allocated implementation of IDiffHunk
       *
       * @param file      the parent of the hunk.
       * @param hunk [out] a pointer to a pointer that the user should set to an
       *  empty implementation of IDiffHunk
       * @return 0 on success
       */
      virtual int addHunk(IDiffFile* file, IDiffHunk **hunk)override;
      /** function sets *line to an allocated DiffLineModelItem object that should be filled by user
       * @param parent the parent hunk of the new line to which the new line should be added
       * @param line [out] a pointer to a pointer of DiffLineModelItem
       * @return 0 on success
       */
      virtual int addLine(IDiffHunk *parent, IDiffLine **line) override;

      /** default impl returns 0 to continue the diff process
       */
      virtual int accept(IDiffDelta *)override{return 0;}

      /** resets tree structure and delete deallocate the owned objects;
       */
      virtual void reset()override;

      virtual void release() override{ delete this;}
    private:
      // owns entries
      modelEntries m_model_entries;
      // the root of the tree
    protected:
      root m_root;

      DiffDeltaModelItemPtr m_delta;
      

  };


  //{{{ helper classes to build the tree

  struct DiffDeltaModelItem:public DiffViewerBuiltinModelItem, public IDiffDelta
  {
    public:
      DiffDeltaModelItem(DiffViewerBuiltinModelItem* root):DiffViewerBuiltinModelItem(root,typeId()){};
      virtual void setStatus(int st) {m_status = st;}
      virtual void setFlag(int flags){m_flag = flags;}
      virtual void setPath(const qigChar *old_path, const qigChar* new_path) {
	m_new_path = new_path;
	m_old_path = old_path;
      }

      constexpr static size_t typeId(){return 1;}// same as root;

      //virtual QString dataDisplay() const;
      virtual void finalise(){};

      int status() const { return m_status;}
      int flags() const { return m_flag;}
    //private:
      int m_flag;
      int m_status;
      QString m_new_path;
      QString m_old_path;
  };

  struct DiffFileModelItem: public DiffViewerBuiltinModelItem, public IDiffFile
  {
    public:
      DiffFileModelItem(DiffViewerBuiltinModelItem* root):DiffViewerBuiltinModelItem(root,typeId()){};
      virtual void setStatus(int st) {m_status = st;}
      virtual void setFlag(int flags){m_flag = flags;}
      void setPath(const qigChar* old_path, const qigChar* new_path){ 
	m_old_path = QString(old_path);
	m_new_path = QString(new_path);
      }
      constexpr static size_t typeId(){return 2;}

      virtual QString dataDisplay() const;

      virtual void finalise(){addToTreeStructure();}

      int status() const { return m_status;}
      int flags() const { return m_flag;}

    private:
      int m_flag;
      int m_status;
      QString m_old_path;
      QString m_new_path;
  };

  /**@class DiffHunkModelItem
   * struct to store properties of a hunk
   */
  struct DiffHunkModelItem: public DiffViewerBuiltinModelItem, public IDiffHunk
  {
    public:
      DiffHunkModelItem(DiffFileModelItem *parent):DiffViewerBuiltinModelItem(parent,typeId()){}
      virtual void setOldStart(int old_start){ m_old_start=old_start; }         /**< Starting line number in old_file */
      virtual void setOldLines(int old_lines){ m_old_lines=old_lines; }         /**< Number of lines in old_file */
      virtual void setNewStart(int new_start){ m_new_start=new_start; }         /**< Starting line number in new_file */
      virtual void setNewLines(int new_lines){ m_new_lines=new_lines; }         /**< Number of lines in new_file */
      virtual void setHeader(const qigChar header[128]){ m_header=header; m_header=m_header.trimmed(); };        /**< Header text, NULL-byte terminated */
      virtual void setHeaderLen(size_t ) {};    /**< Number of bytes in header text */

      int oldStart() const { return m_old_start; }
      int oldLines() const { return m_old_lines; }
      int newStart() const { return m_new_start; }
      int newLines() const { return m_new_lines; }
      int headerLen()const { return m_header.size(); }
      const QString& header() const { return m_header; }

      virtual QString dataDisplay() const;
      virtual void finalise(){addToTreeStructure();}

      constexpr static size_t typeId(){return 3;}
    private:
      int m_old_start;
      int m_old_lines;
      int m_new_start;
      int m_new_lines;
      QString m_header;
  };

  /**@class DiffLineModelItem
   * struct to store properties of a line
   */
  struct DiffLineModelItem: public DiffViewerBuiltinModelItem, public IDiffLine
  {
    public:
      DiffLineModelItem(DiffHunkModelItem *parent):DiffViewerBuiltinModelItem(parent, typeId()){}
      /** A git_diff_line_t value */
      void setOrigin(     char   origin){ m_origin = origin;}
      /** Line number in old file or -1 for added line */
      void setOldLineno(  int    old_lineno){ m_old_lineno = old_lineno;}
      /** Line number in new file or -1 for deleted line */
      void setNewLineno(  int    new_lineno){ m_new_lineno = new_lineno;}
      /** Number of newline characters in content */
      void setNumLines(   int    num_lines){ m_num_lines = num_lines;}
      /** Pointer to diff text, NOT NULL-byte terminated */
      void setContent(    const char *content, size_t content_len){ m_content = QByteArray(content, content_len -1); }// do not include the new line
      //void setContentOffset(size_t content_offset) = 0; /**< Offset in the original file to the content */

      constexpr static size_t typeId(){return 4;}

      virtual QString dataDisplay() const;
      virtual Qt::GlobalColor dataForeground()const;
      virtual void finalise()
      {
	//m_display_data = QString(QString(origin()) % QString(L' ') % content());
	m_display_data = QString(origin()) + QString(L' ') + content();
	addToTreeStructure();
      }

      char origin() const {return m_origin;}
      const QString& content() const {return m_content;}
      int numLines() const { return m_num_lines;}

    private:
      char m_origin;
      int m_old_lineno;
      int m_new_lineno;
      int m_num_lines;
      QString m_content;
      QString m_display_data;
  };
  inline void DiffViewerBuiltinModelItem::addToTreeStructure()
  {
    treeEntry *parent = _parent();
    parent->pushChild(this);
  }
  inline const DiffViewerBuiltinModelItem* DiffViewerBuiltinModelItem::parent()const
  {
    return static_cast<const DiffViewerBuiltinModelItem*>(treeEntry::parent());
  }
  inline DiffViewerBuiltinModelItem* DiffViewerBuiltinModelItem::childByIndex(unsigned int index)
  {
    return static_cast<DiffViewerBuiltinModelItem*>(treeEntry::_childByIndex(index));
  }
  //}}}

}
