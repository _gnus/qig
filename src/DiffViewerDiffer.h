// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include <vector>
#include <stdio.h>
#include <memory>
#include "enums.h"
#include "List.h"
namespace qig
{
  class qigOid;
  class qigDifferInterfaceManager;
  class IDiffViewerExternal;
  class IDiffer;
  struct diffOptions;
  class IDifferFactory;

  template <typename _Tp>
    struct releaser
    {
	releaser(){};
	void operator()(_Tp* p)const{p ->release();}
    };

  /** @ingroup Application
   * @class qigDifferHelper
   * helper class that selects an IDifferFactory and initialise an IDiffer based on diffoptions passed to the static function createDifferHelper
   */
  class qigDifferHelper
  {
    public:
      enum SourceType
      {
	/// file path should be considered as information for the user. file content on disk may differ from blob
	sourceTypeRepresentation,
	/// file from disk should be used as new source
	sourceTypeNewSource,
	/// file from disk should be used as old source
	sourceTypeOldSource
      };
    public:
      virtual ~qigDifferHelper() = default;

      static int diff(const diffOptions &options, const qigDifferInterfaceManager &);

    protected:
      virtual int diff(const diffOptions&)=0;
    private:
      //static int diff(const diffOptions &options, qigDifferHelper & helper);

    protected:

      typedef std::vector<char> dataBuffer;
      // implemented in DiffViewerBuiltinDiffer.cpp
      static std::size_t readFile(const char* file, dataBuffer&);
      // read from file structure file.
      // @param file a file ptr will be used to as source to read. file must be ready to read from ( already opened ) but will be closed automatically; 
      static std::size_t readFile(FILE* file, dataBuffer&);
  };

  /** @class qigDifferInterfaceManager {{{
   * holds interfaces to different difffactories 
   * also stores the currently selected factory.
   *
   * TODO provide pointer to static object and allow registration
   */
  class qigDifferInterfaceManager
  {
    typedef IDifferFactory factory;
    typedef qigList<IDifferFactory*> differList;
    typedef std::unique_ptr <factory,		    releaser<factory>		  > diffFacotryPtr;
    typedef std::unique_ptr <IDiffViewerExternal,   releaser<IDiffViewerExternal> > differExternalPtr;;
    typedef std::unique_ptr <IDiffer,		    releaser<IDiffer>		  > differPtr;;
    public:
      qigDifferInterfaceManager();
      ~qigDifferInterfaceManager();

      IDifferFactory* selectedDifferFactory() const;
      IDiffViewerExternal* selectedDifferExternal() const;
      IDiffer* selectedDiffer() const;
    private:
      IDifferFactory* m_selected_differ_factory;

      differExternalPtr m_selected_differ_external;
      differPtr m_selected_differ;


      std::vector<diffFacotryPtr> m_static_factories;

      differList m_diff_list;
  };
  //}}}
}
