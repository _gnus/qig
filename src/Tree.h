// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include <memory>
#include "gitTypes.h"
#include "Object.h"
#include "Oid.h"
#include "enums.h"

namespace qig
{
  class qigOid;

  /**@class qigTreeEntry  {{{
   * wraps a git_tree_entry;
   */
  class qigTreeEntry
  {
    struct entry_deleter {
      void operator()(git_tree_entry*) const;
    };
      typedef std::unique_ptr<git_tree_entry, entry_deleter> ownedTreeEntryPtr;
    public:
      qigTreeEntry();
      /// TODO not really a copy ctor
      qigTreeEntry(const qigTreeEntry&);
      qigTreeEntry(qigTreeEntry&&)=default;
      ~qigTreeEntry()=default;

      const qigChar* name()const;
      const git_oid* oid() const;
      objectType type()   const;

      unsigned int index() const {return m_idx;}
      int compare(const qigTreeEntry& o) {return m_entry == o.m_entry;}

      // assums that e is owned by someone other;
      void load(const git_tree_entry* e, unsigned int i){m_entry=e; m_idx=i;}
      /// takes ownership of e
      void load(git_tree_entry* e, unsigned int i);
      void reset();
      qigTreeEntry& operator=(const qigTreeEntry&);
      qigTreeEntry& operator=(qigTreeEntry&&)=default;
    private:
      unsigned int m_idx;
      const git_tree_entry *m_entry;
      /// owned by us. must be released by us;
      ownedTreeEntryPtr m_owned;
  };
  //}}}

  /**@class qigTree
   * wraps tree.h from libgit2
   */
  class qigTree: public qigObject
  {
    friend class qigDifferHelperBuiltinTreeToTree;
    friend class qigCommit;
    public://{{{
      qigTree();
      /** creates an tree from given oid. the oid must be valid and peelable to a tree.
       */
      qigTree(const qigOid&);

      /**
       * Get the number of entries listed in a tree
       *
       * @return the number of entries in the tree
       */
      size_t entryCount() const;

      /** returns the name of the tree entry selected by index @a entry_index
       * @param entry_index the index entry. must be in [0;entryCount()[
       */
      const qigChar* name (             unsigned int entry_index) const{ return updateCachedEntry(entry_index) ? m_recently_queried_entry.name() : nullptr;}
      /** copies the oid of the tree entry selected by index @a entry_index
       * @param entry_index the index entry. must be in [0;entryCount()[
       * @param out[out] contains the oid on return
       * @return the name of the object.
       */
      inline int      oid  (qigOid&out, unsigned int entry_index) const;
      /** returns the name of the tree entry selected by index @a entry_index
       * @param entry_index the index entry. must be in [0;entryCount()[
       * @return the type of the object.
       */
      objectType    type (             unsigned int entry_index) const{ return updateCachedEntry(entry_index) ? m_recently_queried_entry.type() : objectTypeBad;}

      /** overload to return the name by name.
       * @param path the path of selected entry
       */
      const qigChar* name (             const qigChar* path) const{ return updateCachedEntry(path) ? m_recently_queried_entry.name() : nullptr;} 
      /** overload to return the oid by entry.
       * @param path the path of selected entry
       * @param out[out] contains the oid on return
       */
      inline int      oid  (qigOid&out, const qigChar* path) const;
      /** overload to return the type by entry.
       * @param path the path of selected entry
       * @return the type of the object.
       */
      objectType    type (             const qigChar* path) const{ return updateCachedEntry(path) ? m_recently_queried_entry.type() : objectTypeBad;}
#if 0
      /**
       * Lookup a tree entry by its filename
       *
       * @param entry[out] the tree entry;
       * @param filename the filename of the desired entry
       */
      int entryByPath(qigTreeEntry &index, const qigChar* path);
      /*
       * Lookup a tree entry by SHA value.
       *
       * Warning: this must examine every entry in the tree, so it is not fast.
       *
       * @param tree a previously loaded tree.
       * @param id the sha being looked for
       * @return the tree entry; NULL if not found
       */
      //int entryById(qigTreeEntry &, const qigOid &oid);
      /**
       * Lookup a tree entry by its index
       *
       * @param entry[out] the tree entry;
       * @param filename the filename of the desired entry
       */
      int entryByIndex(qigTreeEntry &index, size_t index);
#endif

    //}}}

    protected://{{{ protected methods
      qigTree(git_tree*);

      /** reimplemented from qigGitObjectHolder
       * 
       * nothing to do here. just call qigObject::reload
       *
       */
      virtual void reload(){ qigObject::reload();}//TODO check the type of the new object
      /** reimplemented from qigGitObjectHolder
       * 
       * nothing to do here. just call qigObject::reset
       */
      virtual void reset(){m_recently_queried_entry.reset(); qigObject::reset();}
      //}}}

    private://{{{

      bool updateCachedEntry(unsigned int i) const;
      bool updateCachedEntry(const qigChar*name) const;
      /** reimplements qigObject:: to return git_commit or nullptr
       */
      const git_tree* tree()const {return (const git_tree*)object(objectTypeTree);}
      git_tree*       tree()      {return       (git_tree*)object(objectTypeTree);}

      /// override the current tree with the other
      qigTree& operator=(const git_tree*other);
      void _clear();

      mutable qigTreeEntry m_recently_queried_entry;

      static qigObject peeledTree(const qigOid&);
      //}}}
  };
  inline int qigTree::oid(qigOid& out, unsigned int entry_index) const {
    if(updateCachedEntry(entry_index)) {
      out.qigGitOidHolder::load(m_recently_queried_entry.oid(), owner());
      return GIT_OK;
    }
    else {
      out = qigOid();
      return GIT_ENOTFOUND;
    }
  }
  inline int qigTree::oid(qigOid&out, const qigChar* path) const
  {
    if(updateCachedEntry(path)) {
      out.qigGitOidHolder::load(m_recently_queried_entry.oid(), owner());
      return GIT_OK;
    } 
    else {
      out = qigOid();
      return GIT_ENOTFOUND;
    }
  }
}
