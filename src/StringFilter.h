#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <QString>

#include "Filter.h"
#include "enums.h"
#include "gitTypes.h"

namespace qig
{
  class qigStringFilter: public qigFilter
  {

    /** filter for string properties.
     * a container for a string.
     */
    typedef QByteArray stringType;
    public:
      qigStringFilter(const QString& filterstring, logFilter::fields fields):
	qigFilter(fields, filterstring.isEmpty(), logFilter::typeStringFilter),
	m_filterstring(filterstring.toUtf8())
      {
      }

      /** returns the string given in the ctor
       */
      const qigChar* filterstring()const {return m_filterstring.constData();};


    private:
      const stringType m_filterstring;

  };
}
