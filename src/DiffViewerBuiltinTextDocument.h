// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
/// @defgroup Application Application
#include <QTextDocument>
#include <QTextCursor>
#include <QTextCharFormat>

#include "DiffViewerImpl.h"

class QAbstractItemModel;
class QTextCursor;
class QTextEdit;
class QPlainTextEdit;

namespace qig
{
  /**
   * @ingroup Application
   * @class qigDiffViewerBuiltinTextDocument
   * @brief a backend for storing diffs.
   *
   * the qigDiffViewerBuiltinTextDocument operates on a QTextDocument and requires a QTextEdit or QPlainTextEdit to render the text.
   * the model implements the @ref IDiffer interface.
   */
  class qigDiffViewerBuiltinTextDocument: public QTextDocument, public qigDiffViewerImpl
  {
    Q_OBJECT
    public:
      qigDiffViewerBuiltinTextDocument(QObject *parent);
      qigDiffViewerBuiltinTextDocument(const qigDiffViewerBuiltinTextDocument&) = delete;
      virtual ~qigDiffViewerBuiltinTextDocument();

      // impl for IDiffer
      virtual void finalise(IDiffBase *item);

      /** use model as source. reset and insert row is implemented only.
       * deprecated. Use qigDiffViewerDiffer instead
       */
      void setModel(QAbstractItemModel* model);

      // tries to fold block that contains the cursor
      // @return number of lines that were folded
      int fold(QTextCursor&);
      // tries to unfold block that contains the cursor
      // returns 1 on success
      int unfold(QTextCursor&);

      void reset();

      /** set the format that should be used.
       * some properties like the color will be ignored
       */
      void setCharFormat(const QTextCharFormat& format);

    public Q_SLOTS:
      //void rowsAboutToBeInserted(const QModelIndex &parent, int first, int last);
      void rowsInserted(const QModelIndex &parent, int first, int last);
      //void modelAboutToBeReset();
      void modelReset();
      //void rowsAboutToBeMoved( const QModelIndex &sourceParent, int sourceStart, int sourceEnd, const QModelIndex &destinationParent, int destinationRow);
      void rowsMoved( const QModelIndex &, int , int , const QModelIndex &, int ){}
      

    private:
      /// recursively calls append text on item and its children
      void appendTreeItemRecursively(DiffViewerBuiltinModelItem* item);
      /// small wrapper around append text. Should be called from appendTreeItemRecursively only
      inline void _appendTreeItem(DiffViewerBuiltinModelItem*);
      /// deprecated. Appends text based on information from index
      inline void appendText(const QModelIndex& index);

      // inserts text to document and sets fold level on QTextBlock
      void appendText(const QString& text, unsigned int folding_level);
      QTextCharFormat  m_char_format;
      QAbstractItemModel *m_model;
      QTextCursor m_cursor;
  };


  /** @class qigDifferFactoryBuiltin
   *
   * implements the IDifferFactory for the builtin differ
   */
  class qigDifferFactoryBuiltin:public IDifferFactory
  {
    public:
      qigDifferFactoryBuiltin():
	m_view(0),
	m_differ(0)
	{}
      virtual ~qigDifferFactoryBuiltin()=default;


      /** this function may return a differ that uses its own diff engine
       * may return 0 if not supported
       */
      virtual IDiffViewerExternal* createDifferExternal() override {return nullptr;}
      /** this function may return a differ that uses the built-in diff engine.
       * may return 0 if not supported
       */
      virtual IDiffer* createDifferBuiltin() override {return m_differ ? m_differ : m_differ = new qigDiffViewerBuiltinTextDocument(nullptr);}

      /** if this function returns true the factory can provide a QWidget that is embeddable in qig
       */
      virtual bool provideView() const override {return true;}

      /** this function may return a view for the differ returned by createDiffer
       * changes in the differ must be reflected in the view without further calls from qig
       * @sa provideView;
       * @param parent a QWidget that can be used as parent in Qt parent child hierarchy. Otherwise it should be ignored.
       */
      virtual QWidget *createView(QWidget *parent) override;// {return new QTextEdit(parent);} // todo set options

      /**a small string that describes the diff interface. the string could be the program that the interface uses
       */
      virtual const char* name() const override {return "builtin";}
      /** returns an arbitrary string that describes the differ.
       * the description should fit in a small mouse over box and describe the properties of the differ
       *  and description might be "use the external program xy"
       */
      virtual const char* shortDescription(const char* ) const override {return "";};

      // 
      virtual QWidget * propertyWidget(QWidget*) override {return nullptr;}

      virtual void   serialise(      QSettings&)      override{};
      virtual void deserialise(const QSettings&)const override{};

      virtual void release() override{ delete this;};
    private:

      QPlainTextEdit *m_view;
      qigDiffViewerBuiltinTextDocument* m_differ;
  };
}

