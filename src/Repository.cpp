// vim: fdm=marker

#include "Repository.h"
#include "BranchManager.h"
#include "ReferenceManager.h"
#include "Status.h"
#include "Str.h"
#include "Index.h"
#include "Oid.h"

namespace qig
{
  
  //{{{//dummy class that contains the member of qigRepository*/
  /** @class _internalData
   * @internal
   * dummy class that contains the member of qigRepository*/
  class _internalData: private qigBase
  {
    public:
      // ctor
      _internalData(const qigBase& base):
	qigBase(base),
	reference_manager(base),
	branch_manager(base)
      {
      }

    // public attributes
    // the object that is called HEAD in git lang
    qigReferenceManager reference_manager;
    qigBranchManager branch_manager;
    // path to root in the repository
    int repository_root_path_length;
    qigString root_path;
  };
  //}}}
  
  //{{{ ctor,dtor
  qigRepository::qigRepository( qigBase && base):qigBase(std::move(base)),
    _m_internal_data(new _internalData(*this))
  {
  }
  
  qigRepository::~qigRepository()
  {
    if(mainRepo())
    {
      unload();
    }
    delete _m_internal_data;
  }
  //}}}

   //{{{ public methods

  // loads the git repo and initialise libgit2
  int qigRepository::load(qigRepository*& out, const qigChar* path, bool open_bare)
  {
    int ret;
    out = new qigRepository(qigBase::load(path, open_bare, ret));
    if(ret == GIT_OK)
    {
      git_repository* repo = out->mainRepo();
      // load the git repository
      const char *root=git_repository_path(repo);
      // should not fail
      // path includes the .git repo.
      unsigned int len = root ? strlen(root)-5 : 0;
      if(len)
      {
	out->_m_internal_data->repository_root_path_length = len;
	assert(root);
	out->_m_internal_data->root_path=std::string(root, root+len);
      }
    }
    else
    {
      delete out;
      out = nullptr;
    }
    return ret;
  }

  // unload libgit2
  void qigRepository::unload()
  {
    repositoryUnloaded();
  }


  int qigRepository::initialiseOid( qigOid &oid, const unsigned char* data) const
  {
    oid.fromRaw(data, mainRepo());
    return mainRepo() ? GIT_OK : GIT_ERROR;
  }

  const qigReferenceManager& qigRepository::referenceManager() const
  {
    // call the nonconst version;
    return const_cast<qigRepository*>(this)->referenceManager();
  }
  qigReferenceManager& qigRepository::referenceManager()
  {
    return _m_internal_data->reference_manager;
  }

  int qigRepository::relativePathFromAbsolutePath(const qigChar* abs_path, const qigChar** relative_path) const
  {
    *relative_path=nullptr;
    if(mainRepo())
    {
      const char *repository_root = git_repository_workdir(mainRepo());
      if(repository_root && abs_path)
      {
	if(strncmp(repository_root, abs_path, _m_internal_data->repository_root_path_length) == 0)
	{
	  *relative_path = abs_path + _m_internal_data->repository_root_path_length;
	  return GIT_OK;
	}
	return GIT_ENOTFOUND;
      }
    }
    return GIT_ERROR;
  }

  const char * qigRepository::repositoryRootPath() const
  {
    return _m_internal_data->root_path.c_str();
  }
  const char * qigRepository::wdPath() const
  {
    return git_repository_workdir(mainRepo());
  }
  //}}}

  // private method
  // {{{
  int qigRepository::initHead()
  {
    git_reference *p_ref;
    int ret;
    if( ( ret=git_repository_head(&p_ref, mainRepo()) ) == 0)
    {
      //_m_internal_data->HEAD.load(p_ref, mainRepo());
    }
    return ret;
  }

  void qigRepository::_load(git_repository* p_repo)
  {
    const char *root=git_repository_workdir(p_repo);
    // may fail if repo is bare;
    _m_internal_data->repository_root_path_length = root ? strlen(root) : 0;
    //_m_internal_data->branch_manager.init();
    initHead();
  }
  //}}}
}
