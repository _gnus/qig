
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <QObject>
class QWidget;

namespace qig
{
  /**
   * @ingroup Application
   * @class qigFocusWatcher
   * @brief watches for focus events and calls update on parent.
   *
   * this class watches for focus events on widgets that are registerd by watchWidgetForFocus
   * if it detects an focus event it calls update on the widget given in constructor
   */
  class qigFocusWatcher
  {
    public:
      qigFocusWatcher(QWidget* parent):
	widget_has_focus(false),
	m_parent(parent){}

      //TODO check wheter copy ctor works
      qigFocusWatcher(const qigFocusWatcher& other)=delete;

      // whether an watched widget has the focus
      bool widget_has_focus;

    protected:
      virtual bool eventFilter(QObject* o, QEvent*ev) override;

      QWidget *m_parent;
  };

  void qigDockWidget::watchWidgetForFocus(QWidget *obj)
  {
    if(obj) {
      obj->installEventFilter(this);
    }
  }

   bool qigDockWidget::eventFilter(QObject* o, QEvent* ev)
    {
      //qDebug() << "qigTreeContainer::eventFilter: event" << ev->type();
      switch(ev->type())
      {
	case QEvent::FocusIn:
	  widget_has_focus=true;
	  m_parent->update();
	  break;
	case QEvent::FocusOut:
	  widget_has_focus=false;
	  m_parent->update();
	  break;
	case QEvent::Paint:
	  //qDebug() << "qigTreeContainer::eventFilter: received paint event";
	  break;
	default:
	  return false;
      }
      return false;
    }
}
