#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <QAction>
#include <QKeySequence>
#include <QPainter>
#include <QStyleOptionFocusRect>
#include <QPaintEvent>

class QKeyEvent;

namespace qig
{
  /** @ingroup Application
   * @class qigKeyEventHandlerWorker 
   * the worker class for a qigKeyEventHandler
   * if an event matches the worker exactly 
   * the trigger signal will be emitted.
   */
  class qigKeyEventHandlerWorker: public QAction
  {
    static const int maxKeyLength=2;
    friend class qigKeyEventHandler;
    public:
      qigKeyEventHandlerWorker(QObject* parent=0);
      /**
       * @param parent the parent,
       * @param s passed to setKeySequence
       * @param slot the signal triggered will be connected to slot,
       * @param receiver object that owns slot. if receiver is 0 parent is used as receiver.
       * @param i passed to QActions ctor. the icon representation of the icon
       * @param t passed to QActions ctor. the text representation of the icon
       */
      qigKeyEventHandlerWorker(
	 QObject *parent, const QKeySequence& s, const char* slot,
	 QObject *receiver = 0, const QIcon&i=QIcon(), const QString&t=QString());

      /** the key sequence that should be matched.
       * when the key event matches the sequence the triggered signal is emitted.
       * the qigKeyEventHandler currently supports up to 2 keys
       * @param s the key sequence
       * @code
       * auto worker = new qigKeyEventHandlerWorker;
       * woker->setKeySequence(QKeySequence("d,d")) //< the triggered siganal will be emitted when the user preses 'd' two times
       * @endcode
       */
      void setKeySequence(const QKeySequence& s);

      /** the representation of the keysequence set with setKeySequence;
       */
      QString representation() const;

    protected:
      enum matchResult
      {
	// event does not match
	resultNoMatch,
	// event matches first part of worker, but the second part still misses
	resultPartialMatch,
	// event matches exactly
	resultIdentityMatch
      };
      matchResult match(QKeyEvent*, int stage) const;

    private:
      Qt::KeyboardModifiers m_flags[maxKeyLength];
      int m_keys[maxKeyLength];

      // indicates whether the worker uses a double key sequence
      bool m_complex_key_sequence;


  };


  /**@class qigKeyEventHandler
   * event handler that is capable to handle vim-like key events like d+d
   * the class operates in stages. If the event pushed by keyPressEvent matches
   * no qigKeyEventHandlerWorker the event is discarded. If a worker matches the event exactly 
   * the first worker that matches the event exactly is activated.
   */
  class qigKeyEventHandler
  {
    public:
      qigKeyEventHandler();

      // returns true if ev matches a worker either partially or exactly
      bool keyPressEvent(QKeyEvent*ev);
      // returns a string representation of the currently stored event.
      // if no event is stored, an empty string is returned.
      QString currentEvent();
      QStringList autocomplete();

      /** adds a new worker.
       * the qigKeyEventHandler does not take the ownership of the worker.
       */
      int addInputHandler(qigKeyEventHandlerWorker*);
      /** removes an qigKeyEventHandlerWorker
       * the qigKeyEventHandler does not delete worker. it is subject to the user
       * @code
       * if(handler.removeInputHandler(myWorker)==0)
       *   delete myWorker;
       * @endcode
       */
      int removeInputHandler(qigKeyEventHandlerWorker*);
      /** resets the internal state.
       * this is if the previous key event matches a key sequence partially
       * the key sequences will be stored and the next event will be compared to those partial matches only.
       * reset will cause to compare the next event to all qigKeyEventHandlerWorker
       */
      // not implemented
      void reset();


    private:
      //bool keyPressEventStage0(QKeyEvent *ev);
      bool keyPressEventStage1(QKeyEvent *ev);
      void triggerEvent(qigKeyEventHandlerWorker*);
      QVector<qigKeyEventHandlerWorker*> m_events_partial_match;
      QVector<qigKeyEventHandlerWorker*> m_events;
      int m_stage;
  };

  /**@class qigFocusFrameWidget
   * this class is an event observer that watches for FocusIn and FocusOut
   * evnets. if a watched Widget has the focus the the qigFocusFrameWidget 
   * will draw a frame (that must be defined with QStyleSheed) around itself
   *
   */
  template<typename T>
  class qigFocusFrameWidget: public T
  {
    typedef T BaseWidget;
    public:
      qigFocusFrameWidget(QWidget*parent):BaseWidget(parent),
	m_child_has_focus(false)
      { }

      /** adds a QWidget do watch for focus
       * @param w the widget to watch for
       */
      inline void watchChildForFocus(QWidget * w);

      /** @sa qigKeyEventHandlerWorker::addInputHandler
       */
      int addInputHandler(qigKeyEventHandlerWorker* w) {return m_key_event_handler.addInputHandler(w);};
      /** @sa qigKeyEventHandlerWorker::removeInputHandler
       */
      int removeInputHandler(qigKeyEventHandlerWorker* w){return m_key_event_handler.removeInputHandler(w);};
    protected:
      /** override from QWidget*/
      inline virtual void paintEvent(QPaintEvent*ev) override;
      /** override from QObject*/
      inline virtual bool eventFilter(QObject* o, QEvent* ev) override;

      inline virtual void keyPressEvent(QKeyEvent* ev) override;
    private:
      /** true if watched widget or self has the focus*/
      bool m_child_has_focus;

      qigKeyEventHandler m_key_event_handler;
  };

  template <typename T>
  inline void qigFocusFrameWidget<T>::watchChildForFocus(QWidget *obj)
  {
    if(obj) {
      obj->installEventFilter(this);
    }
  }

  template <typename T>
  inline void qigFocusFrameWidget<T>::paintEvent(QPaintEvent *ev)
  {
    if(ev && ev->rect() == this->rect())
    {
      QPainter p(this);
      QStyleOptionFocusRect opt;
      opt.initFrom(this);

      if(m_child_has_focus) {
        opt.state |= QStyle::State_HasFocus | QStyle::State_FocusAtBorder;
	this->style()->drawControl(QStyle::CE_FocusFrame,&opt, &p, this);
      }
    }
    QWidget::paintEvent(ev);
  }

  template<typename T>
  void qigFocusFrameWidget<T>::keyPressEvent(QKeyEvent *p)
  {
    if(!m_key_event_handler.keyPressEvent(p))
      return BaseWidget::keyPressEvent(p);
  }


  template <typename T>
  inline bool qigFocusFrameWidget<T>::eventFilter(QObject*o, QEvent *ev)
  {
    Q_UNUSED(o);
    switch(ev->type())
    {
      case QEvent::FocusIn:
	m_child_has_focus=true;
	this->update();
	break;
      case QEvent::FocusOut:
	m_child_has_focus=false;
	this->update();
	break;
      default:
	return false;
    }
    // we do not filter
    return false;
  }
}
