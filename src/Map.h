#include <map>
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */

namespace qig
{
  template <typename k,v>
    class qigMap: public std::map<k,v>
  {
    qigMap(const qigMap&) = delete;
  };
}

