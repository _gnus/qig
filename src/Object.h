// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once

#include "enums.h"
#include "gitTypes.h"
#include "GitObjectHolder.h"

namespace qig
{
  class qigOid;
  class qigString;
  class qigFilter;
  /**@class qigObject
   * @brief wraps object api of libgit2
   */
  class qigObject: protected qigGitObjectHolder
  {
    friend class qigReference;
    friend class qigHistory;
    public://{{{ public methods
      qigObject();
      explicit qigObject(const qigOid&);
      virtual ~qigObject(){};
      qigObject(const qigObject&) = default;
      qigObject(qigObject&&) = default;
      qigObject& operator=(const qigObject&) = default;
      qigObject& operator=(qigObject&&) = default;


      /** apply filter @a f on the internal object. Returns true if the object matches the filter
       * If the filter has an invalid type. The object tries to peel the type that matches
       * the filter type and apply the filter to the peeled object. For example the f is a file filter and the object is an commit.
       * otherwise it returns false.
       */
      virtual bool applyFilter(const qigFilter& f) const;

      /** 
       * copies string representation of the oid of the current
       * object to str
       * @param str [out] string to which the string is copied.
       * @sa qigOid::asString()
       */
      void copyString(qigString &str) const;

      /** load a new object from an oid
       * @param oid the oid of the new object to load
       * @sa qigObject(const qigOid& ) how to construct an new object from an oid
       */
      void load(const qigOid& oid) { return qigGitObjectHolder::load(oid); }

      /** peels till an object of type type encounterd.
       * returns the peeled object
       */
      qigObject peel(objectType type) const;

      /** returns true is object is valid
       * the object does not recognise when the repository is unloaded.
       * the function may return false positive
       *
       * @return true if object is valid.
       */
      using qigGitObjectHolder::isValid;

      /** returns the oid of the object
       * @param out an object that will be filled
       */
      void oid(qigOid& out) const{ return qigGitObjectHolder::oidCopy(out);}
      /** compare both objects and return true if they are same.
       * two objects are the same if they have the same oid
       * @return true if both object have the same oid
       */
      bool operator==(const qigObject & other) const { return  this == &other? true: qigGitObjectHolder::operator==(other);}
      bool operator!=(const qigObject & other) const { return !(*this == other);}
      bool operator==(const qigOid & other) const { return qigGitObjectHolder::operator==(other);}
      bool operator!=(const qigOid & other) const { return !(*this == other);}

      /** clones the other object
       * compiler defines works well
       */
      //qigObject& operator=( const qigObject& oid);
      //}}}

    protected://{{{ protected methods

      qigObject(git_object*);

      /** reimplemented from qigGitObjectHolder
       * 
       * nothing to do here. 
       */
      virtual void reload()override{};
      /** reimplemented from qigGitObjectHolder
       * 
       * nothing to do here.
       */
      virtual void reset()override{}

    public:
      /** peels till an object of type type encountered and loads the object
       */
      int peelSelf(objectType);
      //}}}

    private://{{{ private methods

      /** free current object and loads object from git_oid o
       *
       * reimplements from qigOid
       *
       * @param o new oid to load
       */
      //void loaded(const git_oid *o);

      // there are two virtual function. from qigOid and qigGitObjectHolder.
      // thile the first does nothing the latter loads the object.
      // here we need the object
      //void reloadFromId(){qigGitObjectHolder::reloadFromId();};

      //}}}
  };
}
