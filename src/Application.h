// vim: syntax=cpp.doxygen
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include <QApplication>
#include <QString>
#include <memory>
#include <QPointer>

#include "enums.h"
#include "gitTypes.h"
/** @defgroup Application Application
 * @brief classes in this group are used as a view or an back end for a view;
 */

class QSettings;
class QVariant;
class QFileSystemWatcher;
class QAbstractItemModel;

class QTextCursor;
template<typename T> class QList;
typedef QList<QModelIndex> QModelIndexList;

#define DECLARE_PROPERTY(type, name)                                            \
  private:                                                                      \
    Q_PROPERTY(type name READ name WRITE set##name NOTIFY name##Changed)        \
  public:                                                                       \
      type name() const { return m_##name;}                                     \
      void set##name(const type& x) {						\
	if(m_##name != x) emit name##Changed(x);				\
	m_##name = x; }								\
  Q_SIGNALS:									\
    void name##Changed(const type& x);						\
  private:                                                                      \
      type m_##name;                                                               

namespace qig
{
  //class qigWorkDirModel;
  class qigRepository;
  class qigCommit;
  class qigMainWindow;
  class qigTreeProxy;
  class qigFilter;
  class qigIndex;
  class qigIndexBuilder;
  class qigLogModel;
  class qigLogFilterList;
  class qigWorkDirModel;
  class qigWorkDirModel2;
  class qigStatus;
  class qigTree;
  class qigTreeModel;
  class qigDiffViewerBuiltinModel;
  class qigDiffViewerBuiltinTextDocument;
  class qigOid;
  class qigDifferInterfaceManager;
  class qigOidListModel;
  struct diffOptions;

  typedef std::unique_ptr<qigStatus> qigStatusPtr;
  typedef void (*releaseRepository_f)(qigRepository*);
  typedef std::unique_ptr<qigRepository, releaseRepository_f> qigRepositoryPtr;
  typedef std::unique_ptr<QSettings> qigSettingsPtr;
  typedef std::unique_ptr<diffOptions> qigDiffOptionsPtr;
  typedef std::unique_ptr<qigDifferInterfaceManager> qigDifferInterfaceManagerPtr;
  typedef std::unique_ptr<qigIndex> qigIndexPtr;
  typedef std::unique_ptr<qigIndexBuilder> qigIndexBuilderPtr;
  /**@ingroup Application
   * @{
   * @class qigApplication
   * @brief main class  first to be constructed last to be destructed. 
   *
   * Also implements a global controller for the application and therefore serves as connection between
   * the @ref qigMainWindow and the back end.
   */
  class qigApplication:  public QApplication
  {
    Q_OBJECT
    public:
      qigApplication(int argc, char **argv);
      ~qigApplication();

    public:
      /// calls diff based on diffOptions
      int diff(diffOptions opts) const;
      /**
       * @brief sets a new state.
       * setting a new state will set different models in the view and emit events to notify
       * the view to adjust the GUI accordingly.
       * @param state the new state
       * @param transittion the transittion from current state to the new state
       * @sa applicationState
       * @sa stateTransition
       */
      int setApplicationState(int state, int transittion = stateTransitionDefault);
      /**
       * call to apply the current state.
       * same as setApplicationState(applicationStateDefault, stateTransitionAccept)
       */
      int accept() { return setApplicationState(applicationStateDefault, stateTransitionAccept);}
   protected:
      /**loads repository given by path path.
       * @return error code
       */
      int load(const QString& path);
      void unload();
      // Q_SLOTS redirected from MainWindow
      void onFilterTriggered(qigLogFilterList& filter, bool reset_old_results);

      /**
       * creates a widget to query the user for a path to a valid repository.
       */
      void onRepositoryLoadRequest(){throw 1;}
      /**
       * initialise a new repository at given path path
       *
       * @param path path to the repository that should be created
       */
      void onRepositoryInitialiseRequest(const QString& path){Q_UNUSED(path);throw 1;}
      
      /* updates new side of diff view.
       *
       * to show path from tree with given tree_oid and blob_oid,
       * both oid can be invalid if they do not exist as objects jet. 
       */
      //void onTreeSelectionChanged(const QModelIndex&, const QVariant& diff_old_side_descriptor, const QVariant& diff_new_side_descriptor);

      void onFoldDiff(QTextCursor& c);

      /** updates the labels in the _m_log_commit_info.
       *
       * see designer for labels
       * @param selection  the current selection
       * @param current the most recent selected item;
       */
      void onLogCommitSelectionChanged(const QModelIndexList& selection, const QModelIndex& current);
      //
      //void onTreeSourceChangeRequest(int type);
      //void onTreeSourceChangeRequest(const QByteArray& oid);

      /** sets the source for TreeView.
       * @param oid the oid that describes the source. can be empty for type Working Dir or index.
       * @param type an treeSource value.
       * @param diff if not empty it is used as diff source. can be empty for WD or index.
       * @param diff_type an treeSource value or -1 if no diff is desired.
       */
      int setTreeSource(const QByteArray& oid, int type, const QByteArray& diff, int diff_type);



      /**
       * creates a new commit.
       * param update_ref defaults to HEAD
       * @param author defaults to comitter.
       * @param committer defaults to current git option user.
       * @param message the commit message.
       * param tree the tree that is the base for the commit
       * param parents if empty the peeled commit of update_ref is used;
       */
      int commit(
	const git_signature *author,
	const git_signature *committer,
	const QString &message
	);

    Q_SIGNALS:
      /** emitted if an error is detected.
       *
       * this signal is emitted if en error is detected while evaluating a slot.
       * @param category an error code see \ref errorCodes.
       * @param message the error message.
       */
      void inconvenienceDetected(int category, const QString& message);

      /**TODO maybe use QStateMachine
       *
       * @brief emitted on state change.
       * @param state the desired target state.
       */
      void beginStateChange(int state);
      /**
       * @brief emitted after state change.
       * @param state the reached target state. the value may differ from beginStateChange in case of an error
       */
      void endStateChange(int state);

    private:
      /** internal load. set up the log model to render the history
       */
      int load();
      /** internal. forwards to setState...
       * should be called by setApplicationState only
       * @param state the new state
       */
      int _setApplicationState(int state);
      /** internal. forwards to setState...
       * should be called by setApplicationState only
       * @param state the new desired state after closing the current
       * @param transittion the transittion from current state to @a state
       */
      int _closeApplicationState(int state, int transittion);

      int setStateDefault();
      /** sets new models in the view;
       */
      int setStateAdd();
      /** sets new models in the view;
       */
      int setStateCommit();
      /**
       */
      int closeStateAdd(int target_state, int transittion);
      int closeStateCommit(int target_state, int transittion);
      /** @brief returns an qigIndexBuilder. The qigIndexBuilder operates on the qigWorkDirModel
       * returns a ref to member variable. Use @ref indexBuilderWrite to write the sync the index to the disk.
       * To select an item that should be written use the QAbstractItemModel's api set the check state of the item.
       * @param out an reference to an qigIndexBuilder. 
       */
      int indexBuilder(qigIndexBuilderPtr* out);
      /**
       * writes all checked items to gits index and destroys the object.
       * object won't be destoyed in case of an error;
       * @return 0 on success or error code.
       */
      int indexBuilderWrite(qigIndexBuilderPtr& out);

    private Q_SLOTS:
      /** clean up our mess and try to exit normally.
       */
      void onAboutToQuit(){finalise();}

      void onDirectoryChanged(const QString& path);
      void onfilechanged(const QString& path);

    private:
      /**
       * set work dir model as tree source.
       * index is used for diff.
       */
      int _setTreeSourceWD ();
      /**
       * set work dir model as tree source.
       * tree @ref diff is used as diff.
       */
      int _setTreeSourceWD (const qigOid& diff);
      /**
       * set a git tree as tree source.
       * tree @ref diff is used as diff.
       */
      int _setTreeSourceOid(const qigOid& oid, const qigOid& diff);
      /** called after gits index changed
       */
      void onIndexChanged();
      /** track Screen to recognise changes in devicepixelratio.
       */
      void onScreenChanged(QScreen*);
      /** clean up the application
       */
      void finalise();
      const QString name(){return QString::fromLatin1("qig");};
      const QString version(){return QString::fromLatin1("0.1");}


      /** process diff.
       * ui does not have access to all relevant data
       */
      void processDiffOptions(diffOptions& opts)const;

      void releaseIndexBuilder();
    private: // private member
      int _m_current_state;
      
      QString m_errString;

      QFileSystemWatcher* _m_file_system_watcher;

      // model for history
      qigLogModel *_m_log_model;
      // the repo
      qigRepositoryPtr _m_repository;

      // model for work dir;
      qigWorkDirModel * _m_workdir_model;
      qigWorkDirModel2 * _m_workdir_model2;
      // model for git_tree;
      qigTreeModel * _m_tree_model;
      // unused
      qigTreeProxy *_m_tree_proxy_for_list_view;
      //qigStatusPtr _m_status;

      //the main window
      qigMainWindow* _m_main_window;

      //the diffModel
      qigDiffViewerBuiltinModel *_m_diff_model;
      // alternative impl using QTextDocument
      //QWidget *_m_diff_document;

      /** model used to fill the QComboBox that describes available tree.
       */
      qigOidListModel *_m_tree_source_descriptor_model;

      qigDifferInterfaceManagerPtr _m_diff_interface_manager;

      qigIndexPtr _m_index;
      qigIndexBuilderPtr _m_index_builder;

      QTranslator *_m_translator;
      friend class qigMainWindow;
  };
  /// @}
}
