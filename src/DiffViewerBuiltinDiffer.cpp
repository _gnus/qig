// vim: fdm=marker
#include <cassert>
#include <git2.h>
#include <stdlib.h>
#include <vector>


#include "DiffViewerBuiltinDiffer.h"
#include "IDiffViewer.h"
#include "GitObjectHolder.h"

#include "diffOptions.h"


namespace qig
{

  //{{{ helper functions
  static qigDifferHelper::SourceType toSourceType(int i)
  {
    qigDifferHelper::SourceType ret;
    switch (i)
    {
      case diffOptions::oldSide:
	ret= qigDifferHelper::sourceTypeOldSource;
	break;
      case diffOptions::newSide:
	ret= qigDifferHelper::sourceTypeNewSource;
	break;
      default:
	ret= qigDifferHelper::sourceTypeRepresentation;
	break;
    }
    return ret;
  }

  class qigGitObjectHolderImpl: public qigGitObjectHolder
  {
    public:
      void reload(){};
      virtual void reset(){};
  };


  static const qigChar *extractPath(const git_diff_file* file)
  {
#ifdef UNICODE
    throw -1;
    return file ?
      ( file->path ? mbstowcs(file->path) : nullptr ):
      nullptr;
#else
    return file ? file->path: nullptr;
#endif
  }
  //}}}

  //{{{ ctor, dtor
  qigDifferHelperBuiltin::qigDifferHelperBuiltin(IDiffer &target):
    m_target(target),
    m_target_delta(m_target,nullptr,nullptr),
    m_target_hunk(m_target,nullptr,nullptr)
  {
    target.reset();
  }

  //}}}

  //{{{ public methods

  // {{{methods for qigDifferHelper
  
  size_t qigDifferHelper::readFile(const char*file_name, dataBuffer& out)
  {
    FILE* file = fopen(file_name,"rb");
    return readFile(file, out);
  }
  size_t qigDifferHelper::readFile(FILE *file, dataBuffer& out)
  {
    int bytes_read=0;
    if(file)
    {
      fseek(file, 0, SEEK_END );

      int size = ftell(file);
      rewind(file);
      if(size > 0)
      {
	out.clear();
	out.resize(size);
      }
      bytes_read=fread(out.data(), sizeof(char), size, file);
      fclose(file);
    }
    return bytes_read;
  };
  //}}}

  // {{{ methods for qigDifferHelperBuiltin
  void qigDifferHelperBuiltin::setSource(const qigOid& oid, objectType, SourceType source)
  {
    if(source == sourceTypeNewSource) {
      m_new_source = oid;
    }
    else if(source == sourceTypeOldSource) {
      m_old_source = oid;
    }
    else {
      throw -21;
    }
  }

  void qigDifferHelperBuiltin::setSource(const qigString& str, SourceType)
  {
    m_file_name = str;
  }
  /*
  void qigDifferHelperBuiltin::setFilePath(const char*path, fileSourceType type)
  {
    if(type == typeRepresentation){
      m_file_name = path;
    }
  }
  */

  //}}}


  //}}}

  //{{{ protected mehtods

  int qigDifferHelperBuiltin::diff(const diffOptions& opts)
  { 
    int ret = GIT_ERROR;
    {
      for( int i =0; i<2; ++i)
      {
	if( opts.tree_source[i] == treeSourceWD)
	{
	  if(!opts.name.empty())
	  {
	    // todo might false
	    setSource(opts.name, toSourceType(i));
	  }
	}
	else if (
	    opts.tree_source[i] == treeSourceOid ||
	    opts.tree_source[i] == treeSourceRef ||
	    opts.tree_source[i] == treeSourceIndex
	    )
	{
	  if(opts.oid[i].isValid()) {
	    setSource(opts.oid[i], opts.object_type[i], toSourceType(i));
	  }
	}
      }
      ret=diff();
    }
    return ret;
  }
  //}}}

  //{{{ private methods

  void qigDifferHelperBuiltin::DiffModelItemFinaliser::operator() (IDiffBase* p)
  {
    p->finalise();
    m_target->finalise(p);
  }

  int qigDifferHelperBuiltin::diffDelta(const git_diff *, const git_diff_delta *delta, const char*)
  {
    const git_diff_file *old_file = &delta->old_file;
    const git_diff_file *new_file = &delta->new_file;

    const qigChar *old_file_path = extractPath(old_file);
    const qigChar *new_file_path = extractPath(new_file);

    IDiffDelta *target_delta;
    m_target.addDelta(&target_delta);


    if(target_delta)
    {
      currentDelta::DiffFileModelItemPtr dum(target_delta, DiffModelItemFinaliser(m_target));
      target_delta->setStatus(delta->status);
      target_delta->setFlag(delta->flags);
      target_delta->setPath(old_file_path, new_file_path);
      return m_target.accept(target_delta);
    }
    return GIT_OK;
  }
  // implementation for git_diff_file_cb
  int qigDifferHelperBuiltin::diffFile( const git_diff_delta *delta, float )
  {
    IDiffFile *target_file;
    
    if(m_target.addFile(&target_file)==GIT_OK)
    {
      const git_diff_file *old_file = &delta->old_file;
      const git_diff_file *new_file = &delta->new_file;

      const qigChar *old_file_path = extractPath(old_file);
      const qigChar *new_file_path = extractPath(new_file);

      target_file->setPath(old_file_path, new_file_path);
      target_file->setFlag(delta->flags);
      target_file->setStatus(delta->status);

      m_target_delta = currentDelta(m_target, delta, target_file);
      return GIT_OK;
    }
    return GIT_ERROR;
  }
  // implementation for git_diff_hunk_cb
  int qigDifferHelperBuiltin::diffHunk( const git_diff_delta *delta, const git_diff_hunk *hunk)
  {
    if(m_target_delta.source_delta != delta)
    {
      // current_delta should be set before otherwise hunk wil be added to a wrong file
      throw std::exception();
    }

    IDiffHunk *target_hunk;
    

    if(m_target.addHunk(m_target_delta.targetFile(), &target_hunk)==0)
    {
      // add properties of old file
      target_hunk->setOldStart(hunk->old_start);
      target_hunk->setOldLines(hunk->old_lines);

      // add properties of new file
      target_hunk->setNewStart(hunk->new_start);
      target_hunk->setNewLines(hunk->new_lines);

      target_hunk->setHeader(hunk->header);
      m_target_hunk = currentHunk(m_target, hunk, target_hunk);

      return GIT_OK;
    }
    return GIT_ERROR;
  }
  int qigDifferHelperBuiltin::diffLine( const git_diff_delta *delta, const git_diff_hunk *hunk, const git_diff_line *line)
  {
    if(m_target_delta.source_delta != delta || m_target_hunk.source_hunk != hunk)
    {
      throw std::exception();
    }
    
    IDiffLine *target_line;
    if(m_target.addLine(m_target_hunk.targetHunk(), &target_line) == GIT_OK)
    {
      target_line->setOrigin(line->origin);
      target_line->setOldLineno(line->old_lineno);
      target_line->setNewLineno(line->new_lineno);
      target_line->setNumLines(line->num_lines);
      target_line->setContent(line->content, line->content_len);

      target_line->finalise();
      m_target.finalise(target_line);

      return GIT_OK;
    }
    return GIT_ERROR;
  }

  //}}}
 
  //{{{qigDifferHelperBuiltinBufferToBlob
  qigDifferHelperBuiltinBufferToBlob::qigDifferHelperBuiltinBufferToBlob(IDiffer& target):
    qigDifferHelperBuiltin(target),
    m_invert_sites(false),
    m_FILE(0)
  {
  }

  void qigDifferHelperBuiltinBufferToBlob::setFilePath(const char*path, qigDifferHelper::SourceType type)
  {
    switch(type)
    {
      case sourceTypeNewSource:
	m_file_name=path;
	m_invert_sites=false;
	break;
      case sourceTypeOldSource:
	m_invert_sites=true;
	m_file_name=path;
	break;
      default:
	break;
    }
  }

  int qigDifferHelperBuiltinBufferToBlob::diff()
  {
    int err = GIT_OK;
    qigGitObjectHolderImpl blob;
    blob.load( m_invert_sites ? m_new_source : m_old_source );
    if(!blob.isValid()) {
      err =  GIT_ENOTFOUND;
    }

    qigDifferHelper::dataBuffer content;
    int size;
    const char *path = m_file_name.c_str();
    if(!m_FILE)
    {
      // m_FILE closed by readFile
      m_FILE = fopen(path,"rb");
    }
    size = readFile(m_FILE, content);

    // TODO check whether unreadable or missing
    if(size==0) {
      err=GIT_DELTA_UNREADABLE;
    }
    
    if(err != GIT_OK)
    {
      return err;
    }


    git_diff_options opts;
    git_diff_init_options(&opts, GIT_DIFF_OPTIONS_VERSION);
    git_blob*b = reinterpret_cast<git_blob*>(blob.object(objectTypeBlob));
    if(!b) {
      return GIT_ENOTFOUND;
    }
    return git_diff_blob_to_buffer(
	b, path, content.data(), size, path, &opts, 
	diffFileCB, 0, diffHunkCB, diffLineCB,
	this
	);
  }
  //}}}

  //{{{qigDifferHelperBuiltinBlobToBlob
  qigDifferHelperBuiltinBlobToBlob::qigDifferHelperBuiltinBlobToBlob( IDiffer &target):
    qigDifferHelperBuiltin(target)
  {
  }

  int qigDifferHelperBuiltinBlobToBlob::diff()
  {
    int err = GIT_OK;
    qigGitObjectHolderImpl new_side;
    qigGitObjectHolderImpl old_side;
    for( auto side : {std::make_pair(&new_side, &m_new_source), std::make_pair(&old_side, &m_old_source)})
    {
      side.first->load(*side.second);
      if(!side.first->isValid()) {
	err =  GIT_ENOTFOUND;
	break;
      }
    }

    git_diff_options opts;
    git_diff_init_options(&opts, GIT_DIFF_OPTIONS_VERSION);
    git_blob*n = reinterpret_cast<git_blob*>(new_side.object(objectTypeBlob));
    git_blob*o = reinterpret_cast<git_blob*>(old_side.object(objectTypeBlob));
    if(!n || !o || err != GIT_OK) {
      err= GIT_ENOTFOUND; //< n==0 o==0 might be valid if objects are new or deleted
    }
    const char* path = m_file_name.c_str();
    return git_diff_blobs(
	o, path, n, path, &opts,
	diffFileCB, 0, diffHunkCB, diffLineCB,
	this
	);
  }
  //}}} 
  //{{{ qigDifferHelperBuiltinTree
  qigDifferHelperBuiltinTreeToTree::qigDifferHelperBuiltinTreeToTree(IDiffer& target):
    qigDifferHelperBuiltin(target)
  {
  }
  void qigDifferHelperBuiltinTreeToTree::setSource(const qigOid &oid, objectType type, SourceType s)
  {
    assert(type == objectTypeTree || type ==objectTypeCommit);
    setSource(qigTree(oid), s);
  }
  void qigDifferHelperBuiltinTreeToTree::setSource(const qigTree &tree, SourceType type)
  {
    switch(type)
    {
      case qigDifferHelper::sourceTypeNewSource:
	m_new_tree = tree;
	break;
      case qigDifferHelper::sourceTypeOldSource:
	m_old_tree = tree;
	break;
      case qigDifferHelper::sourceTypeRepresentation:
      default:
	throw GIT_EINVALID;
	break;
    }
  }
 
  int qigDifferHelperBuiltinTreeToTree::diff()
  {
    if( m_new_tree.owner() != m_old_tree.owner())
    {
      return GIT_ERROR;
    }
    if( !m_new_tree.isValid() || !m_old_tree.isValid())
    {
      return GIT_ENOTFOUND;
    }

    git_diff_options opts = GIT_DIFF_OPTIONS_INIT;
    opts.notify_cb = diffDeltaCB; 
    opts.payload = this;

    git_tree *new_source = m_new_tree.tree();
    git_tree *old_source = m_old_tree.tree();
    git_repository* repo = m_new_tree.owner();
    
    int ret = GIT_ERROR;
    git_diff *diff;
    if( (ret = git_diff_tree_to_tree(&diff, repo, old_source, new_source, &opts)) == GIT_OK)
    {
      ret = git_diff_foreach(diff, diffFileCB, 0, diffHunkCB, diffLineCB, this);

      git_diff_free(diff);
    }

    return ret;
  }

}
