#include <cassert>
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <list>
#include <vector>
#include "LogModelHistoryWorker.h"

namespace qig
{
  class qigLogModelData;
  /** @class lol
   * @brief sparse matrix implemented with List Of List
   */
  class lol
  {
    typedef std::shared_ptr<qigLogModelData> qigLogModelDataPtr;
    typedef std::vector<qigLogModelData> branches;
    // for each commit their is an list of branches inclding the commit itself.
    // the oid of the commit is also stored separatly
    // TODO user deque ???
    typedef std::list<std::pair<qigLogModelData*, branches> > commits;
    public:
      typedef branches::value_type mapped_type;
      class commitIterator: private commits::iterator
      {
	friend class lol;
	public:
	  commitIterator();

	  qigLogModelData& operator*()	 {return *this->commits::iterator::operator*().first;}
	  mapped_type* operator->() {return this->commits::iterator::operator*().first;}

	  using commits::iterator::operator++	    ;//{ ++m_current; return *this;}

	  //using operator==(const commits::iterator& o);
	  
	private:
	  inline constexpr commitIterator(commits::iterator &&i): commits::iterator(std::move(i)){}
      };

      class const_commitIterator: private commits::const_iterator
      {
	friend class lol;
	  using commits::const_iterator::operator++	    ;//{ ++m_current; return *this;}

	  //using operator==(const commits::iterator& o);
	  
	private:
	  inline constexpr const_commitIterator(commits::const_iterator &&i):
	    commits::const_iterator(std::move(i)){}
      };

      class iterator
      {
	public:
	  iterator();
	  bool operator==(const iterator&o) const{return true;}
      };

      typedef const_commitIterator const_iterator;

      inline iterator insert(size_t branch, size_t object, const mapped_type& data);
      inline iterator insert(size_t branch, size_t object, mapped_type&& data);
      inline commitIterator find(size_t branch);
      inline iterator end();
      inline const_commitIterator find(size_t branch) const;
      inline iterator find(size_t branch, size_t object);
      inline void clear() { m_rows.clear();}
    private:
      commits m_rows;
  };

  inline lol::iterator lol::insert(size_t branch, size_t object_cnt, const mapped_type& data)
  {
    iterator ret;
    if(m_rows.size() == object_cnt)
    {
      m_rows.push_back(commits::value_type(0, branches(1,data)));
      if(data.isCommit())
      {
	m_rows.back().first=&m_rows.back().second.back();
      }
      assert(branch==0);
    }
    else
    {
      assert(m_rows.size() < object_cnt);
      auto iter = m_rows.begin();
      std::advance(iter, object_cnt);
      assert(iter->second.size() == branch);
      iter->second.push_back(data);
      if(data.isCommit())
      {
	m_rows.back().first=&m_rows.back().second.back();
      }
    }
  }
  inline lol::commitIterator lol::find(size_t object)
  {
    if(object < m_rows.size())
    {
      auto iter=m_rows.begin();
      //TODO deque: use operator +
      std::advance(iter, object);
      return commitIterator(std::move(iter));
    }
    return commitIterator(m_rows.end());
  }
  inline lol::const_commitIterator lol::find(size_t object) const
  {
    if(object < m_rows.size())
    {
      auto iter=m_rows.begin();
      //TODO deque: use operator +
      std::advance(iter, object);
      return const_commitIterator(std::move(iter));
    }
    return const_commitIterator(m_rows.end());
  }
  inline lol::iterator lol::find(size_t branch, size_t object)
  {
    auto cii=this->find(object);
    if(cii!=m_rows.end())
    {
      assert(false);
    }
    return iterator();
  }
}

