// vim: fdm=marker filetype=cpp.doxygen
#include <assert.h>
#include <QFileSystemModel>
#include <QDebug>
#include <QScreen>
#include <QFileSystemWatcher>

#include "Application.h"
#include "LogModel.h"
#include "MainWindow.h"
#include "Repository.h"
#include "Reference.h"
#include "ReferenceManager.h"
#include "Status.h"
#include "StatusTree.h"
#include "WorkDirModel.h"
#include "DiffViewerBuiltinModel.h"
#include "DiffViewerBuiltinTextDocument.h"
#include "DiffViewerBuiltinDiffer.h"
#include "QOid.h"
#include "Tree.h"
#include "TreeModel.h"
#include "Index.h"
#include "diffOptions.h"
#include "OidListModel.h"
#include "gitTypes.h"


#ifdef _DEBUG
#define VERIFY(x) assert(x)
#else 
#define VERIFY(x) x
#endif

namespace qig
{
  class qigReference;

  static void qigRepositoryDeleter(qigRepository* p) {
    if(p) p->release();
  }

  //static void indexToTree(  const qigIndex& in, qigTree& out);
  //static bool shallBeDiffed(const QModelIndex &index);

  //{{{ ctor and dtor
  qigApplication::qigApplication(int argc, char** argv):
    QApplication(argc, argv),
    _m_current_state(-1),
    _m_file_system_watcher(new QFileSystemWatcher(this)),
    _m_log_model(new qigLogModel(this)), //<by setting parent qt clean up the object
    _m_repository(nullptr, qigRepositoryDeleter),
    _m_workdir_model(new qigWorkDirModel(this)),
    _m_workdir_model2(new qigWorkDirModel2(_m_workdir_model)),
    _m_tree_model(new qigTreeModel(this)),
    _m_tree_proxy_for_list_view(0),
    _m_main_window(new qigMainWindow(*this)),
    //_m_diff_model(new qigDiffViewerBuiltinModel(this)),
    //_m_settings(new QSettings),
    _m_tree_source_descriptor_model(new qigOidListModel(this)),
    _m_diff_interface_manager(new qigDifferInterfaceManager()),
    _m_translator(nullptr)
  {
    setApplicationName(name());
    installTranslator(_m_translator);
    QString pwd = this->applicationDirPath();
    _m_workdir_model->setFilter(QDir::AllEntries | QDir::AllDirs);
    _m_workdir_model2->setSourceModel(_m_workdir_model);
    //_m_workdir_model->setFilter(QDir::NoFilter);
    //_m_tree_proxy_for_list_view->setSourceModel(_m_workdir_model);


    _m_main_window->setTreeModel(_m_workdir_model2);
    _m_tree_source_descriptor_model->setOfferIndex(true);
    _m_tree_source_descriptor_model->setOfferWD(true);
    _m_main_window->setTreeSourceModel(_m_tree_source_descriptor_model);
    _m_main_window->setDiffOldSideModel(_m_tree_source_descriptor_model);
    //_m_main_window->setTreeModel(_m_diff_model);
    _m_main_window->setLogModel(_m_log_model);
    //_m_main_window->setDiffModel(_m_diff_model);
    IDifferFactory *factory = _m_diff_interface_manager->selectedDifferFactory();
    if( factory->provideView() )
    {
      _m_main_window->setDiffView(factory->createView(_m_main_window));
    }

    load(pwd);

    connect(_m_file_system_watcher, &QFileSystemWatcher::fileChanged, this , &qigApplication::onfilechanged);
    connect(_m_file_system_watcher, &QFileSystemWatcher::directoryChanged, this , &qigApplication::onDirectoryChanged);

    _m_main_window->postProcessUi();
    _m_main_window->show();


    VERIFY(connect(this,&qigApplication::aboutToQuit, this, &qigApplication::onAboutToQuit));
  }

  qigApplication::~qigApplication()
  {
  }
  //}}}

  //{{{ public methods
  int qigApplication::load(const QString& path)
  {
    unload();
    qigRepository* pRepo;
    int err = qigRepository::load(pRepo, path.toLocal8Bit().constData());
    if( err == GIT_OK) {
      _m_repository = qigRepositoryPtr(pRepo, qigRepositoryDeleter);
      err = load();
      _m_workdir_model->setRootPath(path);
      /*
      _m_tree_proxy_for_list_view->setRoot(_m_workdir_model->index(path));
      _m_tree_view_as_table->setRootIndex(
	  _m_tree_proxy_for_list_view->mapFromSource(_m_workdir_model->index(path))
	  );
      */
      setApplicationState(applicationStateDefault);
      //qDebug() << QString(_m_repository->repositoryRootPath()) + "/.git";
    }
    return err;
  }

  int qigApplication::setApplicationState(int state, int transittion)
  {
    beginStateChange(state);
    int err = _closeApplicationState(state, transittion);
    if(err == GIT_OK)
    {
      err = _setApplicationState(state);
    }
    if(err == GIT_OK)
    {
      _m_current_state = state;
    }
    endStateChange(_m_current_state);
    return err;
  }

  int qigApplication::_setApplicationState(int state)
  {
    switch(state)
    {
      case applicationStateDefault:
	return setStateDefault();
      case applicationStateAdd:
	return setStateAdd();
      case applicationStateCommit:
	return setStateCommit();
      default:
	throw 1;
    }
    //unreachable
    return GIT_ERROR;
  }
  
  int qigApplication::_closeApplicationState(int target_state, int transittion)
  {
    switch(_m_current_state)
    {
      case applicationStateDefault:
      case -1://< default value upon start up
	// nothing to do;
	return GIT_OK;
      case applicationStateAdd:
	return closeStateAdd(target_state, transittion);
      case applicationStateCommit:
	// TODO clean up;
	break;
    }
    return GIT_ERROR;
  }

  int qigApplication::closeStateAdd(int target_state, int transittion)
  {
    switch(target_state)
    {
      case applicationStateDefault:
	switch(transittion)
	{
	  case stateTransitionAbort:
	    releaseIndexBuilder();
	    return GIT_OK;
	  case stateTransitionAccept:
	    return indexBuilderWrite(_m_index_builder);
	  case stateTransitionDefault:
	    return GIT_EINVALID;
	}
	break;
      case applicationStateAdd:
      case applicationStateCommit:
	// indexBuilderWrite still in use;
	return GIT_OK;
    }
    // unreachable
    return GIT_ERROR;
  }

  void qigApplication::unload()
  {
    releaseIndexBuilder();
    // just in case a another tree model is used
    _m_main_window	->setTreeModel(_m_workdir_model2);
    _m_log_model	->repositoryUnloaded();
    _m_workdir_model	->repositoryUnloaded();
    _m_tree_model	->repositoryUnloaded();
    _m_tree_source_descriptor_model->repositoryUnloaded();
    _m_repository.reset();
  }
  //}}}

  //{{{protected methods

  void qigApplication::onFilterTriggered(qigLogFilterList& filter, bool reset_old_results)
  {
    _m_log_model->applyFilter(filter, reset_old_results, false);
  }
  
  void qigApplication::onFoldDiff(QTextCursor& c)
  {
    Q_UNUSED(c);
  }


  int qigApplication::setTreeSource(const QByteArray& raw, int type, const QByteArray& diff, int diff_type)
  {
    if(_m_current_state != applicationStateDefault)
    {
      return GIT_EINVALID;
    }
    Q_UNUSED(diff_type);
    int err;
    switch(type)
    {
      case treeSourceIndex:
	// TODO peel oid of index and fall through
      case treeSourceOid:
      case treeSourceRef:
	{
	  qigOid old_oid;
	  qigOid new_oid;
	  _m_repository->initialiseOid(old_oid, (const unsigned char*) raw.constData());
	  _m_repository->initialiseOid(new_oid, (const unsigned char*)diff.constData());
	  err = _setTreeSourceOid(old_oid, new_oid);
	}
      break;
    case treeSourceWD:
      if(diff_type == treeSourceIndex) {
	err = _setTreeSourceWD();
      }
      else {
	// TODO respect value of diff
	throw (int) GIT_EUSER;
      }
      break;
    default:
      break;
    }
    return err;
  }

  int qigApplication::indexBuilder(qigIndexBuilderPtr* builder)
  {
    assert(builder == & _m_index_builder);
    releaseIndexBuilder();

    _m_index_builder = qigIndexBuilderPtr(new qigIndexBuilder(this));
    builder = &_m_index_builder;
    _m_index_builder->setSourceModel(_m_workdir_model);
    return _m_index_builder ? GIT_OK: GIT_ERROR;
  }

  int qigApplication::indexBuilderWrite(qigIndexBuilderPtr& ref)
  {
    assert(ref == _m_index_builder);
    int ret = ref && _m_index_builder->sync(*_m_index) ? GIT_OK: GIT_ERROR;
    if(ret == GIT_OK) {
      releaseIndexBuilder();
    }
    return ret;
  }


//  int qigApplication::addToIndex(const QStringList& files)
//  {
//    qigIndex index(*_m_repository);
//    int err = GIT_EINVALID;
//    for(const QString& file : files)
//    {
//      err = index.addEntryByPath(file.toUtf8().constData());
//      if(err != GIT_OK) {
//	break;
//      }
//    }
//    if(err==GIT_OK)
//    {
//      index.writeToDisk();
//      onIndexChanged();
//    }
//    return err;
//  }
  //}}}

  //{{{ private slots;
  void qigApplication::onfilechanged(const QString& str)
  {
    //const char* watch_files[][100] = {"/.git/index", "/.git/refs"};
    qDebug() << Q_FUNC_INFO << str;
  }
  void qigApplication::onDirectoryChanged(const QString& str)
  {
    qDebug() << Q_FUNC_INFO << str;
  }
  //}}}

  //{{{ private methods

  void qigApplication::finalise()
  {
    if(_m_main_window)
    {
      delete _m_main_window;
    }
    if(_m_repository)
    {
      _m_workdir_model->setStatus(qigStatusPtr());
      // reset the unique_ptr
      _m_repository.reset();
    }
  }

  int qigApplication::load()
  {
    qigReference ref;
    int err;
    const qigReferenceManager& refs = _m_repository->referenceManager();
    if( ( err = refs.head(ref) ) == GIT_OK )
    {
      _m_log_model->populate(_m_repository.get(), ref);
    }
    qigStatusPtr status(new qigStatus(*_m_repository));
    _m_workdir_model->setStatus(std::move(status));
    _m_tree_model->setPrefix(QString(_m_repository->wdPath()));
    _m_tree_source_descriptor_model->actualiseReferences(_m_repository->referenceManager());
    _m_index = qigIndexPtr(new qigIndex(*_m_repository));
    // TODO subclass QCombobox and set default in the impl
    _m_main_window->_m_diff_old_tree_selector->setCurrentIndex(1);// select the index;
    return err;
  }

  int qigApplication::setStateDefault()
  {
    _m_main_window->setTreeModel(_m_workdir_model2);
    _m_main_window->setTreeRoot(_m_workdir_model2->mapFromSource(_m_workdir_model->index(_m_workdir_model->rootPath())));
    return GIT_OK;
  }

  int qigApplication::setStateAdd()
  {
    indexBuilder(&_m_index_builder);
    _m_main_window->setTreeModel(_m_index_builder.get());
    // TODO set list model;
    return GIT_OK;
  }

  int qigApplication::setStateCommit()
  {
    _m_main_window->setTreeModel(_m_index_builder.get());
    // TODO set editor;
    // TODO set list model;
    return GIT_OK;
  }

  void qigApplication::onIndexChanged()
  {
    _m_workdir_model->setStatus(qigStatusPtr(new qigStatus(*_m_repository)));
  }
  void qigApplication::onScreenChanged(QScreen* newScreen)
  {
    qreal device_pixel_ratio = newScreen->devicePixelRatio();
    _m_main_window->onDevicePixelRationChanged(device_pixel_ratio);
  }
  void qigApplication::processDiffOptions(diffOptions& opts) const
  {
    for( int i =0; i< 2; ++i)
    {
      if(opts.tree_source[i] == treeSourceOid || opts.tree_source[i] == treeSourceRef )
      {
	qigTree tree(opts.oid[i]);
	const qigChar* relative_path;
	if(_m_repository->relativePathFromAbsolutePath(opts.name.c_str(), &relative_path) == GIT_OK)
	{
	  if(tree.oid(opts.oid[i], relative_path)!=GIT_OK)
	  {
	    // failed to load oid. reset the oid;
	    // could be a deleted file; 
	    opts.oid[i]=qigOid();
	  }
	}
      }
      else if(opts.tree_source[i] == treeSourceIndex)
      {
	// ui does not have access to the index; 
	// set the oid;
	qigIndex index(*_m_repository);
	const qigChar* relative_path;
	if(_m_repository->relativePathFromAbsolutePath(opts.name.c_str(), &relative_path) == GIT_OK) {
	  index.entryOidByPath(opts.oid[i],  relative_path);
	}
      }
      // nothing to do for treeSourceWD
    }
  }

  int qigApplication::diff ( diffOptions opts) const
  {
    if(opts.isValid())
    {
      processDiffOptions(opts);
      assert(opts.isValid());
      return qigDifferHelper::diff(opts, *_m_diff_interface_manager);
    }
    return GIT_EINVALID;
  }

  int qigApplication::_setTreeSourceWD()
  {
    qigStatusPtr status(new qigStatus(*_m_repository));
    _m_workdir_model->setStatus(std::move(status));
    _m_main_window->setTreeModel(_m_workdir_model2);
    // TODO set root path in view;
    _m_main_window->setTreeRoot(_m_workdir_model2->mapFromSource(_m_workdir_model->index(_m_repository->repositoryRootPath() )));
    return GIT_OK;
  }

  int qigApplication::_setTreeSourceOid(const qigOid& tree, const qigOid& diff)
  {
    assert(tree.isValid());
    assert(diff.isValid());

    qigStatusTree status;
    status.compare(diff, tree);

    qigTree t(tree);
    int err =  _m_tree_model->setTree(t, &status);

    if( err == GIT_OK ) {
      _m_main_window->setTreeModel(_m_tree_model);
    }
    else {
    }
    return err;
  }

  void qigApplication::releaseIndexBuilder()
  {
    _m_index_builder.reset();
  }
  /*
  bool shallBeDiffed(const QModelIndex& index)
  {
    bool ret = true;
    // do not diff if state is unchanged
    if(ret)
    {
      const QVariant& val = index.data(modelRoles::roleFileState);
      unsigned int state = val.toUInt(&ret);
      if(ret) ret = (state != fileStateUnchanged);
    }

    // do not diff directories
    if(ret)
    {
      const QVariant& val = index.data(modelRoles::roleIsDir);
      unsigned int isDir = val.toUInt(&ret);
      if(ret) ret = !isDir;
    }
    return ret;
  }
  */

  //}}}

  // {{{ implementeation for various other widgets
  //}}}
}
