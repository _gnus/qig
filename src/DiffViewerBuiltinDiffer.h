// vim: fdm=marker ft=cpp.doxygen
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
/// @defgroup Application Application
#pragma once
#include <git2/diff.h>
#include <stdio.h>
#include <memory>
#include <string>

#include "gitTypes.h"
#include "Tree.h"
#include "DiffViewerDiffer.h"
#include "Oid.h"
#include "Str.h"


namespace qig
{
  //{{{forward declaration
  class IDiffer;
  class IDiffBase;
  class IDiffFile;
  class IDiffHunk;
  //}}}

  //{{{ qigDifferHelperBuiltin
  /**
   * @ingroup Application
   * @class qigDifferHelperBuiltin
   * @brief base class for all builtin differ. qigDifferHelperBuiltin itself is an abstract class that feeds the IDiffer from callbacks.
   *
   * The class itself does not call libgit2 itself but depends on specialisation for calling libgit2. 
   * for specialisation this class provides static methods that calls IDiffer and that can be passed to libgit2.
   */
  class qigDifferHelperBuiltin: public qigDifferHelper
  {
    //{{{ helper classes
    struct DiffModelItemFinaliser
    {
      DiffModelItemFinaliser(IDiffer &target):m_target(&target){}
      void operator()(IDiffBase* p);
      private:
	IDiffer *m_target;
    };
    struct currentDelta
    {
      typedef std::unique_ptr<IDiffFile, DiffModelItemFinaliser> DiffFileModelItemPtr;
      currentDelta(IDiffer& target, const git_diff_delta* a, IDiffFile*b): source_delta(a), target_file_ptr(b, DiffModelItemFinaliser(target)){}
      IDiffFile* targetFile() { return target_file_ptr.get(); }
      const git_diff_delta * source_delta;
      DiffFileModelItemPtr target_file_ptr;
    };
    struct currentHunk
    {
      typedef std::unique_ptr<IDiffHunk, DiffModelItemFinaliser> DiffHunkModelItemPtr;
      currentHunk(IDiffer& target, const git_diff_hunk* a, IDiffHunk *b): source_hunk(a), target_hunk_ptr(b, DiffModelItemFinaliser(target)){}
      IDiffHunk* targetHunk() { return target_hunk_ptr.get(); }
      const git_diff_hunk *source_hunk;
      DiffHunkModelItemPtr target_hunk_ptr;
    };
    //}}}

    public:
      qigDifferHelperBuiltin(IDiffer &target);

      /**
       * determines source for old side of diff. oid must be a id that belongs to a blob.
       * trees are not supported at the moment;
       *
       * @param oid oid to an git blob object
       * @param type must be objectTypeBlob
       */
      virtual void setSource(const qigOid& oid, objectType type, SourceType source);
      virtual void setSource(const qigString& oid, SourceType source);


      virtual int diff() = 0;
      virtual int diff(const diffOptions& opts) override;

    private:

      int diffDelta( const git_diff* diff_so_far, const git_diff_delta* delta, const char* matched_pathspec);
      // implementation for git_diff_file_cb
      int diffFile( const git_diff_delta *delta, float progress);
      // implementation for git_diff_hunk_cb
      int diffHunk( const git_diff_delta *delta, const git_diff_hunk *hunk);
      int diffLine( const git_diff_delta *delta, const git_diff_hunk *hunk, const git_diff_line *line);

    protected:
      static int diffDeltaCB( const git_diff *diff_so_far, const git_diff_delta *delta, const char* matched_pathspec, void *payload)
      {
	// libgit cannot handle exception
	try {
	  return reinterpret_cast<qigDifferHelperBuiltin*>(payload)->diffDelta(diff_so_far, delta, matched_pathspec);
	} catch(...)
	{
	  return GIT_ERROR;
	}
      }
      static int diffFileCB( const git_diff_delta *delta, float progress, void *payload)
      {
	try {
	  return reinterpret_cast<qigDifferHelperBuiltin*>(payload)->diffFile(delta, progress);
	} catch(...)
	{
	  return GIT_ERROR;
	}
      }
      //static int diffBinaryCB( const git_diff_delta *delta, const git_diff_binary *binary, void *payload);
      static int diffHunkCB( const git_diff_delta *delta, const git_diff_hunk *hunk, void *payload)
      {
	try {
	  return reinterpret_cast<qigDifferHelperBuiltin*>(payload)->diffHunk(delta, hunk);
	} catch(...)
	{
	  return GIT_ERROR;
	}
      }
      /**@param delta delta that contains this data */
      /**@param hunk hunk containing this data */
      /**@param line line data */
      /**@param payload user reference data */
      static int diffLineCB( const git_diff_delta *delta, const git_diff_hunk *hunk, const git_diff_line *line, void *payload)
      {
	try {
	  return reinterpret_cast<qigDifferHelperBuiltin*>(payload)->diffLine(delta, hunk, line);
	} catch(...)
	{
	  return GIT_ERROR;
	}
      }

      qigOid     m_new_source;
      qigOid     m_old_source;

      std::string m_file_name;

    private:
      IDiffer&m_target;
      // map current git_file to DiffFileModelItem. git_file is stored to verify that the DiffFileModelItem is the correct object.
      currentDelta m_target_delta;
      // map current git_file to DiffHunkModelItem. git_file is stored to verify that the DiffHunkModelItem is the correct object.
      currentHunk m_target_hunk;
  };
  //}}}

  //{{{ specialisation
  //{{{2 differ for buffer to blob;
  /** @class qigDifferHelperBuiltinBufferToBlob
   * specialisation for qigDifferHelperBuiltin 
   * this differ operates on a blob and an buffer
   */
  class qigDifferHelperBuiltinBufferToBlob: public qigDifferHelperBuiltin
  {
    public:
      qigDifferHelperBuiltinBufferToBlob(IDiffer&);

      /** read source of type type from FILE structe
       * implemented for unittest only
       */
      void setStreamSource(FILE * file, qigDifferHelper::SourceType type);
      void setFilePath(const char*path, qigDifferHelper::SourceType type);
      void setInvertSites(bool val) { m_invert_sites=val; }
      using qigDifferHelper::diff;
      virtual int diff() override;
    private:
      int m_invert_sites;
      FILE *m_FILE;
  };

  class qigDifferHelperBuiltinBlobToBlob: public qigDifferHelperBuiltin
  {
    public:
      qigDifferHelperBuiltinBlobToBlob(IDiffer&);

      virtual int diff() override;
      using qigDifferHelperBuiltin::diff;
  };

  //{{{2 differ tree2tree
  class qigDifferHelperBuiltinTreeToTree: public qigDifferHelperBuiltin
  {
    public:
      qigDifferHelperBuiltinTreeToTree(IDiffer& );
      virtual void setSource(const qigOid& oid, objectType type, SourceType source);
      void setSource(const qigTree&, qigDifferHelperBuiltin::SourceType type);
      virtual int diff();
      using qigDifferHelperBuiltin::diff;
    private:
      qigTree m_new_tree;
      qigTree m_old_tree;
  };
}
