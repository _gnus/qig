#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
/// contains only typedefs and forward declaration of git types. all those typedefs are part of git2/types.h


#ifdef __cplusplus
#define BEGIN_C_DECL extern "C" {
#define END_C_DECL }
#include <inttypes.h>
#else
#define BEGIN_C_DECL
#define END_C_DECL 
#endif

BEGIN_C_DECL

namespace qig{
/** the char type. either wchar_t for unicode or char */
#ifdef WITH_UNICODE
typedef wchar_t qigChar; ///< ligtit2 does not support unicode either
#else
typedef char qigChar; ///< default. libgit2 does not support unicode
#endif
}

typedef int64_t git_off_t;
typedef int64_t git_time_t;
/** An open object database handle. */
typedef struct git_odb git_odb;

/** A custom backend in an ODB */
typedef struct git_odb_backend git_odb_backend;

/** An object read from the ODB */
typedef struct git_odb_object git_odb_object;

/** A stream to read/write from the ODB */
typedef struct git_odb_stream git_odb_stream;

/** A stream to write a packfile to the ODB */
typedef struct git_odb_writepack git_odb_writepack;

/** An open refs database handle. */
typedef struct git_refdb git_refdb;

/** A custom backend for refs */
typedef struct git_refdb_backend git_refdb_backend;

/**
 * Representation of an existing git repository,
 * including all its object contents
 */
typedef struct git_repository git_repository;

/** Representation of a generic object in a repository */
typedef struct git_object git_object;

/** Representation of an in-progress walk through the commits in a repo */
typedef struct git_revwalk git_revwalk;

/** Parsed representation of a tag object. */
typedef struct git_tag git_tag;

/** In-memory representation of a blob object. */
typedef struct git_blob git_blob;

/** Parsed representation of a commit object. */
typedef struct git_commit git_commit;

/** Representation of each one of the entries in a tree object. */
typedef struct git_tree_entry git_tree_entry;

/** Representation of a tree object. */
typedef struct git_tree git_tree;

/** Constructor for in-memory trees */
typedef struct git_treebuilder git_treebuilder;

/** Memory representation of an index file. */
typedef struct git_index git_index;

/** An iterator for conflicts in the index. */
typedef struct git_index_conflict_iterator git_index_conflict_iterator;

/** Memory representation of a set of config files */
typedef struct git_config git_config;

/** Interface to access a configuration file */
typedef struct git_config_backend git_config_backend;

/** Representation of a reference log entry */
typedef struct git_reflog_entry git_reflog_entry;

/** Representation of a reference log */
typedef struct git_reflog git_reflog;

/** Representation of a git note */
typedef struct git_note git_note;

/** Representation of a git packbuilder */
typedef struct git_packbuilder git_packbuilder;

/** Time in a signature */
typedef struct git_time git_time;

typedef struct git_signature git_signature;

/** In-memory representation of a reference. */
typedef struct git_reference git_reference;

/** Iterator for references */
typedef struct git_reference_iterator  git_reference_iterator;

/** Transactional interface to references */
typedef struct git_transaction git_transaction;

/** Annotated commits, the input to merge and rebase. */
typedef struct git_annotated_commit git_annotated_commit;

/** Merge result */
typedef struct git_merge_result git_merge_result;

/** Representation of a status collection */
typedef struct git_status_list git_status_list;

/** Representation of a rebase */
typedef struct git_rebase git_rebase;


/*
 * A refspec specifies the mapping between remote and local reference
 * names when fetch or pushing.
 */
typedef struct git_refspec git_refspec;

/**
 * Git's idea of a remote repository. A remote can be anonymous (in
 * which case it does not have backing configuration entires).
 */
typedef struct git_remote git_remote;

/**
 * Interface which represents a transport to communicate with a
 * remote.
 */
typedef struct git_transport git_transport;

/**
 * Preparation for a push operation. Can be used to configure what to
 * push and the level of parallelism of the packfile builder.
 */
typedef struct git_push git_push;

/* documentation in the definition */
typedef struct git_remote_head git_remote_head;
typedef struct git_remote_callbacks git_remote_callbacks;

/**
 * This is passed as the first argument to the callback to allow the
 * user to see the progress.
 *
 * - total_objects: number of objects in the packfile being downloaded
 * - indexed_objects: received objects that have been hashed
 * - received_objects: objects which have been downloaded
 * - local_objects: locally-available objects that have been injected
 *    in order to fix a thin pack.
 * - received-bytes: size of the packfile received up to now
 */
typedef struct git_transfer_progress git_transfer_progress;



/**
 * Opaque structure representing a submodule.
 */
typedef struct git_submodule git_submodule;


/** A type to write in a streaming fashion, for example, for filters. */
typedef struct git_writestream git_writestream;

typedef struct git_oid git_oid;

END_C_DECL
