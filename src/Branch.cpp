// vim: fdm=marker
#include <git2.h>



#include "Branch.h"

namespace qig
{
  qigBranch::qigBranch(git_reference *p_branch, branchType t): 
    qigReference(p_branch),
    _m_type(t),
    _m_name(_name())
  {
  }
#if 0
  int qigBranch::move(const qigChar* newName)
  {//{{{
    return isValid() ? git_branch_move(*this, newName) : notInitialised();
  }//}}}
#endif
  int qigBranch::isHEAD() const
  {
    // return 0 on failure because this function returns 1 on success
    return isValid() ? git_branch_is_head(const_cast<git_reference*>(this->reference())) : 0;
  }
  const qigChar *qigBranch::name()const
  {
     return _m_name;
  }
  const qigChar *qigBranch::_name()
  {
    int err=-1;
    if(isValid())
    {
      err = git_branch_name(&_m_name, const_cast<git_reference*>(this->reference()));
    }
    return err ?  nullptr : _m_name;
  }

  int qigBranch::isType(branchType type) const
  {
    return type & _m_type;
  }

  /// specialication for qigBranchLocal*
  template <> const qigBranchLocal* qigBranch::toBranchType<const qigBranchLocal*>() const
  {//{{{
    return _m_type == branchTypeLocal ? static_cast<const qigBranchLocal*>(this) : nullptr;
  }//}}}
  /// specialication for qigBranchRemote
  template <> const qigBranchRemote* qigBranch::toBranchType<const qigBranchRemote*>() const
  {//{{{
    return _m_type == branchTypeRemote ? static_cast<const qigBranchRemote*>(this) : nullptr;
  }//}}}
}
