// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include <QAbstractItemModel>
#include <QString>

#include <deque>
#include <memory>

#include "treeEntry.h"
#include "gitTypes.h"
#include "IDiffViewer.h"
#include "DiffViewerImpl.h"

class QFont;
namespace qig
{

  struct DiffBinary;
  struct DiffFileModelItem;
  struct DiffHunkModelItem;
  struct DiffLineModelItem;


  /**
   * @ingroup Application
   * @class qigDiffViewerBuiltinModel
   * container to store the diff.
   * the model is capable to store a pair of files as diff only.
   * the model stores the file, hunks lines in a tree structure. The root is the file
   * the children of the root are hunks, and the children of hunks are lines
   *
   *
   * TODO currently the signals for adding new rows are not synchronised with the real event's.
   * a view may be able to query hunks lines etc before they are made public with beginInsertRows, and endInsertRows.
   * To solve this problem we may have to introduce a second working root next to a model root.
   * in the call to finalise a DiffFileModelItem in the working root we could merge both roots
   */
  class qigDiffViewerBuiltinModel: public QAbstractItemModel, public qigDiffViewerImpl
  {
    Q_OBJECT
    typedef std::unique_ptr<DiffDeltaModelItem> DiffDeltaModelItemPtr;
    typedef std::deque<DiffDeltaModelItemPtr> diffDeltaEntries;
    typedef QSharedPointer<QFont> fontPtr;
    typedef DiffViewerBuiltinModelItem root;
    public:
      qigDiffViewerBuiltinModel(QObject*parent);
      qigDiffViewerBuiltinModel(const qigDiffViewerBuiltinModel&)=delete;
      ~qigDiffViewerBuiltinModel();

      virtual int addDelta(IDiffDelta **delta) override;
      virtual void finalise( IDiffBase *item)  override;
      virtual int accept(IDiffDelta *item) override;

      virtual void reset()override;
      /// override from QAbstractItemModel
      virtual QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const override;
      virtual QModelIndex index(int row, int column, const QModelIndex &parent=QModelIndex()) const override;
      virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;
      virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
      virtual QModelIndex parent(const QModelIndex &child) const override;
      virtual bool hasChildren(const QModelIndex &parent = QModelIndex()) const override;
      virtual Qt::ItemFlags flags(const QModelIndex &index) const override;


    private:

      // inserts all ancestors of item to tree. Calls beginInsertRows
      void insertTree( DiffViewerBuiltinModelItem* item);
      void insertChildrenRecursivly( DiffViewerBuiltinModelItem*);
      // converts model index to tree item
      inline const DiffViewerBuiltinModelItem* toTreeItem(const QModelIndex& index) const;
      // converts tree item to model index
      const QModelIndex toModelIndex(const treeEntry* item) const;

      // worker for data()
      inline QFont& dataFont() const;
      inline QVariant dataDisplay(const QModelIndex&) const;
      QVariant dataForeground(const QModelIndex&)const;
      inline QVariant dataType(const QModelIndex& index)const;
      
      diffDeltaEntries m_deltas;
      // cache the used font
      fontPtr m_font;

      bool m_full_diff;
  };

  // {{{ implementation of static helper function

  inline const DiffViewerBuiltinModelItem* qigDiffViewerBuiltinModel::toTreeItem(const QModelIndex& index) const
  {
    void* internal = index.internalPointer();
    return internal ? 
      reinterpret_cast<const DiffViewerBuiltinModelItem*>(internal):
      &m_root;
  }


  inline QFont& qigDiffViewerBuiltinModel::dataFont() const
  {
    return *m_font;
  }
  inline QVariant qigDiffViewerBuiltinModel::dataDisplay(const QModelIndex& index) const
  {
    const DiffViewerBuiltinModelItem* p= toTreeItem(index);
    return p->dataDisplay();
  }
  //inline QVariant qigDiffViewerBuiltinModel::dataForeground(const QModelIndex& index) const
  //{
  //  const DiffViewerBuiltinModelItem* p= toTreeItem(index);
  //  return QBrush(p->dataForeground());
  //}
  inline QVariant qigDiffViewerBuiltinModel::dataType(const QModelIndex& index)const
  {
    const DiffViewerBuiltinModelItem* p= toTreeItem(index);
    return p->type();
  }
#if 0
  inline void DiffViewerBuiltinModelItem::addToTreeStructure()
  {
    treeEntry *pParent = _parent();
    pParent->pushChild(this);
  }
  inline const DiffViewerBuiltinModelItem* DiffViewerBuiltinModelItem::parent()const
  {
    return static_cast<const DiffViewerBuiltinModelItem*>(treeEntry::parent());
  }
  inline DiffViewerBuiltinModelItem* DiffViewerBuiltinModelItem::childByIndex(unsigned int index)
  {
    return static_cast<DiffViewerBuiltinModelItem*>(treeEntry::_childByIndex(index));
  }
#endif 
  //}}}

}
