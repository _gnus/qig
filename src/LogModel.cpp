// vim: fdm=marker
#include <cassert>
#include <QMimeData>
#include <QDebug>

#include "LogModel.h"
#include "Reference.h"
#include "Oid.h"
#include "Mime.h"
#include "Commit.h"
#include "LogFilterList.h"
#include "QOid.h"
#include <git2/types.h>
#include <QBrush>

static const unsigned int steps=1000;

// {{{ implementation for data()
namespace qig{
//static QVariant dataDecorationRole(const internalPointer&);
/**
* tests if role roleFlag is set in roles
* @return true if @a roleFlag is set in @a roles
*/
static inline bool roleIsSet(qigLogModel::roleBasicInfoFlag roleFlag, int roles){return roles&roleFlag;}
static QVariant dataBackgroundRole(const internalPointer &d);

static inline qigCommit toCommit(const internalPointer& d)
{
  const qigOid& oid=*d;
  return qigCommit(oid);
}
inline static QVariant dataDisplayRole(const internalPointer& d)
{
  if(!d->isCommit())
  {
    qWarning()<<Q_FUNC_INFO<<"querying oid from non commit";
    //throw GIT_ENOTFOUND;
  }
  assert(d->hasRepresentationFlag(1));
  const qigOid& oid=*d;
  return QVariant(QString(oid.asString()));
}

//TODO what is a rev. may use role diff. roleGlob, roleTree...
static QVariant dataRepresentation(const internalPointer& d)
{
  return QVariant(d->representation());
}

static QVariant dataOid(const internalPointer& d)
{
  if(!d->isCommit())
  {
    qWarning()<<Q_FUNC_INFO<<"querying oid from non commit";
    throw GIT_ENOTFOUND;
  }
  const qigOid& oid = *d;
  return QVariant::fromValue(oid);
}

static QVariant dataBackgroundRole(const internalPointer &d)
{
  if(!d->isEnabled())
  {
    return QColor(Qt::gray);
  }
  return QColor(Qt::white);
}

QVariant qigLogModel::dataRoleBrush(const internalPointer&d) const
{
  if(d->isCommit() && d->isEnabled())
  {
    qigCommit ci(toCommit(d));
    const qigSignature* sig = ci.commiter();
    assert(sig);
    QByteArray mail(sig->email);
    auto iter = m_commiter_colour_map.find(mail);
    if(iter == m_commiter_colour_map.end()) {
      iter = m_commiter_colour_map.insert(mail, QColor(nextDefaultColour()));
    }
    return QVariant(QColor(iter.value()));
  }
  return QVariant();
}

QVariantList dataRoleParentOids(const internalPointer&d)
{
  QVariantList out;
  d->parents(out);
  return out;
}

static QStringList dataBasicInfoRole(const internalPointer& d, int role)
{
  if(!d->isCommit())
  {
    qWarning()<<Q_FUNC_INFO<<"querying oid from non commit";
    throw GIT_ENOTFOUND;
  }
  const qigOid& oid = *d;
  qigCommit ci(oid);
  const qigSignature* sig = ci.commiter();
  QStringList ret;
  if (sig)
  {
    ret << (roleIsSet(qigLogModel::roleCommiter, role) ? sig->name		: "");
    ret << (roleIsSet(qigLogModel::roleCommiterMail, role) ? sig->email	: "");
    ret << (roleIsSet(qigLogModel::roleDate    , role) ? "":"");//ci.commitTime()	: "");
    ret << (roleIsSet(qigLogModel::roleAuthor  , role) ? ""			: "");
    ret << (roleIsSet(qigLogModel::roleAuthorMail , role) ? ""			: "");
    ret << (roleIsSet(qigLogModel::roleSHA1    , role) ? oid.asString() 	: "");
    ret << (roleIsSet(qigLogModel::roleTitle   , role) ? ci.commitSummary()	: "");
    ret << (roleIsSet(qigLogModel::roleMessage , role) ? ci.commitMessage()	: "");

    d->attributes["commiter"] = ret.front();
  }
  return ret;
}
}
//}}}

namespace qig
{
  qigLogModel::qigLogModel(QObject *p):QAbstractItemModel(p),
    m_used_color_cnt(0),
    _m_hide_no_match(false),
    m_additional_columns(3), //< short message, author, date
    m_published_rows(0)
  {
  }

  qigLogModel::~qigLogModel() {
  }

  // {{{ public methods
  void qigLogModel::applyFilter(const qigLogFilterList& filter, bool reset_old_result, bool hide_no_match)
  {
    if(reset_old_result)
    {
      m_model_pos_map_working_copy=m_model_pos_map_cached;
    }
    if(hide_no_match){
      _applyFilterAndRemoveFromWorkingCopy(filter);
    } else {
      _applyFilter(filter);
    }
    _updateModel(hide_no_match);
  }

  void qigLogModel::populate(const qigBase* base, const qigCommit& commit, unsigned int max_count)
  {
    qigOid oid;
    commit.oid(oid);
    _populate(QVector<const qigOid*>(1,&oid), max_count, base);
  }

  void qigLogModel::populate(const qigBase* base, const qigReference& ref, unsigned int max_count)
  {
    qigOid oid;
    ref.target(oid);
    {
      populate(base, oid, max_count);
    }
  }

  void qigLogModel::resetInternalData()
  {
    beginResetModel();
    m_model_pos_map_working_copy.clear();
    endResetModel();
    m_model_pos_map_cached.clear();
    _m_hide_no_match=false;
    m_worker.reset();
  }

  // reimplementation for QAbstractItemModel {{{

  bool qigLogModel::canFetchMore(const QModelIndex&) const
  {
    //qDebug() << Q_FUNC_INFO << commitCount() << m_published_rows;
    return commitCount() > m_published_rows;
  }

  void qigLogModel::fetchMore(const QModelIndex &)
  {
    assert(m_published_rows < commitCount());
    // beginInsertRows inserts rows with the last values included, commitCount() and m_published_rows returns last valid+1
    int additional = std::min((int)steps, static_cast<int>( commitCount()-m_published_rows)-1);
    //qDebug() << Q_FUNC_INFO<< m_published_rows << m_published_rows+additional;
    beginInsertRows(QModelIndex(), m_published_rows, m_published_rows+additional);
    m_published_rows+=additional+1;
    endInsertRows();
  }

  int qigLogModel::columnCount(const QModelIndex&) const {
    return m_additional_columns + branchCount();
  }

  int qigLogModel::rowCount(const QModelIndex&) const {
    assert(m_published_rows<= commitCount());
    return m_published_rows;
  }

  QVariant qigLogModel::data(const QModelIndex & index, int role)const
  {
#define TO_PTR(i) reinterpret_cast<internalPointer>(i.internalPointer())
    if(index.column() < m_additional_columns)
    {
      if(role > 0x0fff)
	return dataBasicInfoRole(TO_PTR(index), role);
      else
	if(role == Qt::DisplayRole)
	{
	  switch(index.column())
	  {
	    case ColumnComitter:
	      return dataBasicInfoRole(TO_PTR(index), qigLogModel::roleCommiter)[0].trimmed();
	      break;
	    case ColumnShortMessage:
	      return dataBasicInfoRole(TO_PTR(index), qigLogModel::roleTitle)[6].trimmed();
	      break;
	    case ColumnDate:
	      return dataBasicInfoRole(TO_PTR(index), qigLogModel::roleDate)[2];
	      break;
	    default:
	      break;
	  }
      }
      else if(role == roleOid)
	return dataOid(TO_PTR(index));
      else if ( role == roleParentOids)
	return dataRoleParentOids(TO_PTR(index));
    }
    else
    {
      switch(role)
      {
	//case Qt::DecorationRole:
	  //return dataDecorationRole(TO_PTR(index));
	case Qt::DisplayRole:
	  return dataDisplayRole(TO_PTR(index));
	break;
	case roleGraph:
	  return dataRepresentation(TO_PTR(index));
	  break;
	case modelRoles::roleParentOids:
	  return dataRoleParentOids(TO_PTR(index));
	  break;
	case modelRoles::roleOid:
	  return dataOid(TO_PTR(index));
	  break;
	case Qt::BackgroundRole:
	  return dataBackgroundRole(TO_PTR(index));
	  break;
	case roleBrush:
	  return dataRoleBrush(TO_PTR(index));
	  break;
	default:
	  if( role > 0x0fff)
	    return dataBasicInfoRole(TO_PTR(index), role);
      }
    }
#undef TO_PTR
    
    return QVariant();
  }

  Qt::ItemFlags qigLogModel::flags(const qigLogModelIndex& index) const
  {
    if(!index.isValid()) {
      return Qt::NoItemFlags;
    }
    internalPointer item = reinterpret_cast<internalPointer>(index.internalPointer());
    //Qt::ItemFlags additional = item->isIntractable() ? Qt::ItemIsSelectable | Qt::ItemIsDragEnabled : Qt::ItemFlag(0);
    Qt::ItemFlags additional = item->isCommit() ? Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled : Qt::ItemFlag(0);
    assert( (item->isCommit() && item->hasRepresentationFlag(1) ) || !(item->isCommit() || item->hasRepresentationFlag(1)));
    if(!_m_hide_no_match)
    {
      additional = item->isCommit() && item->isEnabled() ? Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled : Qt::ItemFlag(0);
      return  Qt::ItemNeverHasChildren | additional;
    }
    return  Qt::ItemNeverHasChildren | additional;
  }

  qigLogModel::qigLogModelIndex qigLogModel::index(int row, int column, const QModelIndex& parent) const
  {
    assert(!parent.isValid());
    if(column < m_additional_columns) {
      const qigLogModelData* p = m_model_pos_map_working_copy.commitByIndex(row);
      if(p)
      {
	return createIndex(row,column, const_cast<qigLogModelData*>(p));
      }
    }
    else
    {
      auto iter = m_model_pos_map_working_copy.find(row, column-m_additional_columns);
      if( iter != m_model_pos_map_working_copy.end() && (iter.value().isEnabled() || !_m_hide_no_match) )
      {
	return createIndex(row,column, const_cast<qigLogModelData*>(&iter.value()));
      }
    }
    return createIndex(-1,-1,(quintptr)0);
  }

  QMimeData *qigLogModel::mimeData(const QModelIndexList& list) const
  {
    QMimeData *ret = nullptr;
    if(list.size() == 1)
    {
      const QModelIndex& index = list[0];
      void *data =  index.internalPointer();
      internalPointer d = reinterpret_cast<internalPointer> (data);
      if(d)
      {
	QByteArray data;
	d->rawData(data);
	ret = new QMimeData;
	if(ret)
	{
	  ret->setData(mimeOid(), std::move(data));
	  ret->setData(mimeObjectType(), QByteArray::number(objectTypeCommit));
	}
      }
      assert(ret);
    }
    return ret;
  }
  /** defines a new mime type application/git-oid-commit application/git-oid
   */
  QStringList qigLogModel::mimeTypes() const
  {
    QStringList ret;
    ret << mimeOid()<< mimeObjectType();
    return ret;
  }
  QModelIndex qigLogModel::parent(const QModelIndex&) const {return qigLogModelIndex();}
  // }}} end of reimplementation of QAbstractItemModel
  // }}} end public methods

  //{{{ protected methods

  void qigLogModel::moreDataToProcess(const qigLogModelDataMap&)
  {
    assert(false);
  }
  //}}}

  //{{{ private methods
  void qigLogModel::_applyFilterAndRemoveFromWorkingCopy(const qigLogFilterList& filter)
  {
    if(filter.isEmpty()) return;
    qigLogModelDataMap temp;
    int cnt=0;
    for(auto iter = m_model_pos_map_working_copy.begin(); iter !=m_model_pos_map_working_copy.end(); ++iter)
    {
      if(iter.value().hasRepresentationFlag(qigLogModelHistoryWorker::commit))
      {
	if(filter.applyFilter(qigCommit(iter.value()))) {
	  // show only commits without parent child relation when appying a filter
	  iter.value().setRepresentation(qigLogModelHistoryWorker::commit);
	  temp.insert((int32_t)iter.key(), cnt++, iter.value()); // the first bits of iter.key() contains the branch
	}
      }
    }
    m_model_pos_map_working_copy = std::move(temp);
  }
  void qigLogModel::_applyFilter(const qigLogFilterList& filter)
  {
    if(filter.isEmpty()) return;
    for(auto iter = m_model_pos_map_working_copy.begin(); iter !=m_model_pos_map_working_copy.end(); ++iter)
    {
      if(iter.value().isCommit()) {
	if(iter.value().isEnabled()) {
	  iter.value().setEnabled(filter.applyFilter(qigCommit(iter.value())));
	}
      }
    }
  }
  void qigLogModel::_populate(const QVector<const qigOid*>& oids, unsigned int max_count, const qigBase* m_base)
  {
    m_published_rows=0;
    if(!m_base)
    {
      return;
    }
    m_model_pos_map_cached.clear();
    if(m_worker) {
      m_worker->abort();
    }
    else {
      m_worker=std::unique_ptr<qigLogModelHistoryWorker>(new qigLogModelHistoryWorker(m_base));
    }

    m_worker->setMaxObject(max_count);

    m_model_pos_map_working_copy.clear();
    
    for(int i=0; i<oids.size(); ++i)
    {
      m_worker->pushStart(*(oids[i]));
    }

    m_worker->start();

    beginResetModel();
    m_model_pos_map_cached = m_worker->result();
    m_model_pos_map_working_copy = m_model_pos_map_cached;
    endResetModel();
    // move to thread get a future.
    //
    //while(! hasFinished())
    //{
    //sleep(10);
    //}
  }

  void qigLogModel::_updateModel(bool hide_no_match)
  {
    beginResetModel();
    _m_hide_no_match = hide_no_match;
    endResetModel();
  }
  //}}} end private methods
}


#if 0
#include "LogView.h"
#include <QPainter>

namespace qig
{
  typedef QMap<int, QPixmap> representationImages;
  static representationImages m_representationImages;
inline static QVariant dataDecorationRole(const internalPointer& d)
{
  auto iter = m_representationImages.find(d->representation());
  if(iter == m_representationImages.end())
  {
    const unsigned int s=24;
    QImage image(s,s, QImage::Format_ARGB8);
    //image.fill(Qt::white);
    image.fill(QColor(qRgba(0x66,0x66,0x66,0x66)));
    QRect rect = QRect(0,0, s,s);
    QPainter painter;
    painter.begin(&image);
    painter.initFrom(&image);
    //painter.setRenderHint(QPainter::Antialiasing,false);
    painter.setBackgroundMode(Qt::TransparentMode);
    painter.setBackground(Qt::darkGray);
    painter.setOpacity(.5);
    painter.setPen(Qt::red);
    painter.setBrush(Qt::blue);
    LogIconPainter::paint(&painter, rect, d->representation());
    painter.end();
    iter = m_representationImages.insert(d->representation(), QPixmap::fromImage(image, Qt::NoOpaqueDetection| Qt::ThresholdDither));
  }
  return iter.value();
}


#endif
namespace qig
{
  // list of color with a minimum distance of 42;
  static uint32_t default_colormap[] = {
 0xff0000, 0x800000, 0x00ff00, 0x008000, 0x0000ff, 0x000080, 0x00ffff, 0x008080, 0xff00ff, 0x800080, 0xffff00, 0x808000, 0xa0a0a4, 0x808080, 0xc0c0c0, 
  0x171717, 0x411717, 0x6b1717, 0x951717, 0xbf1717, 0xe91717, 0x174117, 0x414117, 0x6b4117, 0x954117, 0xbf4117, 0xe94117, 0x176b17, 0x416b17, 0x6b6b17, 0x956b17, 0xbf6b17, 0xe96b17, 0x179517, 0x419517, 0x6b9517, 0x959517, 0xbf9517, 0xe99517, 0x17bf17, 0x41bf17, 0x6bbf17, 0x95bf17, 0xbfbf17, 0xe9bf17, 0x17e917, 0x41e917, 0x6be917, 0x95e917, 0xbfe917, 0xe9e917, 0x171741, 0x411741, 0x6b1741, 0x951741, 0xbf1741, 0xe91741, 0x174141, 0x414141, 0x6b4141, 0x954141, 0xbf4141, 0xe94141, 0x176b41, 0x416b41, 0x6b6b41, 0x956b41, 0xbf6b41, 0xe96b41, 0x179541, 0x419541, 0x6b9541, 0x959541, 0xbf9541, 0xe99541, 0x17bf41, 0x41bf41, 0x6bbf41, 0x95bf41, 0xbfbf41, 0xe9bf41, 0x17e941, 0x41e941, 0x6be941, 0x95e941, 0xbfe941, 0xe9e941, 0x17176b, 0x41176b, 0x6b176b, 0x95176b, 0xbf176b, 0xe9176b, 0x17416b, 0x41416b, 0x6b416b, 0x95416b, 0xbf416b, 0xe9416b, 0x176b6b, 0x416b6b, 0x6b6b6b, 0x956b6b, 0xbf6b6b, 0xe96b6b, 0x17956b, 0x41956b, 0x6b956b, 0x95956b, 0xbf956b, 0xe9956b, 0x17bf6b, 0x41bf6b, 0x6bbf6b, 0x95bf6b, 0xbfbf6b, 0xe9bf6b, 0x17e96b, 0x41e96b, 0x6be96b, 0x95e96b, 0xbfe96b, 0xe9e96b, 0x171795, 0x411795, 0x6b1795, 0x951795, 0xbf1795, 0xe91795, 0x174195, 0x414195, 0x6b4195, 0x954195, 0xbf4195, 0xe94195, 0x176b95, 0x416b95, 0x6b6b95, 0x956b95, 0xbf6b95, 0xe96b95, 0x179595, 0x419595, 0x6b9595, 0x959595, 0xbf9595, 0xe99595, 0x17bf95, 0x41bf95, 0x6bbf95, 0x95bf95, 0xbfbf95, 0xe9bf95, 0x17e995, 0x41e995, 0x6be995, 0x95e995, 0xbfe995, 0xe9e995, 0x1717bf, 0x4117bf, 0x6b17bf, 0x9517bf, 0xbf17bf, 0xe917bf, 0x1741bf, 0x4141bf, 0x6b41bf, 0x9541bf, 0xbf41bf, 0xe941bf, 0x176bbf, 0x416bbf, 0x6b6bbf, 0x956bbf, 0xbf6bbf, 0xe96bbf, 0x1795bf, 0x4195bf, 0x6b95bf, 0x9595bf, 0xbf95bf, 0xe995bf, 0x17bfbf, 0x41bfbf, 0x6bbfbf, 0x95bfbf, 0xbfbfbf, 0xe9bfbf, 0x17e9bf, 0x41e9bf, 0x6be9bf, 0x95e9bf, 0xbfe9bf, 0xe9e9bf, 0x1717e9, 0x4117e9, 0x6b17e9, 0x9517e9, 0xbf17e9, 0xe917e9, 0x1741e9, 0x4141e9, 0x6b41e9, 0x9541e9, 0xbf41e9, 0xe941e9, 0x176be9, 0x416be9, 0x6b6be9, 0x956be9, 0xbf6be9, 0xe96be9, 0x1795e9, 0x4195e9, 0x6b95e9, 0x9595e9, 0xbf95e9, 0xe995e9, 0x17bfe9, 0x41bfe9, 0x6bbfe9, 0x95bfe9, 0xbfbfe9, 0xe9bfe9, 0x17e9e9, 0x41e9e9, 0x6be9e9, 0x95e9e9, 0xbfe9e9, 0xe9e9e9
};

uint32_t qigLogModel::nextDefaultColour() const
{
  uint32_t ret = default_colormap[m_used_color_cnt];
  ++m_used_color_cnt;
  m_used_color_cnt %= (sizeof(default_colormap)/sizeof(default_colormap[0]));
  return ret;
}
}
