// vim: fdm=marker
#include <assert.h>
#include <QDebug>


#include "TreeModel.h"
#include "enums.h"
#include "FileIconProvider.h"
#include "Tree.h"
#include "QOid.h"
#include "StatusTree.h"
#include "fileEntry.h"


namespace qig
{

  //{{{ helper class
  class treeEntry2: public fileEntry
  {
    public:
      treeEntry2(): fileEntry(), m_type(objectTypeBad){}
      treeEntry2(const qigTree& t,int entry_index): fileEntry(),
	m_path(t.name(entry_index)),
	m_type(t.type(entry_index))
      {
	t.oid(m_oid, entry_index);
      }
      treeEntry2(treeEntry2* parent, const qigTree &t, int entry_index, const QString& prefix): fileEntry(parent),
	m_name(t.name(entry_index)),
	m_type(t.type(entry_index))
      {
	m_path = prefix + m_name;
	t.oid(m_oid, entry_index);
      }
      const treeEntry2 *parent() const { return static_cast<const treeEntry2*> (fileEntry::parent());}
      const treeEntry2 *childByIndex(unsigned int i) const { return static_cast<const treeEntry2*> (treeEntry::childByIndex(i) );}
      const QString& path() const{return m_path;}
      const QString& name() const{return m_name;}

      const qigOid& oid()const{return m_oid;}
      objectType type() const{ return m_type;}

    private:
      /// name of the file
      QString m_name;
      /// relative path
      QString m_path;
      /// type
      objectType m_type;
      /// oid of entry in git odb
      qigOid m_oid;
  };
  //}}}

  qigTreeModel::qigTreeModel(QObject*parent): QAbstractItemModel(parent),
    m_root(new treeEntry2()),
    m_fileIconProvider(new qigFileIconProvider())
  {
  }

  qigTreeModel::~qigTreeModel()
  {
  }

  void qigTreeModel::resetInternalData()
  {
    beginResetModel();
    m_files.clear();
    endResetModel();
    assert(m_root->childrenCount() == 0);
  }

  // public methods {{{
  int qigTreeModel::columnCount(const QModelIndex& ) const
  {
    return 4;//name, oid, size, last modifier
  }
  Qt::ItemFlags qigTreeModel::flags(const QModelIndex&) const
  {
    return Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsEnabled;
  };
  bool qigTreeModel::hasChildren(const QModelIndex &parent) const
  {
    const treeEntry2* e = toTreeItem(parent);
    return e->hasChildren();
  }
  int qigTreeModel::rowCount(const QModelIndex& i) const
  {
    const treeEntry2* e = toTreeItem(i);
    return e->childrenCount();
  }

  int qigTreeModel::setOid(const qigOid& oid)
  {
    return setTree(qigTree(oid), 0);
  }

  int qigTreeModel::setTree(const qigTree &t, const qigStatusTree* st)
  {
    if(t.isValid() && t != m_tree_oid)
    {
      resetInternalData();
      buildTree(m_files, t, m_root.get(), st);
      return GIT_OK;
    }
    return GIT_EINVALID;
  }

  QVariant qigTreeModel::data(const QModelIndex &index, int role) const
  {
    switch(role)
    {
      case Qt::DisplayRole:
	return dataDisplay(index);
	break; //<anyway
      case Qt::DecorationRole:
	return dataDecoration(index);
	break; //<anyway
      case roleOid:
	return dataOid(index);
	break; //<anyway
      case roleIsDir:
	return dataIsDir(index);
	break;
      case roleFileState:
	return dataFileState(index);
      case roleFilePathRelative:
	return dataFilePathReleative(index);
      case roleFilePath:
	return dataFilePath(index);
      default:
	break; 
    }
    return QVariant();
  }

  QModelIndex qigTreeModel::index(int r, int c, const QModelIndex& parent) const
  {
    if(parent.model() != this && parent.model() != 0)
    {
      qWarning() << Q_FUNC_INFO << "got model index from other model" << r << c << parent.model();
      return QModelIndex();
    }
    const treeEntry2* parent_entry = toTreeItem(parent);
    if( 0<= r && (unsigned int) r < parent_entry->childrenCount())
    {
      return toModelIndex(parent_entry->childByIndex(r), c);
    }
    return QModelIndex();
  }
  QModelIndex qigTreeModel::parent(const QModelIndex& child) const
  {
    const treeEntry2* child_entry = toTreeItem(child);
    return toModelIndex(child_entry->parent());
  }
  //}}}

  void qigTreeModel::buildTree(fileMap& m_files, const qigTree& t, treeEntry2* parent, const qigStatusTree* st, QString prefix)
  {
    const unsigned int n = t.entryCount();
    const QModelIndex& parent_index=toModelIndex(parent);
    beginInsertRows(parent_index, 0, n -1 );
    for(unsigned int i = 0; i < n; ++i)
    {
      treeEntryPtr e( new treeEntry2(parent, t, i, prefix));
      unsigned int state ;
      if(st)
      {
	if( st->statusFile(state, e->path()) == GIT_OK)
	{
	  e->setStateFlag( fileState(state));
	}
	else
	{
	  // might happen; status does not contain clean/unchanged files
	  //qDebug() << "could not resolve state for" << e->path();
	}
      }
      treeEntry2 *p = e.get();
      m_files.push_back( std::move(e) );
      if( t.type(i) == objectTypeTree)
      {
	QString local_prefix = prefix + p->name()+'/';
	qigOid oid;
	t.oid(oid, i);
	buildTree(m_files, qigTree(oid), p, st, local_prefix);
      }
    }
    endInsertRows();
  }

  const QModelIndex qigTreeModel::toModelIndex(const treeEntry2* item, int column) const
  {
    if(item == m_root.get())
    {
      return QModelIndex();
    }
    const int row = item->locate();
    return createIndex( row, column, const_cast<treeEntry2*>(item));
  }

  QVariant qigTreeModel::dataDisplay(const QModelIndex& index) const
  {
    int col = index.column();
    const treeEntry2 * p = toTreeItem(index);
    switch(col)
    {
      case 0:
	return QVariant(QString(p->name()));
      case 1:
	return QVariant(QString(p->oid().asString()));
      case 2:
      case 3:
	return QString("not imp");
      default:
	qDebug()<<"invalid column";
    }
    return QVariant();
  }

  QVariant qigTreeModel::dataOid(const QModelIndex& index) const
  {
    const treeEntry2 * p = toTreeItem(index);
    return QVariant::fromValue(p->oid());
  }

  bool qigTreeModel::dataIsDir(const QModelIndex& index) const
  {
    const treeEntry2 * p = toTreeItem(index);
    return p->type() == objectTypeTree;
  }
  QVariant qigTreeModel::dataDecoration(const QModelIndex&index)const
  {
    if(index.column() == 0)
    {
      const treeEntry2 * p = toTreeItem(index);
      objectType type = p->type();
      fileState state = (fileState) p->stateFlag();
      switch(type)
      {
	case objectTypeTree:
	  return m_fileIconProvider->icon(qigFileIconProvider::Folder, (fileState) state);
	  break;
	case objectTypeBlob:
	  return m_fileIconProvider->icon(qigFileIconProvider::File, (fileState) state);
	  break;
	default:
	  qDebug() << Q_FUNC_INFO << type << state;
	  break;
      }
    }
    return QVariant();
  }
  unsigned int qigTreeModel::dataFileState(const QModelIndex& index)const
  {
    const treeEntry2 * p = toTreeItem(index);
    return p->stateFlag();
  }
  QString qigTreeModel::dataFilePathReleative(const QModelIndex& index)const
  {
    QStringList path;
    const treeEntry2 * p = toTreeItem(index);
    const QString& name = p->name();
    while( m_root.get() != (p=p->parent()) )
    {
      assert(p); //< in theory p==nullptr may occur if `index` is invalid;
      path.push_front(p->name());
    }
    QString ret; ret.reserve(100);
    for(const QString &e: path)
    {
      ret+=e;
      ret+='/';
    }
    ret.append(name);
    return ret;
  }
  QString qigTreeModel::dataFilePath(const QModelIndex& index)const
  {
    return m_working_dir+dataFilePathReleative(index);
  }
}
