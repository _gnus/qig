// vim: fdm=marker ft=cpp.doxygen
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include <QAbstractListModel>
#include <List.h>
#include <Oid.h>
#include <QLinkedList>
#include <map>
#include <memory>
#include "Reference.h"

namespace qig
{

  class qigReference;
  typedef std::unique_ptr<qigReference> qigReferencePtr;
  class qigReferenceManager;
  /**
   * @class qigOidListModel
   * @brief a list model that contains oids and references.
   *
   * model to expose contained oids and references to a view; the model does not fill its list by itsself
   */
  class qigOidListModel: public QAbstractListModel
  {
    Q_OBJECT
    Q_PROPERTY(bool offerIndex READ offerIndex WRITE setOfferIndex)
    Q_PROPERTY(bool offerWD READ offerWD WRITE setOfferWD)
      class entryDescriptor;
      class oidDescriptor;
      class refDescriptor;
      class indexDescriptor;
      class WDDescriptor;
      class nullDescriptor;
      typedef QLinkedList <qigOid> oids;
      typedef std::map <QByteArray, qigReferencePtr> refs;
      typedef std::unique_ptr<entryDescriptor> entryDescriptorPtr;
      typedef qigList <entryDescriptorPtr> entries;
    public:
      qigOidListModel(QObject* parent);
      qigOidListModel(const qigOidListModel&) = delete;

      int addOid(const qigOid& oid);
      int removeOid(const qigOid& oid);

      int actualiseReferences(const qigReferenceManager&);

      /** whether to display an entry for the work directory.
       */
      bool offerIndex() const {return m_has_index_desciptor;}
      void setOfferIndex(bool);

      /** whether to display an entry for the index
       */
      bool offerWD() const {return m_has_wd_desciptor;}
      void setOfferWD(bool);

      /** should be called when the repo is unloaded
       */
      void repositoryUnloaded(){return resetInternalData();}

      /* override QAbstractItemModel */
      virtual QStringList mimeTypes() const override; 
      virtual bool canDropMimeData( const QMimeData* mime, Qt::DropAction action, int row, int column, const QModelIndex&) const override;
      virtual bool    dropMimeData( const QMimeData* mime, Qt::DropAction action, int row, int column, const QModelIndex&)       override;
      virtual Qt::DropActions supportedDropActions() const override;

      virtual QModelIndex index(int row, int col, const QModelIndex& parent) const override;
      virtual QModelIndex parent(const QModelIndex& child) const override;
      virtual int    rowCount(const QModelIndex &parent) const override;
      virtual QVariant data (const QModelIndex&, int role=Qt::DisplayRole) const override;
      virtual Qt::ItemFlags flags(const QModelIndex&) const override;

      virtual bool insertRows(int row, int count, const QModelIndex& parent) override;
      virtual bool setData(const QModelIndex& index, const QVariant& data, int role ) override;

      virtual QModelIndexList match(const QModelIndex& start, int role, const QVariant& value,
	  int hits=1, Qt::MatchFlags = Qt::MatchStartsWith | Qt::MatchWrap) const override;

      /*end override QAbstractItemModel */

    protected:
      void resetInternalData();
    private:
      void appendReference(qigReferencePtr&&);
      bool containsOid(const qigOid& oid);
      void addDescriptor(entryDescriptorPtr&& p, unsigned int pos);
      void addDescriptor(entryDescriptorPtr&& p);
      unsigned int calculateOffset(int descriptor_type);

      /** appends QModelIndexes to out when they matches oid.
       * appends maximum of `hits` indexes
       * @param oid the oid to search for;
       * @param hits maximum indexes to search for;
       * @param out the indexes that matches oid will be appended to out;
       */
      void find(const qigOid& oid, int hits, QModelIndexList& out) const;

      class entryDescriptor {
	public:
	  entryDescriptor(int t):type(t){};
	  virtual ~entryDescriptor()=default;
	  virtual QString representation() const = 0;
	  virtual qigOid oid() const = 0 ;
	  const int type;
	  virtual bool operator==(const entryDescriptor&) const = 0;
	  virtual bool operator==(const qigOid&o) const { return this->oid() == o;}
      };


    private:// member
      /// a list of oids to store the objects. this list owns the oids but is not relevant for the view;
      oids m_oids;
      /// a list of refs to store the objects. this list owns the refs but is not relevant for the view;
      refs m_refs;
      /// the entries that offered to the view;
      entries m_entries;
      /// descriptor for the index. invalid if offerIndex is not set;
      bool m_has_index_desciptor;
      /// descriptor for the working directory. invalid if offerWD is not set;
      bool m_has_wd_desciptor;

    private://static
      static entries::const_iterator toEntry(const QModelIndex&, const entries& m_entries);
  };
}
