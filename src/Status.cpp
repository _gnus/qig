// vim: fdm=marker
#include <git2.h>
#include <assert.h>
#include <string.h>
#include <QDebug>


#include "enums.h"
#include "Status.h"


namespace qig
{
  class qigDiffDelta;
  // {{{ local static declerations
  inline static void initOptions(git_status_options& opts);
  //inline static qigString toPath(const git_diff_file* p);
  inline static void updateResult(statusResult& summary, const git_status_entry* entry);
  inline static void updateSummary(statusSummary& summary, const git_status_entry* entry);
         //static void updateHead2Idx(statusResult& summary, const qigDiffDelta &diff);
         //static void updateIdx2Wd(statusResult& summary, const qigDiffDelta &diff);
  // }}}
  
  //{{{ class qigDiffDelta
  /**@class qigDiffDelta
   * a diff between 2 files
   * contains list of hunks
   * there is no constructor or copy constructor.
   */
  class qigDiffDelta
  {
    friend class qigDiffPersistentDelta;
    public:
      //the lifetime of a git_diff_delta is bound to the statuslist
      //or foreach loop or git_diff etc. To minimise the use of an 
      //invalid git_diff_delta the ctor is private
      qigDiffDelta(git_diff_delta*p):_m_diff_delta(p){}
      //bool statusIsSet(diffDeltaStatus);

      const char* recentFile() const{return _m_diff_delta->old_file.path;}
      const char* newFile()    const{return _m_diff_delta->new_file.path;}
      bool sameFile() const	    {return newFile() == recentFile() ;}
      const git_delta_t& status() const     {return _m_diff_delta->status  ;}

    private:
      // see comment for ctor;
      qigDiffDelta(const qigDiffDelta&)=delete;

      git_diff_delta* _m_diff_delta;
  };
  //}}}

  // {{{ gitStatusListHolder
  class gitStatusListHolder
  {
    public:
      gitStatusListHolder():m_list(0)
      {
      }
      ~gitStatusListHolder()
      {
	if(m_list) {
	  git_status_list_free(m_list);
	}
      }
      size_t entryCount() {
	return m_list ? git_status_list_entrycount(m_list):0U;
      }
      int init(git_repository* repo, git_status_options* opts) {
	return git_status_list_new(&m_list, repo, opts);
      } 

      const git_status_entry* entryByIndex(size_t i)
      {
	return m_list ? git_status_byindex(m_list, i): nullptr;
      }
    private:
      git_status_list* m_list;
  };
  // }}}

  //{{{ qigStatusWorker
  //
  //{{{ static helper function
  inline static void initOptions(git_status_options& opts)
  {
    // prepare the opts
  
    git_status_init_options(&opts, GIT_STATUS_OPTIONS_VERSION);
    //opts->pathspec.count=0;
    //opts->pathspec.strings=0;
    opts.show=GIT_STATUS_SHOW_INDEX_AND_WORKDIR;

    opts.flags = \
	GIT_STATUS_OPT_INCLUDE_UNTRACKED       |
	GIT_STATUS_OPT_INCLUDE_IGNORED         |
	GIT_STATUS_OPT_INCLUDE_UNMODIFIED      |
	GIT_STATUS_OPT_DISABLE_PATHSPEC_MATCH  |
	GIT_STATUS_OPT_RECURSE_IGNORED_DIRS    |
#ifdef UNIX_BUILD
	GIT_STATUS_OPT_SORT_CASE_SENSITIVELY   |
#else
	GIT_STATUS_OPT_SORT_CASE_INSENSITIVELY |
#endif
	//GIT_STATUS_OPT_RENAMES_FROM_REWRITES   |
	//GIT_STATUS_OPT_NO_REFRESH              |
	GIT_STATUS_OPT_UPDATE_INDEX            |
	GIT_STATUS_OPT_INCLUDE_UNREADABLE      ;
    // end opts->flag

  }

  inline static qigString toPath(const git_diff_file* p) {
    return p ? qigString(p->path) : qigString(nullptr);
  }

  static void updateSummary(statusSummary& summary, const git_status_entry* entry)
  {
#define GIT_STATUS_INDEX_FLAGS (GIT_STATUS_INDEX_NEW | GIT_STATUS_INDEX_MODIFIED | GIT_STATUS_INDEX_DELETED | GIT_STATUS_INDEX_RENAMED | GIT_STATUS_INDEX_TYPECHANGE)
#define GIT_STATUS_WT_FLAGS (GIT_STATUS_WT_NEW | GIT_STATUS_WT_MODIFIED | GIT_STATUS_WT_DELETED | GIT_STATUS_WT_RENAMED | GIT_STATUS_WT_TYPECHANGE | GIT_STATUS_WT_UNREADABLE)
    if(entry->status == GIT_STATUS_CURRENT) return;
    if(entry->status == GIT_STATUS_IGNORED) {
      ++summary.ignoredCnt;
      return;
    }
    if(entry->status == GIT_STATUS_CONFLICTED) {
	++summary.conflictedCnt;
	return;
    }
    int status_index=entry->status & GIT_STATUS_INDEX_FLAGS;
    int status_wt   =entry->status & GIT_STATUS_WT_FLAGS;

    switch(status_index)
    {
      case  GIT_STATUS_INDEX_NEW        :
	++summary.newIndexCnt;
	break;
      case  GIT_STATUS_INDEX_MODIFIED   :
	++summary.modifiedIndexCnt;
	break;
      case  GIT_STATUS_INDEX_DELETED    :
	++summary.deletedIndexCnt;
	break;
      case  GIT_STATUS_INDEX_RENAMED    :
	++summary.renamedIndexCnt;
	break;
      case  GIT_STATUS_INDEX_TYPECHANGE :
	++summary.typeChangeIndexCnt;
	break;
      case 0 : //< no flag is ok
	break;
      default:
	assert(false);
    }
    switch(status_wt)
    {
      case  GIT_STATUS_WT_NEW           :
	++summary.newWDCnt;
	break;
      case  GIT_STATUS_WT_MODIFIED      :
	++summary.modifiedWDCnt;
	break;
      case  GIT_STATUS_WT_DELETED       :
	++summary.deletedWDCnt;
	break;
      case  GIT_STATUS_WT_TYPECHANGE    :
	++summary.typeChangeWDCnt;
	break;
      case  GIT_STATUS_WT_RENAMED       :
	++summary.renamedWDCnt;
	break;
      case  GIT_STATUS_WT_UNREADABLE    :
	++summary.unreadableWDCnt;
	break;
      case  GIT_STATUS_CONFLICTED       :
	++summary.conflictedCnt;
	break;
      case 0 : //< no flag is ok
	break;
      default:
	assert(false);
    }
#undef GIT_STATUS_WT_FLAGS
#undef GIT_STATUS_INDEX_FLAGS
  }

  inline static void updateResult(statusResult& result, const git_status_entry* entry)
  {
    //if(entry->head_to_index) {
    //  updateHead2Idx(summary, entry->head_to_index);
    //}
    if(entry->index_to_workdir) {
      QString dum( entry->index_to_workdir->new_file.path );
      statusResult::iterator iter = result.find(dum );
      assert(entry->index_to_workdir->new_file.path);
      if( iter != result.end() )
      {
	iter.value() |= entry->status;
      }
      else
      {
	//qDebug() << "inserting to status" << dum << "with status status"<<entry->status;
	//result.insert(statusResult::value_type(QString(diff.recentFile()), diff.status()));
	result.insert(QString(dum), entry->status);
      }
    }
  }
  //}}}

  // {{{ ctor dtor
  qigStatusWorker::qigStatusWorker(qigStatus& parent):
    m_parent(parent),
    m_repo(nullptr)
  {
    static_assert( (git_status_t) fileStateUnchanged	 == GIT_STATUS_CURRENT           ,"should be the same");

    static_assert( (git_status_t) fileStateIndexNew          == GIT_STATUS_INDEX_NEW         ,"should be the same");
    static_assert( (git_status_t) fileStateIndexModified     == GIT_STATUS_INDEX_MODIFIED    ,"should be the same");
    static_assert( (git_status_t) fileStateIndexDeleted      == GIT_STATUS_INDEX_DELETED     ,"should be the same");
    static_assert( (git_status_t) fileStateIndexRenamed      == GIT_STATUS_INDEX_RENAMED     ,"should be the same");
    static_assert( (git_status_t) fileStateIndexTypeChange   == GIT_STATUS_INDEX_TYPECHANGE  ,"should be the same");

    static_assert( (git_status_t) fileStateWDNew             == GIT_STATUS_WT_NEW         ,"should be the same");
    static_assert( (git_status_t) fileStateWDModified        == GIT_STATUS_WT_MODIFIED    ,"should be the same");
    static_assert( (git_status_t) fileStateWDDeleted         == GIT_STATUS_WT_DELETED     ,"should be the same");
    static_assert( (git_status_t) fileStateWDTypeChange      == GIT_STATUS_WT_TYPECHANGE  ,"should be the same");
    static_assert( (git_status_t) fileStateWDRenamed         == GIT_STATUS_WT_RENAMED     ,"should be the same");
    static_assert( (git_status_t) fileStateWDUnreadable      == GIT_STATUS_WT_UNREADABLE  ,"should be the same");

    static_assert( (git_status_t) fileStateIgnored           == GIT_STATUS_IGNORED        ,"should be the same");
    static_assert( (git_status_t) fileStateConflicted        == GIT_STATUS_CONFLICTED     ,"should be the same");
  }
  
  qigStatusWorker::~qigStatusWorker()
  {
  }
  //}}}

  //{{{ public methods

  void qigStatusWorker::setRepository(git_repository *p)
  {
    m_repo = p;
  }

  int qigStatusWorker::statusFile(unsigned int& status_flags, const char* path)
  {
    return git_status_file(&status_flags, m_repo, path);
  }
  // }}}


  //{{{ private
  void qigStatusWorker::run()
  {
    git_status_options opts;
    initOptions(opts);
    gitStatusListHolder list;
    if(list.init(m_repo, &opts) != GIT_OK ) {
      return;
    }

    statusSummary summary;
    statusResult result;
    for(auto i=0U; i < list.entryCount(); ++i)
    {
      const git_status_entry* entry  = list.entryByIndex(i);
      if(entry)
      {
	updateSummary(summary, entry);
	updateResult(result, entry);
      }
    }
    this->updateFinished(summary, result);
  }
  //}}}

  //}}} end qigStatusWorker

  // {{{ class qigStatus
  qigStatus::qigStatus(const qigBase& base):
    qigStatusWorker(*this),
    qigBase(base)
  {
    setRepository(mainRepo());
  }

  qigStatus::~qigStatus()
  {
  }

  int qigStatus::statusFileFast(unsigned int& status_flags, const QString& path)
  {
    auto iter = m_result.find(path);
    if(iter!=m_result.end())
    {
      status_flags=*iter;
      return GIT_OK;
    }
    status_flags=fileStateUndefined;
    return GIT_ENOTFOUND;
  }

//  bool qigStatus::result( statusResult& result, unsigned int &charge_cnt) const
//  {
//    std::lock_guard<std::mutex> lock(m_result_protector);
//
//    bool ok = false;
//    if(charge_cnt != m_charge_cnt)
//    {
//      charge_cnt=m_charge_cnt;
//      result = m_result;
//      ok=true;
//    }
//    return ok;
//  }

  void qigStatus::updateFinished(const statusSummary &summary, statusResult &result)
  {
    if(m_result!=result)
    {
      ++m_charge_cnt;

      std::lock_guard<std::mutex> lock(m_result_protector);

      memcpy(&m_summary, &summary, sizeof(statusSummary));
      m_result=std::move(result);
    }
  }
  //}}}
}
