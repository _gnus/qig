#include <QMimeData>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QApplication>

#include "TreeContainer.h"
#include "Mime.h"

namespace qig
{
  qigTreeContainer::qigTreeContainer(QWidget* p): qigFocusFrameWidget(p)
  {
    this->setAcceptDrops(true);
  }

  void qigTreeContainer::dragEnterEvent(QDragEnterEvent* ev)
  {
    if(ev && ev->dropAction()==Qt::CopyAction)
    {
      const QMimeData* data = ev->mimeData();
      if(data)
      {
	if(data->hasFormat(mimeOid()) && data->hasFormat(mimeObjectType()))
	{
	  const QByteArray& type_data=data->data(mimeObjectType());
	  int type = type_data.toInt();
	  if(type == objectTypeCommit || type == objectTypeTree)
	  {
	    ev->accept();
	  }
	}
      }
    }
  }
  void qigTreeContainer::dropEvent(QDropEvent* ev)
  {
    if(ev && ev->dropAction()==Qt::CopyAction)
    {
      const QMimeData* data = ev->mimeData();
      if(data)
      {
	if(data->hasFormat(mimeOid()) )
	{
	  const QByteArray& oid= data->data(mimeOid());
	  emit treeSourceChangeRequest(oid);
	  //QString oid ;
	  //foreach(unsigned char d, mime){ oid.append(QString::number((int)d,0x10));}
	  //qDebug() << "qigTreeContainer::dragEnterEvent: mimeOid: "<<oid;
	}
      }
    }
  }


  /*
  void qigTreeContainer::childEvent(QChildEvent*ev)
  {
    QObject *obj = ev->child();
    if(obj && ev->added())
    {
      obj->installEventFilter(this);
    }
  }

  bool qigTreeContainer::eventFilter(QObject* o, QEvent* ev)
  {
    switch(ev->type())
    {
      case QEvent::FocusIn:
	m_child_has_focus=true;
	update();
	break;
      case QEvent::FocusOut:
	m_child_has_focus=false;
	update();
      default:
	return false;
    }
    return false;
  }
  */

}
