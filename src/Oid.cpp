// vim: fdm=marker
#include <git2.h>
#include <assert.h>
#include "Str.h"
#include "Oid.h"

namespace qig
{
 // static git_otype toGitType(object::type t){return (git_otype) t;}

  //{{{ctors
  qigOid::qigOid()
  {
  }
  //}}}

  // destructor
  //qigOid::~qigOid()
  //{
  //}

  // public methods {{{
  const qigChar *qigOid::asString() const
  {
    git_oid* id = const_cast<git_oid*>(oid());
    return owner() ? git_oid_tostr_s(id) : nullptr;
  }

  void qigOid::rawData(const unsigned char **pp_data) const
  {
    *pp_data = oid()->id;
  }

  //bool qigOid::operator==(const qigOid & other) const { return  (*this == *(other.oid())); }

  //}}} end public methods


 // qigOid& qigOid::operator=(const git_oid& id){
 //   loadNewId(&id);
 //   return *this;
 // }

  //git_object* qigOid::resolve(object::type type)
  //{
  //  git_object* ret;
  //  if(!git_object_lookup(&ret, owner(), *this, toGitType(type)))
  //      return ret;
  //  return nullptr;
  //}

  //qigOid& qigOid::operator=(const qigOid& id){
  //  load(id.oid());
  //  //m_base should be nececcary only if initialised by oid. 
  //  //if(!m_base) m_base = new qigBase(*id.m_base);
  //  return *this;
  //}

}
