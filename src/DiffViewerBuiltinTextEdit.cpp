
#include <QPaintEvent>
#include <QPainter>
#include <QTextBlock>
#include "DiffViewerBuiltinTextEdit.h"
namespace qig
{


  // line number code from http://doc.qt.io/qt-5/qtwidgets-widgets-codeeditor-example.html


  class LineNumberArea : public QWidget
  {
    public:
      LineNumberArea(qigDiffViewerBuiltinTextEdit *editor) : QWidget(editor) {
	codeEditor = editor;
      }

      QSize sizeHint() const Q_DECL_OVERRIDE {
	return QSize(codeEditor->lineNumberAreaWidth(), 0);
      }

    protected:
      void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE {
	codeEditor->lineNumberAreaPaintEvent(event);
      }

    private:
      qigDiffViewerBuiltinTextEdit* codeEditor;
  };
  


  qigDiffViewerBuiltinTextEdit::qigDiffViewerBuiltinTextEdit(QWidget* parent):
    //QTextEdit(parent)
    QPlainTextEdit(parent)
    , m_lineNumberArea(new LineNumberArea(this))
  {
    connect(this->document(), (&QTextDocument::blockCountChanged  ), this, (&qigDiffViewerBuiltinTextEdit::updateLineNumberAreaWidth));
    connect(this, (&QPlainTextEdit::updateRequest	  ), this, (&qigDiffViewerBuiltinTextEdit::updateLineNumberArea));
  }

  void qigDiffViewerBuiltinTextEdit::keyPressEvent(QKeyEvent* ev)
  {
    return QPlainTextEdit::keyPressEvent(ev);
  }

  int qigDiffViewerBuiltinTextEdit::lineNumberAreaWidth()
  {
    int digits = 2;
    int max = qMax(1, blockCount());
    while (max >= 100) {
      max /= 10;
      ++digits;
    }
    int space = 3 + fontMetrics().width(QLatin1Char('9')) * digits;
    return space;
  }

  void qigDiffViewerBuiltinTextEdit::updateLineNumberAreaWidth(int /* newBlockCount */)
  {
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
  }

  void qigDiffViewerBuiltinTextEdit::updateLineNumberArea(const QRect &rect, int dy)
  {
    if (dy)
      m_lineNumberArea->scroll(0, dy);
    else
      m_lineNumberArea->update(0, rect.y(), m_lineNumberArea->width(), rect.height());

    if (rect.contains(viewport()->rect()))
      updateLineNumberAreaWidth(0);
  }
  void qigDiffViewerBuiltinTextEdit::resizeEvent(QResizeEvent *e)
  {
    QPlainTextEdit::resizeEvent(e);

    QRect cr = contentsRect();
    m_lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
  }

  void qigDiffViewerBuiltinTextEdit::lineNumberAreaPaintEvent(QPaintEvent *event)
  {
    QPainter painter(m_lineNumberArea);
    painter.fillRect(event->rect(), Qt::lightGray);
    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();
    while (block.isValid() && top <= event->rect().bottom()) {
      if (block.isVisible() && bottom >= event->rect().top()) {
	QString number = QString::number(blockNumber + 1);
	painter.setPen(Qt::black);
	painter.drawText(0, top, m_lineNumberArea->width(), fontMetrics().height(),
	   Qt::AlignRight, number);
      }

      block = block.next();
      top = bottom;
      bottom = top + (int) blockBoundingRect(block).height();
      ++blockNumber;
    }
  }

}

#include "DiffViewerBuiltinTextDocument.h"

  QWidget * qig::qigDifferFactoryBuiltin::createView(QWidget*parent)
  {
    if(!m_view)
    {
      m_view = new qig::qigDiffViewerBuiltinTextEdit(parent);
      m_view->setDocument(m_differ);
    }
    return m_view;
  }
