#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include "gitTypes.h"
#include "enums.h"
#include "Base.h"

namespace qig
{
  // forward declaration
  class qigBranch;
  class qigIndex;
  class qigObject;
  class qigOid;
  class qigReferenceManager;
  class qigStatus;
  class _internalData;



  /**@class qigRepository
   * main class to initialise the qig back end. This class does not hold the git_repository directly
   */
  class qigRepository: public qigBase
  {
    friend class qigRepositoryStub;
    public: //static 
      /** loads a new repository from git repository at @a path
       * @param [out] out a instance of qigRepository or nullptr on failure
       * @param path path where to search for git. Path must be a working directory in a git repository
       * @param open_bare if set the repository will be opened as bare
       * @return errror code
       */
      static int load(qigRepository*& out, const qigChar *path, bool open_bare = false);
      /** creates a new repository from git repository at @a path
       * @param [out] out a new instance of qigRepository. the instance must be deleted with `release`
       * @param path path where to create the git repo
       * @param open_bare if set the repository will be opened as bare
       * @return error code
       */
      int create(qigRepository*& out, const qigChar *path, bool open_bare = false);
    public: //{{{ public methods
      /** unloads the git repository.
       */
      void unload();

      // ** returns number of branches in this repository.
      // *
      // * @param type specifies whether to include only selected branches
      // * @return number of branches in this repository
      // */
      //int branchCount(branch::branchType type = branch::branchTypeAll) const;
      // ** searches a branch by the index
      // * @param index the index of the branch
      // * @param type the type of the desired branch. Use branchTypeAll if the type is irrelevant
      // * @param out[out] a pointer where qigBranch is stored. Do not delta the instance.
      // */
      //int branchLookup(int index, branch::branchType type, qigBranch*& out);
      // ** searches a branch of the given name 
      // * @param branch_name the name of the branch to look up
      // * @param type the type of the desired branch. Use branchTypeAll if the type is irrelevant
      // * @param out[out] a pointer where qigBranch is stored. Do not delete the returned instance.
      // */
      //int branchLookupByName(const qigChar *branch_name, branch::branchType type, qigBranch*& out); 


      const qigReferenceManager& referenceManager() const;
      qigReferenceManager& referenceManager();

      /** initialises the oid id
       * id will become invalid after calling qigRepository::unload.
       * @param id the oid to initialise
       * @param data raw oid value witch is used to initialise id;
       * @return 0 on success. initialising the oid successfully does not mean that the oid corresponds to an object, but id has the same data as data
       */
      int initialiseOid(qigOid& id, const unsigned char* data) const;

      /** the path to the root to the repository that is currently loaded
       */
      const char * repositoryRootPath() const;
      /** the path to the working directory.
       */
      const char * wdPath() const;
      /** returns the relative path, that starts from git repository path
       * @param abs_path the absolute path of a file or directory inside the repository
       * @param relative_path will be set to a location inside abs_path and contains the relative path
       * @return 0 on success. 
       */
      int relativePathFromAbsolutePath(const qigChar* abs_path, const qigChar** relative_path) const;

      void release(){delete this;}
      //}}}
    
    protected://{{{
      // for unittest only
      qigRepository();
      void load(git_repository* repo){qigBase::repositoryLoaded(repo); _load(repo); }
    private://{{{ methods and ctors
      void _load(git_repository* repo);
      qigRepository(qigBase&& base);
      ~qigRepository();
      qigRepository(const qigRepository&) = delete;
      qigRepository(qigRepository&&) = delete;

      int initHead();
      //}}}
    private: //{{{ members
      _internalData *_m_internal_data;
      //}}}
    private://static

  };

}
