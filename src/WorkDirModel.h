#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */

#include <QFileSystemModel>
#include <QFileSystemWatcher>
#include <QSortFilterProxyModel>
#include <map>
#include <memory>
#include "FileIconProvider.h"

#include "enums.h"
#include "Status.h"

namespace qig
{


  class qigStatus;

  class fileEntry;

  /** the qigWorkDirModel supports check state.
   * for ui this feature is not desired. the qigWorkDirModel2 returns an empty QVariant for CheckStateRole
   */
  class qigWorkDirModel2: public QSortFilterProxyModel
  {
    Q_OBJECT
    public:
      qigWorkDirModel2(QObject* parent=0): QSortFilterProxyModel(parent){}

      QVariant data(const QModelIndex& index, int role) const
      {
	return role == Qt::CheckStateRole ? QVariant() : QSortFilterProxyModel::data(index,role);
      }
  };

  /**@class qigWorkDirModel
   * the model to watch for file changes on the working directory and providing the Tree*View with 
   * information for rendering the current state.
   * The class uses the QFileSystemModel to traverse the repo but does not cache the state of an file
   * TODO checking the state of the current file system is not sufficient to track deleted files.
   *      qigWorkDirModel should be a proxy that uses a QFileSystemModel and git_status as source and tracks
   *      the state of a file by itself
   */
  class qigWorkDirModel: public QFileSystemModel
  {
    typedef void (*releaseFileEntry_f)(fileEntry*);
    typedef std::unique_ptr<fileEntry, releaseFileEntry_f> fileEntryPtr;
    typedef std::map<QString, fileEntryPtr> fileMap;
    friend class qigIndexBuilderListModel;
    Q_OBJECT
    public:
      typedef std::unique_ptr<qigStatus> qigStatusPtr;

    public:
      qigWorkDirModel(QObject *parent);
      ~qigWorkDirModel();

      /** reimplementation for QAbstractItemModel
       */
      virtual QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const override;

      /** reimplemented for role Qt::CheckStateRole
       */
      virtual bool setData(const QModelIndex& index, const QVariant& data, int role=Qt::EditRole) override;

      /** hides QFileSystemModel::setRootPath
       * set work dir path of git repo to path. this does not restrict the items accessible for the
       * model to children of path but restrict
       *
       * @param path The path to the work dir of the repository
       */
      void setRootPath(const QString &path);

      /** sets the new qigStatus used to resolve the file state
       * on return p will contain the instance set previously (if any).
       */
      void setStatus(qigStatusPtr&& p);


      /** triggers the work dir model reread the the index and update the status
       * for given files
       * @param files relative path to the files;
       * @note do not call when no repository is loaded;
       */
      void updateStatus(const QStringList& files);
      /** call when the repository become invalid
       */
      void repositoryUnloaded() { resetInternalData();}
    protected:
      void resetInternalData();
      virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
      virtual bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;
    private Q_SLOTS:
      /** updates internal map to track the status.
       * also triggers QFilesysetm mdoel to load children which may emit the signal updatedEntries
       */
      void updatedEntries(const QString &path);

      void onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles = QVector<int>());

      void onRowsAboutToBeInserted(const QModelIndex &parent, int first, int last);
      void onRowsInserted(const QModelIndex &parent, int first, int last);

      void onRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last);
      void onRowsRemoved(const QModelIndex &parent, int first, int last);


    private:
      void updateCache();
      /** returns the current state of the file. 
       * The returned value is the flag with the highest priority.
       * @sa fileState and qigStatus.
       */
      fileState currentState(const QModelIndex &index) const;
      /**
       * returns the state falg for given index;
       */
      unsigned int currentStateFlag(const QModelIndex&) const;
      /// impl for data(index, role==Qt::DecorationRole);
      QVariant dataDecoration(const QModelIndex& index) const{ return QVariant(m_icon_provider.icon( typeToEnum(type(index)), currentState(index)));}
      QVariant dataFilePathRelative(const QModelIndex& index) const;
      QVariant dataCheckState(const QModelIndex& index) const;
      QVariant dataBlobOid(const QModelIndex& index) const;

      int statusLookUp( const QString& child_path, unsigned int& status);

      // deleter for fileEntry. used in fileEntryPtr
      static void releaseFileEntry(fileEntry*);
      inline static const fileMap::key_type toFileKey(const QModelIndex& index);
      const fileMap::key_type toStatusKey(const QString& str) const;

      static inline fileEntryPtr toPtr(fileEntry*);
      static qigFileIconProvider::IconType typeToEnum(const QString& type)
      {
	if(type == QLatin1String("Folder") ) return qigFileIconProvider::Folder;
	return qigFileIconProvider::File;
      }

      // root of the git repo; not really the root of the filesystem but of the repo;
      //QPersistentModelIndex m_root;

      // fileEntryPtr itself build a tree like structure which represents the file system but do not offer standard accessors to find and fileEntryPtr by path path
      // map filepath -> struct fileEntryPtr
      fileMap m_files;

      //map filepath -> status
      //as returned by qigStatus
      //statusResult m_status;

      qigFileIconProvider m_icon_provider;

      //TODO m_file_system_status uses local path as key
      // while m_files uses absolute path as key;
      qigStatusPtr m_file_system_status;
      // libgit uses local path whereas QFileSystemModel uses absolute path.
      // for keys in fileMap to resolve status from qigStatus. just cut the path to the directory to the git repository from
      // the absolute path (m_root_path_length also contains the index for the tailing '/')
      unsigned short m_root_path_length;
  };
}

