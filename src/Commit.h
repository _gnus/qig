// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include "gitTypes.h"
#include "List.h"
#include "Object.h"
#include "enums.h"
#include "Signature.h"
namespace qig
{
  class qigTree;
  class qigOid;
  class qigFilter;
  /**
   * @ingroup LibGitWrap
   * @{
   */
  /**@class qigCommit
   * @brief represents a commit.
   * wraps commit.h from libgit2
   */
  class qigCommit: public qigObject
  {
    public://{{{
      qigCommit();
      /** creates an Commit from given oid. the oid must be valid
       */
      qigCommit(const qigOid&);
      // dtor alread public and virtual
      // defining dtor prevents generation of move operator
      //virtual ~qigCommit();

      /** apply filter on current object.
       * the Commit decide by itsself how to filter;
       *
       * @param f the container that contains the filter properties
       * @return true if object matches filter @a f
       */
      bool applyFilter(const qigFilter& f) const override;
      /** returns the short commit message 
       * @return the short commit message or nullptr if object is empty
       */
      const qigChar* commitSummary()const {return _commitSummary();}
      /** returns the complete commit message 
       * @return the complete commit message message or nullptr if object is empty
       */
      const qigChar* commitMessage()const {return _commitMessage();}
      /** return the commit time in UTC??
       */
      int64_t commitTime()	     const {return _commitTime();}
      /** return the time zone of the commiter (at least the offset)
       */
      int commitTimeOffset()	     const {return _timeOffset();}
      const qigSignature* author()	     const {return _author();}
      const qigSignature* commiter()    const {return _commiter();}

      /**
       * return the number of parents of this commit.
       * if parentCount is > 2 than the commit is a merge commit
       * @return number of parents
       */
      unsigned int parentCount() const;
      /** returns true if commit is a merge commit.
       * this statement equals to parentCount >1
       * @return true if commit is merge commit. Otherwise false
       */
      int isMerge() const { return parentCount() >1;}
      /** returns the id of the parents.
       * in contrast to the qigCommit this function does not resolve the commit in the database. 
       *
       * @param length of the out_array available.
       * @param out_array a list of jet empty qigOid the user must allocate the array on @a length elements of Oid.
       * @param list_length the number of elements used. If @a list_length > length the last qigOid are still empty.
       *
       * @code
       *
       * qigCommit ci(id);
       *
       * int n = ci.parentCount();
       * qigCommit* list = new qigCommit[n];
       * int err = ci.parentList(n,list,n);
       * @endcode
       */
      int parentList(const unsigned int length, qigCommit out_array[], unsigned int &list_length) const;
      /** set out to parent at index
       * @param out commit will be set to parent with index index;
       * @param index determines which parent should be returned. Index must be smaller than parentCount
       * @return 0 on success
       * not implemented jet
       */
      int parent(qigCommit& out,unsigned int index) const;
      /// not implemented
      /// should fill the tree with the state of the commit
      int tree(qigTree&) const;
      /** returns the id of the parents.
       * in contrast to parentList this function does not resolve the commit in the database. 
       *
       * @param length of the out_array available.
       * @param out_array a list of jet empty qigOid the user must allocate the array on @a length elements of Oid.
       * @param list_length the amount of elements used. If @a listLength > length the last qigOid are still empty.
       * @sa parentList(const unsigned int length, qigCommit out_array[], unsigned int &listLength) const;
       */
      int parentList(const unsigned int length, qigOid out_array[], unsigned int &list_length) const;
      /** set out to parent at index
       * @param out commit will be set to parent with index index;
       * @param index determines which parent should be returned. Index must be smaller than parentCount
       * @return 0 on success
       */
      int parent(qigOid& out,unsigned int index) const;

      /**
       * creates a new commit.
       * @param update_ref defaults to HEAD
       * @param author defaults to comitter
       * @param committer defaults to current git option user
       * @param message the commit message
       * @param tree the tree that is the base for the commit
       * @param parents if empty the peeled commit of update_ref is used;
       */
      static int commit(
	qigOid &out,
      const qigChar * update_ref,
      const git_signature *author,
      const git_signature *committer,
      const qigChar* encoding,
      const qigChar* message,
      const qigTree& tree,
      const qigList<qigCommit>& parents
      );
      // compiler generated works fine
      //qigCommit& operator=(const qigCommit& other);

      //}}}

    protected:
      //{{{ protected methods
      qigCommit(git_commit*);

      /** reimplemented from qigGitObjectHolder
       * 
       * nothing to do here. just call qigObject::reload
       *
       */
      virtual void reload() override{ qigObject::reload();}//TODO check the type of the new object
      /** reimplemented from qigGitObjectHolder
       * 
       * nothing to do here. just call qigObject::reset
       */
      virtual void reset()override {qigObject::reset();}
      //}}}
    private:

      /**
       * returns true if @a contained is in @a str
       * @param str the string in which to search for a specific substring
       * @param contained the substring to search for
       */
      static bool _strstr(const qigChar* str, const qigChar* contained);

      /** reimplements qigObject:: to return git_commit or nullptr
       */
      const git_commit* commitish()const {return (const git_commit*)object(objectTypeCommit);}

      /// override the current commit with the other
      qigCommit& operator=(const git_commit*other);
      void _clear();


      const qigChar* _commitSummary()	const;
      const qigChar* _commitMessage()	const;
      int64_t _commitTime()		const;
      int _timeOffset()			const;
      const qigSignature* _author()         const;
      const qigSignature* _commiter()      const;


      //const qigChar* _m_commiter;
      //const qigChar* _m_author;


  };

}

/**
 * @}
 * endingroup
 */
