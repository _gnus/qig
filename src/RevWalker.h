#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include "Base.h"
#include "List.h"
#include "gitTypes.h"
#include "enums.h"


typedef struct git_revwalk git_revwalk;

namespace qig
{
  class qigOid;
  class qigCommit;
  /**@class qigRevWalker
   * wraps the revWalker api of libgit2.
   *
   * there is also the high level api @a qigHistory for traversing the repository
   */
  class qigRevWalker: private qigBase
  {
    public:

      qigRevWalker(const qigBase &);
      ~qigRevWalker();

      /** acquire a walker as a lazy init method. Calling any other method will fail if acquire hasn't be called 
       * at first.
       * to use the revwalker a start must be pushed first. 
       *
       * @param walk_sort the sort method
       */
      int acquire(int  walk_sort);
      //overloaded for the commitish
      //int acquireAt(const qigCommit& commi_id, int  walk_sort = revwalk::revwalkSortNone);
      /**
       * Changing the sorting mode resets the walker.
       */
      void setSortMethod(int);
      /** hides a commitish (and its children) from the walk
       * @param commit_id the commit_id which to hide
       * @return 0 on success
       */
      int pushHide(qigOid& commit_id);
      /** push a committish where to start the walk
       */
      int pushStart(qigOid&);
      /** releases a walker an deallocates its resources
       * should be called when the walker isn't required any longer
       */
      void release();
      /** do the next step in the traverse
       * @param out the oid to store the data
       * @return 0 on success, GIT_ITEROVER when iteration is over
       */
      int next(qigOid& out);
      /** resets the walker. and prepare it for the next walk
       */
      void reset();
    private:

      /**returns qig sort definition to git sort definition (which are the same at the moment
       * @param sort 
       */
      static constexpr int toGitSort(int sort) {return sort;}

      inline git_revwalk* revWalker() const{return _m_revwalker;}
      git_revwalk * _m_revwalker;

  };

}


