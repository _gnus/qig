#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */

class QWidget;
class QKeyEvent;
namespace qig
{
  /** @class qigTreeView
   * view for a git tree. Not related to a QTreeView but. Either the current working directory or a snapshot of a commit
   */
  template <typename QView>
  class qigTreeView: public QView
  {
    public:
      inline qigTreeView(QWidget* parent);

    protected:
      inline void onActivated(const QModelIndex& index);

      inline virtual void keyPressEvent(QKeyEvent* ev) override;

    private:
      virtual void moveUp(){};

      inline void onAnyActivated(const QModelIndex& index);
      // has specialisation for QView = QTableView
      inline void onDictionaryActivated(const QModelIndex& index);
  };
}

#include "TreeView.hpp"
