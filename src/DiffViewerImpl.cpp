// vim: fdm=marker
#include <QStringBuilder>
#include <QFont>
#include <QBrush>

#include "DiffViewerImpl.h"
#include "enums.h"

static const int ROOT_ID=1;

namespace qig
{

  //{{{ ctor,dtor
  qigDiffViewerImpl::qigDiffViewerImpl():
    m_root(ROOT_ID),
    m_delta(new DiffDeltaModelItem(&m_root))
  {
  }

  //}}}


  //{{{ public methods
  // default impl is to continue;
  // but ignore the delta otherwise
  int qigDiffViewerImpl::addDelta( IDiffDelta** delta)
  {
    *delta=m_delta.get();
    return m_delta ? 0 : -1;
  }
  int qigDiffViewerImpl::addFile(IDiffFile**file)
  {
    DiffFileModelItem* p_file = new DiffFileModelItem(&m_root);
    m_model_entries.push_back(DiffViewerBuiltinModelItemPtr(p_file));

    // return created file
    *file = p_file;
    return p_file ? GIT_OK : GIT_ERROR;
  }

  int qigDiffViewerImpl::addHunk(IDiffFile* file, IDiffHunk**hunk)
  {
    if(file)
    {
      DiffHunkModelItem *p_hunk = new DiffHunkModelItem(static_cast<DiffFileModelItem*>(file));
      m_model_entries.push_back(DiffViewerBuiltinModelItemPtr(p_hunk));

      // return created hunk
      *hunk = p_hunk;
      return p_hunk ? GIT_OK : GIT_ERROR;
    }
    *hunk = nullptr;
    return GIT_ERROR;
  }

  int qigDiffViewerImpl::addLine(IDiffHunk* hunk, IDiffLine**line)
  {
    if(hunk)
    {
      DiffLineModelItem *p_line = new DiffLineModelItem(static_cast<DiffHunkModelItem*>(hunk));
      m_model_entries.push_back(DiffViewerBuiltinModelItemPtr(p_line));

      // return created line
      *line = p_line;
      return p_line ? GIT_OK : GIT_ERROR;
    }
    *line = nullptr;
    return GIT_ERROR;
  }

  void qigDiffViewerImpl::reset()
  {
    m_model_entries.clear();
  }
  //}}}

  //{{{ helper classes
  QString DiffFileModelItem::dataDisplay() const
  {
    QString ret;
    if(m_new_path.isEmpty())
    {
      ret = m_old_path;
    }
    else
    {
      ret = m_new_path;
    }
    // ret = ret +"\t%1\t%2";
    //ret = ret.arg(QString::number(m_flag,0x10)).arg(QString::number(m_status,0x10));
    return ret;
  }
  QString DiffHunkModelItem::dataDisplay() const
  {
    return header();
  }
  QString DiffLineModelItem::dataDisplay() const
  {
    return m_display_data;
  }
  Qt::GlobalColor DiffLineModelItem::dataForeground()const
  {
    Qt::GlobalColor colour;
    switch(origin())
    {
      case '+':
	colour = Qt::darkGreen;
	break;
      case '-':
	colour = Qt::red;
	break;
      default:
	colour = Qt::black;
	break;
    }
    return colour;
  }
  //}}} end helper classes
}
