// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once

#include <QWidget>
#include <QByteArray>

#include "KeyEventHandler.h"

class QSettings;

namespace qig
{

  struct diffOptions;
  /** 
   * @ingroup Application
   * @class qigDiffContainer
   * @brief a widget that draws a focus frame around itself
   */

  class qigDiffContainer: public qigFocusFrameWidget<QWidget>
  {
    Q_OBJECT
    public:
      qigDiffContainer(QWidget* parent);
      qigDiffContainer(const qigDiffContainer&)=delete;

  };

}
