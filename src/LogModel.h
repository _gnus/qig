#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */

#include <QAbstractListModel>
#include <memory>
#include <QMap>
#include "LogModelHistoryWorker.h"


namespace qig
{
  class qigRevWalker;
  class qigOid;
  class qigReference;
  class qigLogFilterList;
  //class qigHistoryConstrains;
  typedef qigLogModelData* internalPointer;
  /** @class qigLogModelProxy 
   * @ingroup Application
   * @brief this is an almost empty class because the libgit2 does a great work in mapping the git graph
   * and there is absolutely no reason why there I should reimplement an graph traverse algorithm.
   * This class uses qigLogModelHistoryWorker to initialise the model data and libgit2 for queries from the view. 
   */
  class qigLogModel: public QAbstractItemModel
  {
    typedef QModelIndex qigLogModelIndex;
    enum Column { ColumnComitter, ColumnShortMessage, ColumnDate};

    /** a oid does not need much more space then a ptr.
     * Qt uses internal memory sharing for container. I think its ok to store a QVector a value. A std::vector wouldn't be the best idea.
     *
     * @brief list to track merge commits
     */
    //typedef QMap<mergeCommitOid, parentCommitOidList > mergeCommitList;
    /** @brief container to track fork commits
     */
    //typedef QMap<mergeCommitOid, childrenCommitOidContainer > forkCommitList;

    public:
      explicit qigLogModel(QObject* parent);
      //qigLogModel(const qigLogModel&) = delete;
      virtual ~qigLogModel();

      
      /**
       * applies Filter to either previous result or currently selected branch
       *
       * Filter works on single object only. filter on graph properties (like operator .. and ...) requires a new walk through the repository.
       * @sa populate
       *
       * @param filter the filter to apply to the commits
       * @param reset_old_result if false filter in result of last filter result only. otherwise filter on currently selected branch;
       * @param hide_no_match if true the graph will be redrawn without parent/child relation but without commits that do not match filter. 
       *		    otherwise the graph still contains the complete history, but those commits that do not match will be disabled.
       */
      void applyFilter(const qigLogFilterList& filter, bool reset_old_result, bool hide_no_match);

      /** reinitialise the history graph starting from; TODO allow multiple starts and hides
       */
      void populate(const qigBase* base, const qigOid& commitish, unsigned int max_entries =UINT_MAX){ _populate(QVector<const qigOid*>(1,&commitish), max_entries, base);}
      //void populate(const qigBase* base, const qigHistoryConstrains& commitish, unsigned int max_entries =UINT_MAX);
      void populate(const qigBase* base, const qigCommit& commitish, unsigned int max_entries =UINT_MAX);
      void populate(const qigBase* base, const qigReference& commitish, unsigned int max_entries =UINT_MAX);
      /**
       * repository 
       * should be called just before the repository is going to be unloaded
       */
      void repositoryUnloaded(){resetInternalData();}

      // *******************************************************************
      // reimplementation for QAbstractItemModel
      // *******************************************************************

      /**implementation for QAbstractTableModel::rowCount()
       * there are no children int the model to display.
       * in git children of commits are their their own parents in the model
       */
      virtual int rowCount(const QModelIndex& )const override;
      /**implementation for QAbstractTableModel::coloumnCount()
       */
      virtual int columnCount(const QModelIndex& ) const override;
      /**implementation for QAbstractTableModel::data()
       * @returns data for index index with role role
       * @param index element to query. index has an internal pointer used to store the oid
       * @param role what is required. @sa roleBasicInfoFlag
       * @code
       * if(role == committer)
       *   return qgitCommit(index.internalPointer()).committer();
       * @endcode
       */
      virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole)const override;

      virtual QMimeData *mimeData(const QModelIndexList& list) const override;
      /** defines a new mime type application/git-oid-commit application/git-oid
       */
      virtual QStringList mimeTypes() const override;

      virtual Qt::ItemFlags flags(const qigLogModelIndex& parent) const override;

      virtual qigLogModelIndex parent(const qigLogModelIndex &child) const override;


      /**implementation for QAbstractItemModel::index()
       */
      virtual qigLogModelIndex index(int row, int column, const qigLogModelIndex& parent = qigLogModelIndex()) const override;

      virtual bool canFetchMore(const QModelIndex& index) const override;
      virtual void fetchMore(const QModelIndex& index)  override;

      //sort TODO how
      // allocate new traverser???
      // sort which criteria???
      //   sort by time??? << default given by revwalk
      //   sort by author << requires more data then oid
      //   sort by tree   
      // find 
      //    find commits that matches a specific regex in message log
      //    find commits that has a specific  << revwalk from oid

      /** to query more then one property use the entries as flags the returned QVariant
       * is a QStringList (lowest enum value is first entry) where each entry of the list matches an query flag
       * if the querry can't be fullfilled the QStringList contains an empty string as query result
       */
      enum roleBasicInfoFlag
      {
	/// query for the name of the commit
	roleCommiter = 0x1000,
	/// query for the mail address of the commit
	roleCommiterMail = roleCommiter <<1,
	/// query for the commit date
	roleDate = roleCommiterMail << 1,
	/// query for the Author of the commit. not implemented
	roleAuthor = roleDate << 1,
	/// query for the Authors address of the commit. not implemented
	roleAuthorMail = roleAuthor << 1,
	/// query for the commit hash
	roleSHA1 = roleAuthorMail << 1,
	/// query for the commit message title
	roleTitle = roleSHA1 << 1,
	/// query for the commit message
	roleMessage = roleTitle << 1
      };

      // QFileSystemModel also uses [0x0100; 0x0103]
      enum roleDisplay
      {
	// return value is an int which must be interpreted according to the representationflag
	roleGraph = 0x105,
	roleIsRef
      };

      /** see comments for populate()
       * overloaded as slot.
       */
      //void populate(const qigHistoryConstrains& commitish){populate(commitish, UINT_MAX);};
    protected:
      void resetInternalData();
      void moreDataToProcess(const qigLogModelDataMap&);
    private:

      /** does the filtering by setting a marker in qigLogModelData.
       * but does not update the model.
       * @param filter the filter to apply to the commits.
       * @sa _updateModel
       */
      void _applyFilterAndRemoveFromWorkingCopy(const qigLogFilterList& filter);
      /** does the filtering by setting a marker in qigLogModelData and eventually remove items from m_model_pos_map_working_copy.
       * but does not update the model.
       * @param filter the filter to apply to the commits.
       * @sa _updateModel
       */
      void _applyFilter(const qigLogFilterList& filter);
      /** populates the model by including all oids that are available when starting the traverse at commitish
       */
      void _populate(const QVector<const qigOid*>& commitish, unsigned int max_entries, const qigBase* base);
      /** populates the model by including all oids that are available when starting the traverse at commitish but do not include oids that are reachable from commitishToHide
       */
      void _populate(const QVector<const qigOid*>& commitish, const QVector<const qigOid*>&commitishToHide, unsigned int max_entries );
      void _updateModel(bool);

      uint32_t branchCount() const {return m_model_pos_map_working_copy.branchCount();}
      uint32_t commitCount()const {return m_model_pos_map_working_copy.storedObjectsCounter();}

      uint32_t nextDefaultColour() const;

      /// contains the mapping model position (aka row col) to oid and the relation between oids
      /// The oids are those oids from the selected branch;
      //
      // TODO maybe store all oids contained in the currently loaded repository and use a proxy to do the graph calculation.
      // consequence. the map would be just a list. and the model index would contain the oid as payload only. (just as now);
      // filterproxy would contain an modelPosToOidPtrMap with currently displayed oids
      // -- search might be unexpected; what would be the expected result for query for all commts that contains text xy.
      //      the expected result would be all commits for the current branch;
      // -- when m_model_pos_map_cached creating the index would be harder; // why the proxy would have the same infos as this class
      // better treat complete repository as `special branch` where m_model_pos_map_cached contains all oids of commits;
      //
      // but maybe use a list the is sorted by date(just push all commits to a list when traversing the tree). currently the model index contains a ptr
      // to the m_model_pos_map_cached::value_type. therefore the map does not contain any information that are not available without a map.
      // but the informations of the time is lost
      //
      // the map is used to initialise the QModelIndex. @sa index(int, int, const qigLogModelIndex&)
      //
      // contains the result of the last traverse
      qigLogModelDataMap m_model_pos_map_cached;
      // contains the working copy. May be altered by filter request or similar. In unfiltered state the map contains the same data as m_model_pos_map_cached
      qigLogModelDataMap m_model_pos_map_working_copy;
      std::unique_ptr<qigLogModelHistoryWorker> m_worker;

      typedef QMap<QByteArray, QColor> colourMap;
      // TODO fill map when receiving data instead of when it is required
      mutable colourMap m_commiter_colour_map;

      // used as iterator over builtin color map;
      mutable uint8_t m_used_color_cnt;

      /// whether to hide filtered commits
      bool _m_hide_no_match;

      // number of additional columns to display information about current commit;
      const uint8_t m_additional_columns;

      // counts the number of rows that are published
      unsigned int m_published_rows;


      /**returns info of commit and index index. The size of the returned depends on @a roleBasicInfoFlag
       * for each roleBasicInfoFlag the possibly empty query will be appended to the returnValue
       */
      //const QStringList commitBasicInfos(const QModelIndex & index, int roleBasicInfoFlags ) const
      //const QPointer<qigDiffInfo> commitDiffInfos(const QModelIndex & index, int roleBasicInfoFlags ) const
      QVariant dataDecorationRole(const internalPointer& d)const;
      QVariant dataRoleBrush(const internalPointer& d)const;
  };
}
