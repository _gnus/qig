// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include "gitTypes.h"
#include "Object.h"
#include "enums.h"

namespace qig
{
  class qigTree;
  class qigOid;
  class qigFilter;
  /**@class qigBlob
   * @brief wraps blob.h from libgit2. 
   *
   * A blob is a object in the git odb that was a file before
   */
  class qigBlob: public qigObject
  {
    public://{{{
      qigBlob();
      /** creates an blob from given oid. the oid must be valid
       */
      qigBlob(const qigOid&);

      /**
       * Get a read-only buffer with the raw content of a blob.
       *
       * A pointer to the raw content of a blob is returned;
       * this pointer is owned internally by the object and shall
       * not be free'd. The pointer may be invalidated at a later
       * time.
       *
       * @param buffer [out] pointer will be set to start of internal buffer. do not modify the buffer
       * @return len of buffer in bytes
       */
      int rawContent(const char*& buffer) const;

    //}}}
    protected://{{{ protected methods
      qigBlob(git_blob*);

      /** reimplemented from qigGitObjectHolder
       * 
       * nothing to do here. just call qigObject::reload
       *
       */
      virtual void reload(){ qigObject::reload();}//TODO check the type of the new object
      /** reimplemented from qigGitObjectHolder
       * 
       * nothing to do here. just call qigObject::reset
       */
      virtual void reset(){qigObject::reset();}
      //}}}
    private:

      /** reimplements qigObject:: to return git_commit or nullptr
       */
      const git_blob* blob()const {return (const git_blob*)object(objectTypeBlob);}

      /// override the current commit with the other
      qigBlob& operator=(const git_blob*other);
      void _clear();
  };

}
