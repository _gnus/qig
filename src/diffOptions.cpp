#include "diffOptions.h"

namespace qig
{

  diffOptions diffOptions::diffBufferToIndex(qigString&& path)
  {
    diffOptions opts;
    opts.name = std::move(path);
    opts.target= objectTypeBlob;
    opts.tree_source[newSide] = treeSourceIndex;
    opts.tree_source[oldSide] = treeSourceWD;
    return opts;
  }
  diffOptions diffOptions::diffTreeToTree(qigOid&& old_tree, qigOid&& new_tree)
  {
    diffOptions opts;
    opts.target= objectTypeTree;
    opts.tree_source[newSide] = treeSourceOid;
    opts.tree_source[oldSide] = treeSourceOid;
    opts.oid[newSide] = std::move(new_tree);
    opts.oid[oldSide] = std::move(old_tree);
    return opts;
  }
}
