#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <QTreeView>

#include "TreeView.h"


namespace qig
{
  // pseudo typedef for  qigTreeView<QTreeView>
  // moc does not support templates
  //
  //typedef QTreeView qigTreeTreeView;

  class qigTreeTreeView: public qigTreeView<QTreeView>
  {
    Q_OBJECT
    public:
      qigTreeTreeView(QWidget* p): qigTreeView<QTreeView>(p)
      {
	bool ok = connect(this, SIGNAL(activated(const QModelIndex&)), this, SLOT(onActivated(const QModelIndex&)), Qt::DirectConnection);
	if(!ok)
	  throw 1;
      }

    public Q_SLOTS:
      void onActivated(const QModelIndex& index){return qigTreeView::onActivated(index);}
  };
}
