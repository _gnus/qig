#include "StatusTree.h"
#include "enums.h"
#include "git2/diff.h"


#include "DiffViewerBuiltinDiffer.h"

namespace qig
{
  static fileState toFileState(int t)
  {
    fileState s = fileStateUndefined;
    switch(t)
    {
      case GIT_DELTA_UNMODIFIED:
	s=fileStateUnchanged;
	break;
      case GIT_DELTA_ADDED:
	s=fileStateWDNew;
	break;
      case GIT_DELTA_DELETED:
	s=fileStateWDDeleted;
	break;
      case GIT_DELTA_MODIFIED:
	s=fileStateWDModified;
	break;
      case GIT_DELTA_RENAMED:
	s=fileStateWDRenamed;
	break;
      case GIT_DELTA_COPIED:
	// TODO
	s=fileStateWDNew;
	break;
      case GIT_DELTA_IGNORED:
	s=fileStateIgnored;
	break;
      case GIT_DELTA_UNTRACKED:
	s=fileStateIgnored;
	break;
      case GIT_DELTA_TYPECHANGE:
	s=fileStateWDTypeChange;
	break;
      case GIT_DELTA_UNREADABLE:
	s=fileStateWDUnreadable;
	break;
      case GIT_DELTA_CONFLICTED:
	s=fileStateConflicted;
	break;
    }
    return s;
  }

  qigStatusTree::qigStatusTree()
  {
  }

  int qigStatusTree::statusFile(unsigned int& status_flags, const QString& str) const
  {
    auto iter = m_status.find(str);
    if(iter != m_status.end())
    {
      status_flags = iter.value();
      return GIT_OK;
    }
    return GIT_ENOTFOUND;
  }
  int qigStatusTree::accept(IDiffDelta *)
  {
    // we are not interested in the result of the diff;
    return 1;
  }

  int qigStatusTree::compare(const qigOid& t, const qigOid& s)
  {
    this->reset();

    qigDifferHelperBuiltinTreeToTree filler(*this);

    filler.setSource(s, qigDifferHelper::sourceTypeNewSource);
    filler.setSource(t, qigDifferHelper::sourceTypeOldSource);

    return filler.diff();
  }

  void qigStatusTree::finalise(IDiffBase* item)
  {
    DiffDeltaModelItem * delta = dynamic_cast<DiffDeltaModelItem*>(item);
    if(delta)
    {
      m_status.insert(delta->m_new_path, toFileState(delta->status()));
    }
  }

}
