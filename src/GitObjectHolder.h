// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include "gitTypes.h"
#include "enums.h"
#include "GitOidHolder.h"
//#include "Oid.h"

namespace qig
{
  class qigOid;
  class qigObject;

  /**@ingroup LibGitWrap
   * @class qigGitObjectHolder
   * @brief data holder for git_object and derivation
   *
   * any class that inherit form this class should reimplement reset() and reload()
   * the implementation should call the base class.
   *
   * reset() is emitted if the current object become invalid. for example when loading another object
   * reload() is emitted if another object came in scope. for example after loading another object
   */
  class qigGitObjectHolder: protected qigGitOidHolder
  {
    public: //{{{ public methods
      /** constructs an empty object
       */
      qigGitObjectHolder();
      /** construct from an existing git object
       */
      qigGitObjectHolder(git_object*);
      /** construct from an existing git object
       */
      qigGitObjectHolder(const qigOid&);
      // copy ctor
      qigGitObjectHolder(const qigGitObjectHolder& other);
      qigGitObjectHolder(qigGitObjectHolder&& other);
      // dtor
      virtual ~qigGitObjectHolder();

      /** returns the git_object if it has type @a type otherwise return 0;
       * @param type the desired type
       *
       * @return the object or 0
       */
      git_object* object(objectType type);
      /// and the const overload
      const git_object* object(objectType type)const ;
      /** free current object and loads object @a o
       *
       * emits reset and reload
       *
       * @param o new object to load
       *
       * this method calls reload. Some virtual methods may throw qigAttributeError.
       * @throws qigAttributeError
       */
      void load(git_object *o);
      /** free current object and loads object @a o
       *
       * emits reset and reload
       *
       *
       * @param oid new object to load
       */
      // don't know if this is a good idea but static cast require Oid.h to be included
      void load(const qigOid& oid){
	return load(reinterpret_cast<const qigGitOidHolder&>(oid));}

      /** resets internal state.
       * implementation should reset the internal state
       * note this class cannot be reseted. setting a nullptr is fatal
       */
      virtual void reset()=0;

      /** returns true if current oid is a valid oid
       */
      bool isValid()const {return _m_object != 0 && qigGitOidHolder::isValid();}
      /** returns the oid of the object
       * @param out an object that will be filled
       * @return 0 on success
       */
      void oidCopy(qigOid& out) const;
      /** return true if current object is loaded.
       * two objects are the same if they have the same oid
       */
      bool operator==(const git_object& obj) const;
      /// map to qigGitOidHolder::operator == (const git_object&) const;
      bool operator==(const qigGitObjectHolder& obj) const{
	if(_m_object==obj._m_object) return true;
	return *this == *obj._m_object;
      }
      /// map to qigGitOidHolder::operator == (git_oid&) const;
      bool operator==(const qigOid& obj) const {
	return (qigGitOidHolder::operator==(*reinterpret_cast<const qigGitOidHolder&>(obj).rawValue()));
      }
      // copy operator
      qigGitObjectHolder& operator=(const qigGitObjectHolder& obj);
      // move operator
      qigGitObjectHolder& operator=(qigGitObjectHolder&& obj);

      /** qigGitObjectHolder is private to hide the object. on the other hand libgit2 returns
       * git_object*. other low level api may want to assign a new object to qigObject.
       */
      static void assign(qigObject& object, git_object *p_object)
      {
	qigGitObjectHolder& obj=reinterpret_cast<qigGitObjectHolder&>(object);
	obj.load(p_object);
      }

      //}}}
      
    private://{{{private methods and members
      /** free current object and loads object @a o
       *
       * emits reset and reload
       *
       * @param oid oid of the new object to load
       */
      void load(const qigGitOidHolder& oid);
      /// clear internal buffer unconditionally (except it it 0)
      void _clear();
      /** free current object and loads object @a o
       *
       * emits reset and reload
       *
       * @param o new object to load
       * @param reload_oid if true also reload the new oid. 
       */
      void _load(git_object *o, bool reload_oid);
      /// assign o to internal buffer unconditionally. But ensure to free the buffer before assign new value
      void set(git_object* p);
      /** called when another object is loaded.
       * class that inherit from this class must reimplement reload.
       * the new object can be queried by resolve(type)
       * @sa resolve
       */
      virtual void reload()=0;
      /** look up the object to which the current oid belongs
       * 
       * transparently calls git_object_lookup(...)
       */
      int lookup();
      /** override from qigGitOidHolder
       */
      void reloadFromId();

      git_object * _m_object;
      //}}}
  };
}
