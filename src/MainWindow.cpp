// vim: fdm=marker
#include <memory>

#include <QKeyEvent>
#include <QEvent>
#include <QFileDialog>
#include <QDebug>


#include "MainWindow.h"
#include "enums.h"
#include "Error.h"
#include "KeyEventHandler.h"
#include "LogFilterList.h"
#include "LogModel.h"
#include "QOid.h"
#include "StringFilter.h"
#include "diffOptions.h"



namespace qig
{
  static bool addOidToQComboBox(QComboBox * target, const QVariant& oid);
  static bool shallBeDiffed(const QModelIndex& index);
  static QString getStyleSheet()
  {
    return 
      "QDockWidget:focus, qig--qigDiffContainer:focus {"
      "	border: 1px outset blue;"
      "}";
  }

  //{{{ ctor dtor
  qigMainWindow::qigMainWindow(qigApplication &app):
    QMainWindow(nullptr, Qt::Window),
    m_current_state(-1),
    m_app(app),
    m_keyEventHandler(new qigKeyEventHandler)
  {

    setupUi(this);
    connect(&m_app, (&qigApplication::inconvenienceDetected), this, (&qigMainWindow::onInconvienceDetected));
    connect(&m_app, (&qigApplication::beginStateChange), this, (&qigMainWindow::onApplicationStateChange));
    connect(&m_app, (&qigApplication::endStateChange), this, (&qigMainWindow::onApplicationStateChange));
    _m_log_view_dock_container->setWidget(_m_log_view);
    dockWidgetContents->hide();
    _m_log_view->setColumnWidth(0,300);
    _m_editor_view->hide();
    //_m_builtin_diff_view->hide();
    for(auto iter = m_keyEventHandlerWorkerList.begin(); iter != m_keyEventHandlerWorkerList.end(); ++iter)
    {
      m_keyEventHandler->addInputHandler(iter->get());
    }
    //qigKeyEventHandlerWorker* worker;
    //worker = new qigKeyEventHandlerWorker(
	//_m_builtin_diff_text_view, tr("z,c","fold close"), SLOT(onFoldCloseRequest()), this
	//); // _m_builtin_diff_text_view shall delete the worker
    //_m_diff_container->addInputHandler(worker);

    //worker = new qigKeyEventHandlerWorker(
	//_m_builtin_diff_text_view, tr("z,o","fold open"), SLOT(onFoldOpenRequest()), this
	//); // _m_builtin_diff_text_view shall delete the worker
    //_m_diff_container->addInputHandler(worker);

    setupActionsForTreeView();
  }

  qigMainWindow::~qigMainWindow(){}

  //}}}

  //{{{public methods
  void qigMainWindow::setDiffDocument(QTextDocument *)
  {
  }
  void qigMainWindow::setDiffModel(QAbstractItemModel*)
  {
    //this->_m_builtin_diff_view->setModel(p);
  }

  void qigMainWindow::setTreeModel(QAbstractItemModel *p)
  {
    /*  when not unsetting the root a modelindex from another
     *  model will be passed to QAbstractItemModel::index;
     */
    _m_tree_view_as_table->setRootIndex(QModelIndex());
    _m_tree_view_as_tree->setRootIndex(QModelIndex());
    this->_m_tree_view_as_table->setModel(p);
    this->_m_tree_view_as_tree->setModel(p);
  }

  void qigMainWindow::postProcessUi()
  {
    //auto dum = new qigTreeContainer2(this);
    //dum->setupUi(dum);
    //dum->show();
    //addDockWidget(static_cast<Qt::DockWidgetArea>(4), dum);

    statusbar->insertWidget(0, &evil_edit, 2);
    evil_edit.show();
    statusbar->hide();

    QWidget* mainWidgets [] = { _m_tree_container, _m_diff_container, _m_log_view };
    for( unsigned int i=0; i<sizeof(mainWidgets) / sizeof(mainWidgets[0]); ++i)
    {
      mainWidgets[i] -> setStyleSheet( getStyleSheet() );
    }
    _m_tree_container->watchChildForFocus(_m_tree_view_as_table);
    _m_tree_container->watchChildForFocus(_m_tree_view_as_tree);
    _m_tree_container->watchChildForFocus(_m_tree_source_selector);

    //_m_diff_container->watchChildForFocus(_m_builtin_diff_text_view);
    _m_diff_container->watchChildForFocus(_m_diff_old_tree_selector);

    qigMainWindow *Application = this;
    // connections for actions with sender
    //QObject::connect(actionExit, SIGNAL(triggered()), Application, SLOT(close()));
    //QObject::connect(actionViewShowHistory, SIGNAL(toggled(bool)), _m_log_view_dock_container, SLOT(setVisible(bool)));
    //QObject::connect(actionViewShowFilter, SIGNAL(toggled(bool)), _m_log_filter_container, SLOT(setVisible(bool)));

    // connections for actions as receiver
    //QObject::connect(_m_log_view_dock_container, SIGNAL(visibilityChanged(bool)), actionViewShowHistory, SLOT(setChecked(bool)));


    // other stuff
    //QObject::connect(_m_filter_string_selection_line_edit, SIGNAL(editingFinished()), Application, SLOT(onFilterTriggered()));

    QItemSelectionModel *p_selection_model = _m_log_view->selectionModel();
    connect(p_selection_model, (&QItemSelectionModel::currentChanged), this, (&qigMainWindow::onLogCommitSelected));

    connect(_m_button_ok, (&QPushButton::pressed), this, (&qigMainWindow::onTreeViewAccept));
    connect(_m_button_ok, (&QPushButton::pressed), this, (&qigMainWindow::onTreeViewAbort));

    connect(_m_tree_view_as_table, SIGNAL(selectionChanged()), Application, SLOT(onTreeSelectionChanged()));
    connect(_m_tree_container, (&qigTreeContainer::treeSourceChangeRequest),
	this, static_cast<void (qigMainWindow::*)(const QByteArray&)>(&qigMainWindow::onTreeSourceChangeRequest));
    connect(_m_tree_source_selector, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
	this, static_cast<void (qigMainWindow::*)(int)>(&qigMainWindow::onTreeSourceChangeRequest));
    
    // the designer embeds an custom widget into an layout into an widget into an dockwidget.
    // delete the layout and the widget in the enforced widget. embed the custom widget into to dockwidget direclty
    //_m_log_view_dock_container->setWidget(_m_log_view);
    //delete horizontalLayout_2;
    //delete dockWidgetContents;
  }

  //}}}

  //{{{ slots

#define START_EVAL try { 
#define STOP_EVAL } catch(int i) { onInconvienceDetected(i, "received Error" );}
  void qigMainWindow::onFilterTriggered()
  {
    if(m_current_state != applicationStateDefault) {
      return;
    }
    START_EVAL
    const QString filterstring = _m_filter_string_selection_line_edit->text();
    logFilter::fields field = comboBoxIndexTofield(_m_filter_field_selection_combo_box->currentIndex());
    bool reset_old_results = _m_filter_reset_old_results_check_box->checkState() == Qt::Checked;
    qigFilter* filter = createFilterList(filterstring, field) ;

    if(filter)
    {
      qigLogFilterList filter_list;
      filter_list.addFilter(filter);

      m_app.onFilterTriggered(filter_list, reset_old_results);
    }
    STOP_EVAL
  }

  void qigMainWindow::onLogCommitSelected(const QModelIndex& index, const QModelIndex&)
  {
    if(m_current_state != applicationStateDefault) {
      return;
    }
    START_EVAL


    onInspectCommit(index);

    //const QVariant& new_oid = _m_tree_source_selector->currentData(roleOid);
    //const QVariant& old_oid = _m_diff_old_tree_selector->currentData(roleOid);

    //diffTree(new_oid.value<QOid>(), old_oid.value<QOid>());

    STOP_EVAL;
    return;
  }

  void qigMainWindow::onFoldCloseRequest()
  {
    START_EVAL
    //QTextCursor pos=_m_builtin_diff_text_view->textCursor();
    //m_app.onFoldDiff(pos);
    STOP_EVAL
  }

  void qigMainWindow::onInspectCommit(const QModelIndex& index)
  {
    if(m_current_state != applicationStateDefault) {
      return;
    }
    const QVariant & parents = index.data(roleParentOids);

    if(parents.isValid() && parents.type() == static_cast<QVariant::Type>(QMetaType::QVariantList)) {
      const QVariantList& list = parents.toList();
      if(!list.empty())
      {
	const QVariant& oid_var = list.front();
	if(oid_var.canConvert<QOid>())
	{
	  addOidToQComboBox(_m_diff_old_tree_selector, oid_var );
	}
      }
    }

    const QVariant oid_var = index.data(roleOid);
    addOidToQComboBox(_m_tree_source_selector, oid_var);
  }

  void qigMainWindow::onShowCommandLine()
  {
    START_EVAL
    statusbar->show();
    evil_edit.setFocus(Qt::OtherFocusReason);
    STOP_EVAL
  }

  void qigMainWindow::onTreeSelectionChanged()
  {
    START_EVAL
    const QModelIndex& current_selection = _m_tree_view_as_table-> currentIndex();
    diffBlob(current_selection);

    STOP_EVAL
  }

  void qigMainWindow::onTreeSourceChangeRequest(int )
  {
    if(m_current_state != applicationStateDefault) {
      return;
    }
    START_EVAL
    const QVariant& type = _m_tree_source_selector->currentData(roleTreeSource);
    const QVariant& var = _m_tree_source_selector->currentData(roleOid);
    bool ok;
    int treeSourceType = type.toInt(&ok);
    if(var.isValid() && ok)
    {
      // TODO query type
      const qigOid& oid = var.value<QOid>();
      const char unsigned *data;
      oid.rawData(&data);

      QByteArray raw(reinterpret_cast<const char*>(data), OID_RAWSZ);

      int old_type = _m_diff_old_tree_selector->currentData(roleTreeSource).toInt();
      const qigOid& old_oid = _m_diff_old_tree_selector->currentData(roleOid).value<QOid>();
      const char unsigned *old_data;
      old_oid.rawData(&old_data);

      QByteArray old_raw(reinterpret_cast<const char*>(old_data), OID_RAWSZ);
      m_app.setTreeSource(raw, treeSourceType, old_raw, old_type);
    }
    STOP_EVAL
  }

  void qigMainWindow::onTreeSourceChangeRequest(const QByteArray& raw)
  {
    if(m_current_state != applicationStateDefault) {
      return;
    }
    START_EVAL
    m_app.setTreeSource(raw, treeSourceOid, QByteArray(), -1);
    STOP_EVAL
  }

  void qigMainWindow::onTreeViewActionaddTriggered()
  {
    START_EVAL
    const QModelIndex& current_index = _m_tree_view_as_table->currentIndex();

    if(current_index.isValid())
    {
#if _DEBUG
      if(_m_tree_view_as_table->model() != m_app._m_workdir_model) { throw GIT_ERROR; }
#endif 
      bool ok;
      bool is_dir = current_index.data(roleIsDir).toInt(&ok);
      if(!ok)
      {
	assert(false);
      }
      // setApplicationState should change the treeModel;
      if(m_app.setApplicationState(applicationStateAdd) != GIT_OK)
      {
	showError();
      }
      assert(m_current_state == applicationStateAdd);

      QAbstractItemModel* bld = _m_tree_view_as_table->model();
      auto root=bld->match(QModelIndex(), Qt::DisplayRole, current_index.data(roleFilePath));
      if(!root.empty()) {
        bld->setData(root.front(), Qt::Checked, Qt::CheckStateRole);
      }
      if(is_dir)
      {
	_m_tree_view_as_tree->setRootIndex(root.front());
	// wait for user interaction
      }
      else {
        // do not query for more files if user selects a specific file;
        if(!(root.front().flags() & Qt::ItemIsEnabled)) {
	  setError(tr("selected item cannot be added to the index"));
	  showError();
	  throw 1;
        }
	m_app.accept();
      }
    }
    STOP_EVAL
  }
  void qigMainWindow::onTreeViewActioncommitTriggered()
  {
    m_app.setApplicationState(applicationStateCommit);
  }

  void qigMainWindow::onTreeViewAccept() {
    m_app.setApplicationState(applicationStateDefault, stateTransitionAccept);
  }
  void qigMainWindow::onTreeViewAbort() {
    m_app.setApplicationState(applicationStateDefault, stateTransitionAbort);
  }

  void qigMainWindow::onOpenRequest()
  {
    START_EVAL
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), ".",
	      QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(dir.isEmpty())
    {
      return;
    }
    m_app.load(dir);
    STOP_EVAL
  }

  void qigMainWindow::onApplicationStateChange(int state)
  {
    if(m_current_state == state)
    {
      return;
    }
    switch(state)
    {
      case applicationStateAdd:
	setupUiAdd();
	break;
      case applicationStateCommit:
	setupUiCommit();
	break;
      case applicationStateConflict:
      case applicationStateDefault:
	setupUiDefault();
	break;
    }
    m_current_state = state;
    return;
  }

#define asString(s) #s
  void qigMainWindow::onInconvienceDetected(int err, const QString&str)
  {
    switch(err)
    {
      case GIT_OK:
	qDebug()<<asString(GIT_OK)<<str;	    
	break;
      case GIT_ERROR:
	qDebug()<<asString(GIT_ERROR)<<str;
	break;
      case GIT_ENOTFOUND:
	qDebug()<<asString(GIT_ENOTFOUND)<<str;
	break;
      case GIT_EEXISTS:
	qDebug()<<asString(GIT_EEXISTS)<<str;
	break;
      case GIT_EAMBIGUOUS:
	qDebug()<<asString(GIT_EAMBIGUOUS)<<str;
	break;
      case GIT_EBUFS:
	qDebug()<<asString(GIT_EBUFS)<<str;
	break;
      case GIT_EUSER:
	qDebug()<<asString(GIT_EUSER)<<str;
	break;
      case GIT_EBAREREPO:
	qDebug()<<asString(GIT_EBAREREPO)<<str;
	break;
      case GIT_EUNBORNBRANCH:
	qDebug()<<asString(GIT_EUNBORNBRANCH)<<str;
	break;
      case GIT_EUNMERGED:
	qDebug()<<asString(GIT_EUNMERGED)<<str;
	break;
      case GIT_ENONFASTFORWARD:
	qDebug()<<asString(GIT_ENONFASTFORWARD)<<str;
	break;
      case GIT_EINVALIDSPEC:
	qDebug()<<asString(GIT_EINVALIDSPEC)<<str;
	break;
      case GIT_ECONFLICT:
	qDebug()<<asString(GIT_ECONFLICT)<<str;
	break;
      case GIT_ELOCKED:
	qDebug()<<asString(GIT_ELOCKED)<<str;
	break;
      default:
	qDebug()<<err<<str;
    }
  }
#undef asString

  //}}}

  //{{{ protected;
  void qigMainWindow::keyPressEvent(QKeyEvent *p)
  {
    if(p->key() == Qt::Key_Colon)
    {
      onShowCommandLine();
      return;
    }
    if(!m_keyEventHandler->keyPressEvent(p))
      return QMainWindow::keyPressEvent(p);
  }

  //}}}
  
  //{{{ private;
  void qigMainWindow::setupActionsForTreeView()
  {
    QStringList tree_view_actions;
    tree_view_actions << tr("commit") << tr("checkout") << tr("add") << tr("patch") << tr("branch");
    for( const QString& name: tree_view_actions) {
      QAction *act = new QAction(name, _m_tree_view_as_table);
      act->setObjectName(name);
      m_actions_for_tree_view.push_back(act);
      // TODO use meta object?
      QByteArray ar = QString("1onTreeViewAction%1Triggered()").arg(name).toLatin1();
      if(! connect( act, SIGNAL(triggered(bool)), this, QMetaObject::normalizedSignature(ar.constData())))
      {
	assert(false);
      }
      _m_tree_view_as_table->addAction(act);
      _m_tree_view_as_table->setContextMenuPolicy(Qt::ActionsContextMenu);
    }
  }

  int qigMainWindow::diffBlob(const QModelIndex& current_selection)
  {
    if(shallBeDiffed(current_selection))
    {
      diffOptions opts;
      opts.target=objectTypeBlob;
      for( const auto& source:
	  { std::make_pair(diffOptions::newSide, _m_tree_source_selector),
	  std::make_pair(diffOptions::oldSide, _m_diff_old_tree_selector) }
	  )
      {
	const QVariant& diff_side_type = source.second->currentData(roleTreeSource);
	bool ok;
	int  type = diff_side_type.toInt(&ok);
	if(ok)
	{
	  switch(type)
	  {
	    case treeSourceIndex:
	    case treeSourceWD:
	      opts.tree_source[source.first] = type;
	      opts.name = current_selection.data(roleFilePath).toString().toStdString();
	      break;
	    case treeSourceOid:
	    case treeSourceRef:
	      opts.tree_source[source.first] = treeSourceOid;
	      opts.oid[source.first] = source.second->currentData(roleOid).value<QOid>();
	      opts.object_type[source.first] = objectTypeCommit;
	      opts.name = current_selection.data(roleFilePath).toString().toStdString();
	  }
	}
      }
      int err;
      if((err=m_app.diff(std::move(opts))) != GIT_OK)
      {
	qDebug() << "diff failed with: "<<err;
      }
      return err;
    }
    return GIT_OK;
  }

  void qigMainWindow::diffTree(qigOid&& new_tree, qigOid&& old_tree)
  {
    diffOptions opts;
    opts.target = objectTypeTree;
    opts.oid[diffOptions::oldSide] = std::move(old_tree);
    opts.oid[diffOptions::newSide] = std::move(new_tree);
    for(int i = diffOptions::oldSide; i <= diffOptions::newSide; ++i)
    {
      opts.object_type[i] = objectTypeTree;
      opts.tree_source[i] = treeSourceOid;
    }
    m_app.diff(std::move(opts));
  }

#define EXPECTED 8
  void qigMainWindow::setupUiDefault()
  {
    QWidget* visible[] = {
      _m_tree_view_as_list,
      _m_tree_view_as_table,
      _m_diff_view
    };
    QWidget * hidden[] = {
      _m_tree_view_as_tree,
      _m_button_ok,
      _m_button_commit,
      _m_button_cancel,
      _m_editor_view
    };
    for( auto widget : visible) {
      widget->show();
    }
    for( auto widget : hidden) {
      widget->hide();
    }
    static_assert(sizeof(visible) + sizeof(hidden) == EXPECTED * sizeof(QWidget*), "unexpected number of widgets");
  }
  
  void qigMainWindow::setupUiAdd()
  {
    QWidget* visible[] = {
      _m_tree_view_as_list,
      _m_tree_view_as_tree,
      _m_button_ok,
      _m_button_commit,
      _m_button_cancel,
      _m_diff_view
    };
    QWidget * hidden[] = {
      _m_tree_view_as_table,
      _m_editor_view
    };
    for( auto widget : visible) {
      widget->show();
    }
    for( auto widget : hidden) {
      widget->hide();
    }
    static_assert(sizeof(visible) + sizeof(hidden) == EXPECTED * sizeof(QWidget*), "unexpected number of widgets");
  }
  
  void qigMainWindow::setupUiCommit()
  {
    QWidget* visible[] = {
      _m_tree_view_as_list,
      _m_tree_view_as_tree,
      _m_button_commit,
      _m_button_cancel,
      _m_diff_view,
      _m_editor_view
    };
    QWidget * hidden[] = {
      _m_button_ok,
      _m_tree_view_as_table,
    };
    for( auto widget : visible) {
      widget->show();
    }
    for( auto widget : hidden) {
      widget->hide();
    }
    static_assert(sizeof(visible) + sizeof(hidden) == EXPECTED * sizeof(QWidget*), "unexpected number of widgets");
  }
#undef EXPECTED
  void qigMainWindow::showError()
  {
    this->statusbar->showMessage(getError());
  }
  //}}}

  //{{{ static function
  qigFilter* qigMainWindow::createFilterList(const QString& filterstring, const logFilter::fields fields)
  {
    using namespace logFilter;
    switch(fields)
    {
      case fieldCommitHeader:
      case fieldCommitMessage:
      case fieldCommiter:
	return new qigStringFilter(filterstring, fields);
	break;
      default:
	break;
    }
    return nullptr;
  }


  bool shallBeDiffed(const QModelIndex& index)
  {
    bool ret = true;
    // do not diff if state is unchanged
    if(ret)
    {
      const QVariant& val = index.data(modelRoles::roleFileState);
      unsigned int state = val.toUInt(&ret);
      if(ret) ret = (state != fileStateUnchanged);
    }

    // do not diff directories
    if(ret)
    {
      const QVariant& val = index.data(modelRoles::roleIsDir);
      unsigned int isDir = val.toUInt(&ret);
      if(ret) ret = !isDir;
    }
    return ret;
  }
  static bool addOidToQComboBox(QComboBox * target, const QVariant& oid_var)
  {
    if(oid_var.isValid() && oid_var.canConvert<QOid>()) {
      int index;
      if( (index = target->findData(oid_var, roleOid)) != -1) {
	target->setCurrentIndex(index);
	return true;
      }
      else {
	QAbstractItemModel*model = target->model();
	const QModelIndex root;
	if(model->insertRows(model->rowCount(), 1, root) )
	{
	  QModelIndex model_index = model->index(model->rowCount()-1, 0, root);
	  if(model->setData(model_index, oid_var, roleOid))
	  {
	    if( (index = target->findData(oid_var, roleOid)) != -1) {
	      target->setCurrentIndex(index);
	      return true;
	    }
	  }
	}
      }
    }
    return false;
  }
  //}}}
}
