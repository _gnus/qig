// vim: fdm=marker ft=cpp.doxygen
#include <cassert>
#include "OidListModel.h"

#include "enums.h"
#include "Mime.h"
#include "QOid.h"
#include "ReferenceManager.h"

#include <QDebug>


namespace qig
{
  
  /********************************************************************
   * Declaration of descriptors
   * *****************************************************************/
  //{{{
  class qigOidListModel::oidDescriptor: public qigOidListModel::entryDescriptor
  {
    qigOid * m_oid;
    public:
      oidDescriptor(qigOid* o):entryDescriptor(treeSourceOid), m_oid(o){}
      QString representation() const {return m_oid->asString();}
      qigOid oid() const {return *m_oid;}
      inline bool operator==(const entryDescriptor &o) const;
      inline bool operator==(const qigOid &o) const;
  };
  class qigOidListModel::refDescriptor: public qigOidListModel::entryDescriptor
  {
    qigReference * m_ref;
    public:
      refDescriptor(qigReference* o):entryDescriptor(treeSourceRef), m_ref(o){}
      QString representation() const {return m_ref->name();}
      inline qigOid oid () const ;
      inline bool operator==(const entryDescriptor &o) const;
  };
  class qigOidListModel::nullDescriptor: public qigOidListModel::entryDescriptor
  {
    public:
      nullDescriptor(): entryDescriptor(-1){}
      nullDescriptor(int type): entryDescriptor(type){}
      QString representation() const {return QString::fromLatin1("<null>");}
      qigOid oid() const { return qigOid();}
      bool operator==(const entryDescriptor&o) const {return this->type == o.type;}
      inline bool operator==(const qigOid &) const {return false;}
  };
  class qigOidListModel::indexDescriptor: public qigOidListModel::nullDescriptor
  {
    QT_TR_FUNCTIONS
    public:
      indexDescriptor():nullDescriptor(treeSourceIndex){}
      QString representation() const {return tr("Index");}
  };
  class qigOidListModel::WDDescriptor: public qigOidListModel::nullDescriptor
  {
    QT_TR_FUNCTIONS
    public:
      WDDescriptor():nullDescriptor(treeSourceWD){}
      QString representation() const {return tr("working directory");}
  };
  //}}}

  qigOidListModel::qigOidListModel(QObject* parent):
    QAbstractListModel(parent),
    m_has_index_desciptor(false),
    m_has_wd_desciptor(false)
  {
  }

  /* ******************************************************************************************************************************
   * public {{{
   * *****************************************************************************************************************************/

  int qigOidListModel::actualiseReferences(const qigReferenceManager& reference_provider)
  {
    qigReferencesList list;
    int err = reference_provider.references(list, branchTypeAll);
    if(err == GIT_OK)
    {
      for(auto iter=list.begin(); iter != list.end() ;++iter)
      {
	qigReferencePtr swapped;
	swapped.swap(*iter);
	appendReference(std::move(swapped));
      }
      // head is not part of list;
      qigReferencePtr head(new qigReference());
      err = reference_provider.head(*head);
      if( err == GIT_OK)
      {
	appendReference(std::move(head));
      }
    }
    return err;
  }

  void qigOidListModel::setOfferIndex(bool val)
  {
    if(m_has_index_desciptor == val) {
      return;
    }
    if(val)
    {
      addDescriptor(entryDescriptorPtr (new indexDescriptor()));
      m_has_index_desciptor=true;
    }
    else 
    {
      assert(false);
    }
  }

  void qigOidListModel::setOfferWD(bool val)
  {
    if(m_has_wd_desciptor == val) {
      return;
    }
    if(val)
    {
      addDescriptor(entryDescriptorPtr (new WDDescriptor()));
      m_has_wd_desciptor=true;
    }
    else 
    {
      assert(false);
    }
  }

  void qigOidListModel::resetInternalData()
  {
    beginResetModel();
    m_entries.clear();
    endResetModel();
    m_oids.clear();
    m_refs.clear();

    if(m_has_wd_desciptor)
    {
      // set flag to false other setOffer would return;
      m_has_wd_desciptor=false;
      setOfferWD(true);
    }
    if(m_has_index_desciptor)
    {
      // set flag to false other setOffer would return;
      m_has_index_desciptor=false;
      setOfferIndex(true);
    }
  }


  /* ******************************************************************************************************************************
   * impl of api of QAbstractListModel{{{
   * *****************************************************************************************************************************/
  QStringList qigOidListModel::mimeTypes() const
  {
    return QStringList(mimeOid());
  }

  bool qigOidListModel::canDropMimeData( const QMimeData* mime, Qt::DropAction action, int row, int column, const QModelIndex&) const 
  {
    qDebug() << mime << action << row << column;
    return false;
  }

  bool qigOidListModel::dropMimeData( const QMimeData* mime, Qt::DropAction action, int row, int column, const QModelIndex&)
  {
    qDebug() << mime << action << row << column;
    return false;
  }

  Qt::DropActions qigOidListModel::supportedDropActions() const
  {
    return Qt::CopyAction | Qt::LinkAction;
  }

  QModelIndex qigOidListModel::index(int row, int col, const QModelIndex& parent) const
  {
    if(parent.isValid() || col != 0 || (row <0 || row > (int) m_entries.size() ))
    {
      return QModelIndex();
    }
    return createIndex(row, col, nullptr);
  }

  QModelIndex qigOidListModel::parent(const QModelIndex& ) const
  {
    return QModelIndex();
  }

  int qigOidListModel::rowCount(const QModelIndex &parent) const
  {
    if(parent.isValid())
    {
      return 0;
    }
    return m_entries.size();
  }

  QVariant qigOidListModel::data(const QModelIndex& index, int role) const
  {
    if(index.isValid())
    {
      switch(role)
      {
	case Qt::DisplayRole:
	  return (*toEntry(index, m_entries))->representation();
	case qig::roleOid:
	  return QVariant::fromValue<QOid>((*toEntry(index, m_entries))->oid());
	case qig::roleTreeSource:
	  return QVariant( (int) (*toEntry(index, m_entries))->type);
      }
    }
    return QVariant();
  }

  Qt::ItemFlags qigOidListModel::flags(const QModelIndex& ) const
  {
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemNeverHasChildren;
  }


  QModelIndexList qigOidListModel::match(const QModelIndex& start, int role, const QVariant& value,
	  int hits, Qt::MatchFlags) const
  {
    //qDebug() << Q_FUNC_INFO << start << role << value;
    QModelIndexList ret;
    if(start.isValid() && value.isValid())
    {
      switch(role)
      {
	case qig::roleOid:
	  find(value.value<QOid>(), hits, ret);
	  break;
	default:
	  break;
      }
    }
    return ret;
  }

  bool qigOidListModel::insertRows(int row, int count, const QModelIndex& parent)
  {
    // we calculate the pos based on algortithm;
    Q_UNUSED(row);
    Q_UNUSED(count);
    Q_UNUSED(parent);
    //qDebug() << __func__ << rows << count << parent;
    if(count <1 || parent.isValid()) {
      return false;
    }
    ++count;
    while(--count)
    {
      addDescriptor(entryDescriptorPtr(new nullDescriptor()));
    }
    return true;
  }
  bool qigOidListModel::setData(const QModelIndex& index, const QVariant& data, int role)
  {
    //qDebug() << __func__ << index << data << role;
    if(index.isValid())
    {
      if((uint32_t) index.row() < m_entries.size() && role == roleOid && data.canConvert<qigOid>())
      {
	m_oids.push_back(data.value<QOid>());
	int row = index.row();
	auto iter = m_entries.begin();
	std::advance(iter, row);
	*iter = entryDescriptorPtr(new oidDescriptor(&m_oids.back()));
	dataChanged(index, index, QVector<int>(Qt::DisplayRole,1));
	return true;
      }
    }
    return false;
  }
  //}}}

  //}}}

  void qigOidListModel::appendReference(qigReferencePtr &&p)
  {
      const QByteArray& name = p->name();
      auto iter = m_refs.find(name);
      if(iter == m_refs.end())
      {
	qigReference* ref = p.get();
	entryDescriptorPtr entry(new refDescriptor(ref));
	addDescriptor(std::move(entry));
	m_refs.insert(refs::value_type(name, std::move(p)));
      }
  }

  void qigOidListModel::addDescriptor(entryDescriptorPtr&& p)
  {
    unsigned int pos = calculateOffset(p->type);
    addDescriptor(std::move(p),pos);
  }
  void qigOidListModel::addDescriptor(entryDescriptorPtr&& p, unsigned int pos)
  {
    auto iter = m_entries.begin();
    std::advance(iter, pos);

    beginInsertRows(QModelIndex(), pos,pos);
    m_entries.insert(iter, std::move(p));
    endInsertRows();
  }

  unsigned int qigOidListModel::calculateOffset(int type)
  {
    unsigned int ret = 0;
    // show entries by category:
    // first wd
    // then index
    // then refs
    // then oids;

    ret+= m_has_wd_desciptor;
    if(type == treeSourceWD)
    {
      assert(ret==0);
      goto finished;
    }
    ret+= m_has_index_desciptor;
    if(type==treeSourceIndex)
    {
      assert(ret<=1);
      goto finished;
    }
    ret+= m_refs.size();
    if(type==treeSourceRef)
    {
      goto finished;
    }
    ret+=m_oids.size();
finished:
    return ret;
  }

  void qigOidListModel::find(const qigOid& oid, int _hits, QModelIndexList&out) const
  {
    // intertpret _hits as unsigend int;
    unsigned int &hits = *(reinterpret_cast<unsigned int*>(&_hits));
    bool cont=true;
    while(hits > 0 && cont)
    {
      auto iter = std::find_first_of(
	  m_entries.begin(), m_entries.end(),
	  &oid, (&oid)+1,
	  [](const entryDescriptorPtr& e, const qigOid&o){ return *e==o;}
	  );
      cont =   iter != m_entries.end();
      if(cont)
      {
	//qDebug(QString::number(std::distance(m_entries.begin(), iter)).toLatin1().constData());
	out.push_back(createIndex(std::distance(m_entries.begin(), iter), 0, nullptr));
	--hits;
      }
    }
  }

  qigOidListModel::entries::const_iterator qigOidListModel::toEntry(const QModelIndex& index, const qigOidListModel::entries& m_entries)
  {
    auto iter = m_entries.begin();
    std::advance(iter, index.row());
    return iter;
  }


  /* ******************************************************************************************************************************
   * impl of helper class oidDescriptor
   * ******************************************************************************************************************************/
  //{{{


  qigOid qigOidListModel::refDescriptor::oid() const
  {
    qigOid oid;
    m_ref->target(oid);
    return oid;
  }

  inline bool qigOidListModel::oidDescriptor::operator==(const entryDescriptor& o) const { 
    return this->type == o.type && *this->m_oid == *static_cast<const oidDescriptor&>(o).m_oid;
  }
  inline bool qigOidListModel::oidDescriptor::operator==(const qigOid& o) const { 
    return *this->m_oid == o;
  }

  inline bool qigOidListModel::refDescriptor::operator==(const entryDescriptor& o) const { 
    return this->type == o.type && *this->m_ref == *static_cast<const refDescriptor&>(o).m_ref;
  }
  //}}}
}

