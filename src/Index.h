#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */

#include "gitTypes.h"
#include <memory>
#include <Base.h>
#include <QIdentityProxyModel>
#include <QStringListModel>


namespace qig
{
  class qigOid;
  class qigRepository;
  class qigWorkDirModel;

  /** @class qigIndexHolder
   * something like unique_ptr for git_index
   */
  class qigIndexHolder
  {
    public:
      qigIndexHolder();
      qigIndexHolder(git_index*);
      qigIndexHolder(const qigIndexHolder&) = delete;
      qigIndexHolder(qigIndexHolder &&);
      ~qigIndexHolder();

      /// set index that should be stored. 
      /// this function function can be called only once
      void setIndex(git_index *p){ if (m_index == nullptr) m_index = p;}

      git_index * index(){return m_index;}

    private:
      git_index* m_index;
  };

  /** @class qigIndex
   * @brief wraps libgit2 git_index and its entries
   */
  class qigIndex: private qigBase
  {
    typedef std::unique_ptr<git_index, void(*)(git_index*)> qigIndexPtr;
    public:
      qigIndex(const qigBase &base);

      /** query the oid by path
       * @param out the oid that belongs to item at path;
       * @param path the relative path of the quried item
       * @return error code;
       */
      int entryOidByPath(qigOid& out, const char* path) const;


      /**@brief adds a file to the index of the repo
       *
       * if path contains conflict the conflict will be marked as resolved.
       * @param path a readable file that should be added.
       * @return errorCode
       */
      int addEntryByPath(const char* path);

      /**@brief adds a file to the index of the repo
       *
       * if path contains conflict the conflict will be marked as resolved.
       * @param path a file path that should be added.
       * @param buffer the content that should be added.
       * @param buffer_len the length of the buffer.
       */
      int addEntryFromBuffer(const unsigned char* buffer, unsigned int buffer_len, const char* path);

      /** removes an entry by its path
       */
      int removeEntryByPath(const char*path);


      /** writes current content to the disk
       */
      int writeToDisk()const;
      /** read index from disk;
       */
      int read(bool force);

      /**
       * compares 2 indexs and return true if both holds the same content
       */
      bool operator==(const qigIndex& o) const;

      bool compareWithDisk()const;

    private:
      qigIndexPtr m_index_ptr;
  };

  /** helper class to create an index
   */

  class qigIndexBuilder : public QIdentityProxyModel
  {
    Q_OBJECT
    public:
      qigIndexBuilder(QObject* parent=0);
      ~qigIndexBuilder();

      // override the setSourceModel with an extendend model;
      void setSourceModel(qigWorkDirModel*);
      // writes the checked entries to the disk
      bool sync(qigIndex& index);

      /**whether ignored values are checkable
       */
      void setAllowIgnored(bool);

      // life time o returned object is bound to this life time
      QAbstractItemModel* asListModel();

      //{{{ override the QAbstractItemModel

      virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
      virtual Qt::ItemFlags flags(const QModelIndex& index) const override;
      virtual bool setData(const QModelIndex& index, const QVariant& data, int role=Qt::EditRole) override;
      // behaves like QFileSystemModel::index(QString);
      virtual QModelIndexList match(const QModelIndex& index, int role, const QVariant& data, int hits=1, Qt::MatchFlags = Qt::MatchStartsWith|Qt::MatchWrap) const override;

   public Q_SLOTS:
     void resetInternalData();
      //}}}
    private:
      virtual void setSourceModel(QAbstractItemModel*) override {throw -1;} ;

      // returns all relative path that are checked and enabled;
      QStringList toList() const;

      qigWorkDirModel* m_source;
  };

  /**
   * a proxy model for the qigWorkDirModel;
   * it operates directly on the qigWorkDirModel entries;
   * the model shows checked and dirty entries only;
   */
  class qigIndexBuilderListModel: public QStringListModel
  {
    Q_OBJECT
      typedef QSet<QString> stringsEntries;
    public:
      qigIndexBuilderListModel(QObject* parent=0);
      void setSourceModel(qigWorkDirModel* source);
      void reset(){ setStringList( QStringList() );}
    private Q_SLOTS:
      void sourceModelDataChanged(const QModelIndex&, const QModelIndex&, const QVector<int>& );
    private:
      void insertString(const QString&);
      void removeString(const QString&);
      void init();

      qigWorkDirModel* m_source;
      //stringsEntries m_entries;
  };
}
  

