// vim: fdm=marker

#include <assert.h>
#include <Qt> //<qnamespace.h
#include <QAbstractItemModel>
#include <QTextBlock>
#include <QDebug>
#include <QTextEdit>
#include <QPlainTextDocumentLayout> //< pulls dependency to QtWidget

#include "DiffViewerBuiltinTextDocument.h"


// we use 6 bits for folding level.
#define INDENT_NUMBER_BITMASK ( (1<<6) -1 )

namespace qig
{
  static inline unsigned int previousFoldLevel(const QTextBlock& b);
  static inline unsigned int toFold(const QTextBlock& block);
  static inline void toState(QTextBlock& block, int folding);

#if 0
  file{{{

    hunk{{{
      -line
      -line
      }}}

    hunk{{{
      -line
      -line
      }}}
  }}}
#endif
  

  //{{{ helper class to store data
 
  struct userData: public QTextBlockUserData
  {
    userData():  state(Qt::Unchecked), fold_level(0), source(0){}
    Qt::CheckState state; //<Qt::Checked if block will be added to index;
    uint16_t fold_level;
    uint8_t  source; //< 0 old 1 new side;
  };
  //}}}

  //{{{ ctor dtor
  qigDiffViewerBuiltinTextDocument::qigDiffViewerBuiltinTextDocument(QObject*parent):
    QTextDocument(parent),
    m_model(nullptr),
    m_cursor(this)
  {
    m_char_format.setFont(QFont("mono"),QTextCharFormat::FontPropertiesSpecifiedOnly);
    setDocumentLayout(new QPlainTextDocumentLayout(this));
  }

  qigDiffViewerBuiltinTextDocument::~qigDiffViewerBuiltinTextDocument() { }

  //}}}

  //{{{ public methods

  void qigDiffViewerBuiltinTextDocument::finalise(IDiffBase *i)
  {
    DiffViewerBuiltinModelItem* item = dynamic_cast<DiffFileModelItem*>(i);
    if(item)
    {
      appendTreeItemRecursively(item);
    }
  }

  /** TODO this is a mess
   */
  int qigDiffViewerBuiltinTextDocument::fold( QTextCursor& cursor)
  {
    QTextBlock block = cursor.block();
    // used to trak the folded (dirty) region
    int start_position = 0, pos_cnt = 0;
    // count the folded blocks
    int block_cnt = 0;
    unsigned int current_folding = toFold(block);
    if(current_folding)
    {
      //qDebug() << "qigDiffViewerBuiltinTextDocument::fold: found block with folding_level: " << current_folding;
      // search for the first with a lower fold level. but also stop at the first block
      while(toFold(block) >= current_folding && block != begin())
      {
	block = block.previous();
      }
      cursor.beginEditBlock();
      cursor.setPosition(block.position());
      cursor.movePosition(QTextCursor::EndOfBlock);
      // we went to far by one block. 
      block = block.next();


      start_position = block.position();
      // hide the all blocks till we reach the a block with lesser fold level
      while( block.isValid() && toFold(block) >= current_folding)
      {
	pos_cnt+=block.length();
	++block_cnt;
	block.setVisible(false);
	//qDebug() << "qigDiffViewerBuiltinTextDocument::fold: hiding block: " << block.text() << " with level"<<toFold(block);
	block=block.next();
      }
      markContentsDirty(start_position, pos_cnt);

      // insert notifier for that some lines were folded
      cursor.insertBlock();
      cursor.insertText(QString(" +--- %1 lines: %2").arg(block_cnt).arg("TODO"));
	// notifier should be foldable
      block=cursor.block();
      toState(block, current_folding);

      cursor.endEditBlock();

      emit cursorPositionChanged(cursor);
    }
    return block_cnt;
  }

  void qigDiffViewerBuiltinTextDocument::reset()
  {
      QTextDocument::clear();
      qigDiffViewerImpl::reset();
      //qDebug()<<"qigDiffViewerBuiltinTextDocument::reset: blockCount after reset:"<<blockCount();
  }

  void qigDiffViewerBuiltinTextDocument::setModel(QAbstractItemModel*model)
  {
    if(m_model)
    {
      disconnect(m_model, 0, this,0);
    }
    m_model=model;
    connect(model, &QAbstractItemModel::rowsInserted, this, &qigDiffViewerBuiltinTextDocument::rowsInserted);
    connect(model, &QAbstractItemModel::modelReset, this, &qigDiffViewerBuiltinTextDocument::modelReset);
  }
  //}}}

  //{{{ public slots
  void qigDiffViewerBuiltinTextDocument::rowsInserted(const QModelIndex&parent, int first, int last)
  {
    for(int i = first; i<=last; ++i)
    {
      const QModelIndex& index = m_model->index(i, 0, parent);
      appendText(index);
    }
  }

  void qigDiffViewerBuiltinTextDocument::modelReset()
  {
    clear();
  }
  //}}}

  //{{{ private methods
  
  inline void qigDiffViewerBuiltinTextDocument::appendText(const QModelIndex&index)
  {
    const QString& text = index.data().toString();
    bool hasChildren = m_model->hasChildren(index);
    appendText(text, hasChildren);
  }

  void qigDiffViewerBuiltinTextDocument::appendText(const QString& text, unsigned int folding_level)
  {
    // always append by default

    const QTextBlock& begin = m_cursor.block();
    assert(begin.isValid());
    assert(begin.isVisible());
    m_cursor.insertText(text, m_char_format);
    m_cursor.insertBlock();

    // prepare folding
    const QTextBlock& last = m_cursor.block();
    QTextBlock current = begin;
    
    //qDebug() << "qigDiffViewerBuiltinTextDocument::appendText: " << begin.text();
    for(current = begin;  current != last && current.isValid(); current = current.next())
    {
      toState(current, folding_level); // we set the folding level as state
    }
  }

  void qigDiffViewerBuiltinTextDocument::appendTreeItemRecursively(DiffViewerBuiltinModelItem*item)
  {
    _appendTreeItem(item);
    for(unsigned int i=0; i< item->childrenCount(); ++i)
    {
      appendTreeItemRecursively(item->childByIndex(i));
    }
  }
  
  inline void qigDiffViewerBuiltinTextDocument::_appendTreeItem(DiffViewerBuiltinModelItem*item)
  {
    const QString& text=item->dataDisplay(); 
    int folding_level = item->height();
    m_char_format.setForeground(QBrush(item->dataForeground()));
    appendText(text, folding_level);
  }
  //}}}

  //{{{ static helper

  static inline unsigned int previousFoldLevel(const QTextBlock& b)
  {
    if(b.isValid())
    {
      return toFold(b.previous());
    }
    return 0;
  }
  static inline unsigned int toFold(const QTextBlock& b)
  {
    return (b.userState() + 1) & INDENT_NUMBER_BITMASK;
  }
  static inline void toState(QTextBlock& b, int folding)
  {
    assert(folding < INDENT_NUMBER_BITMASK);
    b.setUserState((folding-1) & INDENT_NUMBER_BITMASK);
  }
  //}}}

}
