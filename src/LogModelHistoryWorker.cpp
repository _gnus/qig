// vim: fdm=marker
#include <assert.h>

#include "LogModelHistoryWorker.h"
#include "Oid.h"
#include "RevWalker.h"
#include <QDebug>

namespace qig
{
  /** @internal
   * @struct pushedCommitProperties
   * @brief global properties of the currently pushed oid
   */
  struct pushedCommitProperties
  {
    pushedCommitProperties(){ memset(this,0,sizeof(pushedCommitProperties));}
    pushedCommitProperties(const pushedCommitProperties&) = delete;
    /// iter to first occurrence
    unsigned int first_occurrence;
    /// number of parents of this commit. if parents_count>1 this is a merge_commit
    unsigned int parents_count;
    /// flag that indicates if pushed commit is a fork
    bool is_fork;
    bool isMerge() const {return parents_count>1;}
  };

  //{{{ ctor, dtor
  qigLogModelHistoryWorker::qigLogModelHistoryWorker(const qigBase*base):
    m_objects_cnt(0),
    m_max_history(UINT_MAX),
    m_iteration_over_flag(ATOMIC_FLAG_INIT),
    m_abort(ATOMIC_FLAG_INIT),
    m_base(base)
  {
  }
  qigLogModelHistoryWorker::~qigLogModelHistoryWorker()
  {
    abort();
  }
  //}}}

  //{{{ public methods
  void qigLogModelHistoryWorker::abort()
  {
    m_abort.clear(std::memory_order_release);
    if (m_thread.joinable())
      m_thread.join();
  }

  void qigLogModelHistoryWorker::start()
  { 
    //TODO move to thread
    prepareInternalState();
    run();
    //m_thread = std::thread(qigLogModelHistoryWorker::srun, this);
  }

  //}}}

  //{{{ private methods
  void qigLogModelHistoryWorker::prepareInternalState()
  {
    m_objects_cnt=0;
    m_abort.test_and_set(std::memory_order_relaxed);
    m_iteration_over_flag.test_and_set(std::memory_order_release);
    m_model_pos_map.clear();
    m_current_graph.clear();
  }

  void qigLogModelHistoryWorker::run()
  { 
    // do a sync
    m_iteration_over_flag.test_and_set(std::memory_order_acquire);
    qigRevWalker rev_walker(*m_base);
    int err = rev_walker.acquire(revwalk::revwalkSortTime);
    if (err != GIT_OK) {
        return;
    }
    while(!m_start_commitishes.empty())
    {
      rev_walker.pushStart(m_start_commitishes.front());
      // init all branches
      // fails if 
      value_type t={ qigLogModelData(m_start_commitishes.front()), _helper()};
      m_current_graph.push_back(std::move(t));
      m_start_commitishes.pop_front();
    }
    while(!m_hide_comitishes.empty())
    {
      rev_walker.pushHide(m_hide_comitishes.front());
      m_hide_comitishes.pop_front();
    }
    int cancel=0;
    qigOid out;
    while((err = rev_walker.next(out)) != GIT_ITEROVER && ! cancel)
    {
      if(err == GIT_OK) {
	cancel=this->addObject(out);
      }
    }
    if(err == GIT_ITEROVER)
    {
      err = GIT_OK;
      //err = 0;
    }
    iterationOver();
    if(!err)
    {
      /*
      //out << "CommitHistoryModelFiller::run:  iterationOver over "
//	  << m_model_pos_map.size() << "commits in " << m_max_branch_cnt <<"branches finished\n"
//	  << "the history is\n";
      for (auto iter = m_model_pos_map.begin(); iter != m_model_pos_map.end(); ++iter)
      {
	const int64_t& key = iter.key();
	const int32_t *const ci_cnt = (const int32_t*const) &key;
	const int32_t *const br_cnt = ci_cnt+1;
	out << "\t("<<*ci_cnt<<","<<*br_cnt<<")"<<iter.value().asString() <<"\n";
	out << "\t\tparents are:\n";
	auto dum = iter->getChildren();
	for(int i =0; i<dum.size(); ++i)
	{
	  const int64_t& key = dum[i];
	  const int32_t *const ci_cnt = (const int32_t*const) &key;
	  const int32_t *const br_cnt = ci_cnt+1;
	  const qigOid& oid = m_model_pos_map[key];
	  out << "\t\t("<<*ci_cnt<<","<<*br_cnt<<")"<<oid.asString() <<"\n";
	}
      }
      */
    }
    else {
      //out << "iteration failed\n";
    }
  }

  void qigLogModelHistoryWorker::ensureAllocation(int  required_size)
  {
    im_here_to_avoid_allocation_of_memory_to_often_and_to_avoid_static_variables.reserve(required_size);
    int remaining_commtis_to_allocate = required_size -  im_here_to_avoid_allocation_of_memory_to_often_and_to_avoid_static_variables.size();
    while(--remaining_commtis_to_allocate >= 0)
    {
      im_here_to_avoid_allocation_of_memory_to_often_and_to_avoid_static_variables.append(qigOid());
    }
  }



  int qigLogModelHistoryWorker::addObject(const qigOid& oid)
  {
    if(++m_objects_cnt >= m_max_history)
    {
      return GIT_ITEROVER;
    }
    qigCommit commit(oid);

    bool item_appended=false;

    struct pushedCommitProperties pushed_commit;
    /* current state (B pushed), before copy
     *
     * HEAD -- A -- - -- C
     *    \ -- - -- B
     * cur A, HEAD
     * parents: C, B
     */
    updateCurrentOid(pushed_commit, m_current_graph, commit);
    /* current state (B pushed) after copy, before addParents
     *
     * HEAD -- A -- - -- C
     *    \ -- - -- B -- - D
     * cur A, B
     * parents: C, -
     */
    addParents(pushed_commit, m_current_graph, commit);
    /* current state (B pushed) after addParents, before detectAndProcessMerge
     *
     * HEAD -- A -- - -- C
     *    \ -- - -- B -- - -- D
     * cur B, B
     * parents: C, D
     */

    if( (m_current_graph.end()-1)->helper.shift_width>0)
    {
      // warning this will push an invalid oid to the model.
      m_current_graph.push_back({qigLogModelData(), _helper()});
      item_appended=true;
      (m_current_graph.end()-1)->current.addRepresentationFlag(internal_visited); //<TODO this could be also a shiftOutStart
    }
    detectAndProcessForks(pushed_commit, m_current_graph);
    processUnrelatedConnections();
    if( item_appended)
    {
      if( (m_current_graph.end()-2)->current.hasRepresentationFlag(connection))
      {
	(m_current_graph.end()-1)->current.addRepresentationFlag(mergeEnd|internal_visited); //<TODO this could be also a shiftOutStart
      }
      else {
	(m_current_graph.end()-1)->current.addRepresentationFlag(shiftOutEnd | internal_visited); //<TODO this could be also a shiftOutStart
      }
    }
    storeResult();
    if(item_appended)
    {
      m_current_graph.pop_back();
    }
    consolidate(m_current_graph);
    // update representation except for forks
    //int err = updateLeaves(commit, p);
    // findForks()
    int ret = -31;
    // normally set but cleared if abort is called
    if (m_abort.test_and_set(std::memory_order_acquire)) {
      ret = 0;
    }
    return ret;
  }

  void qigLogModelHistoryWorker::processUnrelatedConnections()
  {
    for(auto iter = m_current_graph.begin(); iter!=m_current_graph.end(); ++iter) {
      if(!iter->current.hasRepresentationFlag(internal_visited))
	iter->current.addRepresentationFlag(connection | internal_visited);
    }
  }

  void qigLogModelHistoryWorker::storeResult()
  {
    int branch_cnt =0;
    for(auto iter = m_current_graph.begin(); iter !=m_current_graph.end(); ++iter, ++branch_cnt) {
      //if(!iter->current.hasRepresentationFlag(internal_visited)) {
      //  iter->current.addRepresentationFlag(connection | internal_visited);
      //}
      iter->current.finalise();
      m_model_pos_map.insert(branch_cnt, m_objects_cnt-1, iter->current);
    }
  }

  // {{{ static private methods

  /** search in current for duplicated entries 
   * set Fork  representation flag where appropriate 
   *
   * TODO processing each iter (~O(n**2)) is more readable than walking the list twice (~O(2n))
   */
  void qigLogModelHistoryWorker::detectAndProcessForks(const pushedCommitProperties& props, currentGraphList &m_current_graph)
  {
    auto iter_pushed = m_current_graph.begin()+props.first_occurrence;

    if( props.is_fork)
    {
      iter_pushed->current.addRepresentationFlag(forkStart);
    }
    if(props.isMerge())
    {
      iter_pushed->current.addRepresentationFlag(mergeStart);
    }

    auto iter = m_current_graph.end()-1;
    bool require_fork = false;
    bool require_merge = false; //< when set a merge flag is set
    bool require_shift = false;
    // when shift_width > 1 a shift and shiftOutEnd flag is set. when == 1 a shiftOutEnd flag is set
    int shift_width;
    for( ; iter != iter_pushed; --iter)
    {
      shift_width = iter->helper.shift_width ;//+ props.parents_count-1;

      if(require_fork) {
	iter->current.addRepresentationFlag(fork);
      }
      if(shift_width>=-1) {
	require_shift=false;
      }
      if(require_shift) {
	iter->current.addRepresentationFlag( shiftIn | internal_visited);
      }

      if(iter->helper.hasFlag(isDuplicate)) {
	iter->current.addRepresentationFlag(forkEnd | internal_visited);
	require_fork = true;
      }
      else
      {
	if(shift_width >0) {
	  iter->current.addRepresentationFlag( shiftOutStart | internal_visited);
	}
	else if(shift_width<0)
	{
	  iter->current.addRepresentationFlag( shiftInStart | internal_visited);
	  (iter+shift_width)->current.addRepresentationFlag(shiftInEnd|internal_visited);
	  if(shift_width<-1) {
	    require_shift=true;
	  }
	}
      }
    }

    if(props.isMerge())
    {
      //TODO ensure thate m_current_graph is large enough
      int offset = props.first_occurrence + props.parents_count;
      if(offset > m_current_graph.size() ){
	qDebug() <<"error";
	return;
      }
      iter = m_current_graph.begin() +1 + props.first_occurrence;
      auto iter_merge_end = m_current_graph.begin() + props.first_occurrence + props.parents_count;
      for (; iter != iter_merge_end; ++iter)
      {
	if(require_merge)
	{
	  iter->current.addRepresentationFlag( merge | internal_visited);
	}
	iter->current.addRepresentationFlag( mergeEnd | internal_visited);
	require_merge=true;
      }
      
      for( ;iter != m_current_graph.end(); ++iter)
      {
	shift_width = iter->helper.shift_width;// + props.parents_count-1;

	if(shift_width > 1) {
	  iter->current.addRepresentationFlag( shiftOut | shiftOutEnd | internal_visited); //<does not work
	}
	else if(shift_width == 1) {
	  iter->current.addRepresentationFlag( shiftOutEnd  | internal_visited);
	}
	else if(shift_width==0 && iter->helper.hasFlag(isDuplicate))
	{
	  iter->current.addRepresentationFlag( shiftOutEnd  | internal_visited);
	}
      }
    }
  }

  void qigLogModelHistoryWorker::updateCurrentOid(pushedCommitProperties& props, currentGraphList& m_current_graph, const qigCommit& pushed_commit)
  {
    /**
     * for all iter in list
     *   for all p in iter.parent[1+]
     *     inert p to list an remove p from iter parents
     * for all iter in list 
     *   if pushed_commit is in iter.parent
     *     if first occurrence of pushed_commit
     *       stor offset
     *
     *     copy iter.parent to iter
     */
    qigOid pushed_oid;
    pushed_commit.oid(pushed_oid);
    for(auto iter = m_current_graph.begin(); iter != m_current_graph.end(); ++iter)
    {
	if(iter->current.isMerge())
	{
	  auto parents = iter->current.m_parents;
	  iter->current.m_parents.clear();
	  iter->current.pushParent(parents[0]);
	  for(unsigned int i = 1;  i<parents.size(); ++i)
	  {
	    // TODO shuftwidecnt is specufuc to iter
	    iter = m_current_graph.insert(iter+1, {qigLogModelData(parents[i]), _helper()});
	  }
	}
    }
    unsigned int cnt=0;
    bool updated_first_occurrence=false;
    int shift_width_cnt=0;
    for(auto iter = m_current_graph.begin(); iter != m_current_graph.end(); ++iter, ++cnt)
    {
      iter->current.setRepresentation(0);
      if(iter->current.hasParent(pushed_commit))
      {
	// TODO itr+= posin parents
	if(!updated_first_occurrence)
	{
	  props.first_occurrence=cnt;
	  updated_first_occurrence=true;
	  iter->current.addRepresentationFlag(qig::qigLogModelHistoryWorker::commit); //< do not add the internal_visited flag. if it is a normal commit we'll add the connection flag at the end.
	  iter->current.updateFromParent();
	  shift_width_cnt=pushed_commit.parentCount()-1;
	}
	else
	{
	  props.is_fork=true;
	  --shift_width_cnt;
	  iter->helper.addFlag(isDuplicate);
	}
      }
      iter->helper.shift_width=shift_width_cnt;
    }
    if(!updated_first_occurrence)
    {
      // should not happen.
      // each pushed commit must have parent;
      qigOid oid;
      pushed_commit.oid(oid);
      qDebug() << Q_FUNC_INFO << "failed to find a parent for" << oid.asString(); //<< pushed_commit;
      //m_current_graph.push_back( { qigLogModelData(pushed_commit),0});
    }
  }


  // reimplemented from IDataFiller
  // assuming history:
  // HEAD -- A -- - -- C
  //           \- B 
  // and A is pushed. 
  //  when entering this function m_current_graph contain oid of A in current and ahead
  //
  int qigLogModelHistoryWorker::addParents(pushedCommitProperties& props, currentGraphList& m_current_graph, const qigCommit &pushed_commit) //{{{
  {
    // TODO reuse iter->current._m_parents[0]
    // with iter->current.m_parents.clear we delete the object and push a copy of parent_oid later to m_parents.
    qigOid parent_oid;

    unsigned int n_parents=pushed_commit.parentCount();

    props.parents_count = n_parents;

    // update parents. see comments for
    // HEAD -- A -- - -- C
    //           \- B 
    int err=0;
    auto iter = m_current_graph.begin() + props.first_occurrence;
    //if(n_parents>1) {
    //  iter->current.addRepresentationFlag(mergeStart);
    //}
    iter->current.m_parents.clear();
    for(unsigned int i =0; i<n_parents; ++i)// Traverse parents of A
    {
      // get parent by index i;
      err = pushed_commit.parent(parent_oid, i);
      if(!err)
      {
	iter->current.pushParent(parent_oid);
      }
      else
      {
	// err occured
	//throw 1;
      }
    }
    return err;
  } //}}}
      


  // history:
  // HEAD -- A -- - -- C -- D
  //           \- B -- - -/
  // and C is pushed. 
  //  when entering this function m_oid_leaves contain oid of C and D (because is is parent of B which is pushed before C (in time sort))
  //  at the end m_oid_leaves contains oid of D only
  //  D has a ref to B and C
  int qigLogModelHistoryWorker::consolidate(currentGraphList& m_current_graph) //{{{
  {
    // clean up merges see comments for
    // HEAD -- A -- - -- C -- D
    //           \- B -- - -/
    //
    // at the moment it is
    // HEAD -- A -- - -- C -- D
    //           \- B -- - -- D
    // we have to delete one entry D; or all D except of one in octupus merge;
    // and update the children of D so it knows of its children B,C

    for(auto iterOuter=m_current_graph.end() - 1; iterOuter!= m_current_graph.begin() ; --iterOuter)
    {
      if(iterOuter->helper.hasFlag(isDuplicate))
      {
	iterOuter=m_current_graph.erase(iterOuter);
      }
      //assert(iterOuter->current.m_parents.size()>0);
    }
    return 0;
  } //}}}

  //}}}
  //}}}

  void qigLogModelData::rawData(QByteArray& data_out) const
  {
    const char* data_in;
    _m_internal_data->rawData((const unsigned char**)&data_in);
    if(data_in)
    {
      data_out.resize(OID_RAWSZ);
      memcpy(data_out.data(), data_in, OID_RAWSZ);
    }
  }

  const qigLogModelData* qigLogModelDataMap::commitByIndex(int commit) const
  {
    positionMap::const_iterator iter;
    for(auto i = 0; i< m_branch_cnt; ++i)
    {
      if( (iter=find(commit, i))!= m_map.end())
      {
	if(iter->isCommit())
	{
	  return &iter.value();
	}
      }
    }
    return 0;
  }

}
