
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once

namespace qig
{
  class qigOid;
  class qigIPayload
  {
  };
  /** @ingroup Application
   *  @class qigIDataFiller
   *  interface used in tree traversel.
   *  the interface must be be reimplemented to recive git objects that are visited.
   *  when the iteration is over iterationOver is called which does nothing on default;
   */
  class qigIDataFiller
  {
    public:
      /**
       * must be implemented by user
       * called for each commit with id @a id that the rev walker visits.
       *
       * @param id the git oid for an object that should be added
       * @param p the object that was passed to qigHistory::history.
       * @return the implementation should return 0 on success or not 0 to abort the iteration
       */
      virtual int addObject(const qigOid &id, qigIPayload* p) =0;

      /**called when after the no more object are left to iterate over.
       * not called when the user request to abort the iteration. 
       * default implementation does nothing
       */
      virtual void iterationOver(){};
  };
}
