// vim: fdm=marker
#include <assert.h>
#include <unistd.h>
#include <QKeyEvent>
#include <QDebug>

#include "enums.h"

namespace qig
{

  template<typename QView>
  inline qigTreeView<QView>::qigTreeView(QWidget* parent):QView(parent)
  {
    this->setTabKeyNavigation(false);
  }

  // {{{ protected methods

  template<typename QView>
  inline void qigTreeView<QView>::keyPressEvent(QKeyEvent *p)
  {
    if(!p) {
      return;
    }
    QView::keyPressEvent(p);
  }
  template<typename QView>
  inline void qigTreeView<QView>::onActivated(const QModelIndex& index)
  {
    QVariant d(index.data(modelRoles::roleIsDir));
    if(!d.isValid()) {
      return;
    }
    bool ok;
    if(d.toInt(&ok) && ok)
    {
      onDictionaryActivated(index);
    }
    else
    {
      onAnyActivated(index);
    }
  }
  // }}}

  //{{{ private methods
  template <typename QView>
  inline void qigTreeView<QView>::onAnyActivated(const QModelIndex &index)
  {
    Q_UNUSED(index);
      if(fork() == 0) { 
	//if(dpy)
	//  close(ConnectionNumber(dpy));
	setsid();
	//execl("/usr/bin/xdg-open", "/usr/bin/xdg-open", filepath.toLatin1().constData(), nullptr);
	execl("/home/sung/dum.sh", "/home/sung/dum.sh",".", nullptr);
	exit(0);
      }    
  }

  template <typename QView>
  inline void qigTreeView<QView>::onDictionaryActivated(const QModelIndex& )
  {
  }
  //}}}
}
