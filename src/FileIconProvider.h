#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <QFileIconProvider>
#include <QIcon>
#include <QHash>

#include "enums.h"

namespace qig
{
  class qigFileIconProvider: private QFileIconProvider
  {
    public:
      enum IconType { Computer, Desktop, Trashcan, Network, Drive, Folder, File };
      struct key {
	key( qigFileIconProvider::IconType t, fileState s):type(t), state(s){}
	qigFileIconProvider::IconType type;
	fileState state;
	bool operator==(const key& o) const { return type==o.type && state == o.state;}
      };
      typedef QHash<key, QIcon> iconMap;
    public:
      qigFileIconProvider();
      // same as QFileIconProvider
      QIcon icon(IconType type, fileState) const;
      using QFileIconProvider::icon;
      iconMap m_iconMap;
    private:
  };
  inline uint qHash(const qigFileIconProvider::key &k, uint =0) { return qHashBits(&k, sizeof(qigFileIconProvider::key));}
}
