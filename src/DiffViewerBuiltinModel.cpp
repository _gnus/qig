// vim: fdm=marker
#include <cassert>
#include <QStringBuilder>
#include <QFont>
#include <QBrush>

#include "DiffViewerBuiltinModel.h"
#include "enums.h"

namespace qig
{

  //{{{ ctor,dtor
  qigDiffViewerBuiltinModel::qigDiffViewerBuiltinModel(QObject*parent):
    QAbstractItemModel(parent),
    m_font(new QFont("mono")),
    m_full_diff(false)
  {
    m_font->setStyleHint(QFont::TypeWriter);
  }

  qigDiffViewerBuiltinModel::~qigDiffViewerBuiltinModel(){}
  //}}}

  //{{{ public methods
  int qigDiffViewerBuiltinModel::addDelta(IDiffDelta **delta)
  {
    if(m_full_diff)
    {
      return qigDiffViewerImpl::addDelta(delta);
    }
    DiffDeltaModelItem* delta_impl = new DiffDeltaModelItem(&m_root);
    m_deltas.push_back( DiffDeltaModelItemPtr(delta_impl) );
    *delta = delta_impl;
    return delta_impl ? 1 : GIT_ERROR;
  }

  int qigDiffViewerBuiltinModel::accept( IDiffDelta* i)
  {
    DiffDeltaModelItem* item=dynamic_cast<DiffDeltaModelItem*>(i);
    if(item->status() == 0)
    {
      return 1;
    }
    //item->addToTreeStructure();
    //insertTree(item);
    return GIT_OK;
  }

  void qigDiffViewerBuiltinModel::finalise( IDiffBase* i)
  {
    DiffViewerBuiltinModelItem* item=dynamic_cast<DiffViewerBuiltinModelItem*>(i);
    if(item->type() == DiffFileModelItem::typeId())
    {
      insertTree(item);
    }
  }

  void qigDiffViewerBuiltinModel::reset()
  {
    beginResetModel();
    m_deltas.clear();
    qigDiffViewerImpl::reset();
    endResetModel();
  }
  //{{{ public methods overridden for QAbstractItemModel
  QVariant qigDiffViewerBuiltinModel::data(const QModelIndex &index, int role) const
  {
    if(index.isValid())
    {
      switch(role)
      {
	case Qt::DisplayRole:
	  return dataDisplay(index);
	  break;
	case Qt::FontRole:
	  return QVariant(dataFont());
	  break;
	case Qt::ForegroundRole:
	  return dataForeground(index);
	default:
	  break;
      }
    }
    return QVariant();
  }
  QModelIndex qigDiffViewerBuiltinModel::index(int row, int column, const QModelIndex &parent) const
  {
    const treeEntry *item = toTreeItem(parent);
    if((unsigned int) row < item->childrenCount() && column ==0)
    {
      return createIndex(row,column, const_cast<treeEntry*>(item->childByIndex(row)) );
    }
    return QModelIndex();
  }
  QModelIndex qigDiffViewerBuiltinModel::parent(const QModelIndex &child_index) const
  {
    const DiffViewerBuiltinModelItem *child_item = toTreeItem(child_index);
    const treeEntry *parent_item = child_item->parent();
    return toModelIndex(parent_item);
  }
  bool qigDiffViewerBuiltinModel::hasChildren(const QModelIndex &parent) const
  {
    const treeEntry*item = toTreeItem(parent);
    bool ret=item->hasChildren();
    if(parent.isValid())
    {
      const DiffViewerBuiltinModelItem* parent_item = toTreeItem(parent);
      ret = parent_item->childrenCount() > 0;
    }
    return ret;
  }
  int qigDiffViewerBuiltinModel::columnCount(const QModelIndex &parent) const
  {
    assert(!parent.isValid());
    return 1;
  }
  int qigDiffViewerBuiltinModel::rowCount(const QModelIndex &parent) const
  {
    const DiffViewerBuiltinModelItem* parent_item = toTreeItem(parent);
    return parent_item->childrenCount();
  }
  Qt::ItemFlags qigDiffViewerBuiltinModel::flags(const QModelIndex &index) const
  {
    assert(index.isValid());
    return \
        Qt::ItemIsSelectable |
        Qt::ItemIsEnabled ;
  }
  //}}}
  //}}}

  //{{{ private method
  void qigDiffViewerBuiltinModel::insertTree(DiffViewerBuiltinModelItem*item)
  {
    const DiffViewerBuiltinModelItem* parent_item = item->parent();
    int row = item->locate();
    const QModelIndex& parent=toModelIndex(parent_item);
    beginInsertRows(parent, row, row);

    insertChildrenRecursivly( item);
    endInsertRows();
  }

  void qigDiffViewerBuiltinModel::insertChildrenRecursivly(DiffViewerBuiltinModelItem*item)
  {
    unsigned int cnt= item->childrenCount();
    if(cnt)
    {
      const QModelIndex& parent = toModelIndex(item);
      beginInsertRows(parent, 0,cnt-1);
      for (unsigned int i = 0; i < cnt; ++i)
      {
	DiffViewerBuiltinModelItem* child = item->childByIndex(i);
	insertChildrenRecursivly(child);
      }
      endInsertRows();
    }
  }


  const QModelIndex qigDiffViewerBuiltinModel::toModelIndex(const treeEntry* item) const
  {
    if(item == &m_root)
    {
      return QModelIndex();
    }
    const int row = item->locate();
    const int column = 0;
    return createIndex( row, column, const_cast<treeEntry*>(item));
  }

  QVariant qigDiffViewerBuiltinModel::dataForeground(const QModelIndex& index) const
  {
    const DiffViewerBuiltinModelItem* p= toTreeItem(index);
    return QBrush(p->dataForeground());
  }
  //}}}

#if 0
  //{{{ helper classes
  QVariant DiffFileModelItem::dataDisplay() const
  {
    return QVariant(QString("file"));
  }
  QVariant DiffHunkModelItem::dataDisplay() const
  {
    return QVariant(header());
  }
  QVariant DiffLineModelItem::dataDisplay() const
  {
    return QString(QString(origin()) % QString(L' ') % content());
  }
  Qt::GlobalColor DiffLineModelItem::dataForeground()const
  {
    Qt::GlobalColor colour;
    switch(origin())
    {
      case '+':
	colour = Qt::darkGreen;
	break;
      case '-':
	colour = Qt::red;
	break;
      default:
	colour = Qt::black;
	break;
    }
    return colour;
  }
  //{
  //  const DiffViewerBuiltinModelItem* p= toTreeItem(index);
  //  return QBrush(p->dataForeground());
  //}
  //}}} end helper classes
#endif
}
