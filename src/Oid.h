#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */

#include "gitTypes.h"
#include "GitOidHolder.h"

#define OID_RAWSZ 20

typedef struct git_oid git_oid;

namespace qig
{
  class qigBase;
  class qigGitObjectHolder;
  class qigString;
  class out;
  /**@class qigOid
   * represents a git oid (git sha1) and is the base class for all objects in a git repository
   * the oid uniquely identifies an object in an git repository
   *
   * This class provides an high level api but it would be nice if there would be another level of abstraction to hide the qigGitOidHolder.
   */
  class qigOid: private qigGitOidHolder
  {
    friend class qigRevWalker;
    friend class qigReference;
    friend class qigRepository;
    friend class qigGitObjectHolder;
    friend class qigIndex;
    friend class qigTree;
    public:
      qigOid();
      virtual ~qigOid(){}
      // also define copy and move ctor/ operator because we define the dtor virtual
      // dtor causes move operator to be deleted.
      // defining the move operator causes the copy operator to be deleted
      // and both operator deletes the ctors
      qigOid(const qigOid&) = default;
      qigOid(qigOid&&) = default;
      qigOid& operator=(const qigOid&) = default;
      qigOid& operator=(qigOid&&) = default;

      /** returns the pointer to an internal structure of libgit2 which represents the oid as an string.
       * do not free the pointer. Successive calls to this function may alter the content.
       * this function is thread safe
       */
      const qigChar* asString() const;
      /**assign the string repr of the sha1 sum to the string.
       * this copies the content to an user allocated string
       */
      void asString(qigString&)const;

      /** empty implementation. overrides qigGitOidHolder::reset
       */
      void reset(){};

      using qigGitOidHolder::isValid;

      /** returns the raw data in an NOT NULL terminated buffer;
       * @param out returns the oid as binary data.
       * @sa asString;
       */
      void rawData(const unsigned char*out[OID_RAWSZ]) const;

      /** compares two oids
       * @return true if the oid is equal
       */
      bool operator==(const qigOid& oid)const{return qigGitOidHolder::operator==(oid);}



      //operator const qigGitOidHolder& ()const{return *this;};
      using qigGitOidHolder::load;
      //void load(const git_oid* oid, git_repository* r){return qigGitOidHolder::load(oid, r);}
    protected:

    private:
      /** reimplemented for qigGitOidHolder
       * nothing to do here
       */
      virtual void reloadFromId(){};
  };
}
