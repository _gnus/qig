// vim: fdm=marker
#include <git2/blob.h>

#include "Blob.h"
namespace qig
{
  qigBlob::qigBlob()
  {
  }
  /** creates an Commit from given oid. the oid must be valid
   */
  qigBlob::qigBlob(const qigOid& oid):
    qigObject(oid)
  {
  }

  int qigBlob::rawContent(const char*& buffer) const
  {
    git_blob* blob = const_cast<git_blob*>(this->blob());
    int len = git_blob_rawsize(blob);
    buffer  = (const char*)  git_blob_rawcontent(blob);
    return len;
  }
}
