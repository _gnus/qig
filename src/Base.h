// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include <memory>

#include "gitTypes.h"

/// @defgroup Application Application

class test_Base;
namespace qig
{
  /**
   * @class qigBase
   * @ingroup Application
   * @brief holder for the main repo
   */
  class qigBase
  {
    friend class ::test_Base;
    // private typedefs {{{
    struct libgit2Initialiser {
      libgit2Initialiser();
      libgit2Initialiser(const libgit2Initialiser&){++ref_cnt;}
      libgit2Initialiser(libgit2Initialiser&&){++ref_cnt;};
      ~libgit2Initialiser();
    private:
      static int ref_cnt;
    };
    typedef std::shared_ptr<git_repository> repositoryPtr;
    //}}}
    protected:
      qigBase(const qigBase&) = default;
      qigBase( qigBase&&) = default;
    protected: //{{{ methods 
      static qigBase load(const qigChar*, bool open_bare, int &err);
      static qigBase create(const qigChar*, bool open_bare);


      /** should be called before the repository is unloaded.
       */
      void repositoryUnloaded() ;
      /** should be called with an newly loaded repo to make the repo known throughout the lib
       */
      void repositoryLoaded(git_repository* p_repo);

      /** returns 1 if no repository loaded and therefore repository is invalid
       * return 0 otherwise
       */
      int isValid() const;
      /** distributes the repo set by repositoryLoaded
       * @return a valid repo or a nullptr @sa isValid();
       */
      git_repository* mainRepo() const { return _m_repo.get();}
      //}}}
    private: // {{{ member
      // private ctor
      qigBase(git_repository*);

      libgit2Initialiser _m_libgit2_initialiser;

      repositoryPtr _m_repo;
      //}}}
  };
}
