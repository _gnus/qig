// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <git2.h>
#include <assert.h>
#include <functional>

#include "Branch.h"
#include "gitTypes.h"


namespace qig
{
  /**
   * @ingroup Application
   * @class qigBranchManager
   * @brief this class essentially wraps the functions of git2/branch.h which create new branches.
   */
  class qigBranchManager: private qigBase, private qigList<std::unique_ptr<qigBranch, std::function<void(qigBranch*)> > >
  {
    typedef qigList<
      std::unique_ptr<
	qigBranch,  std::function<void(qigBranch*)>
	>
      > branchList;

    public://{{{ methods

      qigBranchManager(const qigBase&);
      //~qigBranchManager();

      /** returns number of branches in this repository.
       *
       * @param type specifies whether to include only selected branches
       * @return number of branches in this repository
       */
      int branchCount(branchType type = branchTypeAll) const;
      /**initialise the git branches
       */
      void init() {initBranchList();}
      /** clean up
       * should be called before the loaded branches become invalid. for example before unloading a repository
       */
      void finalise(){clear();}

      /** searches a branch of the given name 
       * @param branch_name the name of the branch to look up
       * @param type the type of the desired branch. Use branchTypeAll if the type is irrelevant
       * @param out[out] a pointer where qigBranch is stored. Do not delete the returned instance.
       */
      int lookupByName(const qigChar *branch_name, branchType type, const qigBranch*& out) const;
      /// overloaded non-const versioin
      int lookupByName(const qigChar *branch_name, branchType type, qigBranch*& out); 
      /** searches a branch by the index
       * @param index the index of the branch
       * @param type the type of the desired branch. Use branchTypeAll if the type is irrelevant
       * @param out[out] a pointer where qigBranch is stored. Do not delta the instance.
       */
      int lookup(int index, branchType type, const qigBranch*& out) const;
      /// overloaded non-const versioin
      int lookup(int index, branchType type, qigBranch*& out);


      /**
       * Move/rename an existing local branch reference.
       *
       * The new branch name will be checked for validity.
       * See `git_tag_create()` for rules about valid names.
       *
       * @param out [out] the new reference of renamed branch.
       * @param in Current underlying reference of the branch.
       *
       * @param name Target name of the branch once the move
       * is performed; this name is validated for consistency.
       *
       * @param force Overwrite existing branch.
       *
       * @return 0 on success, GIT_EINVALIDSPEC or an error code.
       */
      int move(qigBranch& out, const qigBranch& in, const qigChar* name, int force);

      /** returns all branches in this repository
       * @return a list of branches
       */
      //const qigList<qigBranch>& branches() const;


      //}}}

    private: //{{{private methods
      void initBranchList();
      // returning an iterator might be very bad for multi threading
      branchList::const_iterator locateIndex(int index) const;

      /// converts git_branch_t to branch::branchType
      branchType& toqgitBrachType(git_branch_t& branch_type){ return (branchType&) branch_type;}

      //}}}
  };
}
