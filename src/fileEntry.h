// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once

#include <QString>
#include <vector>


#include "enums.h"

#include "treeEntry.h"
namespace qig
{
  struct workDirPayload
  {
    workDirPayload(): m_file_state_flag(0), m_check_state(Qt::Unchecked){}
    //public attributes
    /// an or combination of of qigFileIconProvider::fileState
    unsigned int m_file_state_flag;
    Qt::CheckState m_check_state;
  };
  /** @class fileEntry
   * rebuild file system structure in tree structure.
   * removing entries does not leaf dangling pointer
   */
  class fileEntry: public treeEntry
  {
    public:
      fileEntry(): treeEntry(0){} 
      fileEntry(fileEntry* parent): treeEntry(parent, 0){} 
      /** sets a stateflag in current fileEntry and propagate state to parent if necessary
       * remove undefined flag if set. because either the new state is a defined state
       * or state is undefined
       * warning this function does not set the state flag to the value state but sets a flag
       */
      // TODO return list of fileEntries that have been modified
      inline void setStateFlag(unsigned int state);
      inline int stateFlag()const {return m_payload.m_file_state_flag;}

      /** 
       * removes flag from fileState and propagate new state to parent in an heuritic way;
       * whether new state is propagated to parent depends on state and other settings.
       * for example a file is reverted and the state fileStateWDModified should be removed.
       * the parent folder may contain another modified file then the fileStateWDModified shouldn't be 
       * removed from parent.
       */
      inline void removeStateFlag(unsigned int state);
      Qt::CheckState checkState() const{ return m_payload.m_check_state;}
      void setCheckState(Qt::CheckState state);
    protected:
      fileEntry* _parent(){return static_cast<fileEntry*>(treeEntry::_parent());}
      fileEntry* _childByIndex(unsigned int i){return static_cast<fileEntry*>(treeEntry::_childByIndex(i));}

    private:
      inline void setStateCheckedParent(Qt::CheckState state);
      /** implements logic whether flag should be set in parent(s) recursively or not;
       * a state of parent should be modified
       */
      inline bool shouldSetStateFlagRecursive(unsigned int);
      /** returns true if any child of current object has flag.
       * does not check grand children
       */
      inline bool anyChildHasFlag(unsigned int);
      /** if state s also affects parent the state returned by this function should be set as parent state;
       */
      unsigned int mapToState(unsigned int s){return s;}
      // worker for the first iteration step;
      inline void _setStateFlag(unsigned int state);
      // worker for recursive state modifier.
      // if recursive is false the recursion aborts;
      inline void _setStateFlag(unsigned int state, bool recursive);
      // removes state flag if state flag should be set recursively and no children has state state set
      inline void tryRemoveStateFlag(unsigned int state);

    private:
      workDirPayload m_payload;
  };
//////////////////////////
// implementation
// if you see errors about multiple defined symbols see comment at the top of this file

inline void fileEntry::setStateFlag(unsigned int state)
{
  // remove fileStateUndefined flag
  m_payload.m_file_state_flag &= ~fileStateUndefined;
  _setStateFlag( state );
}

inline void fileEntry::removeStateFlag(unsigned int state)
{
  // remove fileStateUndefined flag
  m_payload.m_file_state_flag &= ~state;
  if(parent()) {
    static_cast<fileEntry*>(_parent())->tryRemoveStateFlag(state);
  }

}

inline bool fileEntry::shouldSetStateFlagRecursive(unsigned int state)
{
  return state & ( fileStateIndexModified | fileStateWDModified | fileStateConflicted);
}

inline void fileEntry::_setStateFlag(unsigned int state)
{
  m_payload.m_file_state_flag |= state;
  if(shouldSetStateFlagRecursive(state) && parent()) {
    _parent()->_setStateFlag(mapToState(state), true);
  }
}
inline void fileEntry::_setStateFlag(unsigned int state, bool recursive)
{
  // do not set state flag if it is already set.
  if( (m_payload.m_file_state_flag & state) != state)
  {
    m_payload.m_file_state_flag |= state;
    if(recursive && parent()) {
      _parent()->_setStateFlag(state, recursive);
    }
  }
}
void fileEntry::tryRemoveStateFlag(unsigned int state)
{
  if(shouldSetStateFlagRecursive(state))
  {
    if(!anyChildHasFlag(state))
    {
      removeStateFlag(state);
    }
  }
}

inline bool fileEntry::anyChildHasFlag(unsigned int state)
{
  bool ret = false;
  for(unsigned int i = 0 ; i< childrenCount() && ! ret; ++i)
  {
    fileEntry* p = _childByIndex(i);
    ret = p -> m_payload.m_file_state_flag & state;
  }
  return ret;
}
inline void fileEntry::setCheckState(Qt::CheckState state)
{
  if(state == Qt::PartiallyChecked)
  {
    // should not happen;
    // call this function with checked or unchecked
    assert(false);
    return;
  }
  traverse(this, [](fileEntry*p, Qt::CheckState st){p->m_payload.m_check_state = st;}, state);
  m_payload.m_check_state=state;
  setStateCheckedParent(state);
}
inline void fileEntry::setStateCheckedParent(Qt::CheckState state)
{
  if(_parent())
  {
    fileEntry& parent = *_parent();
    if(parent.m_payload.m_check_state == state)
    {
      //nothing to do
      return;
    }
    for( size_t i=0; i < parent.childrenCount(); ++i)
    {
      if(parent._childByIndex(i)->m_payload.m_check_state!=state)
      {
	state = Qt::PartiallyChecked;
	break;
      }
    }
    parent.m_payload.m_check_state = state;
    setStateCheckedParent(state);
  }
}
}
//}}}
