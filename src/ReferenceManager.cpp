
#include "ReferenceManager.h"


#include <git2/refs.h>
#include <git2/repository.h>

#include "enums.h"


namespace qig
{
  qigReferenceManager::qigReferenceManager(const qigBase& base):
    qigBase(base)
  {
  }

  int qigReferenceManager::references(qigReferencesList& list, int type) const
  {
    git_reference_iterator * iter;
    int err=GIT_ERROR;
    if( (err = git_reference_iterator_new(&iter, mainRepo())) == GIT_OK)
    {
      git_reference * ref;
      while(err == GIT_OK)
      {
	err = git_reference_next(&ref, iter);
	if(err == GIT_OK) {
	  if( git_reference_type(ref) & type) {
	    list.push_back(qigReferencePtr(new qigReference(ref)));
	  }
	  else {
	    git_reference_free(ref);
	  }
	}
      }
      if(err == GIT_ITEROVER) {
	err = GIT_OK;
      }
      git_reference_iterator_free(iter);
    }
    return err;
  }

  int qigReferenceManager::head(qigReference& out) const
  {
    git_reference * head;
    int err = git_repository_head(&head, mainRepo());
    if(err == GIT_OK) {
      out = head;
    }
    return err;
  }


}
