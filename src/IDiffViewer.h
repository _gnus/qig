#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include "gitTypes.h"

class QWidget;
class QSettings;

namespace qig
{
  enum fileIdentifier: int16_t
  {
    identifierUnknown=0, ///< used internally only. Indicates an error when used in api
    identifierOldFile,   ///< content or file belongs to the old file
    identifierNewFile,   ///< content or file belongs to the new file
    identifierBothFile   ///< content belongs to both files
  };

  enum fileLineEnd: int16_t
  {
    lineEndunknown,            ///< used intenrally only. Indicates an error when used in api
    lineEndBothNoEOFNL, ///< both file do not end with old file
    lineEndNewNoEOFNL,  ///< new file does not end with LF, old does
    lineEndOldNoEOFNL,  ///< old file does not end with LF, new does
  };

  enum fileType: int16_t
  {
    typeBin,
    typeText
  };

  class IDiffViewerExternal;
  class IDiffer;
  class IDiffFile;
  class IDiffHunk;
  class IDiffLine;
  class IDiffBase;
  class IDiffFileExternal;
  typedef class IDiffFile IDiffDelta;


  /** factory class to create a Differ
   * the diff plug-in for qig consists of 2 classes that must be implemented.
   * The first class is the IDiffViewerExternal. This class must implement its own diff engine
   * to compare pairs of 2 files. The class IDiffViewerExternal receives the data for the files
   * which the class created by createView must display.
   *
   * createView may return a nullptr when an external program is used to display the diff.
   */
  class IDifferFactory
  {
    public:
      /** this function may return a differ that uses its own diff engine
       * may return 0 if not supported
       */
      virtual IDiffViewerExternal* createDifferExternal()  = 0;
      /** this function may return a differ that uses the built-in diff engine.
       * may return 0 if not supported
       */
      virtual IDiffer* createDifferBuiltin()  = 0;

      /** if this function returns true the factory can provide a QWidget that is embeddable in qig
       */
      virtual bool provideView() const = 0;

      /** this function may return a view for the differ returned by createDiffer
       * changes in the differ must be reflected in the view without further calls from qig
       * @sa provideView;
       * @param parent a QWidget that can be used as parent in Qt parent child hierarchy. Otherwise it should be ignored.
       */
      virtual QWidget *createView(QWidget *parent) = 0;

      /**a small string that describes the diff interface. the string could be the program that the interface uses
       */
      virtual const char* name() const =0;
      /** returns an arbitrary string that describes the differ.
       * the description should fit in a small mouse over box and describe the properties of the differ
       *  and description might be "use the external program xy"
       */
      virtual const char* shortDescription(const char* LC) const = 0;

      // 
      virtual QWidget * propertyWidget(QWidget*parent) = 0;

      virtual void   serialise(      QSettings&) = 0;
      virtual void deserialise(const QSettings&) const = 0;

      virtual void release() = 0;

  };

  /** @class IDiffFileExternal
   * interface for diff engine
   * interface must be implemented by the user and is called by qig
   * this interface provides information to the file only. The
   * implementation must use its own diff engine to compare both files
   * qig just ensures that the files represented by IDiffFileExternal differ.
   * the content of the files might be even binary.
   *
   * The class must be capable to diff a set of 
   */
  class IDiffViewerExternal
  {
    public:
      /** will be called by qig if interface isn't required any longer.
       * implementation should clean up state before it returns;
       */
      virtual void release() = 0;

      /** the initialiser method.
       * called exactly once. 
       */
      virtual void init() = 0;

      /** will be called by qig to show a diff between a set of  2 files
       * user must set *old_file and *new_file to empty implementation of
       * IDiffFileExternal;
       * qig will fill both file
       *
       * this function is called for each pair of files in a set. A set of pairs
       * might be all files that differs between 2 commits.
       *
       *  @param file A pointer a structure that represents the files
       *  @return 0 on success
       */
      virtual int addFile( IDiffFileExternal **file) = 0;

      /**
       * differ should reset the internal state. The differ should discard the content of the recently loaded set.
       */
      virtual void reset() = 0;
  };

  /**
   * @class IDiffer
   * interface for diff viewer
   * interface must be implemented by user and is called by qig
   *
   * the user obtains informations about a diff without having to implement
   * a diffengine on its own. Instead the diff consists of file, hunks and 
   * lines. the user returns empty objects that represents a file hunk or line
   * and the object will be filled by qig.
   *
   * - A `hunk` is a span of modified lines in a delta along with some stable
   *   surrounding context.  You can configure the amount of context and other
   *   properties of how hunks are generated.  Each hunk also comes with a
   *   header that described where it starts and ends in both the old and new
   *   versions in the delta.
   *
   * - A `line` is a range of characters inside a hunk.  It could be a context
   *   line (i.e. in both old and new versions), an added line (i.e. only in
   *   the new version), or a removed line (i.e. only in the old version).
   *   Unfortunately, we don't know anything about the encoding of data in the
   *   file being diffed, so we cannot tell you much about the line content.
   *   Line data will not be NULL-byte terminated, however, because it will be
   *   just a span of bytes inside the larger file.
   */
  class IDiffer
  {
    public:
    /**
     * Diff notification callback function.
     *
     * The callback will be called for each file, just before the `git_delta_t`
     * gets inserted into the diff.
     *
     * When the callback:
     * - returns < 0, the diff process will be aborted.
     * - returns > 0, the delta will not be inserted into the diff, but the
     *		diff process continues.
     * - returns 0, the delta is inserted into the diff, and the diff process
     *		continues.
     */
      virtual int addDelta( IDiffDelta **delta) = 0;
      /** function sets *file to an allocated IDiffFile object that
       * @param file [out] a pointer to a pointer of IDiffFile
       * @return 0 on success
       */
      virtual int addFile( IDiffFile**file) = 0;
      /** function sets *hunk to an allocated IDiffHunk object that
       * @param file      the parent of the hunk. file will be the same object as the object obtained by addFile
       * @param hunk [out] a pointer to a pointer of IDiffHunk
       * @return 0 on success
       */
      virtual int addHunk( IDiffFile * file, IDiffHunk** hunk)  = 0;
      /** function sets *line to an allocated DiffLineModelItem object that will be filled by qig
       * @param parent the parent hunk of the new line to which the new line should be added. hunk will be the same object as the object obtained by addHunk
       * @param line [out] a pointer to a pointer of IDiffLine 
       * @return 0 on success
       */
      virtual int addLine( IDiffHunk *parent, IDiffLine **line) = 0;

      /** called by qig if @a item item obtained all data and @a item is not needed any longer.
       *  not called for IDiffDelta
       */
      virtual void finalise(IDiffBase *item) = 0;
      /** called by qig if item obtained all data and @a item is not needed any longer.
       * @return 0 to add the delta to the diff,
       * @return >0 this will prevents calls to addHunk and addFile for given delta
       * @return <0 an error code aborts the diff process.
       */
      virtual int accept(IDiffDelta *item) = 0;

      /**
       * differ should reset the internal state. The differ should discard the content of the recently loaded set.
       */
      virtual void reset()=0;

      /** called by qig if IDiffer is not needed any longer
       */
      virtual void release() = 0;
  };

  /** base class for all IDiff* classes
   */
  class IDiffBase
  {
    public:
      /** called by qig if all data available are set.
       *  and the interface is no longer needed any longer.
       */
      virtual void finalise() = 0;
  };


  /**
   * @class IDiffFile
   * interface implemented by user and used by implementation of IDiffExternalViewer
   * qig uses this interface to inform the user about properties of diffed files
   */
  class IDiffFileExternal: public IDiffBase
  {
    public:
      /** called by qig to inform the user about the file path
       * note given path may not exist because the queried object was an object from gits ODB and is deleted meanwhile
       *
       * @param path The path to the file including file name.
       * pointer owned by qig and stays valid till finalise is called
       */
      virtual void setPath(const char *path) = 0;

      /** returns the maximal number of bytes that writeData must support.
       * the actual number of bytes available in current stream may differ.
       */
      virtual unsigned int maxDataLength() const = 0;

      /** called by qig to inform user about content of current file
       *  might be called several times for a single a file
       *
       *  @param data pointer to data. will become invalid when function returns
       *  data may not be 0-terminated for binary files or if file is larger
       *  then addData supports. in latter case addData is called several times
       *  till all data were transmitted.
       *  @param len number of bytes available in data.
       *  @return user should return number of bytes processed.
       *  if returned value != len the transmittion aborts
       */
      virtual unsigned int addData(const qigChar *data, unsigned int len);


      virtual void finalise() = 0;
  };

  /** @class IDiffLine
   * - A `line` is a range of characters inside a hunk.  It could be a context
   *   line (i.e. in both old and new versions), an added line (i.e. only in
   *   the new version), or a removed line (i.e. only in the old version).
   *   Unfortunately, we don't know anything about the encoding of data in the
   *   file being diffed, so we cannot tell you much about the line content.
   *   Line data will not be NUL-byte terminated, however, because it will be
   *   just a span of bytes inside the larger file.
   */
  class IDiffLine: public IDiffBase
  {
    public:
      virtual void setOrigin(   qigChar origin) = 0;       /**< A git_diff_line_t value */
      virtual void setOldLineno(int    old_lineno) = 0;   /**< Line number in old file or -1 for added line */
      virtual void setNewLineno(int    new_lineno) = 0;   /**< Line number in new file or -1 for deleted line */
      virtual void setNumLines( int    num_lines) = 0;    /**< Number of newline characters in content */
      virtual void setContent(const qigChar *content, size_t content_len) = 0;/**< Pointer to diff text, NOT NULL-byte terminated and Number of bytes of data */

  };

  /** @class IDiffHunk
   * - A `hunk` is a span of modified lines in a delta along with some stable
   *   surrounding context.  You can configure the amount of context and other
   *   properties of how hunks are generated.  Each hunk also comes with a
   *   header that described where it starts and ends in both the old and new
   *   versions in the delta.
   *
   */
  class IDiffHunk: public IDiffBase
  {
    public:
      virtual void setOldStart(int old_start) = 0;         /**< Starting line number in old_file */
      virtual void setOldLines(int old_lines) = 0;         /**< Number of lines in old_file */
      virtual void setNewStart(int new_start) = 0;         /**< Starting line number in new_file */
      virtual void setNewLines(int new_lines) = 0;         /**< Number of lines in new_file */
      virtual void setHeaderLen(size_t header_len) = 0;    /**< Number of bytes in header text */
      virtual void setHeader(const qigChar header[128]) = 0;        /**< Header text, NULL-byte terminated */
  };
  /**
   * @class IDiffFile
   * interface implemented by user and used by implementation of IDiffer
   * qig uses this interface to inform the user about properties of diffed files
   * if user does not have his own diff engine
   */
  class IDiffFile: public IDiffBase
  {
    public:
      /** called by qig to inform about  the user about the file path
       * note given path may not exist becuase IDiffFile is an old file
       * and was deleted meanwhile
       *
       * @param old_path The path to the old file including file name.
       * pointer owned by qig and stays valid till finished is called
       * @param new_path The path to the file including file name.
       * pointer owned by qig and stays valid till finished is called
       */
      virtual void setPath(const qigChar *old_path, const qigChar* new_path) = 0;
      virtual void setStatus(int st)  = 0;
      virtual void setFlag(int flags) = 0;
  };
  /*
  class IDiffDelta
  {
    public:
      virtual void setStatus(int st)  = 0;
      virtual void setFlag(int flags) = 0;
      virtual void setPath(const qigChar *old_path, const qigChar* new_path) = 0;
  };
  */
}

