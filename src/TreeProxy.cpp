// vim: fdm=marker
#include <QDebug>
#include "TreeProxy.h"

namespace qig
{

  struct ProxyEntry {
    ProxyEntry *parent;
    QVector<ProxyEntry*> children;
    unsigned int state_flag;
    qreal source_payload;
  };




  qigTreeProxy::qigTreeProxy(QObject* parent): QAbstractProxyModel(parent),
    m_add_parent_child(false)
  {
  }

  QModelIndex qigTreeProxy::setRoot(const QModelIndex& new_root)
  {
    if( m_virtual_root == new_root) {
      return m_virtual_root;
    }
    connect(sourceModel(), SIGNAL(directoryLoaded(const QString&)), this, SLOT(reset(const QString&)));

    QModelIndex ret = m_virtual_root;
    beginResetModel();
    m_virtual_root = new_root;
    QString path = m_virtual_root.data(0x101).toString();
    qDebug() << " new Root is "<< path;
    path = m_virtual_root.parent().data(0x101).toString();
    qDebug() << " parent is is "<< path;
    
    endResetModel();
    return ret;
  }

  QVariant qigTreeProxy::data(const QModelIndex& proxyIndex, int role) const
  {
    if(proxyIndex.isValid())
    {
      if( role == Qt::DisplayRole && proxyIndex.row() == 0 && m_add_parent_child) {
	return QVariant(QString(".."));
      } 
      return sourceModel()->data(mapToSource(proxyIndex), role);
    }
    qDebug() << "received query for invalid proxyIndex" << proxyIndex;
    return QVariant();
  }


  QModelIndex qigTreeProxy::mapToSource(const QModelIndex& proxyIndex) const
  {
    if(proxyIndex.isValid() )
    {
      if(proxyIndex.row()==0 && m_add_parent_child) {
	// in case we add the '..' entry; the 0th row is the root
	  return m_virtual_root;
      }
      // otherwise return query the source model. but we may reserve space for a dummy row;
      return sourceModel()->index(proxyIndex.row() - m_add_parent_child, proxyIndex.column(), m_virtual_root);
    }
    return m_virtual_root;
  }

  QModelIndex qigTreeProxy::mapFromSource(const QModelIndex& sourceIndex) const
  {
    if(sourceIndex.isValid() && m_virtual_root == sourceIndex.parent())
    {
      return index(sourceIndex.row() + m_add_parent_child, sourceIndex.column());
    } else if ( sourceIndex == m_virtual_root ) {
      return index(0, sourceIndex.column());
    } else {
      return QModelIndex();
    }
  }

  QModelIndex qigTreeProxy::parent(const QModelIndex& proxyIndex) const
  {
    return QModelIndex();
  }

  QModelIndex qigTreeProxy::index(int row, int column, const QModelIndex& parent) const
  {
    QModelIndex proxyIndex = createIndex(row, column, nullptr);
    // if m_add_parent_child is set we reserve row0 for our own index
    if(row == 0 && m_add_parent_child) {
      return proxyIndex;
    }
    QModelIndex index ( mapToSource(proxyIndex) );
    if ( index.isValid() ) {
      return proxyIndex;
    }
    qDebug() << "index for " << QString::number(row) << " and " << QString::number(column) << " is invalid";
    return QModelIndex();
  }

  int qigTreeProxy::columnCount(const QModelIndex &parent) const
  {
    static int _i =0;
    int j = sourceModel()->columnCount(m_virtual_root);
    if (  _i!=j  ){
      _i=j;
      qDebug() << "columnCount changed to " << QString::number(j);
    }
    return j;
  }

  int qigTreeProxy::rowCount(const QModelIndex &parent) const
  {
    static int _j = 0;
    int j = sourceModel()->rowCount(m_virtual_root)+m_add_parent_child;
    if (  _j!=j  ){
      _j=j;
      qDebug() << "rowCount changed to " << QString::number(j);
    }
    return j;
  }

  Qt::ItemFlags qigTreeProxy::flags(const QModelIndex& index) const
  {
    return sourceModel()->flags(mapToSource(index));
  }

  bool qigTreeProxy::setData(const QModelIndex& index, const QVariant& data, int role)
  {
    if( role == root )
    {
      setRoot( mapToSource(index) );
      return true;
    }
    return QAbstractItemModel::setData(index, data, role);
  }
  void qigTreeProxy::setSourceModel(QAbstractItemModel*p){return QAbstractProxyModel::setSourceModel(p);}
}
