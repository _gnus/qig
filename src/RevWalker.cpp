#include <git2/revwalk.h>

#include "RevWalker.h"
#include "Oid.h"


namespace qig
{

  qigRevWalker::qigRevWalker(const qigBase& base):qigBase(base),
    _m_revwalker(0)
  {
  }
  qigRevWalker::~qigRevWalker()
  {
    release();
  }

  int qigRevWalker::acquire(int sort)
  {
    if(_m_revwalker)// maybe someone tried to acquired twice
    {
      throw std::exception();
    }
    int err = GIT_ERROR;
    git_revwalk *iter;
    err = git_revwalk_new(&iter, mainRepo());

    if(err == GIT_OK)
    {
      _m_revwalker = iter;
      // GIT_SORT_NONE is default. Don't set this sort method
      if((int) sort !=GIT_SORT_NONE) { 
	setSortMethod(sort);
      }
    }
    else
    {
      release();
    }
    return err;
  }

  int qigRevWalker::pushHide(qigOid& commit_id)
  {
    return  git_revwalk_hide(revWalker(), commit_id.rawValue());
  }


  int qigRevWalker::next(qigOid& out)
  {
    git_oid oid;
    int err = git_revwalk_next(&oid, revWalker());
    if(!err) out.load(&oid, git_revwalk_repository(revWalker()));
    return err;
  }

  int qigRevWalker::pushStart(qigOid& commit_id)
  {
    return git_revwalk_push(revWalker(), commit_id.rawValue());
  }

  void qigRevWalker::reset()
  {
    if(revWalker())
      git_revwalk_reset(revWalker());
  }

  void qigRevWalker::release()
  {
    if(revWalker())
    {
      git_revwalk *tmp = _m_revwalker;
      _m_revwalker=0;
      git_revwalk_free(tmp);
    }
  }

  void qigRevWalker::setSortMethod(int sort)
  {
    int gitSort = qigRevWalker::toGitSort(sort);
    return git_revwalk_sorting(revWalker(), gitSort);
  }
}

