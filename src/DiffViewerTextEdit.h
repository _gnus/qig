#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <QTextEdit>
namespace qig
{
  /**
   * @ingroup Application
   * @class DiffViewerBuiltinTextEdit
   * @brief view to render diffs based on qigDiffViewerBuiltinTextDocument
   *
   * reimplements the keyPressEvent;
   *
   */
  class DiffViewerBuiltinTextEdit: public QTextEdit
  {
    public:
      // implemented in Application.cpp
      DiffViewerTextEdit(QWidget* parent);

    protected:
      // implemented in Application.cpp
      virtual void keyPressEvent(QKeyEvent*ev) override;
    Q_SIGNALS:
      void foldRequest(const QTextCursor& c);
      void updateLineNumberAreaWidth(int newBlockCount);
      void updateLineNumberArea(const QRect &, int);

    private:
      QWidget * m_lineNumberArea;
  };
}
