// vim: fdm=marker
#include <git2.h>
#include <assert.h>
#include <string.h>
#include <vector>
#include <memory>

#include "Commit.h"
#include "Oid.h"
#include "Tree.h"
#include "StringFilter.h"


namespace qig
{
  //{{{ ctor, dtor
  qigCommit::qigCommit()
  {
  }

  qigCommit::qigCommit(git_commit* p_commit):
    qigObject((git_object*) p_commit)
  {
  }

  qigCommit::qigCommit(const qigOid& id):
    qigObject(id)
  {
  }
  //}}}

  //{{{ public methods
  bool qigCommit::applyFilter(const qigFilter& f) const
  {
    switch(f.type())
    {
      case logFilter::typeStringFilter:
      {
	const qigStringFilter& stringFilter = static_cast<const qigStringFilter&>(f);
	switch(f.fields())
	{
	  case logFilter::fieldCommiter:
	    return _strstr( _commiter()->name, stringFilter.filterstring());
	    break;
	  case logFilter::fieldCommitHeader:
	    return _strstr( _commitSummary(), stringFilter.filterstring());
	    break;
	  case logFilter::fieldCommitMessage:
	    return _strstr( _commitMessage(), stringFilter.filterstring());
	    break;
	  case logFilter::fieldDate:
	    assert(false);
	  default:
	    break;
	}
      }
	break;
      case logFilter::typeGraphFilter:
      default:
	break;
    }
    return qigObject::applyFilter(f);
  }

  int qigCommit::tree(qigTree& t) const
  {
    git_tree * tree;
    int ret;
    if( (ret = git_commit_tree(&tree, commitish() ) )== GIT_OK)
    {
      t.qigGitObjectHolder::load( (git_object*) tree);
    }
    return ret;
  }

  int qigCommit::commit(
      qigOid &out,
    const qigChar * update_ref,
    const git_signature *author,
    const git_signature *committer,
    const qigChar* encoding,
    const qigChar* message,
    const qigTree& tree,
    const qigList<qigCommit>& parents
    )
  {
    git_signature* _committer;
    git_repository *repo = tree.owner();

    update_ref = update_ref ? update_ref : "HEAD";
    if(committer) {
      int err = git_signature_dup(&_committer, committer);
      if(err != GIT_OK) return err;
    }
    else {
      int err;
      if( (err=git_signature_default(&_committer, repo)) != GIT_OK) {
	return err;
      }
    }
    std::unique_ptr<git_signature, void(*)(git_signature*) > sigPtr(_committer, git_signature_free);
    author    = author ? author : _committer;
    
    const git_tree * _tree = tree.tree();

    std::vector<const git_commit*> _parents;
    for( const qigCommit& parent: parents)
    {
      _parents.push_back(parent.commitish());
    }

    git_oid _out;
    int err= git_commit_create(&_out, repo, update_ref, author, sigPtr.get(), encoding, message, _tree, _parents.size(), _parents.data());
    out.load(&_out, repo);
    return err;

  }


  //}}}


  const qigSignature* qigCommit::_author() const
  {
    git_commit * ci = const_cast<git_commit*>(commitish());
    return ci ? git_commit_author(ci) :0;
  }

  const qigSignature* qigCommit::_commiter() const
  {
    git_commit * ci = const_cast<git_commit*>(commitish());
    return ci ? git_commit_committer(ci) :0;
  }

  unsigned int qigCommit::parentCount() const
  {
    git_commit * ci = const_cast<git_commit*>(commitish());
    return ci ? git_commit_parentcount(ci) :0;
  }
  const qigChar* qigCommit::_commitSummary() const
  {
    git_commit * ci = const_cast<git_commit*>(commitish());
    return ci ? git_commit_summary(ci) : 0;
  }
  const qigChar* qigCommit::_commitMessage() const
  {
    git_commit * ci = const_cast<git_commit*>(commitish());
    return ci ? git_commit_message(ci) : 0;
  }
  git_time_t qigCommit::_commitTime() const
  {
    git_commit * ci = const_cast<git_commit*>(commitish());
    return ci ? git_commit_time(ci) : 0;
  }
  int qigCommit::_timeOffset() const
  {
    git_commit * ci = const_cast<git_commit*>(commitish());
    return ci ? git_commit_time_offset(ci) : 0;
  }

  int qigCommit::parentList(const unsigned int length, qigCommit out_array[], unsigned int &i) const
  {
    git_commit * ci = const_cast<git_commit*>(commitish());
    unsigned int n = std::min(parentCount(), length);
    int err = 0;
    git_commit* parent;
    for( i=0; i< n && !err; ++i){
      err = git_commit_parent( &parent, ci, i);
      out_array[i]=parent;
    }
    return err;
  }

  int qigCommit::parentList(const unsigned int length, qigOid out_array[], unsigned int &i) const
  {
    git_commit * ci = const_cast<git_commit*>(commitish());
    unsigned int n = parentCount();
    int err = 0;
    for( i=0; i< n && i<  length && !err; ++i){
      qigGitOidHolder::assign(out_array[i], git_commit_parent_id(ci, i), git_commit_owner(ci));
      //out_array[i]=*git_commit_parent_id(ci, i);
    }
    return err;
  }

  int qigCommit::parent(qigOid &out, unsigned int index) const
  {
    git_commit * ci = const_cast<git_commit*>(commitish());
    int err = 0;
    qigGitOidHolder::assign(out, git_commit_parent_id(ci, index), git_commit_owner(ci));
    return err;
  }

  qigCommit& qigCommit::operator=(const git_commit* other)
  {
    reset();
    // initialise parent
    qigGitObjectHolder::load((git_object*) other);
    return *this;
  }

  bool qigCommit::_strstr(const qigChar* str, const qigChar* contained)
  {
    // use strstr from string.h
    return strstr(str, contained) != 0;
  }
}
