#include <git2.h>
#include <cassert>
#include <utility>
#include <string.h>
#include "GitObjectHolder.h"
#include "Oid.h"


namespace qig
{
  static git_otype toGitType(objectType t){return (git_otype) t;}

  qigGitObjectHolder::qigGitObjectHolder():_m_object(nullptr)
  {
  }

  qigGitObjectHolder::qigGitObjectHolder(git_object* p):
    qigGitOidHolder(git_object_id(p), git_object_owner(p)),
    _m_object(p)
  {}
  qigGitObjectHolder::qigGitObjectHolder(const qigOid&o):
    qigGitOidHolder(o),
    _m_object(0)
  {
    if(lookup()!=0)
    {
      assert(false);
      throw -3;
    }
  }

  qigGitObjectHolder::qigGitObjectHolder(const qigGitObjectHolder&o ):
    qigGitOidHolder(o)
  {
    git_object_dup(&_m_object, o._m_object);
  }

  qigGitObjectHolder::qigGitObjectHolder(qigGitObjectHolder&&o ):
    qigGitOidHolder(std::move(o)),
    _m_object(o._m_object)
  {
    o._m_object=0;
  }

  qigGitObjectHolder::~qigGitObjectHolder()
  {
    if(_m_object)
    {
      git_object_free(_m_object);
    }
  }

  git_object* qigGitObjectHolder::object(objectType type)
  {
    // return const version of object
    const qigGitObjectHolder* _const_this = this;
    return const_cast<git_object*>(_const_this->object(type));
  }
  const git_object* qigGitObjectHolder::object(objectType type) const
  {
    if(type == objectTypeAny)
    {
      return _m_object;
    }
    return ( _m_object && git_object_type(const_cast<git_object*>(_m_object)) ==(int) type) ? _m_object : nullptr;
  }

  void qigGitObjectHolder::load(git_object *o)
  {
    _load(o,true);
  }
  // if there are some strange (concerning the memory) objects o here see also load(qigOid&o) in the header;
  void qigGitObjectHolder::load(const qigGitOidHolder &o)
  {
      qigGitOidHolder::load(o.oid(), o.owner());
  }
  void qigGitObjectHolder::_load(git_object *o, bool reload_oid)
  {
    if(_m_object == o){
      // the user expects that we take the ownership of o;
      git_object_free(o);
      return; // we could also check the oid for object that are not cached
    }
    set(o);
    if(reload_oid) {
      qigGitOidHolder::load(git_object_id(o), git_object_owner(o));
    }
    // tell other that a new object is available
    reload();
  }

  void qigGitObjectHolder::oidCopy(qigOid& out) const
  {
    out.load(qigGitOidHolder::oid(), owner());
  }

  int qigGitObjectHolder::lookup()
  {
    assert(owner());
    assert(_m_object==nullptr);
    return git_object_lookup(&_m_object, owner(), oid(), toGitType(objectTypeAny));
  }

  void qigGitObjectHolder::_clear()
  {
    // nullptr check in libgit
    git_object_free(_m_object);
    _m_object=0;
  }
  void qigGitObjectHolder::set(git_object*p)
  {
    _clear();
    _m_object=p;
  }

  void qigGitObjectHolder::reloadFromId()
  {
    if(qigGitOidHolder::isValid() )
    {
      // do not load the object if we alrady have it;
      if( !_m_object || (_m_object && !(this->qigGitOidHolder::operator==(*git_object_id(_m_object)))))
      {
	git_object* p;
	// don't use qigGitObjectHolder::lookup otherwise we couldn't check whether the object p really had changed
	git_object_lookup(&p, owner(), oid(), toGitType(objectTypeAny));
	// load the object but do not reload the oid
	_load(p, false);
      }
    }
    else{
      _load(nullptr, false);
    }
  }
  bool qigGitObjectHolder::operator==(const git_object& obj) const
  {
    if(&obj == _m_object) // < should be enough in most cases or the objects differ
      return true;
    return qigGitOidHolder::operator==(*git_object_id(const_cast<git_object*>(&obj)));
  }
  qigGitObjectHolder& qigGitObjectHolder::operator=(const qigGitObjectHolder& obj)
  {
    load(obj);
    return *this;



    set(obj._m_object);
    qigGitOidHolder::load(obj);
    return *this;
  }

}
