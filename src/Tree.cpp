// vim: fdm=marker
//
#include <cassert>
#include <climits>
#include <algorithm>
#include <git2/tree.h>

#include "Tree.h"
#include "Oid.h"
#include <string.h>

namespace qig
{
  //{{{ helper qigTreeEntry
  void qigTreeEntry::entry_deleter::operator()(git_tree_entry*p) const {
    git_tree_entry_free(p);
  }
  
  qigTreeEntry::qigTreeEntry():
    m_idx(UINT_MAX),
    m_entry(0)
  {
  }

  /// TODO not really a copy ctor
  /// currently this class is a private class to cache a tree entry;
  /// if the cache is invalid qigTree must lookup the entry;
  qigTreeEntry::qigTreeEntry(const qigTreeEntry&):
    m_idx(UINT_MAX),
    m_entry(0),
    m_owned( nullptr )
  {
  }

  // see comments for copy ctor
  qigTreeEntry& qigTreeEntry::operator=(const qigTreeEntry&)
  {
    this->reset();
    return *this;
  }

  const char* qigTreeEntry::name() const
  {
    return m_entry ? git_tree_entry_name(m_entry) : nullptr;
  }

  const git_oid* qigTreeEntry::oid() const
  {
    return m_entry ? git_tree_entry_id(m_entry) : nullptr;
  }

  ::qig::objectType qigTreeEntry::type() const
  {
    return m_entry ? (objectType) (git_tree_entry_type(m_entry) )  : objectTypeBad;
  }
  void qigTreeEntry::load(git_tree_entry* e, unsigned int i)
  {
    reset();
    load(static_cast<const git_tree_entry*>(e),i);
    assert(m_owned==nullptr);
    m_owned=ownedTreeEntryPtr(e);
  }
  void qigTreeEntry::reset()
  {
    m_owned.reset();
    m_entry=0;
    m_idx=UINT_MAX;
  }
  //}}}

  qigTree::qigTree()
  {
  }
  qigTree::qigTree(const qigOid& oid): qigObject(oid)
  {
    if(peelSelf(objectTypeTree))
    {
      throw std::exception();
    }

  }

  bool qigTree::updateCachedEntry(unsigned int i) const
  {
    int err = true;
    if(m_recently_queried_entry.index() != i)
    {
      const git_tree* t = tree();
      if(t &&  i < git_tree_entrycount(t)) {
	m_recently_queried_entry.load(git_tree_entry_byindex(t,i), i);
      }
      else {
	err = false;
	m_recently_queried_entry.reset();
      }
    }
    return err;
  }

  bool qigTree::updateCachedEntry(const qigChar * path) const
  {
    const qigChar* current = m_recently_queried_entry.name();
    bool update = true;
    bool err = true;
    if(current && path) {
      if( strcmp(current, path) !=0 ) {
	update=false;
      }
    }
    if(update)
    {
      const git_tree* t = tree();
      git_tree_entry * new_current;
      if(t &&  git_tree_entry_bypath(&new_current, t,  path) == GIT_OK) {
	m_recently_queried_entry.load( new_current, -1);
      }
      else {
	err = false;
	m_recently_queried_entry.reset();
      }
    }
    return err;
  }

  size_t qigTree::entryCount() const
  {
      const git_tree* t = tree();
      return t ? git_tree_entrycount(t) : 0U;
  }

  qigObject qigTree::peeledTree(const qigOid& o)
  {
    qigObject current(o);
    current.peelSelf(objectTypeTree);
    return current;
  }
}
