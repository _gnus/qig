#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <QHash>
#include <QMap>
#include <QString>

#include "DiffViewerImpl.h"
namespace qig
{
  class qigOid;
  /**
   * @class qigStatusTree
   * @brief a class that operates in the diff frame work to extract the status when comparing 2 trees;
   *
   * @sa qigStatus to compare the working direcotry with the index
   */
  class qigStatusTree: public qigDiffViewerImpl
  {
    public:
      qigStatusTree();
      qigStatusTree(const qigOid& old_side, const qigOid& new_side);

      int statusFile(unsigned int& status_flags, const QString& path) const;

      int compare(const qigOid& old_side, const qigOid& new_side);
    protected:
      // should not be called
      virtual int addFile(IDiffFile**) override { throw 1;}
      virtual int accept(IDiffDelta *)override;

      virtual void finalise(IDiffBase *item);
    private:
    public:
      QMap<QString, int> m_status;
  };
}
