// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once

#include <memory>
#include <thread>
#include <atomic>
#include <QVector>
#include <QMap>
#include <QVariantMap>
#include <QLinkedList>

#include "QOid.h"

#include "IDataFiller.h"
#include "Oid.h"
#include "Commit.h"



namespace qig
{

  struct pushedCommitProperties;

  //{{{ helper classes


  typedef std::shared_ptr<qigOid> qigOidPtr;

  /** @class qigLogModelData
   * @brief the class that will be stored in the in the model. 
   * helper class to store properties for the visual representation of commit graph (history)
   */
  class qigLogModelData
  {
    friend class qigLogModelHistoryWorker;
    public:
    typedef std::vector<qigOidPtr> oidVec;
      qigLogModelData(){init();}
      explicit qigLogModelData(const qigOid &parent):_m_internal_data(std::make_shared<qigOid>(parent)){init();pushParent(_m_internal_data);}
      explicit qigLogModelData(const qigOidPtr &parent):_m_internal_data(parent){init();pushParent(parent);}

      bool operator  == (const qigLogModelData& o)const { return _m_internal_data == o._m_internal_data;}

      void setEnabled(bool enable){_m_is_enabled= enable;}
      /** display hint for the view. if this function returns false. the item may or may not be displayed
       */
      bool isEnabled()const{return _m_is_enabled;}
      /** return 1 if item is intractable otherwise 0.
       * an intractable item is a git_object and has an oid.
       */
      unsigned int isCommit()const{return hasRepresentationFlag(1);}

      void setRepresentation(unsigned int statebitmask){ _m_representation = statebitmask;}
      // add another representation to the state. does not clear the old
      void addRepresentationFlag(unsigned int statebitmask){_m_representation |= statebitmask;}
      // remove specified bits. other bits are untouched;
      void removeRepresentationFlag(unsigned int statebitmask){ _m_representation &= ~statebitmask;}
      unsigned int representation()const{ return _m_representation;}
      // returns the and operator between statebitmask and the currently flags set
      unsigned int hasRepresentationFlag(unsigned int statebitmask) const {return _m_representation & statebitmask;}


      // saves internal state
      // any call to save will override a previous saved stated
      void save(){_m_is_enabled_cached=_m_is_enabled; _m_representation_cached = _m_representation;}
      /// synonym for save();
      void finalise(){save();}
      // restore a previous saved state. A state can be restored an arbitrary times
      void restore(){_m_is_enabled=_m_is_enabled_cached; _m_representation = _m_representation_cached;}

      void rawData(QByteArray &data) const;

      // TODO m_parents related methods should not be part of this class.
      bool isMerge()const {return m_parents.size()>1;}

      bool hasParent(const qigOid& o) const { 
	for(auto iter = m_parents.begin(); iter != m_parents.end(); ++iter) {
	  if(*iter->get() == o) return true;
	}
	return false;
      }
      bool hasParent(const qigCommit& o) const { 
	for(auto iter = m_parents.begin(); iter != m_parents.end(); ++iter) {
	  if(o==*iter->get() ) return true;
	}
	return false;
      }
      void pushParent(const qigOid& o)
      {
	m_parents.push_back(std::make_shared<qigOid>(o));
      }

      void pushParent(const qigOidPtr&o)
      {
	m_parents.push_back(o);
      }

      void updateFromParent()
      {
	_m_internal_data=m_parents.front();
      }

      void parents(QVariantList& out)
      {
	for(const qigOidPtr& item: m_parents)
	{
	  out.push_back(QVariant::fromValue<QOid>(*item));
	}
      }


      qigLogModelData& operator=(const qigOidPtr &o)
      {
	_m_internal_data=o;
	return *this;
      }
      //qigLogModelOidExtensionPtr& data() {return _m_internal_data;}

      // WARNING may crash. using this method is valid for commits only.
      operator const qigOid& ()const{return *_m_internal_data;}

    private:
      void init(){_m_is_enabled=true; _m_representation=0;}
      bool _m_is_enabled;
      bool _m_is_enabled_cached;
      unsigned int _m_representation;
      // cache 
      unsigned int _m_representation_cached;
      qigOidPtr _m_internal_data;
      /// list of parents of this commit;
      oidVec m_parents;
    public:
      /// not used internally;
      /// can be used to store attributes;
      QVariantMap attributes;
  };
}
//#include "lol.h"
namespace qig
{

  /**@class qigLogModelDataMap
   * just an ordinary map with new accessors function
   * TODO inherit private and make method public;
   */
  class qigLogModelDataMap
  {
    public:
      typedef QMap<uint64_t, qigLogModelData > positionMap;
      //typedef lol positionMap;

    private:
    //typedef union {
    //  struct{
    //    int32_t commit_cnt;
    //    int32_t branch_cnt;
    //  } index;
    //  int64_t id;
    //} key;
    public:
      qigLogModelDataMap():m_objects_cnt(0), m_branch_cnt(0){}
      typedef int64_t key_type;
      //mapped to branch column
      bool contains (int row, int column){return m_map.contains(toKey(row, column));}
      // mapped to row column
      positionMap::iterator insert (int branch, int commit, const qigLogModelData& data){
	if(data.hasRepresentationFlag(1)) ++m_objects_cnt;
	m_branch_cnt = std::max(branch+1, m_branch_cnt);
	return m_map.insert(toKey(commit, branch), data);
	//return m_map.insert( branch, commit, data);
      }

      positionMap::const_iterator find(int branch, int commit)const {return m_map.find(toKey(branch, commit));}
      positionMap::iterator begin() {return m_map.begin();}
      positionMap::iterator end() {return m_map.end();}
      positionMap::const_iterator begin()const {return m_map.begin();}
      positionMap::const_iterator end()  const {return m_map.end();}
      void clear() {return m_map.clear();}

      /** returns the n-th commit.
       * equal to find(int branch, int commit) where branch is the value that contains the commit
       */
      const qigLogModelData* commitByIndex(int nth ) const;

      int storedObjectsCounter() const {return m_objects_cnt;}
      int branchCount() const {return m_branch_cnt;}
    private:
      positionMap m_map;
      static uint64_t toKey(int32_t col, int32_t row){ uint64_t dum= col | ((uint64_t) row <<32); return dum;}
      int m_objects_cnt;
      int m_branch_cnt;
  };

  //}}}

  /**@ingroup Application
   * @class qigLogModelHistoryWorker
   * @brief traverse the git repository and build up a mapping from a commit graph to a QTableModel Matrix.
   * the class uses the terminology branch as rows in the QTableModel and commits as columns respectively.
   * This also means that a branch is no directly related to a git-branch. A branch is more a consecutive list of parallel commits
   * between a fork and a merge. After the merge in git a new branch may be created for a developing new features but for this class
   * it is not relevant whether a new branch was born or the old one was recycled.
   */
  class qigLogModelHistoryWorker
  {
    friend class LogModelHistoryWorkerMock;
    struct _helper
    {
      _helper(){memset(this,0,sizeof(_helper));}
      /** @brief if in addObject pushed oid is a fork or merge this value holds the number of 
       * created/deleted branches that placed between the added oid of the new branch and `current`
       */
      int shift_width;
      bool hasFlag(int f) const { return flags&f;}
      bool addFlag(int f) { return flags|=f;}
    private:
      /** @brief internal flag
       */
      int flags;
    };
    enum internalFlags
    {
      /// entry is duplicate
      isDuplicate     =	1,
      /// already visited/processed
      visited	    = isDuplicate <<1,
    };

    struct value_type
    {
      /** data relevant for the model comes here
       */
      qigLogModelData current;

      // @internal private data relevant for building the model comes here

      _helper helper;
    };
    public:
    enum state
    {
      commit = 1,            // draws a normal commit with one parent and one child               //0x0001
      fork   = commit << 1,  // in case of octupus merge					  //0x0002
      forkStart = fork <<1,  // commit with 2 children. Can be ORed (operator |) with merge	  //0x0004
      //forkStartAndCommit = forkStart | commit,  // a fork is always a commit                      //0x0005
      forkEnd = forkStart<<1,// for each forkStart should also there should be also a mergeEnd	  //0x0008
      merge = forkEnd << 1,  //									  //0x0010
      mergeStart = merge <<1,// commit with 2 parents.						  //0x0020
      //mergeStartAndCommit = mergeStart | commit,// a merge commit is always a commit.		  //0x0050
      mergeEnd = mergeStart <<1,//								  //0x0040
      connection = mergeEnd <<1, // simple connection. no commit				  //0x0080
      shiftOut = connection <<1, // in case we want to shift a hole branch			  //0x0100
      shiftOutStart = shiftOut <<1, // in case we want to shift a hole branch			  //0x0200
      shiftOutEnd = shiftOutStart <<1, // in case we want to shift a hole branch		  //0x0400
      shiftIn  = shiftOutEnd << 1,                                                                //0x0800
      shiftInStart =shiftIn  <<1,                                                                 //0x1000
      shiftInEnd   = shiftInStart <<1,                                                            //0x2000
      internal_visited = shiftInEnd <<1
    };
    typedef QList<qigOid> oidList;
    typedef QLinkedList<value_type> currentGraphList;
    //typedef QList<qigLogModelData*> currentGraphList;
    public:
      qigLogModelHistoryWorker(const qigBase* base);
      ~qigLogModelHistoryWorker();

      /** abort current evaluation of history and stops the thread;
       * blocks the current thread till worker thread finishes
       */
      void abort();
      /** where to start the traverse of the graph
       * method can be called more often.
       *
       * This function is not thread safe. Do not call this function when Worker is running
       */
      void pushStart(const qigOid& commitish){m_start_commitishes.append(commitish);}
      void pushStart(const qigCommit cmomitish);

      /* set maximum nodes to visit
       * traverse will visit maximal max_entries commits.
       */
      //void setHistoryCount(unsigned int max_entries){m_max_history=max_entries;}

      /** start to traverse the graph with parameter set by...
       */
      void start();
      /** returns true if iteration is over
       * return value is undefined when has not start jet.
       */ // TODO maybe use an std::atomic_bool
      bool hasFinished(){return !m_iteration_over_flag.test_and_set(std::memory_order_acquire);}

      /// returns the result of the graph traverse
      /// result is the branch counter and the modelpos to oid map
      qigLogModelDataMap result(){return  m_model_pos_map; }
       
      /**
       * sets the maximal number of objects to visit while traversing the tree;
       * defaults to UINT_MAX if not set explicitly;
       */
      void setMaxObject(uint32_t val) { m_max_history = val;}

    private:
      void run();
      static void srun(qigLogModelHistoryWorker *_this) {_this->run();}

      /**
       * prepare the object for the traverse.
       */
      void prepareInternalState();

      void processUnrelatedConnections();

      /** callback for qigIDataFiller
       *
       * updates member m_oid_leaves.
       * adds a new entry to m_oid_leaves if id has has more than one parent.
       * also adds id to m_oid_list
       */
      int addObject(const qigOid &id);

      /** callback for qigIDataFiller
       * clear the iteration flag.
       */
      void iterationOver(){m_iteration_over_flag.clear(std::memory_order_release); }

      /** @brief stores current graph to m_model_pos_map
       * meanwhile also adds connection flag if node hasn't been visited jet;
       */
      void storeResult();

      uint32_t m_objects_cnt;
      uint32_t m_max_history;

      // iteration in progress when flag is set;
      std::atomic_flag m_iteration_over_flag;
      // flag whether iteration should abort.
      // normally set but cleared if abort is called
      std::atomic_flag m_abort;

      std::thread m_thread;


      ///List of objects that represents the current history
      // the list holds the most recent oids for all active branches. each time a new oid is pushed
      // the list updates itself by replacing the commits which parents is the pushed oid with the pushed oid and afterwards replacing duplicates.
      // after each cycle the list copies itself to m_model_pos_map.
      //
      // in the example below the list would contain HEAD (at the end of addObject)
      // if HEAD would have been pushed.
      // |HEAD|-- A -- B
      // |  \ |- - -- - -- C
      // m_current_graph would contain the oid of HEAD as entry and an empty entry that is the parent
      // of C
      //
      //
      // other example C being pushed:
      // HEAD -- A -- -|-- C |- D
      //    \ -- - -- B|-/  \|- - -- E
      //
      // at the BEGINNING of addObject the m_current_graph would have to entries A and B
      // at the end of addObject m_current_graph has 2 entries C and an empty entry;
      //
      //
      currentGraphList m_current_graph;


      qigLogModelDataMap m_model_pos_map;

      /// TODO property of im_here_to_avoid_allocation_of_memory_to_often_and_to_avoid_static_variables;
      /// ensures that im_here_to_avoid_allocation_of_memory_to_often_and_to_avoid_static_variables has a capacity of a least n objects
      void ensureAllocation(int n);
      QVector<qigOid> im_here_to_avoid_allocation_of_memory_to_often_and_to_avoid_static_variables;
      QList<qigOid> m_start_commitishes;
      QList<qigOid> m_hide_comitishes;
      const qigBase* m_base;

      /** @brief updates parents the pushed item
       * updates qigLogModelData::m_parents for the pushed oid;
       *
       * this function expects that first_occurrence is set in p and set the parents_count if p is a merge commit
       *
       * @param repr the representation to update
       * @param ci the new ci that is pushed in addObject
       * @param oid the oid that belongs to ci. the oid is the same oid as ci.oid would return
       * @param p the the struct contains accumulated information of ci
       * @return 0 on success or GIT_ERROR otherwise
       */
      static int addParents(pushedCommitProperties& p, currentGraphList& repr, const qigCommit& ci);
      /** sets the representation flags to the @a current item in repr if commit is a merge.
       * the function does not add any items to repr even if ci is a commit. Instead the function
       * expects that all parents will be next to commit in repr. (as addParents does)
       * the function furthermore does not reset the representation flags if any are set;
       *
       * @param props a struct that describes the commit
       * @param m_current_graph the representation to update
       */
      static void detectAndProcessForks(const pushedCommitProperties& props, currentGraphList& m_current_graph);
      /** @brief removes double entries from the list
       */
      static int consolidate(currentGraphList&);

      /** @brief updates parents of the previous run and process merges.
       * Updates parents of each entry in @a repr by moving the oids from entry.parents to @a repr.
       * If an item contains more than one parent the all except of one parent is moved to @a repr.
       * If an parents item is @a ci it is replaced by its parent and @a ci s parents are adjusted.
       * @param p the property stuct that describes the pushed oid
       * @param repr the list that describes the current state 
       * @param ci the currently pushed oid
       */
      static void updateCurrentOid(pushedCommitProperties& p,currentGraphList& repr, const qigCommit& ci);
  };

}
