#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <string>
#include "gitTypes.h"
namespace qig
{
  class  qigString:public std::basic_string<qigChar>
  {
    public:
      qigString(){}
      qigString(const qigChar*s):basic_string(s){}
      qigString(const std::basic_string<qigChar> &s):basic_string(s){}
  };
}

