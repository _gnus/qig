#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */

#include <new>
#include "gitTypes.h"
#include "Base.h"


namespace qig
{
  class qigOid;
  class qigObject;
  
  /**@class qigReference
   * wraps the reference protocol of libgit2.
   * a reference is a pointer to an object. for example a branch or HEAD
   */
  class qigReference
  {
    friend class qigReferenceManager;
    public:
      qigReference();
      qigReference(const qigReference&) = delete;
      qigReference(qigReference&& o);
      virtual ~qigReference();

      /**returns name of the reference;
       */
      virtual const qigChar* name() const;
      /** resolves the git oid (the sha1 sum) the reference points to.
       * but does not look up the object itself
       * @param id [out] the id to fill
       * @return 0 on success
       */
      int target(qigOid& id) const;

      /** resolves the git oid (the sha1 sum) the reference points to
       * this method also allocates the object
       *
       * @param id [out] the id to fill
       * @return 0 on success
       */
      int target(qigObject& id) const;

      virtual bool isValid() const {return reference() != nullptr;}

      bool operator==(const qigOid & other)const;
      bool operator==(const qigReference & other)const;
    protected:
      /** creates an reference from the git_reference ref;
       * the ctor steals the ownership to ref
       */ 
      qigReference(git_reference* ref);
      /* retunrs pointer to member
       * do not pass the returned pointer to git_oid free
       * @sa reference()
       */
      //operator git_reference*(){return _m_reference;}
      qigReference& operator=(git_reference*);
      /** retunrs pointer to member
       * do not pass the returned pointer to git_oid free
       * @sa operator git_reference()
       */
      const git_reference* reference()const {return _m_reference;}


      bool operator==(const git_oid& id)const;
    private:
      void release();
      git_reference* _m_reference;
  };
}
