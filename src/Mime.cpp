#include <QString>
#include "Mime.h"


namespace qig
{
  static const QString _oid ("application/oid");
  static const QString object_type ("application/object-type");
  //static const QString object_commit ("application/object-commit");
  //static const QString object_tree ("application/object-tree");
  //static const QString object_blob ("application/object-blob");
  //static const QString object_tag ("application/object-tag");
  /// returns "application/object-type"
  const QString& mimeOid()
  {
    return _oid;
  }
  /// returns "application/object-type"
  const QString& mimeObjectType()
  {
    return object_type;
  }
  ///// returns "application/object-commit"
  //const QString& mimeObjectCommit()
  //{
  //  return object_commit;
  //}
  ///// returns "application/object-tree"
  //const QString& mimeObjectTree()
  //{
  //  return object_tree;
  //}
  ///// returns "application/object-blob"
  //const QString& mimeObjectBlob()
  //{
  //  return object_blob;
  //}
  ///// returns "application/object-tag"
  //const QString& mimeObjectTag()
  //{
  //  return object_tag;
  //}
}
