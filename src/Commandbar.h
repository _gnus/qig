// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
/// @defgroup Application Application

#include <QLineEdit>

namespace qig
{
  /** @ingroup Application
   * @brief once will be a bar to enter commands
   */
  class qigCommandbar: public QLineEdit
  {
    public:
      qigCommandbar();

    protected:
      virtual void keyPressEvent(QKeyEvent*) override;





  };
}
