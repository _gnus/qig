// vim: fdm=marker
#include <assert.h>
#include <QDebug>
#include <QDataStream>
#include <QApplication>
#include <QStyle>
#include <QStringBuilder>

#include "WorkDirModel.h"
#include "fileEntry.h"

static_assert( (int) qig::modelRoles::roleFilePath == (int) QFileSystemModel::FilePathRole, "forward declaration of value of FilePathRole should be the same");

#define STATUS_LOOKUP 1
namespace qig
{
  //{{{ static helper function
  /*
  static void statusLookUp(const statusResult& m_status, const QString& child_path, int& status)
  {
    status = fileStateUndefined;
    auto status_iter = m_status.find(child_path);
    if( status_iter != m_status.end()) {
      status = status_iter.value();
    //  qDebug() << "WorkDirModel::statusLookUp: child_path" << child_path << "has status" << status;
    }
    //else {
    //  qDebug() << "WorkDirModel::statusLookUp: child_path" << child_path << "not found" << m_status.size();
    //}
  }
  */
  //}}}

  //{{{ ctor, dtor
  qigWorkDirModel::qigWorkDirModel(QObject *parent):
    QFileSystemModel(parent)
  {
    connect(this, SIGNAL(directoryLoaded(const QString&)), SLOT(updatedEntries(const QString&) ) );
    setResolveSymlinks(false);
    connect(this, &QFileSystemModel::rowsAboutToBeInserted, this, &qigWorkDirModel::onRowsAboutToBeInserted);
    connect(this, &QFileSystemModel::rowsInserted, this, &qigWorkDirModel::onRowsInserted);
    connect(this, &QFileSystemModel::rowsAboutToBeRemoved, this, &qigWorkDirModel::onRowsAboutToBeRemoved);
    connect(this, &QFileSystemModel::rowsRemoved, this, &qigWorkDirModel::onRowsRemoved);

  }

  qigWorkDirModel::~qigWorkDirModel()
  {
    //m_files.begin()->second->dump();
  }
  //}}}

  //{{{ public methods
  QVariant qigWorkDirModel::data(const QModelIndex& index, int role) const
  {
    switch (role)
    {
      case Qt::DecorationRole:
	if( index.column() == 0 && index.row() >=0 ) {
	  return dataDecoration(index);
	  break; //< anyway
	}
      case modelRoles::roleOid:
	{
	  //return dataBlobOid(index);
	  //break; //< anyway
	}
      case modelRoles::roleFileState:
	{
	  return QVariant( currentState(index) );
	  break; //< anyway
	}
      case Qt::ToolTipRole:
	switch(currentState(index))
	{
	  case fileStateUnchanged         : return QString("fileStateUnchanged");
	  case fileStateIndexNew          : return QString("fileStateIndexNew");
	  case fileStateIndexModified     : return QString("fileStateIndexModified");
	  case fileStateIndexDeleted      : return QString("fileStateIndexDeleted");
	  case fileStateIndexRenamed      : return QString("fileStateIndexRenamed");
	  case fileStateIndexTypeChange   : return QString("fileStateIndexTypeChange");
	  case fileStateWDNew             : return QString("fileStateWDNew");
	  case fileStateWDModified        : return QString("fileStateWDModified");
	  case fileStateWDDeleted         : return QString("fileStateWDDeleted");
	  case fileStateWDTypeChange      : return QString("fileStateWDTypeChange");
	  case fileStateWDRenamed         : return QString("fileStateWDRenamed");
	  case fileStateWDUnreadable      : return QString("fileStateWDUnreadable");
	  case fileStateIgnored           : return QString("fileStateIgnored");
	  case fileStateConflicted        : return QString("fileStateConflicted");
	  case fileStateUndefined:
	  default:
	     return QString("other:");
	}
	break;
      case modelRoles::roleIsDir:
	return QVariant( isDir(index) );
	break; //< anyway
      case modelRoles::roleFilePathRelative:
	return dataFilePathRelative(index);
	break; //< anyway
      case Qt::CheckStateRole:
	return dataCheckState(index);
	break; //< anyway
      default:
	return QFileSystemModel::data(index, role);
    }
    return QVariant();
  }

  bool qigWorkDirModel::setData(const QModelIndex& index, const QVariant& var, int role)
  {
    if(role == Qt::CheckStateRole) {
      auto iter=m_files.find(toFileKey(index));
      if(iter != m_files.end()) {
	iter->second->setCheckState(var.value<Qt::CheckState>());

	// update the tree;
	// updating the checkstate may cause global changes. update the complete tree;
	emit dataChanged(QModelIndex(), QModelIndex(), QVector<int>(1,role));
	return true;
      }
    }
    return false;
  }

  void qigWorkDirModel::setStatus(qigStatusPtr&& p)
  {
    m_file_system_status=std::move(p);
    if(m_file_system_status) {
      m_file_system_status->updateStatus();
    }
  }

  void qigWorkDirModel::updateStatus(const QStringList& files)
  {
    assert(m_file_system_status);
    m_file_system_status->updateStatus();
    for(const QString& file : files)
    {
      unsigned int st;
      if(statusLookUp(file,st)==GIT_OK)
      {
	qDebug() << rootPath();
	auto iter = m_files.find(rootPath() %"/"% file);
	if(iter != m_files.end())
	{
	  iter->second->setStateFlag(st);
	}
      }
    }

  }

  void qigWorkDirModel::setRootPath(const QString& path)
  {
    m_files.clear();

    QFileSystemModel::setRootPath(path);
    auto m_root = QFileSystemModel::index(path);
    m_root_path_length = path.size();
    if(!path.endsWith(L'/')) {
      //also use the tailing '/' in length
      ++m_root_path_length;
    }
    m_files.insert(
	fileMap::value_type(
	  toFileKey(m_root), toPtr(new fileEntry())
	  )
	);
  }
  //}}}

  //{{{ protected methods

  void qigWorkDirModel::resetInternalData()
  {
    beginResetModel();
    m_files.clear();
    endResetModel();
    m_file_system_status.reset();
    m_root_path_length=0;
  }
  bool qigWorkDirModel::insertRows(int row, int count, const QModelIndex &parent)
  {
    //qDebug() << Q_FUNC_INFO;
    return QFileSystemModel::insertRows(row,count,parent);
  }
  bool qigWorkDirModel::insertColumns(int column, int count, const QModelIndex &parent)
  {
    //qDebug() << Q_FUNC_INFO;
    return QFileSystemModel::insertColumns(column,count,parent);
  }
  //}}}
  
  //{{{ private slots
  //
  // add children of path to internal map already if
  // - path is not .. or . 
  // - child is not .. or .
  // - child not in internal map m_files already
  void qigWorkDirModel::updatedEntries(const QString &path)
  {
    if( path.endsWith(QLatin1String("/.."), Qt::CaseSensitive) || path.endsWith(QLatin1String("/."), Qt::CaseSensitive) ) {
      return;
    }
    const QModelIndex& parent_index = index(path);
    auto iter_parent = m_files.find(path);
    if ( parent_index.isValid() && iter_parent!= m_files.end() ) 
    {
      fileEntryPtr& parent_entry = iter_parent->second;
      int row_cnt = rowCount(parent_index); 
      for( int i=0; i<row_cnt; ++i)
      {
	
	{
	  const QModelIndex& child = parent_index.child(i,0);
	  const QVariant& dum = child.data(QFileSystemModel::FilePathRole);
	  const QString& child_path = dum.toString();

	  unsigned int status;

	  // do not add .. or .
	  if( !(child_path.endsWith(QLatin1String("/.."), Qt::CaseSensitive) || child_path.endsWith(QLatin1String("/."), Qt::CaseSensitive) ) )
	  {
#if STATUS_LOOKUP
	    // find the current state of child_path
	    if( statusLookUp(toStatusKey(child_path), status) !=0) {
	      // if lookup fails mark file or most probably directory as unchanged.
	      status=0;
	    }
#else
	    status=0;
#endif
	    fileMap::iterator iter_child ;
	    // do not add path if it already exists;
	    if( (iter_child = m_files.find(child_path)) == m_files.end() ) 
	    {
	      fileEntryPtr child_entry = toPtr(new fileEntry());

	      // pushChild implicitly mark parent_entry as parent of child_entry
	      parent_entry->pushChild( child_entry.get());


	      // TODO track state changes and inform view about changes
	      child_entry->setStateFlag(status);
	      m_files.insert(fileMap::value_type( child_path, std::move(child_entry) ) );
	    }
	    else
	    { // entry already exists just update the state_flag
	      unsigned int current_state = iter_child->second->stateFlag();
	      if( current_state  != status)
	      {
		//qDebug() << "qigWorkDirModel::updatedEntries: changing fileState from "<< iter_child->second->stateFlag() << "to "<< status;
		switch(status)
		{
		  case fileStateUnchanged:
		    iter_child->second->removeStateFlag(current_state);
		    break;
		  case fileStateUndefined:
		  default:
		    iter_child->second->setStateFlag( (fileState) status);
		}
		emit dataChanged(child,child, QVector<int>(1, Qt::DecorationRole));
	      }
	    }
	    // triggers FilesystemModel to load child
	    if(isDir(child) && canFetchMore(child)){
	      fetchMore(child);
	    }
	  }
	}
      }
    }
  }
  void qigWorkDirModel::onRowsAboutToBeInserted(const QModelIndex &parent, int first, int last)
  {
    Q_UNUSED(parent);
    Q_UNUSED(first);
    Q_UNUSED(last);
    //qDebug() << Q_FUNC_INFO << parent.data();
  }
  void qigWorkDirModel::onRowsInserted(const QModelIndex &parent, int first, int last){
    Q_UNUSED(parent);
    Q_UNUSED(first);
    Q_UNUSED(last);
    //qDebug() << Q_FUNC_INFO << parent.data();
  }

  void qigWorkDirModel::onRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last)
  {
    Q_UNUSED(parent);
    Q_UNUSED(first);
    Q_UNUSED(last);
    //qDebug() << Q_FUNC_INFO;
  }
  void qigWorkDirModel::onRowsRemoved(const QModelIndex &parent, int first, int last)
  {
    Q_UNUSED(parent);
    Q_UNUSED(first);
    Q_UNUSED(last);
    //qDebug() << Q_FUNC_INFO;
  }
  void qigWorkDirModel::onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &)
  {
    qDebug() << topLeft << bottomRight;
  }
  //}}}

  //{{{ private methods
  unsigned int qigWorkDirModel::currentStateFlag(const QModelIndex& index) const
  {
    auto iter=m_files.find(toFileKey(index));
    if(iter != m_files.end())
    {
      return iter->second->stateFlag();
    }
    // most probably not part of git repo
    // is ignored ok, or should we introduce another state flag
    return fileStateIgnored;
  }
  fileState qigWorkDirModel::currentState(const QModelIndex& index) const
  {
    fileState ret = fileStateUndefined;
    //auto iter=m_files.find(toFileKey(index));
    //if(iter != m_files.end())
    {
      // first test if file is unchanged
      unsigned int state = currentStateFlag(index);
      if( state == fileStateUnchanged ) {
	ret = fileStateUnchanged;
      } else {
	// order determines priority. most recent has highest priority
	fileState stateEntries[] = { 
	  fileStateConflicted,
	  fileStateIndexModified,
	  fileStateWDModified,
	  fileStateIndexRenamed,
	  fileStateWDRenamed,
	  fileStateIndexNew,
	  fileStateWDNew,
	  fileStateIndexDeleted,
	  fileStateWDDeleted,
	  fileStateIgnored
	};
	for(size_t i=0U; i<sizeof(stateEntries)/sizeof(stateEntries[0])  ; ++i)
	{
	  if(state & (stateEntries[i] ) ) {
	    ret = stateEntries[i];
	    break;
	  }
	}
      }
    }
    return ret;
  }

  QVariant qigWorkDirModel::dataFilePathRelative(const QModelIndex& index) const
  {
    const QString& absPath = QFileSystemModel::data(index, FilePathRole).toString();
    // currently toStatusKey returns the relative path
    return toStatusKey(absPath);
  }

  QVariant qigWorkDirModel::dataCheckState(const QModelIndex& index) const
  {
    auto iter=m_files.find(toFileKey(index));
    if(iter != m_files.end())
    {
      return iter->second->checkState();
    }
    // most probably not part of git repo
    // is ignored ok, or should we introduce another state flag
    return Qt::Unchecked;
  }

  int qigWorkDirModel::statusLookUp(const QString& child_path, unsigned int& status)
  {
    if(m_file_system_status)
      return m_file_system_status->statusFileFast(status , child_path.toLocal8Bit().constData());
    return -1;
  }

  inline const qigWorkDirModel::fileMap::key_type qigWorkDirModel::toFileKey(const QModelIndex& index)
  {
    return index.data(QFileSystemModel::FilePathRole).toString();
  }

  const qigWorkDirModel::fileMap::key_type qigWorkDirModel::toStatusKey(const QString& str) const
  {
    QString dum = str.right(str.size() - m_root_path_length);
    return dum;
  }

  void qigWorkDirModel::updateCache()
  {
    for(auto iter=m_files.begin(); iter!= m_files.end(); ++iter)
    {
      if(isDir(index(iter->first))) {
	qDebug() << Q_FUNC_INFO << iter->first;
	updatedEntries(iter->first);
      }
    }
  }
  //}}}

  //{{{ private static
  inline qigWorkDirModel::fileEntryPtr qigWorkDirModel::toPtr(fileEntry *p)
  {
    return fileEntryPtr(p, releaseFileEntry);
  }

  void qigWorkDirModel::releaseFileEntry(fileEntry*p)
  {
    if(p) delete p;
  }
  //}}}

  //{{{ helper class qigFileIconProvider
  static Q_CONSTEXPR const QSize iconSize() { return QSize(32, 32);}
  static  const QMatrix scale() { return QMatrix( 0.50, 0, 0, 0.50, 0, 0 );}

  QIcon buildIcon(const QIcon& b, const QIcon& st)
  {
    QPixmap base_pix = b.pixmap(iconSize());
    QPixmap state_pix=st.pixmap(iconSize());

    QImage base = base_pix.toImage();
    QImage state = state_pix.toImage();
    state = state.transformed(scale(), Qt::SmoothTransformation) ;

    // by: the line to modify;
    // for each iteration over a line by is incemented by one 
    int by = base.height() - state.height();
    for (int sy =0; sy < state.height(); ++sy)
    {
      for(int sx = 0; sx < state.width(); ++ sx)
      {
	base.setPixel(sx, by, state.pixel(sx, sy));
      }
      ++by;
    }
    return QIcon(QPixmap::fromImage(base));
  }

  qigFileIconProvider::qigFileIconProvider()
  {
    QStyle* style=qApp->style();

    QIcon dir = style->standardIcon(QStyle::SP_DirIcon);
    QIcon file = style->standardIcon(QStyle::SP_FileIcon);
    // build dir modified
    QIcon mod = style->standardIcon(QStyle::SP_MessageBoxWarning);
    QIcon dirModified=buildIcon(dir, mod);
    m_iconMap.insert({Folder, fileStateWDModified}, dirModified);
    m_iconMap.insert({Folder, fileStateIndexModified}, dirModified);

    // build file modified
    QIcon fileModified = buildIcon(file, mod);
    m_iconMap.insert({File, fileStateWDModified}, fileModified);
    m_iconMap.insert({File, fileStateIndexModified}, fileModified);

    // bulid dir new
    QIcon _new = style->standardIcon(QStyle::SP_MessageBoxQuestion);
    QIcon dirNew=buildIcon(dir, _new);
    m_iconMap.insert({Folder, fileStateWDNew}, dirNew);
    m_iconMap.insert({Folder, fileStateIndexNew}, dirNew);

    // bulid file new
    QIcon fileNew=buildIcon(file, _new);
    m_iconMap.insert({File, fileStateWDNew}, fileNew);
    m_iconMap.insert({File, fileStateIndexNew}, fileNew);

    // bulid dir deleted 
    QIcon del = style->standardIcon(QStyle::SP_MessageBoxInformation);
    QIcon dirDel=buildIcon(dir, del);
    m_iconMap.insert({Folder, fileStateWDDeleted}, dirDel);
    m_iconMap.insert({Folder, fileStateIndexDeleted}, dirDel);
    // bulid file deleted
    QIcon fileDel=buildIcon(file, del);
    m_iconMap.insert({File, fileStateWDDeleted}, fileDel);
    m_iconMap.insert({File, fileStateIndexDeleted}, fileDel);

    // bulid dir unmodified
    QIcon unmod = style->standardIcon(QStyle::SP_DialogOkButton);
    QIcon dirUnmod=buildIcon(dir, unmod);
    m_iconMap.insert({Folder, fileStateUnchanged}, dirUnmod);
    // bulid file unmodified
    QIcon fileUnmod=buildIcon(file, unmod);
    m_iconMap.insert({File, fileStateUnchanged}, fileUnmod);
  }


  QIcon qigFileIconProvider::icon(IconType type, fileState state) const
  {
    auto e = m_iconMap.cend();
    iconMap::const_iterator iter = m_iconMap.find( {type, state} );
    return ( iter !=e ? iter.value() : QFileIconProvider::icon((QFileIconProvider::IconType)type) );
  }
  //}}}
}
