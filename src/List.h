#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include<list>

namespace qig
{
  template<typename T>
  class qigList: public std::list<T>
  {
    public:
      qigList():std::list<T>(){}
      // coping list is clumsy
      qigList(const qigList&)=delete;
  };
}
