#include <cassert>
#include <QAbstractListModel>

#include <Error.h>

#include <git2/errors.h>


namespace qig
{

struct WarningEntry
{
  WarningEntry():category(-1){}
  WarningEntry(int c, const QString& w):category(c),warning(w){}
  int category;
  QString warning;
};

class WarningModel:public QAbstractListModel
{
  typedef QList<WarningEntry> warningList;
  public:
    WarningModel(){};

    virtual int rowCount(const QModelIndex&) const{return m_warning_list.size();}
    virtual QModelIndex index(int r, int c, const QModelIndex&) const {
      return createIndex(r,c,nullptr);
    }

    void addWarning(WarningEntry && w)
    {
      int where = m_warning_list.size();
      beginInsertRows(QModelIndex(), where, where);
      m_warning_list.push_back(std::move(w));
      endInsertRows();
      // TODO remove row
    }
    QVariant data(const QModelIndex& index, int role) const {
      if(index.isValid())
	return QVariant();
      assert(index.row() < m_warning_list.size());
      switch(role)
      {
	case Qt::DisplayRole:
	  return m_warning_list[index.row()].warning;
      }
      return QVariant();
    }
    int entryCount() const {return m_warning_list.size();}

  private:
    warningList m_warning_list;
};

  qigErrorHandler::qigErrorHandler()
    : m_error(new QString())
    , m_warning_model(new WarningModel())
  {
  }

  qigErrorHandler::~qigErrorHandler()
  {
  }

  const QString& qigErrorHandler::getErrorText() const
  {
    return *m_error;
  }

  void qigErrorHandler::clearErrorText() 
  {
    m_error->clear();
  }

  void qigErrorHandler::setErrorText(const QString&e)
  {
    *m_error = e;
  }

  void qigErrorHandler::addWarning(int category, const QString& text)
  {
    m_warning_model->addWarning(WarningEntry(category, text));
  }

  size_t qigErrorHandler::size() const
  {
    return m_warning_model->entryCount();
  }
}

namespace qig
{
  static qigErrorHandler s_handler;

  void setError(const char* c){ setError(QString(c));}

  void setError(const QString& c) { s_handler.setErrorText(c);}

  const QString& getError() { return s_handler.getErrorText();}

  void addWarning(int c, const char*t) { return addWarning(c, QString(t));}
  void addWarning(int c, const QString&t) { return s_handler.addWarning(c,t);}

  void errorFromGitError() {
    const git_error *err = giterr_last();
    if(err){
      setError( err->message );
      giterr_clear();
    }
  }
}
