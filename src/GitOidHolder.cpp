#include <stdlib.h>
#include <string.h>// for memcmp
#include <git2.h>
#include <assert.h>

#include "GitOidHolder.h"
#include "Base.h"


namespace qig
{
#define SHA1_SIZE 20

  //static git_otype toGitType(oid::type t){return (git_otype) t;}

  qigGitOidHolder::qigGitOidHolder():_m_oid(new git_oid),_m_repo(nullptr)
  {
  }

  qigGitOidHolder::qigGitOidHolder(const git_oid* p, git_repository* p_repo):
    _m_oid(new git_oid),
    _m_repo(p_repo)
  {
    memcpy(_m_oid->id, p->id, SHA1_SIZE);
  }

  qigGitOidHolder::qigGitOidHolder(const qigGitOidHolder&o ):
    _m_oid(new git_oid),
    _m_repo(o._m_repo)
  {
    memcpy(_m_oid->id, o._m_oid->id, SHA1_SIZE);
  }

  qigGitOidHolder::qigGitOidHolder(qigGitOidHolder&&o ):
    _m_oid(o._m_oid),
    _m_repo(o._m_repo)
  {
    o._m_oid=0;
    o._m_repo=0;
  }

  qigGitOidHolder::~qigGitOidHolder()
  {
    if(_m_oid)
      delete _m_oid; 
    // the repo does not belong to us 
  }

  void qigGitOidHolder::load(const git_oid *o, git_repository* p)
  {
    //if(*this == *o) return;
    assert(o);
    /// tell the classes that inherit from GitOidHolder that the oid becomes invalid
    reset();
    set(o, p);
    // and tell everyone that a new oid is available
    reloadFromId();
  }

  void qigGitOidHolder::fromRaw(const unsigned char* data, git_repository *p)
  {
    reset();
    _clear();
    git_oid_fromraw(_m_oid, data);
    _m_repo=p;
    reloadFromId();
  }

  qigGitOidHolder& qigGitOidHolder::operator=(qigGitOidHolder&& oid)
  {
    // swap oids. otherwise we would have to delete the old one
    if(_m_oid) {
      delete _m_oid;
    }
    _m_oid = oid._m_oid;
    _m_repo = oid._m_repo;
    // also reset the oid;
    oid._m_oid=nullptr;
    oid._m_repo=nullptr;
    // and reload
    reloadFromId();
    return *this;
  }

  void qigGitOidHolder::_clear()
  {
    //delete _m_oid;
    //if(_m_repo){
    //  git_repository_free(_m_repo);
      _m_repo=0;
   // }
  }
  void qigGitOidHolder::set(const git_oid*p, git_repository* r)
  {
    _clear();
    memcpy(_m_oid->id, p->id, SHA1_SIZE);
    _m_repo=r;
  }

  git_repository* qigGitOidHolder::owner() const
  {
    return const_cast<git_repository*>(_m_repo);
  }
  bool qigGitOidHolder::operator==(const git_oid& id) const {
    return !memcmp(this->_m_oid->id, id.id, GIT_OID_RAWSZ);
  }

}
