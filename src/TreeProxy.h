#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */

#include <QAbstractProxyModel>

class QFileSystemModel;

namespace qig
{
  class qigTreeProxy: public QAbstractProxyModel
  {
    Q_OBJECT
    public:
      qigTreeProxy(QObject*parent);

      // insert a parent model index also to its list of childs
      // can be used to render '..' and '.' in the TreeView
      void setAddParentToChild(bool add_parent_to_child){m_add_parent_child=add_parent_to_child;}
      bool addParentToChild(){return m_add_parent_child;}//<require namerefacturing
      /** sets the new root. the proxy filters out any index which parent is not new_root
       * if new root is invalid. the proxy does nothing. and simply forwards any model index
       * form model to view and vice versa
       * @param new_root the new root;
       * @return the old root. can be an invalid index.
       */
      QModelIndex setRoot(const QModelIndex& new_root);

      virtual void setSourceModel(QAbstractItemModel*) override;
      QModelIndex mapToSource(const QModelIndex& proxyIndex) const override;
      QModelIndex mapFromSource(const QModelIndex& sourceIndex) const override;
      QVariant data(const QModelIndex& proxyIndex, int role = Qt::DisplayRole ) const override;
      int rowCount(const QModelIndex& parent) const override;
      int columnCount(const QModelIndex& parent) const override;
      QModelIndex parent(const QModelIndex& proxyIndex) const override;
      QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
      virtual Qt::ItemFlags flags(const QModelIndex& parent) const override;
      virtual bool setData(const QModelIndex& index, const QVariant& data, int role=Qt::EditRole) override;


    public:
      enum Roles
      {
	root=0x110 
      };

    private Q_SLOTS:
      void reset(){beginResetModel(); endResetModel();}
      void reset(const QString&){beginResetModel(); endResetModel();}

    private:
      // in filesystem this would be '..'
      QPersistentModelIndex m_virtual_root;


      bool m_add_parent_child;

  };
}
