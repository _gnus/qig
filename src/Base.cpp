#include <git2/repository.h>
#include <git2/global.h>
#include <git2/common.h>
#include <git2/version.h>

#include "Base.h"
#include "enums.h"


namespace qig
{
  qigBase::qigBase(git_repository* p_repo)
  {
    repositoryLoaded(p_repo);
  }

  void qigBase::repositoryLoaded(git_repository *p_repo)
  {
    if(_m_repo.get()==p_repo) {
      return;
    }
    _m_repo=repositoryPtr(p_repo, git_repository_free);
  }

  int qigBase::isValid() const
  {
    return mainRepo() !=0;
  }

  void qigBase::repositoryUnloaded()
  {
    _m_repo.reset();
  }

  qigBase qigBase::load(const qigChar* path, bool open_bare, int &err)
  {
    libgit2Initialiser dummy;
    git_repository* repo;
    err = open_bare ? git_repository_open_bare(&repo, path) : git_repository_open(&repo, path);
    return qigBase( err == GIT_OK ? repo : nullptr);
  }

  /* ****************************************
   *       impl for libgit2 initialiser
   * ***************************************/
  int qigBase::libgit2Initialiser::ref_cnt=0;
  qigBase::libgit2Initialiser::libgit2Initialiser()
  {
    int major,minor,path;
    git_libgit2_version(&major, &minor, &path);
    if(major != LIBGIT2_VER_MAJOR || minor > LIBGIT2_VER_MINOR) 
    {
      throw std::exception();
    }
    // initialise libgit; before loading the repository
    if(ref_cnt++ == 0) {
      git_libgit2_init();
    }
  }
  qigBase::libgit2Initialiser::~libgit2Initialiser()
  {
    // shutdown libgit; when we are finished;
    if(!--ref_cnt) {
      git_libgit2_shutdown();
    }
  }
}
