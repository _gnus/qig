// vim: fdm=marker
#include <assert.h>
#include <QAction>
#include <QContextMenuEvent>
#include <QMenu>
#include <QPainter>
#include <QHeaderView>
#include <QDebug>

#include "LogView.h"
#include "LogModel.h" //< for enum state

#include <QApplication> //
// can't be const int due to int arithmethic
#define MERGE_CONNECTION_RATIO 3 / 4
#define FORK_CONNECTION_RATIO 1 / 4
#define COMMIT_RADIUS 6

namespace qig
{
static const int ENTRY_SIZE=24;
  QSize s_size_hint(ENTRY_SIZE, ENTRY_SIZE);
}

namespace qig
{
  //{{{ qigLogView

  qigLogView::qigLogView(QWidget*p): QTableView(p),
    actionCommit(new QAction(tr("commit"),this)),
    //actionDiffThis(new QAction(this)),
    actionMerge(new QAction(tr("merge"),this)),
    actionTag(new QAction(tr("tag"),this)),
    actionBranchOut(new QAction(tr("create branch"),this)),
    m_actions()
  {
    m_actions.reserve(6);
    m_actions.append(actionCommit);
    //m_actions.append(actionDiffThis);
    m_actions.append(actionMerge);
    m_actions.append(actionTag);
    m_actions.append(actionBranchOut);
    for(auto iter=m_actions.begin(); iter != m_actions.end(); ++iter)
    {
      addAction(*iter);
    }
    // set the item delegate. Qt should clean up the instanciated delegate by parent child relations
    // should be ok as long as we do this once
    QAbstractItemDelegate* default_delegate = itemDelegate();
    setItemDelegate(new qigLogViewDelegate(this));
    setItemDelegateForColumn(0, default_delegate);
    setItemDelegateForColumn(1, default_delegate);
    setItemDelegateForColumn(2, default_delegate);
    QHeaderView *hdr;
    hdr = horizontalHeader();
    hdr->setSectionResizeMode(QHeaderView::ResizeToContents);
    hdr->setSectionsMovable(true);
    //hdr = verticalHeader();
    //hdr->setSectionResizeMode(QHeaderView::ResizeToContents);

    setTabKeyNavigation(false);
  }

  void qigLogView::contextMenuEvent(QContextMenuEvent *p)
  {
    if(p->reason() == QContextMenuEvent::Mouse)
    {
      QMenu * menu = new QMenu(this);
      menu->setAttribute(Qt::WA_DeleteOnClose);
      for (auto iter = m_actions.begin(); iter != m_actions.end(); ++iter)
      {
	menu->addAction(*iter);
      }
      menu->popup(p->globalPos(),actionCommit);
    }
    else
    {
      p->ignore();
    }
  }

  void qigLogView::keyboardSearch(const QString& )
  {
    // Qt 5.5 impl also searches for disabled item.
    // we do not support keyboadSearch anyway
    return;
    /*
    const QModelIndex& selection = currentIndex();
    if( (selection.flags() & Qt::ItemIsSelectable) == 0)
    {
      qDebug() <<"qigLogView::keyboardSearch: detected invalid selection";
      setCurrentIndex(QModelIndex());
    }
    */
  }

  //}}} // end LogView


  //{{{ begin qigLogViewDelegate
  qigLogViewDelegate::qigLogViewDelegate(QWidget* parent): QStyledItemDelegate(parent)
  //qigLogViewDelegate::qigLogViewDelegate(QWidget* parent): QAbstractItemDelegate(parent)
  {
  }


  void qigLogViewDelegate::paint(QPainter* p, const QStyleOptionViewItem& o, const QModelIndex &i) const
  {
    p->save();
    const int x = o.rect.topLeft().x();
    const int y = o.rect.topLeft().y();
    const int dx = o.rect.bottomRight().x() - x;
    const int dy = o.rect.bottomRight().y() - y;
    float dx_over_dy = (float) dx/dy;
    float dy_over_dx = (float) dy/dx;
    // the paint function paints left to rights. 
    p->setWorldTransform(QTransform(0, dy_over_dx, dx_over_dy, 0, x-y*dx_over_dy, y-x*dy_over_dx ));
    //const QPixmap &img = i.data(Qt::DecorationRole).value<QPixmap>();
    const QVariant& brush = i.data(roleBrush);
    if(brush.isValid())
    {
      p->setBrush(brush.value<QBrush>());
    }
    //const QVariant& bg = i.data(Qt::BackgroundRole);
    //if(bg.isValid())
    //{
    //  p->setBackground(brush.value<QBrush>());
    //}
    LogIconPainter::paint(p, o.rect, i.data(qigLogModel::roleGraph).toInt());
    p->restore();
  }

  void LogIconPainter::paint(QPainter*p, const QRect& o, unsigned int repr)
  {
    if( repr & qigLogModelHistoryWorker::commit)	{ paintCommit(p, o);       } 
    if( repr & qigLogModelHistoryWorker::fork)		{ paintFork(p, o);         } 
    if( repr & qigLogModelHistoryWorker::forkStart )	{ paintForkStart(p, o);    } 
    if( repr & qigLogModelHistoryWorker::forkEnd )	{ paintForkEnd(p, o);      } 
    if( repr & qigLogModelHistoryWorker::merge)        { assert(false);           } 
    if( repr & qigLogModelHistoryWorker::mergeStart )  { paintMergeStart(p, o);   } 
    if( repr & qigLogModelHistoryWorker::mergeEnd )    { paintMergeEnd(p, o);     } 
    if( repr & qigLogModelHistoryWorker::connection)   { paintConnection(p , o);  }
    if( repr & qigLogModelHistoryWorker::shiftOut     ){ paintShiftOut(p, o);     }
    if( repr & qigLogModelHistoryWorker::shiftOutStart){ paintShiftOutStart(p, o);}
    if( repr & qigLogModelHistoryWorker::shiftOutEnd  ){ paintShiftOutEnd(p, o);  }
    if( repr & qigLogModelHistoryWorker::shiftIn     ) { paintShiftIn(p, o);      }
    if( repr & qigLogModelHistoryWorker::shiftInStart) { paintShiftInStart(p, o); }
    if( repr & qigLogModelHistoryWorker::shiftInEnd  ) { paintShiftInEnd(p, o);   }
  }

  QSize qigLogViewDelegate::sizeHint(const QStyleOptionViewItem& , const QModelIndex &) const
  {
    return s_size_hint;
  }

  //{{{ static function

  void qigLogViewDelegate::setDevicePixelRatio(qreal device_pixel_ratio)
  {
    s_size_hint = QSize(ENTRY_SIZE, ENTRY_SIZE) * device_pixel_ratio;
  }


  // simple circle
  void LogIconPainter::paintCommit(QPainter* p, const QRect& rect)
  {
    int mid_point_height = rect.height()/2;
    int mid_point_width  = rect.width()/2;
    p->drawEllipse(
	QRect(
	  rect.topLeft()+QPoint(mid_point_width - COMMIT_RADIUS, mid_point_height - COMMIT_RADIUS),
	  rect.topLeft()+QPoint(mid_point_width + COMMIT_RADIUS, mid_point_height + COMMIT_RADIUS)
	  )
	);
  }

  // simple line at half height over full width
  void LogIconPainter::paintConnection(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(0, rect.height()/2),
        rect.topLeft()+QPoint(rect.width(), rect.height()/2)
	);
  }

  // + junction
  void LogIconPainter::paintFork(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(rect.width() * FORK_CONNECTION_RATIO, 0),
        rect.topLeft()+QPoint(rect.width() * FORK_CONNECTION_RATIO, rect.height())
	);
    /*p->drawLine(
	rect.topLeft()+QPoint(rect.width() * FORK_CONNECTION_RATIO + COMMIT_RADIUS, rect.height()/2),
        rect.topLeft()+QPoint(rect.width()                        , rect.height()/2)
	);
	*/
    /*
    p->drawLine(
        rect.topLeft()+QPoint(0 , rect.height()/2),
	rect.topLeft()+QPoint(rect.width() * FORK_CONNECTION_RATIO - COMMIT_RADIUS, rect.height()/2)
	);
	*/
  }

  // end of fork. ends at first commit of new branch (not at the commit itself but but on its row
  void LogIconPainter::paintForkEnd(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(0, rect.height()/2),
        rect.topLeft()+QPoint(rect.width() * FORK_CONNECTION_RATIO, 0)
	);
  }
  // start of fork. start at commit that starts a new branch
  void LogIconPainter::paintForkStart(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(rect.width()/2, rect.height()/2),
        rect.topLeft()+QPoint(rect.width() * FORK_CONNECTION_RATIO, rect.height())
	);
  }

  void LogIconPainter::paintMergeStart(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(rect.width()/2, rect.height()/2),
        rect.topLeft()+QPoint(rect.width() * MERGE_CONNECTION_RATIO, rect.height())
	);
  }

  void LogIconPainter::paintMergeEnd(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(rect.width() * MERGE_CONNECTION_RATIO, 0),
        rect.topLeft()+QPoint(rect.width(), rect.height() / 2)
	);
  }
  void LogIconPainter::paintShiftOut(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(rect.width()/2, 0),
        rect.topLeft()+QPoint(rect.width()/2, rect.height())
	);
  }
  void LogIconPainter::paintShiftOutStart(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(0                                          , rect.height()/2),
        rect.topLeft()+QPoint(rect.width()/2, rect.height())
	);
  }
  void LogIconPainter::paintShiftOutEnd(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(rect.width()/2, 0),
        rect.topLeft()+QPoint(rect.width() , rect.height()/2)
	);
  }
  void LogIconPainter::paintShiftIn(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(rect.width()/2, 0),
        rect.topLeft()+QPoint(rect.width()/2, rect.height())
	);
  }
  void LogIconPainter::paintShiftInStart(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(rect.width()/2, 0),
        rect.topLeft()+QPoint(0                    , rect.height()/2)
	);
  }
  void LogIconPainter::paintShiftInEnd(QPainter* p, const QRect& rect)
  {
    p->drawLine(
	rect.topLeft()+QPoint(rect.width()/2, rect.height()  ),
        rect.topLeft()+QPoint(rect.width()  , rect.height()/2)
	);
  }
  //}}} end static function
  //}}} end qigLogViewDelegate
}
