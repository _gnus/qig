
#include "LogFilterList.h"
#include "Filter.h"
#include "Object.h"

namespace qig
{
  qigLogFilterList::qigLogFilterList()
  {
  }

  void qigLogFilterList::releaseFilter(qigFilter*p)
  {
    delete p;
  }
  bool qigLogFilterList::applyFilter(const qigObject& o) const
  {
    //qigFilter* p = m_filters.begin()->get()
    bool ret=true;
    for (auto iter = m_filters.begin(); iter != m_filters.end() && ret; ++iter)
    {
      ret=o.applyFilter(*(iter->get()));
    }
    return ret;
  }
  bool qigLogFilterList::isEmpty() const
  {
    bool is_empty=true;
    for (auto iter = m_filters.begin(); iter != m_filters.end() && is_empty; ++iter)
    {
      is_empty = (*iter)->isEmpty();
    }
    return is_empty;
  }
}
