#include <cassert>
#include <git2/refs.h>

#include "Reference.h"
#include "Object.h"
#include "Oid.h"

namespace qig
{
  qigReference::qigReference():
    _m_reference(nullptr)
  {
  }
  qigReference::qigReference(git_reference* ref):
    _m_reference(ref)
  {
  }
  qigReference::qigReference(qigReference&& o):
    _m_reference(o._m_reference)
  {
    o._m_reference=0;
  }
  qigReference::~qigReference() {
    release();
  }

  int qigReference::target(qigObject& obj) const
  {
    qigOid oid;
    int err = target(oid);
    if(!err) obj.load(oid);
    return err;
  }

  int qigReference::target(qigOid& id)const
  {
    int err = GIT_EINVALID;
    // TODO check for symbolic refs
    if(_m_reference) {
      err = GIT_OK;
      if(git_reference_type(_m_reference) == GIT_REF_SYMBOLIC) 
      {
	git_oid oid;
	git_repository* owner = git_reference_owner(_m_reference);
	git_reference_name_to_id(&oid, owner, git_reference_name(_m_reference));
	id.load(&oid, owner);
      }
      else 
      {
	id.load(git_reference_target(_m_reference), git_reference_owner(_m_reference));
      }
    }
    return err;
  }

  const qigChar* qigReference::name() const
  {
    return _m_reference ? git_reference_shorthand(_m_reference) : nullptr;
  }

  bool qigReference::operator==(const qigReference&) const
  {
    assert(false);
    return false;
  }
  // protected
  //{{{

  qigReference& qigReference::operator=(git_reference*ref)
  {
    release();
    _m_reference = ref;
    return *this;
  }
  //}}}

  void qigReference::release()
  {
    if(_m_reference) {
      git_reference_free(_m_reference);
    }
    _m_reference=nullptr;
  }


}
