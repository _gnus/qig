// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include <vector>
#include <string>

namespace qig
{

  /**@class treeEntry
   * simple class to build a tree structure.
   * a treeEntry does not own its children. deleting a treeEntry will not delete its children.
   */
  class treeEntry
  {
    private:
      typedef std::vector<treeEntry*> childrenList;
    public:
      treeEntry(int type):m_parent(nullptr), m_type(type){}
      treeEntry(treeEntry *parent, int type):m_parent(nullptr),m_type(type){if(parent)parent->pushChild(this);}
      /** dtor
       * unregisters @a this at parent element but does not deallocate the children
       */
      virtual ~treeEntry();
      /// returns number of children
      size_t childrenCount() const { return m_children.size(); }
      /** equivalent to testing return value of childrenCount for 0
       * @return true if entry has no children
       */
      bool hasChildren() const {return !m_children.empty();}
      /// returns position of selected entry in parents structure
      size_t locate() const;// { return parent ? parent->m_children.indexOf(this) : -1; }
      /** adds entry p to child of @a this.
       * also tells p that it has a parent @a this therefore you should not call setParent on p
       * before
       */
      void pushChild(treeEntry* p);
      /** traverse the tree an calls visit on each node
       * @param p an opaque handle that is passed to visit
       */
      void traverse(void*p){traverse(*this, p);}

      /** returns parent of @a this
       */
      const treeEntry* parent() const{ return m_parent; }
      /** returning child by index
       * warning validity of index is not checked
       * index must be therefore between 0 (included) and childrenCount(excluded)
       */
      const treeEntry* childByIndex(unsigned int index)const { return m_children[index];}
      /** remove current node from tree structure.
       * but it is subject to the user to free the object and its children.
       * after calling this function the node still knows its children but the children do not
       * know the node as parent.
       * but the object tries to remove itself from the tree structure when it is deleted
       */
      void remove();

      /** returns value set by parameter type in ctor
       */
      int type() const {return m_type;}

      /** value used as invalid pos in locate
       */
      static const size_t npos;

    protected:
      /** returns parent of @a this
       * non const overload
       */
      treeEntry * _parent() { return m_parent; }
      /** returning child by index
       * warning validity of index is not checked
       * index must be therefore between 0 (included) and childrenCount(excluded)
       */
      treeEntry*  _childByIndex(unsigned int index){ return m_children[index];}
      /** marks p as parent of this but without registering this as child of p.
       * calling this method may turn the tree in an inconsistent state
       *
       * normally this function is shouldn't be used explicitly
       * because pushChild also sets the parent.
       * @sa pushChild
       */
      void setParent( treeEntry* p) { m_parent=p;}

      /**
       * returns true if p is already child of this.
       * @param p the treeEntry to look for
       * @return true if p is child of `this`
       */
      bool isChild(treeEntry *p ) const;

      template<typename T, typename F, typename P>
	inline static void traverse(T*t, F f, P p)
	{
	  f(t,p);
	  for(treeEntry* item : t->m_children){
	    traverse(static_cast<T*>(item),f, p);
	  }
	}

    private:
      // override to vist an entry
      // default does nothing
      inline virtual void visit(void * p);
      // default visit all nodes
      static void traverse(treeEntry& entry, void *p);
      // m_parent
      treeEntry* m_parent;
      // the list of children;
      childrenList m_children;
      // the type of the entry.
      const int m_type;
  };
  void treeEntry::visit(void*) {}
}
