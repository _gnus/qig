// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include <QTableView>
#include <QList>
#include <QStyledItemDelegate>

namespace qig
{
  /**@class qigLogViewDelegate {{{
   * Delegate for qigLogView. Does the rendering of the graph based on the representation flags set
   * by qigLogModeHistoryWorker
   * the painting should respect scaling for high DPI set by the operating system but uses pixel
   * to render the graph nonetheless
   */
  class qigLogViewDelegate : public QStyledItemDelegate
  //class qigLogViewDelegate : public QAbstractItemDelegate
  {
    Q_OBJECT
    public:
      qigLogViewDelegate(QWidget* parent);
      qigLogViewDelegate(const qigLogViewDelegate&)=delete;
      ~qigLogViewDelegate()=default;

      /** overrite from QAbstractItemDelegate
       *  do the painting.
       */
      virtual void paint(QPainter* p, const QStyleOptionViewItem& o, const QModelIndex &i) const override;
      /** overrite from QAbstractItemDelegate
       */
      virtual QSize sizeHint(const QStyleOptionViewItem& o, const QModelIndex &i) const override;
      /** updates layout to adapt for the new device_pixel_ratio
       * @param device_pixel_ratio the new ratio.
       * TODO Qts doc is not clear. maybe this could be slot connected to physicalDotsPerInchChanged.
       * at the moment it is the devicPixelRatio
       */
      static void setDevicePixelRatio(qreal device_pixel_ratio);
    private:
      // the size hint. 
      //static QSize s_size_hint;
  };

  /**@class LogIconPainter
   * @brief a set of function that paints the history graph on a paint device
   */
  class LogIconPainter
  {
    public:
      /** paint a part of the history graph
       * @param p the initialised painter to use.
       * @param p where to paint
       * @param repr what to paint. An integer that is interpreted according to LogModelHistoryWorker::GraphRole
       */
      static void paint(QPainter *p, const QRect& rect, unsigned int repr);
    private:
      /** worker for paint.
       * paints a commit. a commit is represented by a circle
       */
      static void paintCommit(QPainter* p, const QRect& o);
      /** worker for paint.
       * a connection is a straight line from one end of the delegate to the other
       */
      static void paintConnection(QPainter* p, const QRect& o);
      /** worker for paint.
       * a fork is the junction between forkStart and forkEnd. it is a + junction with a small gap
       */
      static void paintFork(QPainter* p, const QRect& o);
      /** worker for paint.
       * a forkStart is a painted at the root of a new branch/tag. a fork commit has 2 ore more children
       */
      static void paintForkStart(QPainter* p, const QRect& o);
      /** worker for paint.
       * a forkEnd is a painted at the first commit of a newly created branch.
       */
      static void paintForkEnd(QPainter* p, const QRect& o);
      /** worker for paint
       * the target commit with 2 parents
       */
      static void paintMergeStart(QPainter* p, const QRect& o);
      /** worker for paint
       * the mergeEnd is one parent of a mergeStart
       */
      static void paintMergeEnd(QPainter* p, const QRect& o);

      static void paintShiftOut(QPainter* p, const QRect& o);
      static void paintShiftOutStart(QPainter* p, const QRect& o);
      static void paintShiftOutEnd(QPainter* p, const QRect& o);
      static void paintShiftIn(QPainter* p, const QRect& o);
      static void paintShiftInStart(QPainter* p, const QRect& o);
      static void paintShiftInEnd(QPainter* p, const QRect& o);

  };//}}}


  // forward declaration
  class qigOid;

  /**@ingroup Application
   * @class qigLogView {{{
   * offers the paint device for rendering the history graph.
   * uses qigLogViewDelegate for rendering.
   *
   * This class is a QTableView with some extensions for context menus
   */
  class qigLogView: public QTableView
  {
    Q_OBJECT
    public:
      qigLogView(QWidget* P);
      qigLogView(const qigLogView&)=delete;

      /** updates layout to adapt for the new device_pixel_ratio
       * @param device_pixel_ratio the new ratio.
       * TODO Qts doc is not clear. maybe this could be slot connected to physicalDotsPerInchChanged.
       * at the moment it is the devicPixelRatio
       */
      void setDevicePixelRatio(qreal device_pixel_ratio){ qigLogViewDelegate::setDevicePixelRatio(device_pixel_ratio);}
    protected:

      /**
       * reimplemented from QWidget
       */
      virtual void contextMenuEvent(QContextMenuEvent*) override;
      virtual void keyboardSearch(const QString& search) override;
    Q_SIGNALS:
      void commitRequest(const qigOid&);
      void mergeRequest(QList<const qigOid*>);
      //void tagRequest(...);
      void branchOutRequest(const qigOid&);


    private:
      QAction *actionCommit;
      //QAction *actionDiffThis;
      QAction *actionCherryPick;
      QAction *actionMerge;
      QAction *actionTag;
      QAction *actionBranchOut;

      QVector<QAction*> m_actions;
  };
  //}}}
}
