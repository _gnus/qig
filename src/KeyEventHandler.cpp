#include <assert.h>
#include <QKeyEvent>
#include <QDebug>
#include "enums.h"
#include "KeyEventHandler.h"

#define MODIFIERFLAGS Qt::ControlModifier | Qt::ShiftModifier | Qt::AltModifier

namespace qig
{
  qigKeyEventHandlerWorker::qigKeyEventHandlerWorker(QObject* parent):
    QAction(parent),
    m_complex_key_sequence(0)
  {
    m_flags[0]=0;
    m_flags[1]=0;
    memset(m_keys, 0, sizeof(m_keys));
  }
  qigKeyEventHandlerWorker::qigKeyEventHandlerWorker(
      QObject* parent, const QKeySequence& s, const char* slot,
      QObject *receiver, const QIcon& i, const QString& t):
    QAction(i, t, parent),
    m_complex_key_sequence(0)
  {
    m_flags[0]=0;
    m_flags[1]=0;
    memset(m_keys, 0, sizeof(m_keys));
    setKeySequence(s);
    if(!receiver) receiver = parent;
    bool ok = connect(this, SIGNAL(triggered()), receiver, slot);
    assert(ok);
  }
  void qigKeyEventHandlerWorker::setKeySequence(const QKeySequence &s)
  {
    int mod = 0, key =0;
    for(int i=0; i<s.count() && i <maxKeyLength; ++i)
    {
      int val = s[i];
      m_flags[mod++] = (Qt::KeyboardModifier) (val &(MODIFIERFLAGS)) ;
      m_keys[key++] = (val &~(MODIFIERFLAGS));
    }
    m_complex_key_sequence = (key==2);
  }


  qigKeyEventHandlerWorker::matchResult qigKeyEventHandlerWorker::match(QKeyEvent* ev, int stage)const
  {
    int offset = stage;
    //qDebug() << "qigKeyEventHandlerWorker::match: keys "<<ev->key()<< " modifiers:"<<ev->modifiers();
    if(ev->modifiers() == m_flags[offset]) {
      if(ev->key() == m_keys[offset]) {
	// if this is vim-like key sequnece and stage is 0 we can't have a exact match
	return m_complex_key_sequence && !stage ? resultPartialMatch : resultIdentityMatch;
      }
    }
    return resultNoMatch;
  }

  qigKeyEventHandler::qigKeyEventHandler():
    m_stage(0)
  {
  }

  int qigKeyEventHandler::addInputHandler(qigKeyEventHandlerWorker* ev)
  {
    int err =GIT_ERROR;
    if(m_events.indexOf(ev) != -1) {
       err = GIT_EEXISTS; 
    }
    else {
      m_events.push_back(ev);
      m_events_partial_match.reserve(m_events.size());
      err = GIT_OK;
    }
    return err;
  }

  int qigKeyEventHandler::removeInputHandler(qigKeyEventHandlerWorker* ev)
  {
    int err =GIT_ERROR;
    if((err= m_events.indexOf(ev) )!= -1) {
      m_events.remove(err);
      err = GIT_OK;
    }
    else {
      err = GIT_ENOTFOUND;
    }
    return err;
  }

  bool qigKeyEventHandler::keyPressEvent(QKeyEvent *ev)
  {
    int match = 0;
    if(m_stage)
    {
      keyPressEventStage1(ev);
    }
    else {
      // if otherwise we check ev against all key sequences
      for(int i=0; i<m_events.size() && match!=qigKeyEventHandlerWorker::resultIdentityMatch; ++i)
      {
	match = m_events[i]->match(ev,m_stage);
	switch(match)
	{
	  case qigKeyEventHandlerWorker::resultPartialMatch:
	    m_events_partial_match.push_back(m_events[i]);
	    m_stage = 1;
	    break;
	  case qigKeyEventHandlerWorker::resultIdentityMatch:
	    // if we ha an unique match we do not store the partial matches
	    triggerEvent(m_events[i]);
	    return true;
	    break;
	  default:
	    break;
	}
	// if m_stage is set we have a partial match
	return m_stage;
      }
    }
    return false;
  }

  bool qigKeyEventHandler::keyPressEventStage1(QKeyEvent* ev)
  {
    int match=0;
    // if we still have a partial match we check for those key sequence only
    for(int i=0;
	i<m_events_partial_match.size() && match!=qigKeyEventHandlerWorker::resultIdentityMatch;
	++i)
    {
      match = m_events_partial_match[i]->match(ev, m_stage);
      if(match == qigKeyEventHandlerWorker::resultIdentityMatch)
      {
	triggerEvent(m_events_partial_match[i]);
	return true;
      }
    }
    // no key sequence matched
    m_events_partial_match.clear();
    m_stage=0;
    return false;
  }

  void qigKeyEventHandler::triggerEvent(qigKeyEventHandlerWorker* ev)
  {
    m_events_partial_match.clear();
    m_stage=0;
    ev->trigger();
  }
}
