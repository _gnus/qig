
#include "DiffViewerDiffer.h"
#include "diffOptions.h"
#include "IDiffViewer.h"
#include "DiffViewerBuiltinDiffer.h"
#include "DiffViewerBuiltinTextDocument.h"
#include "enums.h"

#include <QDebug>


namespace qig
{

  int qigDifferHelper::diff(const diffOptions &opts, const qigDifferInterfaceManager & differInterfaceManager)
  {
    IDiffer *differ=differInterfaceManager.selectedDiffer();

    int ret=GIT_ERROR;
    if(differ)
    {
      switch(opts.target)
      {
	case objectTypeBlob:
	  {
	    if(opts.tree_source[diffOptions::oldSide] == treeSourceWD || opts.tree_source[diffOptions::newSide] == treeSourceWD)
	    {
	      auto helper = qigDifferHelperBuiltinBufferToBlob(*differ);
	      ret=helper.diff(opts);
	    }
	    else
	    {
	      auto helper = qigDifferHelperBuiltinBlobToBlob(*differ);
	      ret=helper.diff(opts);
	    }
	    //ret = diff(opts, helper);
	  }
	  break;
	case objectTypeTree:
	  {
	    auto helper = qigDifferHelperBuiltinTreeToTree(*differ);
	    qigDifferHelper* helperBase = &helper;
	    ret=helperBase->diff(opts);
	    //ret = diff(opts, helper);
	  }
	  break;
	default:
	  break;
      }
    }
    return ret;
  }

  /*
  int qigDifferHelper::diff(const diffOptions &opts, qigDifferHelper& helper)
  {
    return helper.diff(opts);
  }
  */


  qigDifferInterfaceManager::~qigDifferInterfaceManager()
  {
  }
  qigDifferInterfaceManager::qigDifferInterfaceManager():
    m_selected_differ_factory(nullptr)
  {
    m_static_factories.push_back(diffFacotryPtr(new qigDifferFactoryBuiltin()));
    m_selected_differ_factory=m_static_factories.front().get();

    m_selected_differ_external = differExternalPtr(m_selected_differ_factory->createDifferExternal());
    if(!m_selected_differ_external)
    {
      m_selected_differ=differPtr(m_selected_differ_factory->createDifferBuiltin());
    }
  }

  IDifferFactory* qigDifferInterfaceManager::selectedDifferFactory() const
  {
    return m_selected_differ_factory;
  }

  IDiffViewerExternal* qigDifferInterfaceManager::selectedDifferExternal() const
  {
    return m_selected_differ_external.get();
  }

  IDiffer* qigDifferInterfaceManager::selectedDiffer() const
  {
    return m_selected_differ.get();
  }

}
