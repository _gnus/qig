cmake_minimum_required(VERSION 3.0)

PROJECT(${PROJECT_NAME}_LIB)


MESSAGE("${CMAKE_CXX_FLAGS_DEBUG}")
set (CMAKE_INCLUDE_CURRENT_DIR TRUE)
find_package(Qt5Widgets REQUIRED)

set(SOURCES)
#aux_source_directory("${CMAKE_CURRENT_SOURCE_DIR}/" SOURCES)
LIST(APPEND 
  SOURCES
  Application.cpp
  Base.cpp
  Blob.cpp
  Branch.cpp
  BranchManager.cpp
  Commit.cpp
  DiffContainer.cpp
  diffOptions.cpp
  DiffViewerDiffer.cpp
  DiffViewerBuiltinDiffer.cpp
  DiffViewerBuiltinModel.cpp
  DiffViewerBuiltinTextEdit.cpp
  DiffViewerBuiltinTextDocument.cpp
  DiffViewerImpl.cpp
  Error.cpp
  GitObjectHolder.cpp
  GitOidHolder.cpp
  Index.cpp
  KeyEventHandler.cpp
  LogFilterList.cpp
  LogModel.cpp
  LogModelHistoryWorker.cpp
  LogView.cpp
  MainWindow.cpp
  Mime.cpp
  Object.cpp
  Oid.cpp
  OidListModel.cpp
  Reference.cpp
  ReferenceManager.cpp
  Repository.cpp
  RevWalker.cpp
  Status.cpp
  StatusTree.cpp
  treeEntry.cpp
  Tree.cpp
  TreeContainer.cpp
  TreeListView.cpp
  TreeModel.cpp
  TreeTreeView.cpp
  WorkDirModel.cpp
  )

add_library(${PROJECT_NAME} STATIC "${SOURCES}")

#enable the qt mocker and the ui compiler
set_target_properties(${PROJECT_NAME} PROPERTIES
  AUTOMOC TRUE
  AUTOUIC TRUE
  )
target_link_libraries(${PROJECT_NAME} Qt5::Widgets pthread)
