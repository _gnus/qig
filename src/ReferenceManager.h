// vim: fdm=marker ft=cpp.doxygen
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once

#include "Base.h"
#include "List.h"
#include "Reference.h"



namespace qig
{
  typedef std::unique_ptr<qigReference> qigReferencePtr;
  typedef qigList<qigReferencePtr> qigReferencesList;
  class qigReferenceManager: private qigBase
  {
    public:
      qigReferenceManager(const qigBase&);
      qigReferenceManager(const qigReferenceManager&) = delete;

      /** @brief returns list of all references that matches type @a ref.
       * @param [out] out a list that will be filled by @a references.
       * @param type currenlty ignored.
       */
      int references(qigReferencesList& out, int type ) const;

      int reference(qigReference&, int type ) const;

      int referencesByName(qigReference&, qigChar* name ) const;

      int head(qigReference&) const;
      
    private:
  };
}
