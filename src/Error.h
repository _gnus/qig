#pragma once
#include <memory>

class QAbstractItemModel;
class QString;
class QStringList;

namespace qig
{
  class WarningModel;

  /** @ingroup error handling
   *
   * TODO offer stream oprators
   */
  void setError(const char*);
  void setError(const QString&);
  const QString& getError();
  void addWarning(int category, const char*);
  void addWarning(int category, const QString&);
  void errorFromGitError();
  /**@class qigErrorHandler
   * @brief a container that holds warnings and errors;
   */
  class qigErrorHandler
  {
    public:
      qigErrorHandler();
      ~qigErrorHandler();
      /** returns the last error message;
       */
      const QString& getErrorText() const;
      /** clears the error message
       */
      void clearErrorText();

      /** sets the error message
       * does nothing if an message is set
       */
      void setErrorText(const QString&);

      /** adds a warning with category
       */
      void addWarning(int category, const QString& text);
      /** limits the number of cached warnings to size
       */
      void restrictWarnings(size_t size);

      /** returns all warnings an an item model;
       */
      QAbstractItemModel * warnings();// { return m_warning_model.get();}
      /** returns the last n-th warnings;
       */
      void recentWarnings(size_t n, QStringList& out) const;

      /** the number of stored warnings
       */
      size_t size() const;

    private:
      std::unique_ptr<QString> m_error;
      std::unique_ptr<WarningModel> m_warning_model;
  };
}
