// vim: fdm=marker
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#pragma once
#include <memory>
#include "gitTypes.h"
#include "Reference.h"
#include "Base.h"
#include "List.h"
#include "enums.h"


namespace qig
{

  /**@class qigBranch
   * @ingroup LibGitWrap
   * @brief represents a branch in qig. 
   * this class essentially wraps the functions of git2/branch.h which operates on branches.
   */
  class qigBranch: public qigReference
  {
    friend class qigBranchManager;

    public:
      /* @param oid the ref to the head of the branch
       */
      //qigBranch(const qigBase&, const qigOid& oid) throw qigAttributeError;
      // see git branch move. move creates a new branch. must be implemented in qigBranchManager
      //int move(const qigChar* newName);
      //unsigned int  countCommits();
      /* returns all references on this branch. this includes commits...
       *  wie sieht es mit der reference auf sich selber aus???
       */
      //unsigned int  countRefs() const;


      /** returns 1 if branch is head.
       * @returns 1 if branch is HEAD, 0 otherwise.
       */
      int isHEAD() const;

      /**returns name of the branch
       */
      const qigChar* name() const;

      /** return 1 if instance is branch of type @a type 
       * @param type query value. either local or remote branch
       *  @return 1 if instance is branch of type @a type 
       *  @sa toBranchType
       */
      int isType(branchType type) const;

      /**
       * returns pointer to the template parameter if cast is valid
       *
       * Valid template parameters are qigBranchRemote* and qigBranchLocal*
       * throws 0 for other template parameters
       * @throws int
       * @return pointer to the template template if cast is valid or 0 otherwise
       *
       * @sa isType()
       *
       * @code
       * // assuming p_branch is a qigBranch and a local branch in git language
       * qigBranchLocal* p_branch->toBranchType<qigBranchLocal*> ();
       * @endcode
       */
      template <typename T>
	inline T toBranchType() const;
    protected:
      /**constructs a new qigBranch from an git_reference that points to a 
       * branch and the branchType
       *
       * @param p_branch a valid git reference from .git/refs/heads. The class steals the ownership of p_branch
       * @param branchType either local or remote
       */
      qigBranch(git_reference* p_branch, branchType branchType);
      /**dtor
       */
      //~qigBranch();
    private:
      /// retrieve name from libgit2
      ///TODO make static
      const qigChar* _name();
      /// the reference to the head of the branch
      const branchType _m_type;
      /// the name of the branch.
      const qigChar* _m_name;
  };


  typedef qigBranch qigBranchRemote;
  /**@class qigBranchLocal 
   * this class represents a local Branch. It extends the qigBranch by upstream capabilities
   */
  class qigBranchLocal:public qigBranch
  {
    friend class qigBranchManager;

    public:
      int setUpStream(qigBranchRemote&);
      /** returns name of upstream for this branch.
       *
       *  @return name of upstream for this branch
       */
      const qigChar* getUpStream();
  };

  //{{{ implementation for template functions
  template <typename T>
    inline  T qigBranch::toBranchType() const
  {
    throw 1;
    // anyway
    return T();
  }

  //}}}
}
