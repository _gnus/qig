// vim: fdm=marker
#include <git2.h>
#include <assert.h>
#include <string.h>

#include "BranchManager.h"
#include "gitTypes.h"


namespace qig
{
  // public members{{{
  qigBranchManager::qigBranchManager(const qigBase& base):
    //{{{
    qigBase(base)
    //qigList<std::unique_ptr<qigBranch> >()
  {
  }
  //}}}

  int qigBranchManager::branchCount(branchType type) const
  {//{{{
    int counter = 0;
    for(auto iter = begin(); iter != end(); ++iter)
    {
      if(iter->get()->isType(type))
      {
	++counter;
      }
    }
    return counter;
  }//}}}
  
  int qigBranchManager::lookupByName(const qigChar *branch_name, branchType type, const qigBranch*& out) const
  {//{{{
    int ret = GIT_ENOTFOUND;
    out = nullptr;
    for(auto iter = begin(); iter != end() && ret; ++iter)
    {
      if(iter->get()->isType(type) && strncmp(branch_name, iter->get()->name(), strlen(iter->get()->name())) ==0 )
      {
	out = iter->get();
	ret = GIT_OK;
      }
    }
    return ret;
  }//}}}

  int qigBranchManager::lookupByName(const qigChar *branch_name, branchType type, qigBranch*& out)
  {//{{{
    // do a const cast. otherwise the lookup method would be implemented twice. the const method cannot call this method due to constnes of this and the non const method cannot call the const version due to the cnstnes of out
    const qigBranch*& const_out = const_cast<const qigBranch*&>( out);
    const qigBranchManager* const_this = this;
    return const_this->lookupByName(branch_name,type, const_out);
  }//}}}

  int qigBranchManager::lookup(int index, branchType type, const qigBranch*& out) const
  {//{{{
    int ret = GIT_ENOTFOUND;
    out = nullptr;
    branchList::const_iterator iter(locateIndex(index));
    if(iter != end())
    {
      const qigBranch * p;
      p = iter->get();
      if(p->isType(type)) {
	out = p;
	ret=GIT_OK;
      }
    }
    return ret;
  }//}}}
  int qigBranchManager::lookup(int index, branchType type, qigBranch*& out)
  {//{{{
    // do a const cast. otherwise the lookup method would be implemented twice. the const method cannot call this method due to constnes of this and the non const method cannot call the const version due to the cnstnes of out
    const qigBranch*& const_out = const_cast<const qigBranch*&>( out);
    const qigBranchManager* const_this = this;
    return const_this->lookup(index,type, const_out);
  }//}}}
      
  //}}}
  //private members{{{
  void qigBranchManager::initBranchList()
  {//{{{
    int errcode = 1;
    if(isValid())
    {
      git_branch_iterator *iter;
      if(!git_branch_iterator_new(&iter, qigBase::mainRepo(), GIT_BRANCH_ALL ))
      {
	git_reference *pRef;
	git_branch_t type;
	while( (errcode=git_branch_next(&pRef, &type, iter) ) == 0) //< works on gcc. but why
	{
	  // I thought lambdas should make code more readable ...
	  push_back(
	      branchList::value_type( // < a unique_ptr<...>
		new qigBranch(pRef, toqgitBrachType(type)), //< first argument to the ctor of unique_ptr<...>. the object itself
		[](qigBranch* p){ if(p) delete p;} //< the second param to the ctor of unique_ptr<...>. A lambda expression to delete the object.
		)
	      );
	  //push_back(std::unique_ptr<qigBranch, std::function<void (qigBranch*)> >(new qigBranch(pRef, toqgitBrachType(type)), [](qigBranch*p){delete p;}));
	}
	git_branch_iterator_free(iter);
      }
    }
    assert(errcode == GIT_ITEROVER);
  }//}}}
  qigBranchManager::branchList::const_iterator qigBranchManager::locateIndex(int index) const
  {//{{{
    if(index < 0)
    {
      assert(false);
    }
    else
    {
      // incement index, so we can use pre operator
      ++index;
      //
      // what happens in iter+=index if index is too large??

      // iterate over the list till index is 0
      auto iter = begin();
      while( iter != end() && --index )
	++iter;
      // because we use pre operator index should be 0 otherwise iteration ended before we reached pos list[index]
      if ( index == 0 && iter != end())
      {
	return iter;
      }
    }
    return end();
  }//}}}
  //}}} end of private member

}// end of namespace

