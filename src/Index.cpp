// vim: fdm=marker
#include <cassert>
#include <git2/errors.h>
#include <git2/index.h>
#include <git2/repository.h>
#include <algorithm>

#include <QDebug>

#include "Index.h"
#include "enums.h"
#include "Oid.h"
#include "WorkDirModel.h"
#include "fileEntry.h"


namespace qig
{
  //{{{ ctor dtor
  qigIndexHolder::qigIndexHolder():
    m_index(nullptr)
  {
  }
  qigIndexHolder::qigIndexHolder(git_index* p):
    m_index(p)
  {
  }

  qigIndexHolder::qigIndexHolder(qigIndexHolder &&o):
    m_index(o.m_index)
  {
    o.m_index=nullptr;
  }

  qigIndexHolder::~qigIndexHolder()
  {
    if(m_index)
    {
      git_index_free(m_index);
    }
  }


  qigIndex::qigIndex(const qigBase& base): 
    qigBase(base),
    m_index_ptr(nullptr,git_index_free)
  {
    git_repository* repo = mainRepo();
    git_index* index;
    if(git_repository_index(&index, repo) == GIT_OK) {
      m_index_ptr=qigIndexPtr(index,git_index_free);
    }
    else {
      throw std::exception();
    }
  }

  //}}}


  int qigIndex::entryOidByPath(qigOid& out, const char* path) const
  {
    int ret = GIT_ERROR;
    //if(m_index_ptr.index())
    {
      ret = GIT_ENOTFOUND;
      const git_index_entry* entry  = git_index_get_bypath(m_index_ptr.get(), path, GIT_INDEX_STAGE_NORMAL);
      if(entry)
      {
	git_repository* repo = git_index_owner(m_index_ptr.get());
	out.load(&(entry->id), repo);
	ret= GIT_OK;
      }
    }
    return ret;
  }

  /* modifier function {{{ */

  int qigIndex::addEntryByPath(const char *path)
  {
    return git_index_add_bypath(m_index_ptr.get(), path);
  }

  int qigIndex::addEntryFromBuffer(const unsigned char* buffer, unsigned int buffer_len, const char *path)
  {
    git_index_entry entry;
    entry.path = path;
    return git_index_add_frombuffer(m_index_ptr.get(), &entry, buffer, buffer_len);
  }

  /** removes an entry by its path
   */
  int qigIndex::removeEntryByPath(const char*path)
  {
    return git_index_remove_bypath(m_index_ptr.get(), path);
  }
  
  /** writes current content to the disk
   */
  int qigIndex::writeToDisk()const
  {
    return git_index_write(m_index_ptr.get());
  }
  /** read index from disk;
   */
  int qigIndex::read(bool force)
  {
    return git_index_read(m_index_ptr.get(), force);
  }

  /**
   * compares 2 indexs and return true if both holds the same content
   */
  bool qigIndex::operator==(const qigIndex& o) const
  {
    const git_oid* toid = git_index_checksum(m_index_ptr.get());
    const git_oid* ooid = git_index_checksum(o.m_index_ptr.get());
    return git_oid_cmp(toid, ooid)==0;
  }

  bool qigIndex::compareWithDisk()const
  {
    const qigBase& base=*this;
    // constructed always uses current repo;
    return qigIndex(base) == *this;
  }
  //}}}
  //

  //{{{ qigIndexBuilder

  qigIndexBuilder::qigIndexBuilder(QObject* parent):
    QIdentityProxyModel(parent),
    m_source(0)
  {
  }

  qigIndexBuilder::~qigIndexBuilder()
  {
  }

  QVariant qigIndexBuilder::data(const QModelIndex& index, int role) const
  {
    if(role == Qt::CheckStateRole && index.column() != 0 )
    {
      return QVariant();
    }
    return QIdentityProxyModel::data(index, role);
  }

  Qt::ItemFlags qigIndexBuilder::flags(const QModelIndex& index) const
  {
    // TODO make item modifiable
    if(index.data(roleFileState).toInt() == fileStateUnchanged)
    {
      // disable if file is not modified;
      return QIdentityProxyModel::flags(index) ^ Qt::ItemIsEnabled;
    }
    // add ItemIsUserCheckable for column 0;
    return index.column() == 0 ?
      QIdentityProxyModel::flags(index) | Qt::ItemIsUserCheckable :
      QIdentityProxyModel::flags(index);
  }

  bool qigIndexBuilder::setData(const QModelIndex& index, const QVariant& data, int role)
  {
    if(role == Qt::CheckStateRole)
    {
      // set state for column 0 only
      return index.column() == 0 ? QIdentityProxyModel::setData(index, data, role) : false;
    }
    return QIdentityProxyModel::setData(index, data, role);
  }

  bool qigIndexBuilder::sync(qigIndex &i)
  {
    qigIndexBuilderListModel list(this);
    list.setSourceModel(m_source);
    const QStringList& l = list.stringList();
    qDebug() << Q_FUNC_INFO << l;
    int err = GIT_OK;
    for( const QString& file: l)
    {
      if( (err=i.addEntryByPath(file.toUtf8().constData())) != GIT_OK)
      {
	if(err == GIT_EDIRECTORY)
	{
	  // tried to add a directory;
	  // ignore the error;
	  giterr_clear();
	  err = GIT_OK;
	  continue;
	}
	break;
      }
    }
    return err==GIT_OK ? i.writeToDisk() == GIT_OK: false;
  }

  QModelIndexList qigIndexBuilder::match(const QModelIndex& index, int role, const QVariant& data, int hits, Qt::MatchFlags flags ) const
  {
    qDebug()<<Q_FUNC_INFO;
    if(role == Qt::DisplayRole)
    {
      const QModelIndex& index = m_source->index(data.toString());
      if(index.isValid())
      {
	QModelIndexList ret({mapFromSource(index)});
	return ret;
      }
    }
    return QIdentityProxyModel::match(index,role, data,hits, flags);
  }

  void qigIndexBuilder::resetInternalData()
  {
    // TODO unset the check state;
  }

  void qigIndexBuilder::setSourceModel(qigWorkDirModel* p)
  {
    QIdentityProxyModel::setSourceModel(reinterpret_cast<QAbstractItemModel*>(p));
    m_source=p;
  }
  //}}}
  //
  //{{{ qigIndexBuilderListModel
 
  qigIndexBuilderListModel::qigIndexBuilderListModel(QObject* parent)
    : QStringListModel(parent)
    , m_source(0)
  {
  }

  void qigIndexBuilderListModel::setSourceModel(qigWorkDirModel* s)
  {
    reset();
    m_source=s;
    if(s)
    {
      connect(s, (&qigWorkDirModel::dataChanged), this, (&qigIndexBuilderListModel::sourceModelDataChanged));
      init();
    }
  }

  void qigIndexBuilderListModel::sourceModelDataChanged(const QModelIndex& top_left, const QModelIndex& bottom_right, const QVector<int>& roles)
  {
    if(roles.isEmpty())
    {
      return;
    }
    if(roles.indexOf(Qt::CheckStateRole) == -1)
    {
      return;
    }
    // changed element specified;
    if(top_left.isValid() && bottom_right.isValid())
    {
      for(int i = top_left.row(); i<= bottom_right.row(); ++i)
      {
	const QModelIndex& iter = top_left.sibling(i,0);
	const QString& entry = iter.data(Qt::DisplayRole).toString();
	if(!entry.isEmpty())
	{
	  if(iter.data(Qt::CheckStateRole).value<Qt::CheckState>() == Qt::Unchecked)
	  {
	    removeString(entry);
	  }
	  else
	  {
	    insertString(entry);
	  }
	}
      }
    }
    // no element specified reinit()
    else
    {
      reset();
      init();
    }
  }

  void qigIndexBuilderListModel::insertString(const QString&)
  {
    // todo check if string exists;
    assert(false);
    // not implemented;
  }

  void qigIndexBuilderListModel::removeString(const QString&)
  {
    assert(false);
    // not implemented;
  }

  void qigIndexBuilderListModel::init()
  {
    QStringList list;
    // use const ref;
    // shouldn't do any difference when using non const anyway;
    const qigWorkDirModel::fileMap& files = m_source->m_files;
    for( auto & pair : files)
    {
      if(pair.second->hasChildren())
      {
	// skip directories
	continue;
      }
      // do not include directories that are partially checked
      if(pair.second->checkState() == Qt::Checked && pair.second->stateFlag() != fileStateUnchanged)
      {
	  if(pair.second->stateFlag() != fileStateIgnored || false)
	  {
	    list << m_source->toStatusKey(pair.first);
	  } 
      }
    }
    setStringList(list);
  }
  
  //}}}
}
