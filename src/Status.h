#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */

#include <mutex>
#include <QHash>
#include <QString>

#include "gitTypes.h"
#include "enums.h"
#include "Str.h"
#include "List.h"
#include "Base.h"

namespace qig
{

  class qigStatus;
  //typedef std::pair<QString,int> statusResultEntry;
  //typedef std::unordered_map<QString, int> statusResult;
  typedef QHash<QString, int> statusResult;

  // counts the modified deleted etc. entries in an 
  // status
  struct statusSummary {
    int        newIndexCnt;
    int   modifiedIndexCnt;
    int    deletedIndexCnt;
    int    renamedIndexCnt;
    int typeChangeIndexCnt;

    int        newWDCnt;
    int   modifiedWDCnt;
    int    deletedWDCnt;
    int typeChangeWDCnt;
    int    renamedWDCnt;
    int unreadableWDCnt;

    int    ignoredCnt;
    int conflictedCnt;
  };

  class qigStatusWorker
  {
    protected:

      qigStatusWorker(qigStatus&);
      ~qigStatusWorker();

      /** sets the repository to work on to p
       */
      void setRepository(git_repository* p);
      /** terminates crrent thread
       * not implemented
       */
      void abort();

      /** returns the status for given file @a path.
       * does not work on dirctories;
       *
       * @param status_flags an or-combination of fileStatus::fileState
       * @param path to path to file.
       * @return 0 on sucess otherwise an errorcode
       */
      int statusFile(unsigned int& status_flags, const char* path);
      /** updates result by comparing the current state of the filesystem with index and tree
       * This function returns immidiatly if class is not initialised jet;
       * @sa updateFinished to get notification when update completed
       */
      void updateStatus(){ return start(this);}
      /** called after updateStatus finished.
       * overrite to get latest result.
       * updateFinished may be called from another thread
       */
      virtual void updateFinished(const statusSummary&, statusResult &)=0;

      /** must be called if current repository is going to be unloaded
       */
      void repositoryUnloaded();

    private:
      /** the main function to update the cache
       */
      void run();
      /** should do the same as std::bind
       */
      static void start(qigStatusWorker* _this){_this->run();}

      qigStatus& m_parent;
      bool _m_abort_flag;

      git_repository *m_repo;
  };

  class qigStatus: private qigStatusWorker, private qigBase
  {
    public:
      qigStatus(const qigBase& );
      qigStatus(qigStatus&&) = default;
      ~qigStatus();

      // triggers the worker to update the current status
      using qigStatusWorker::updateStatus;
      /** returns the status for given file @a path.
       * does not work on dirctories;
       *
       * @param status_flags an or-combination of fileStatus::fileState
       * @param path to path to file.
       * @return 0 on sucess otherwise an errorcode
       */
      using qigStatusWorker::statusFile;
      /** same as statusFile but uses cached values
       * @sa update
       */
      int statusFileFast(unsigned int& status_flags, const QString& path);
      //void updateStatus(){qgiM;
      /** query for a summary of the last run. the summary contains the 
       * number of modified, added, deleted... files in workdir and index
       *
       * @param summary the struct to write the result to
       * @param charge_cnt [inout] a int that will be set to an internal counter.
       *			   the counter increases if the
       *                           status changed. if the charge_cnt matches the
       *                           internal counter nothing is written to summary
       * @return true if summary is updated.
       */
      bool summary( statusSummary& summary, unsigned int& charge_cnt) const;


    private:
      virtual void updateFinished(const statusSummary&, statusResult &);
      unsigned int m_charge_cnt;
      statusSummary m_summary;
      statusResult m_result;
      mutable std::mutex m_result_protector;
  };

}
