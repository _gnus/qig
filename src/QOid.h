#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include "Oid.h"

namespace qig
{
  typedef qig::qigOid QOid;
}
Q_DECLARE_METATYPE(qig::qigOid);
#if 0
#include <QSharedPointer>

namespace qig
{
  class QOid: public qigOid
  {
    public:
      /** creates an empty QOid.
       * the shared pointer is a nullpointer
       */
      QOid();

      /** writes the oid to the QByteArray data.
       *  @param data [out] a QByteArray to store the 20 bytes of the sha1 sum
       */
      void serialise(QByteArray &data);
      /** inistialise the Oid from data
       * if data does not contain 20 bytes this function is an noop
       * @param data a QByteArray with an oid
       */
      void deserialise(const QByteArray& data);
      friend QDataStream& operator<<(QDataStream&, const QOid&);
      friend QDataStream& operator>>(QDataStream&, const QOid&);
  };
}
Q_DECLARE_METATYPE(qig::QOid);
#endif
