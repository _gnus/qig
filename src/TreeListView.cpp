// vim: fdm=marker
#include <QDebug>
#include <QApplication>
#include <QPainter>

#include "TreeListView.h"
#include "QOid.h"
#include "Oid.h"
#include "enums.h"

namespace qig
{
  static QString debug_recently_selected_file;
  static QPersistentModelIndex debug_recent_index;
  
  qigTreeListView::qigTreeListView(QWidget* p):qigTreeView(p)
  {
    setShowGrid(false);
    connect(this, &qigTreeListView::activated, this, &qigTreeListView::onActivated, Qt::DirectConnection);
  }


  /** override from QAbstractItemView
   */
  void qigTreeListView::selectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
  {
    if(selected.size() == 1)
    {
      // do not use indezes from QItemSelection. those indexes causes random crashes
      //const QModelIndex& index = selected.indexes().front();

      //const QString& path = index.data().toString();
      //if( path == debug_recently_selected_file)
      //{
      //  qDebug() << "qigTreeListView::selectionChanged: recent index is: " << debug_recent_index;
      //  qDebug() << "qigTreeListView::selectionChanged:    new index is: " <<index;
      //}
      //debug_recently_selected_file = path;
      //debug_recent_index = index;
      //const qigOid& oid = index.data(modelRoles::roleBlobOid).value<qigOid>();

      emit selectionChanged();
    }
    QView::selectionChanged(selected, deselected);
  }

  void qigTreeListView::keyPressEvent(QKeyEvent *p)
  {
    if(p->modifiers() == Qt::AltModifier )
    {
      switch(p->key())
      {
	case Qt::Key_Up:
	case Qt::Key_Left:
	  moveUp();
	  return;
	default:
	  break;
      }
    }
    else if(p->modifiers() == Qt::ControlModifier)
    {
      switch(p->key())
      {
	case Qt::Key_U:
	  {
	    QKeyEvent ev(QEvent::KeyPress, Qt::Key_PageUp, Qt::NoModifier);
	    qig::QView::keyPressEvent(&ev);
	  }
	  return;
	  break;
	case Qt::Key_D:
	  {
	    QKeyEvent ev(QEvent::KeyPress, Qt::Key_PageDown, Qt::NoModifier);
	    qig::QView::keyPressEvent(&ev);
	  }
	  return;
	default:
	  break;
      }
    }


    return ::qig::QView::keyPressEvent(p);// parent class QView either typedef to QTableView or QListView
  }

  void qigTreeListView::dataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles)
  {
    qDebug()<< "qigTreeListView::dataChanged: index"<< topLeft << "and roles"<<roles;
    qDebug()<< "qigTreeListView::dataChanged: index points to"<< topLeft.data();
    qDebug()<< "qigTreeListView::dataChanged: new value is"<< topLeft.data(modelRoles::roleFileState);
    QView::dataChanged(topLeft, bottomRight, roles);
  }
  void qigTreeListView::moveUp()
  {
    const QModelIndex& parent = this->rootIndex().parent();
    this->setRootIndex(parent);
  }
  /** specialisation for qigTreeView<::qig::QView> {{{
   *  sets the new root to respect the users request to move up in filestructure
   */
  template <>
  void qigTreeView<::qig::QView>::onDictionaryActivated(const QModelIndex& index)
  {
    const QModelIndex& sib = index.sibling(index.row(),0);
    if(sib.data().toString() == QString("..")) {
      moveUp();
    }
    else if(sib.data().toString() == QString(".")) {
    }
    else {
      if(sib.isValid())
	this->setRootIndex(sib);
      assert(rootIndex()==sib);
    }
  }
  //}}}
}
