#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <QMainWindow>
#include <QList>
#include <memory>
#include <deque>

#include "Application.h"
#include "ui_Application.h"
class QModelIndex;
class QAbstractItemModel;
class QAction;

namespace qig
{
  enum inputState
  {
    // normal input mode.
    stateDefault,
    // do i need this ?
    stateInput,
    reserved,
    // command console open
    stateCommand
  };
  class qigKeyEventHandler;
  class qigKeyEventHandlerWorker;
  class qigOid;
  class qigTree;
  /**@ingroup Application
   * @class qigMainWindow
   *
   * @brief main connection between view and back end. feeds qigApplication with user input
   */
  class qigMainWindow: public QMainWindow, private Ui::Application
  {
    friend class qigApplication;
    Q_OBJECT
      typedef std::unique_ptr<qigKeyEventHandler> qigKeyEventHandlerPtr;
      typedef std::unique_ptr<qigKeyEventHandlerWorker> qigKeyEventHandlerWorkerPtr;
      typedef std::deque<qigKeyEventHandlerWorkerPtr> qigKeyEventHandlerWorkerList;

    public: 
      qigMainWindow(qigApplication &app);
      ~qigMainWindow();

      void setDiffView(QWidget*p)
      {
	if(_m_diff_view)
	{
	  delete _m_diff_view;
	}
	_m_diff_view = p;
	splitter->insertWidget(0,p);
      }
      void setDiffDocument( QTextDocument* );
      void setDiffModel(QAbstractItemModel*p);
      /** sets model for TreeView
       * the tree view represents the git tree (work dir or snap shot)
       */
      void setTreeModel(QAbstractItemModel *p);
      QAbstractItemModel* treeModel() const {
	return this->_m_tree_view_as_table->model();
      }
      void setLogModel(QAbstractItemModel  *p) { this->_m_log_view->setModel(p); }
      void setTreeRoot(const QModelIndex& index){
	_m_tree_view_as_table->setRootIndex(index);
	_m_tree_view_as_tree->setRootIndex( index);
      }
      void setTreeSourceModel(QAbstractItemModel *p) {
	_m_tree_source_selector->setModel(p);
      }
      void setDiffOldSideModel(QAbstractItemModel*p) {
	_m_diff_old_tree_selector->setModel(p);
      }
      void onDevicePixelRationChanged( qreal device_pixel_ratio ) {
	_m_log_view->setDevicePixelRatio(device_pixel_ratio);
      }

      static void reportError(const QString& message);
      /** maybe clean up the mess created by the designer.
       * and do Qt connections;
       * do not call twice
       */
      void postProcessUi();


    private Q_SLOTS:
      /**
       * this slot should be invoked to inspect a commit;
       * the method loads the commit to the tree view;
       */
      void onInspectCommit(const QModelIndex& index);
      /**
       * this slot should be invoked to filter or reset the filter result
       */
      void onFilterTriggered();
      /** this slot should be invoked to pop up an dialog to query for a repo to load
       */
      void onRepositoryLoadRequest(){m_app.onRepositoryLoadRequest();}

      /** updates the labels in the _m_log_commit_info
       * see designer
       * @param i the currently selected item in the logmodel
       * @param j the deselected item in the logmodel
       */
      void onLogCommitSelected(const QModelIndex &i, const QModelIndex &j);

      /** updates new side of diff view
       * to show path from tree with given tree_oid and blob_oid,
       * both oid can be invalid if they do not exist as objects jet. 
       *
       * diff is done between two blobs/file
       */
      void onTreeSelectionChanged();
      /** selects the tree source based on the index of the combo box
       */
      void onTreeSourceChangeRequest(int index);
      /**selects another source as tree entry. oid should contain oid as 20 byte raw data (not string representation)
       */
      void onTreeSourceChangeRequest(const QByteArray& raw);
      /** infos about qt
       */
      void onAboutQt(){m_app.aboutQt();}

      void onFoldCloseRequest();
      void onFoldOpenRequest(){};

      void onShowCommandLine();
      void onOpenRequest();

      void onInconvienceDetected(int type, const QString& msg);

      void onTreeViewActionaddTriggered();
      void onTreeViewActionbranchTriggered(){ qDebug("branch");}
      void onTreeViewActioncommitTriggered();
      void onTreeViewActioncheckoutTriggered(){ qDebug("checkout");}
      void onTreeViewActionpatchTriggered(){ qDebug("patch");}
      /** abort and return to default state
       */
      void onTreeViewAbort();
      /** accept current state
       */
      void onTreeViewAccept();

      void onApplicationStateChange(int state);

    protected:
      virtual void keyPressEvent(QKeyEvent*ev) override;

    private:
      void setupUiDefault();
      void setupUiAdd();
      void setupUiCommit();
      /** shows current editor;
       */
      void prepareCommit();
      /**
       *  diff tree to tree
       */
      void diffTree(qigOid&& new_tree, qigOid&& old_tree);
      /** 
       * diff index if is modified
       * @return error code
       */
      int diffBlob(const QModelIndex&);
      /**creates a new filter based on parameter. this function returns a new object that must be released.
       */
      static qigFilter* createFilterList(const QString& filterstring, const logFilter::fields);
      /** the central  place to map the index of the combo box to the field.
       */
      static logFilter::fields comboBoxIndexTofield(int index){ return (logFilter::fields) index;}

      void setupActionsForTreeView();

      void showError();

    private://static
      static int setTreeSource(qigApplication&, const qigOid& old_oid, int old_type, const qigOid& new_oid, int new_type);
    private:
      int m_current_state;
      qigApplication& m_app;
      qigKeyEventHandlerPtr m_keyEventHandler;
      qigKeyEventHandlerWorkerList m_keyEventHandlerWorkerList;
      inputState m_state;
      QLineEdit evil_edit;
      typedef QList<QAction*> QActionList;
      QActionList m_actions_for_tree_view;
  };
}
