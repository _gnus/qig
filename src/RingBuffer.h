#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */

#include <atomic>

namespace qig
{
  /**@class qigRingBuffer
   * a very simple ring buffer with protection against accidental override
   * The class is not thread safe.
   * T must be copyable and T(0) must be valid.
   */
  template <typename T>
  class qigRingBuffer
  {
    public:
      inline qigRingBuffer(unsigned int size);
      inline ~qigRingBuffer();

      inline bool push(T);
      inline T pop();

//      void reserver(unsigned int size);
//      void resize(unsigned int size);

    private:
      /** returns pos % _m_current_size
       * @param pos the global pos that might be a multiple of _m_current_size
       */
      unsigned int toLocalPos(uint64_t pos){ return pos % _m_current_size;}
      T* _m_data;
      unsigned int _m_current_size;
      // the location where to store the next element that is pushed added by a multiple of _m_current_size
      uint64_t _m_read_pos; ///< may cause race conditions on on 32-bit processors on qigRingBuffer11;
      //std::atomic_ullong _m_read_pos;
      // the location where to read the next element that is pooped added by a multiple of _m_current_size
      uint64_t _m_write_pos; ///< may cause race conditions on on 32-bit processors on qigRingBuffer11;
      //std::atomic_ullong _m_write_pos;
      //std::atomic_flag _m_sync_flag;
  };

  /**@class qigRingBuffer11
   * a very simple ring buffer with protection against accidental override
   * The class is  thread safe as long there is one feeder and one reader
   * T must be copyable and T(0) must be valid.
   */
  template <typename T>
  class qigRingBuffer11: private qigRingBuffer<T>
  {
    public:
      qigRingBuffer11(unsigned int size):qigRingBuffer<T>(size), _m_sync_flag(ATOMIC_FLAG_INIT){}
      ~qigRingBuffer11(){}

      // sync not at the right pos.
      // push may succeed, then thread schedule kicks in. reader thread may read location at newly written memory but nithout sync.
      // the value may still an old value.
      bool push(T t){ return qigRingBuffer<T>::push(t) ?  _m_sync_flag.test_and_set(std::memory_order_release) || true : false; }
      T pop(){ _m_sync_flag.test_and_set(std::memory_order_acquire); return qigRingBuffer<T>::pop();}
    private:
      // flag used to sync. the value is irelevant.
      std::atomic_flag _m_sync_flag;
  };
}
namespace qig
{
  template <typename T>
  inline qigRingBuffer<T>::qigRingBuffer(unsigned int size):
    _m_data ( (T*) malloc(size* sizeof(T)) ),
    _m_current_size(size),
    _m_read_pos(0),
    _m_write_pos(0)
    //_m_sync_flag(ATOMIC_FLAG_INIT)
  {
    if(!_m_data) {
      throw std::bad_alloc();
    }
  }
  template <typename T>
  inline qigRingBuffer<T>::~qigRingBuffer()
  {
    // make sure there no elements remains;
    if(pop()) {
      //throw std::exception();
    }
    free(_m_data);
  }
  template <typename T>
  inline T qigRingBuffer<T>::pop()
  {
    // sync before we try to read;
    //_m_sync_flag.test_and_set(std::memory_order_acquire);
    // reader position must be smaller than writer position
    // this statement is also true if an overflow in the ring buffer occur because both position increase monotony
    if(_m_read_pos < _m_write_pos ) {
      T ret= _m_data[ toLocalPos(_m_read_pos) ];//<< do not use inline post increment operator. May cause race condition. toLocalPos(_m_read_pos++) is executed but query of _m_data not because the thread stalls for a moment and but the writer thread receive the new value of _m_read_pos even without sync
      ++_m_read_pos;
      return ret;
    }
    return T(0);
  }

  template <typename T>
  inline bool qigRingBuffer<T>::push(T o)
  {
    // we can store _m_current_size items. If _m_write_pos  - _m_read_pos > _m_current_size we would store more than _m_current_size, or more precisely override items that are not popped jet.
    if(_m_write_pos < _m_read_pos + _m_current_size) {  //< on 32 bit processor _m_read_pos may be invalid due to race conditions. on 64 bit this _m_read_pos may need no  synchronisation but may cause false negative. i.e. _m_read_pos is smaller than it actually would be with synchronisation
      _m_data[ toLocalPos( _m_write_pos)]  = o;  //<< do not use inline post increment operator. May cause race condition. toLocalPos(_m_write_pos++) is executed but query of _m_data not because thread stalls for a moment and the reader thread may receive the new value of _m_write_pos even without sync

      ++_m_write_pos;
      // do a sync after storing the data;
      //_m_sync_flag.clear(std::memory_order_release);
      return true;
    }
    return false;
  }

}

