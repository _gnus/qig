#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <QDockWidget>
#include "KeyEventHandler.h"

namespace qig
{
  class qigTreeContainer: public qigFocusFrameWidget<QDockWidget>
  {
    Q_OBJECT
    public:
      qigTreeContainer(QWidget* parent=0);

    protected:
      // checks if event ev is related to a git tree
      // tries to load the new tree
      virtual void dropEvent(QDropEvent* ev) override;
      virtual void dragEnterEvent(QDragEnterEvent* ev) override;

      //virtual bool eventFilter(QObject* o, QEvent* ev) override;
      //virtual void childEvent(QChildEvent *ev) override;
    Q_SIGNALS:
      void treeSourceChangeRequest(const QByteArray&);
  };
}
