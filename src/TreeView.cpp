#if 0
#include <assert.h>
#include <QTableView>
#include <QListView>
#include <QDebug>

#include "TreeView.h"
namespace qig
{
  template <>
  void qigTreeView<QTableView>::onDictionaryActivated(const QModelIndex& index)
  {
    if(index.data().toString() == QString("..")) {
      moveUp();
    }
    else if(index.data().toString() == QString(".")) {
    }
    else {
      if(index.isValid())
	qDebug() << "setting new root to "<<index.data();
	qDebug() << "\t new root has"<<model()->rowCount(index) << "entries";
	this->setRootIndex(index);
      assert(rootIndex()==index);
    }
  }
  template <>
  void qigTreeView<QListView>::onDictionaryActivated(const QModelIndex& index)
  {
    if(index.data().toString() == QString("..")) {
      clearSelection();
      moveUp();
      update();
    }
    else if(index.data().toString() == QString(".")) {
    }
    else {
      if(index.isValid())
	qDebug() << "setting new root to "<<index.data();
	qDebug() << "\t new root has"<<model()->rowCount(index) << "entries";
	this->setRootIndex(index);
      assert(rootIndex()==index);
    }
  }
}
#endif
