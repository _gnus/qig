#include <git2.h>
#include <assert.h>

#include "Object.h"
#include "Str.h"
#include "Oid.h"


namespace qig
{

  static git_otype toGitType(objectType t){return (git_otype) t;}
  qigObject::qigObject()
  {
  }

  qigObject::qigObject(const qigOid& id):qigGitObjectHolder(id)
  {
  }
  qigObject::qigObject(git_object *p):qigGitObjectHolder(p)
  {
  }

  // default: retrun false;
  bool qigObject::applyFilter(const qigFilter& ) const
  {
    return false;
  }
  void qigObject::copyString(qigString& out) const
  {
    qigOid dum;
    oid(dum);
    out.append(dum.asString());
  }
  qigObject qigObject::peel(objectType type) const
  {
    qigObject ret(*this);
    ret.peelSelf(type);
    return ret;
  }

  int qigObject::peelSelf(objectType type)
  {
    if(type == objectTypeAny) {
      return GIT_EAMBIGUOUS;
    }
    git_object *peeled;
    int ret=git_object_peel(&peeled, object(objectTypeAny), toGitType(type) );
    if( ret == GIT_OK)
    {
      qigGitObjectHolder::load(peeled);
    }
    return ret;
  }

//  void qigObject::reload()
//  {
//    git_object* p_obj=object(objectTypeAny);
//    const git_oid* id = git_object_id(p_obj);
//    loadNewId(id);
//  }
}


