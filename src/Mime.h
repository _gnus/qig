#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include "enums.h"

class QString;


namespace qig
{
  /// returns "application/oid"
  const QString& mimeOid();
  /// returns "application/object-type"
  const QString& mimeObjectType();
  ///// returns "application/object-commit"
  //const QString& mimeObjectCommit();
  ///// returns "application/object-tree"
  //const QString& mimeObjectTree();
  ///// returns "application/object-blob"
  //const QString& mimeObjectBlob();
  ///// returns "application/object-tag"
  //const QString& mimeObjectTag();
}
