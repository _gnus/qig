#pragma once
/*
 * Copyright (C) the qig contributors. All rights reserved.
 *
 * This file is part of libgit2, distributed under the GNU GPL v3 with
 * a Linking Exception. For full terms see the included COPYING file.
 */
#include <List.h>
#include <memory>

namespace qig
{
  class qigFilter;
  class qigObject;
  /**
   * @ingroup Application
   * @class qigLogFilterList
   * a container for a container with some convenience function.
   * does not do a lot
   */
  class qigLogFilterList
  {
    typedef void(*releaseFilter_f)(qigFilter*);
    typedef qigList <
	  std::unique_ptr <
	    //qigFilter, std::function<void(qigFilter*)>
	    qigFilter, releaseFilter_f
	    > > filterList;
    public: 
      qigLogFilterList();


      /** filter on Object property*/
      //bool applyFilter(const qigObject&) const;
      /** apply all filter added on object obj. if the qigLogFilterList does not contain a filter the method return true;
       * otherwise a AND combination of all filters.
       * @return the filter result of all internal filters.
       */
      bool applyFilter(const qigObject& obj) const ;

      /** adds a new filter. the qigLogFilterList steals the ownership of the filter.
       * @param p the new filter that should be added to the list.
       * p must be allocated on the heap and will be destroyed together with the list.
       */
      void addFilter(qigFilter* p) {m_filters.push_back(filterList::value_type(p, releaseFilter));}

      /** returns true if all filter are empty
       */
      bool isEmpty() const;

    private:
      static void releaseFilter(qigFilter*);
      filterList m_filters;
  };
}
