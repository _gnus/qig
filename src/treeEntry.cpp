#include <assert.h>
#include <algorithm>

#include "treeEntry.h"

namespace qig
{
  const size_t treeEntry::npos = -1;
  //{{{ dtor
  treeEntry::~treeEntry()
  {
    remove();
  }

  //}}}
  
  //{{{ public function
  void treeEntry::pushChild(treeEntry*p)
  {
    if(p) {
      // p should not be contained as child nor should p have a parent
      assert( !isChild(p) && !( p->parent() && p->parent() != this  ) );
      m_children.push_back(p);
      p->setParent(this);
    }
  }

  size_t treeEntry::locate() const
  {
    size_t parent_n_child = 0, i=npos;
    if(parent())
    {
      parent_n_child = parent()->m_children.size();
      for (i=0; i<parent_n_child; ++i)
      {
	if(parent()->childByIndex(i) == this) {
	  break;
	}
      }
    }
    return i >= parent_n_child ? npos: i;
  }
  //}}}

  //{{{ protected function
  bool treeEntry::isChild(treeEntry*p) const
  {
    const typename childrenList::const_iterator& cend = m_children.cend();
    return std::find(m_children.cbegin(), cend, p) != cend;
  }

  void treeEntry::remove()
  {
    if(parent())
    {
      const typename childrenList::const_iterator& cend = parent()->m_children.cend();
      const typename childrenList::const_iterator& iter = std::find(parent()->m_children.cbegin(), cend, this);
      if(iter != cend) {
	_parent()->m_children.erase(iter);
      }
    }
    // treeEntry not owned by node. just make sure there are no dangling pointer
    for(auto iter = m_children.begin(); iter != m_children.end(); ++iter) {
      (*iter)->setParent(0);
    }
    m_children.clear();
    setParent(nullptr);
  }
  //}}}
  //

  template <typename T, typename F>
    void f(T* t, F f)
    {
      (t->*f)(0);
      //t->(*f)();
    }

  //{{{ private methods
  void treeEntry::traverse( treeEntry& entry, void * p)
  {
    entry.visit(p);
    for(treeEntry* item : entry.m_children){
      traverse(*item, p);
    }
  }
  //}}}

}
