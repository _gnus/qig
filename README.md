# README #
### What is this repository for? ###

* a new git client based on qt and libgit2
* the program is under heavy development and lacks many features

### How do I get set up? ###

```
#!bash
cmake .
make
```


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact