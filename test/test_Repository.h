
#pragma once
#include <framework.h>
#include <../src/Repository.h>

class test_Repository:public QObject
{
  Q_OBJECT

    // the tests
  private Q_SLOTS:
    void test_init();
    // end tests

  private Q_SLOTS:
    // global cleanup
    //void initTestCase();
    //void cleanupTestCase();

    //init for each test
    void init();
    void cleanup();

  // helper function
  private:
    qig::qigRepository *m_repo;
};

DECLARE_TEST(test_Repository);
