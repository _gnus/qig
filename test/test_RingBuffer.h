#pragma once
#include <framework.h>
#include <../src/RingBuffer.h>

//#include "repo_loader.h"

using namespace qig;
/** test for qig::RingBuffer
 * @class test_GitObjectHolder
 */
class test_RingBuffer:public QObject
{
  Q_OBJECT

  typedef void (test_RingBuffer::*startReaderThread_f)();
    // the tests
  private Q_SLOTS:
    void test_feed_and_read_single_threaded();
    void test_feed_and_read_threaded();
    // end tests

  private Q_SLOTS:
    // global cleanup
    //void initTestCase();
    //void cleanupTestCase();

    //init for each test
    //void init(){};
    //void cleanup();

  // helper function
  public:
    test_RingBuffer();

    static void startThread(test_RingBuffer* p){ (p->*(p->start_reader_thread))();}

  private:
    const unsigned int buffer_size;
    qig::qigRingBuffer11<int*> m_buffer;
    int test_again_single_threaded;
    int test_again_threaded;


    startReaderThread_f start_reader_thread;

    void simpleReader();
    const unsigned int reader_test_objects;

    std::vector<int*> popped_items;

    // will be cleared by Reader thread when the thread starts;
    std::atomic_flag reader_is_running;
    std::atomic_flag reader_finished_reading;
};

//DECLARE_TEST(test_RingBuffer);
