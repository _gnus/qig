#include <framework.h>

#include "../src/Status.h"
#include "stubs/Repository.h"

class test_Status: public QObject
{
  Q_OBJECT
  public:
    test_Status();
  private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void test_status();

  private:
    git_repository * m_repo;
    qig::qigRepositoryStub m_repo_wrap;
};
DECLARE_TEST(test_Status);
