#include "test_KeyEventHandler.h"



test_KeyEventHandler::test_KeyEventHandler()
{
}

void test_KeyEventHandler::init()
{
  m_event_triggered = false;
  m_test_object_worker = new qig::qigKeyEventHandlerWorker;
  m_test_object.addInputHandler(m_test_object_worker);
  connect(m_test_object_worker, &qig::qigKeyEventHandlerWorker::triggered, this, &test_KeyEventHandler::onKeyPress);
};
void test_KeyEventHandler::cleanup()
{
  m_event_triggered = false;
  m_test_object.removeInputHandler(m_test_object_worker);
  delete m_test_object_worker;
  m_test_object_worker=0;
}

void test_KeyEventHandler::test_single_key()
{
  m_test_object_worker->setKeySequence(QKeySequence("d"));
  QKeyEvent ev(QEvent::KeyPress, Qt::Key_D, Qt::KeyboardModifiers(), QString("D"));
  m_test_object.keyPressEvent(&ev);

  QCOMPARE(m_event_triggered, true);
}

void test_KeyEventHandler::test_single_key_with_modifer()
{
  m_test_object_worker->setKeySequence(QKeySequence("Ctrl+d"));
  QKeyEvent ev(QEvent::KeyPress, Qt::Key_D, Qt::KeyboardModifiers(Qt::ControlModifier), QString("D"));
  m_test_object.keyPressEvent(&ev);

  QCOMPARE(m_event_triggered, true);
}

void test_KeyEventHandler::test_single_key_with_modifers()
{
  m_test_object_worker->setKeySequence(QKeySequence("Alt+Ctrl+d"));
  QKeyEvent ev(QEvent::KeyPress, Qt::Key_D, Qt::KeyboardModifiers(Qt::AltModifier|Qt::ControlModifier), QString("D"));
  m_test_object.keyPressEvent(&ev);

  QCOMPARE(m_event_triggered, true);
}

void test_KeyEventHandler::test_double_key()
{
  m_test_object_worker->setKeySequence(QKeySequence("d,a"));
  QKeyEvent ev_d(QEvent::KeyPress, Qt::Key_D, Qt::KeyboardModifiers(), QString("D"));
  QKeyEvent ev_a(QEvent::KeyPress, Qt::Key_A, Qt::KeyboardModifiers(), QString("a"));

  // should not be recognised because a must be preceded by d
  m_test_object.keyPressEvent(&ev_a);
  QCOMPARE(m_event_triggered, false);

  m_test_object.keyPressEvent(&ev_d);
  QCOMPARE(m_event_triggered, false);
  // should be recognised
  m_test_object.keyPressEvent(&ev_a);
  QCOMPARE(m_event_triggered, true);

  m_event_triggered=false;
  m_test_object.keyPressEvent(&ev_d);
  QCOMPARE(m_event_triggered, false);
  // should not be recognised because event should be recognised as d, d
  m_test_object.keyPressEvent(&ev_d);
  QCOMPARE(m_event_triggered, false);

  // should not be recognised because event should be recognised as first event
  m_test_object.keyPressEvent(&ev_a);
  QCOMPARE(m_event_triggered, false);

  m_test_object.keyPressEvent(&ev_d);
  QCOMPARE(m_event_triggered, false);
  m_test_object.keyPressEvent(&ev_a);
  QCOMPARE(m_event_triggered, true);
}

void test_KeyEventHandler::test_double_key_with_modifer()
{
  m_test_object_worker->setKeySequence(QKeySequence("Shift+d, Ctrl+A"));
  QKeyEvent ev_d(QEvent::KeyPress, Qt::Key_D, Qt::KeyboardModifiers(Qt::ShiftModifier), QString("D"));
  QKeyEvent ev_a(QEvent::KeyPress, Qt::Key_A, Qt::KeyboardModifiers(Qt::ControlModifier), QString("a"));


  m_test_object.keyPressEvent(&ev_d);
  QCOMPARE(m_event_triggered, false);
 
  m_test_object.keyPressEvent(&ev_a);
  QCOMPARE(m_event_triggered, true);
}
