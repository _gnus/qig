

#include <unistd.h>

#include <QString>
#include "test_Repository.h"
#include "../src/enums.h"
#include "../src/Oid.h"


void test_Repository::init()
{
  QVERIFY( qig::GIT_OK==qig::qigRepository::load(m_repo, ".", false));
}

void test_Repository::cleanup()
{
  m_repo->release();
}

void test_Repository::test_init()
{
  // 
  const qig::qigChar* relative_path;
  char pwd [101]={0};
  strncpy(pwd, m_repo->repositoryRootPath(),100);

  // check if path is the root itself
  QCOMPARE(qig::GIT_OK|0 , m_repo->relativePathFromAbsolutePath(pwd, &relative_path));
  QCOMPARE('\0', *relative_path);

  // build a abs path 
  QVERIFY(strlen(pwd)); // check if pwd is valid;
  strcat(pwd,"foobar");
  // try to obtain the relative path 
  QCOMPARE(qig::GIT_OK|0 , m_repo->relativePathFromAbsolutePath(pwd, &relative_path));
  QCOMPARE(0, strncmp(relative_path, "foobar",100));


  qig::qigOid oid;
  uint8_t data[20]={0x0f};
  m_repo->initialiseOid(oid, data);
  QVERIFY(oid.isValid()); // < does not test whether oid exists in odb
  const unsigned char *d;
  oid.rawData(&d);
  QCOMPARE(0, memcmp(d,data,20));
}
