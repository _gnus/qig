#include "framework.h"
#include "../src/gitTypes.h"

#include "stubs/Repository.h"

class test_StatusTree: public QObject
{
  Q_OBJECT
  private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void test_status();

  public: 
    test_StatusTree();
  private:
    git_repository * m_repo;
    git_tree * m_object;
    qig::qigRepositoryStub m_repo_wrap;
};
DECLARE_TEST(test_StatusTree);
