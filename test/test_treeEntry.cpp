// vim: fdm=marker
#include <assert.h>
#include "test_treeEntry.h"
#include <../src/fileEntry.h>

using namespace qig;
class treeEntryMock: public qig::fileEntry
{
  public:
    treeEntryMock();
    treeEntryMock(const treeEntryMock&) = delete;

    int visitCnt(){return is_visited;}
    using fileEntry::parent;
    using fileEntry::isChild;
    treeEntryMock* childByIndex(unsigned int i) { return static_cast<treeEntryMock*>(fileEntry::_childByIndex(i));}
    template <typename F, typename P>
      void traverse(F f, P p) {
	treeEntry::traverse(this, f, p);
      }


  public:
    virtual void visit(void *) override;
    void markAsVisisted(const treeEntryMock& entry);
    int is_visited;
};


//{{{ impl of mock treeEntryMock

treeEntryMock::treeEntryMock(): fileEntry(), is_visited(0)
{
}

void treeEntryMock::visit(void*)
{
  markAsVisisted(*this);
}

void treeEntryMock::markAsVisisted(const treeEntryMock &entry)
{
  treeEntryMock& e = const_cast<treeEntryMock&>(entry);
  ++(e.is_visited);
}

//}}}


test_treeEntry::test_treeEntry()
{
}

void test_treeEntry::init()
{
  createTree(m_tree_entries);
}

void test_treeEntry::cleanup()
{
  m_tree_entries.clear();
}

void test_treeEntry::test_ctor()
{
  typedef qig::treeEntry entry;

  entry root(0);
  entry child(&root,0);

  const entry& const_child = child;

  QVERIFY(const_child.parent() == & root);
  //QVERIFY(const_child_with_payload.parent() == & root);


}

void test_treeEntry::test_createTree()
{
  // m_tree_entries is empty indicates an error in test setup
  QVERIFY( !m_tree_entries.empty() );

  auto root =  m_tree_entries.find(treeEntryPtr());
  // check for our root impl
  QVERIFY(root->second == this->root());
  QCOMPARE(this->root()->locate(), qig::treeEntry::npos);
  QVERIFY( root!=m_tree_entries.end() );
  root->second->treeEntryMock::traverse([](treeEntryMock*p, void*q){p->visit(q);}, (void*) nullptr );

  const treeEntryList::const_iterator special_node = m_tree_entries.find(treeEntryPtr());
  int root_cnt=0;
  for(auto iter= m_tree_entries.cbegin(); iter!=m_tree_entries.cend(); ++iter)
  {
    // exclude the special (empty) node that points to the root of our unordered_multimap
    if(iter  == special_node) {
      continue;
    }
    // dump should traverse the tree and visit all nodes excatly once
    QCOMPARE(iter->first->visitCnt(), 1);
    
    // count the roots
    // roots do not have parents
    if(iter->first->parent()  == 0) {
      ++root_cnt;
    }
  }
  // there should be only one root
  QCOMPARE(root_cnt, 1);

  // special handling for parent
  std::pair<treeEntryList::const_iterator, treeEntryList::const_iterator> entries = m_tree_entries.equal_range(treeEntryPtr());
  int nchildren = std::distance( entries.first, entries.second);
  // there must be excalty one root. otherwise there is an error in createTree
  QVERIFY(nchildren==1);
  on_step = &test_treeEntry::checkChildrenCount;
  unsigned long nodes_visited = traverse(entries.first->second, 0);
  // substract the entry pointing to root
  QCOMPARE(nodes_visited, m_tree_entries.size()-1);
}


void test_treeEntry::test_delete()
{
  //select a random item with children and parent and try to delete it
  auto iter = m_tree_entries.cbegin();
  while( ( iter->second->parent()==0 || iter->second->childrenCount() == 0) && iter != m_tree_entries.end())
  {
    ++iter;
  }
  QVERIFY(iter!= m_tree_entries.end());

  bool found=false;
  auto parent = iter->first->parent();
  for(auto i=0U; i< parent->childrenCount(); ++i)
  {
    if( parent->childByIndex(i) == iter->first.get() )
    {
      found=true;
    }
  }
  QVERIFY(found);

  // test method locate
 
  {
    treeEntryPtr child = iter->second;
    const qig::treeEntry* parent = child->parent();
    int pos = child->locate();
    QVERIFY(pos != -1);
    QCOMPARE(parent->childByIndex(pos), child.get());
  }


  auto children = m_tree_entries.equal_range(iter->first);
  // remove the element from the tree structure
  iter->first->remove();

  // parent must not have a pointer of the removed element
  for(auto i=0U; i< parent->childrenCount(); ++i)
  {
    QVERIFY( parent->childByIndex(i) != iter->first.get() );
  }
  for(auto iter= children.first; iter!=children.second; ++iter)
  {
    QVERIFY(iter->second->parent() == 0);
  }
}

void test_treeEntry::test_setFlag()
{
  auto iter = m_tree_entries.begin();
  while( ( iter->second->parent()==0 || iter->second->childrenCount() == 0) && iter != m_tree_entries.end())
  {
    ++iter;
  }
  QVERIFY(iter!= m_tree_entries.end());
  // modified flag should propagate to parent
  iter->second->setStateFlag(qig::fileStateIndexModified);
  QVERIFY(iter->first->stateFlag() == qig::fileStateIndexModified);

  // confilict flag should propagate to parent
  iter->second->setStateFlag(qig::fileStateConflicted);
  QVERIFY(iter->first->stateFlag() & qig::fileStateConflicted);
}
void test_treeEntry::test_parentChildRelation()
{
  using namespace qig;
  treeEntryMock parent;
  treeEntryMock child;
  treeEntryMock child2;
  parent.pushChild(&child);
  parent.pushChild(&child2);

  QCOMPARE(parent.isChild(&child), true);
  QCOMPARE(child.parent(), &parent);
  QCOMPARE(parent.childByIndex(0), &child);
  QCOMPARE(parent.childrenCount(), size_t(2U));
  QCOMPARE(child.childrenCount(), size_t(0U));

  // test flags
  child.setStateFlag(fileStateIndexModified);
  child2.setStateFlag(fileStateIndexModified);
  QCOMPARE(parent.stateFlag(),(int) fileStateIndexModified);
  // remove fileStateIndexModified from child. parent should still has flag modified
  child.removeStateFlag(fileStateIndexModified);
  QCOMPARE(parent.stateFlag(), (int)fileStateIndexModified);
  QCOMPARE(child.stateFlag(), (int)fileStateUnchanged);
  // add another flag
  child2.removeStateFlag(fileStateIndexModified);
  QCOMPARE(parent.stateFlag(), (int)fileStateUnchanged); 
}

void test_treeEntry::checkChildrenCount(treeEntryPtr& parent, void*)
{
  //std::pair<treeEntryList::const_iterator, treeEntryList::const_iterator> entries = m_tree_entries.find(parent);
  auto  entries = m_tree_entries.equal_range(parent);
  size_t nchildren = std::distance( entries.first, entries.second);

  QCOMPARE(nchildren, parent->childrenCount());
}

test_treeEntry::treeEntryPtr& test_treeEntry::root()
{
  std::pair<treeEntryList::iterator, treeEntryList::iterator> entries = m_tree_entries.equal_range(treeEntryPtr());
  return entries.first->second;
}

int test_treeEntry::traverse(const treeEntryPtr& parent, void *p)
{
  int cnt=0;
  auto  entries = m_tree_entries.equal_range(parent);
  // test number of children for children recursively
  for(auto iter = entries.first; iter != entries.second; ++iter)
  {
    ++cnt;
    if(on_step)
    {
      (this->*on_step)(iter->second, p);
    }
    cnt+=traverse(iter->second, p);
  }
  return cnt;
}

void test_treeEntry::test_checkState()
{
  auto root = this->root();
  root->setCheckState(Qt::Checked);
  root->traverse([](qig::fileEntry*p, int){QCOMPARE(p->checkState() , Qt::Checked);}, 0);

  root->setCheckState(Qt::Unchecked);
  root->traverse( [](qig::fileEntry*p, int){QCOMPARE(p->checkState() , Qt::Unchecked);},0);

  // test partially checked
  QVERIFY(root->childrenCount()>0);
  treeEntryMock* child=root->childByIndex(0);
  QVERIFY(child->childrenCount()>1);
  treeEntryMock* grand_child=child->childByIndex(1);
  grand_child->setCheckState(Qt::Checked);
  QCOMPARE( child->checkState(), Qt::PartiallyChecked  );

  // test transition partially -> checked when last child get checked
  child->childByIndex(0)->setCheckState(Qt::Checked);
  QCOMPARE( child->checkState(), Qt::Checked );
  
  // the other way round
  child->childByIndex(0)->setCheckState(Qt::Unchecked);
  QCOMPARE( child->checkState(), Qt::PartiallyChecked  );
  // test transition partially -> unchecked when last check child get unchecked
  grand_child->setCheckState(Qt::Unchecked);
  QCOMPARE( child->checkState(), Qt::Unchecked  );
}

/** builds a special tree with a depth of 50
 * each height h as one child more than h-1
 * the last node is parent of the next height
 *
 * the created tree with a height of 3 would be
 *              n0
 *             /  \
 *           n1    n2
 *               / | \
 *              n3 n4 n5
 */
void test_treeEntry::createTree(treeEntryList& list) const
{
  const int depth=5;
  // due to special structe we can calcualte the number of elements in the tree
  const int entries = (depth+1) * depth / 2;
  int cnt=0;
  // construct the root element
  treeEntryPtr parent=treeEntryPtr(new treeEntryMock);
  // and add a special ptr to find the root
  list.insert(treeEntryList::value_type(treeEntryPtr(), parent));
  treeEntryWeakPtr temp_parent=parent;
  for ( int i = 0; i<=depth; ++i)
  {
    for( auto j = 0; j< i; ++j)
    {
      ++cnt;
      treeEntryPtr entry (new treeEntryMock);
      list.insert(treeEntryList::value_type( parent, entry));
      parent->pushChild(entry.get());
      temp_parent = entry;
    }
    parent=temp_parent.lock();
  }
  // check number of the entries 
  QCOMPARE(cnt , entries);
}
