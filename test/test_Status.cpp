#include <git2.h>

#include "test_Status.h"
#include "stubs/ObjectStub.h"
#include "stubs/repository.h"
#include "../src/Status.h"

test_Status::test_Status()
{
  m_repo=0;
}

void test_Status::initTestCase()
{
  m_repo = loadLocal();

  m_repo_wrap.init(m_repo);
}

void test_Status::cleanupTestCase()
{
  m_repo_wrap.cleanUp();
}

void test_Status::test_status()
{
  using qig::qigStatus;
  qigStatus st(m_repo_wrap);
  st.updateStatus();

  //TODO how to est result

}
