#include <git2.h>
#include <QDebug>

#include "test_DiffViewerBuiltinDiffer.h"
#include "stubs/ObjectStub.h"
#include "stubs/repository.h"
#include "../src/DiffViewerBuiltinTextDocument.h"
#include "../src/DiffViewerBuiltinDiffer.h"

test_DiffViewerBuiltinDiffer::test_DiffViewerBuiltinDiffer()
{
  m_repo=0;
}

void test_DiffViewerBuiltinDiffer::initTestCase()
{
  m_repo = loadLocal();

  m_repo_wrap.init(m_repo);
}

void test_DiffViewerBuiltinDiffer::cleanupTestCase()
{
  m_repo_wrap.cleanUp();
}

void test_DiffViewerBuiltinDiffer::test_diff()
{
  /**
  qig::qigDiffViewerBuiltinTextDocument model(this);
  qig::qigDiffViewerBuiltinDifferBufferToBlob differ(model);
  const char old_buffer[]="abc\ncde";
  const char new_buffer[]="abc";
  size_t old_len = strlen(old_buffer);
  size_t new_len = strlen(new_buffer);
  differ.diff(old_buffer, old_len,"old", new_buffer, new_len,"new");

  QCOMPARE(model.rowCount(), 1);

  const QModelIndex &file = model.index(0,0);
  QVERIFY(file.isValid());
  const QModelIndex &hunk = model.index(0,0,file);
  QVERIFY(hunk.isValid());
  QString hunkStr = hunk.data().toString();
  QCOMPARE(hunkStr, QString::fromLatin1("@@ -1,2 +1 @@\n"));
  const QModelIndex &line = model.index(0,0,hunk);
  QVERIFY(line.isValid());
  QString lineStr = line.data().toString();
  QCOMPARE(lineStr, QString::fromLatin1("- abc\n"));
  //TODO how to est result
  */

}
