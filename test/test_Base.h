
#include <framework.h>

struct git_repository;

namespace qig
{
  class qigBase;
}

class test_Base: public QObject
{
  Q_OBJECT
  private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();
    void test_valid();
    void test_copy();

  private:
    git_repository* m_repo;
    qig::qigBase * m_test_object;
};
DECLARE_TEST(test_Base);
