#pragma once
#include <../src/GitOidHolder.h>
#include <framework.h>

//#include "repo_loader.h"

using namespace qig;
/** test for qig::qigGitOidHolder
 * @class test_GitOidHolder
 * TODO test that all objects are freed 
 * but objects are ref_cnt don't know how to do;
 */
class test_GitOidHolder:public QObject,private qigGitOidHolder//, public repo_loader
{
  Q_OBJECT

    // the tests
  private Q_SLOTS:
    void test_ctor();
    void test_operator();
    void test_assign();

  public:
    test_GitOidHolder();

  private:
    virtual void reloadFromId(){++reload_called;};
    virtual void reset(){++reset_called;};


  private:
    int reload_called;
    int reset_called;
};

DECLARE_TEST(test_GitOidHolder);
