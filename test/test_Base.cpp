#include "test_Base.h"
#include "stubs/Repository.h"
#include "stubs/repository.h"

void test_Base::initTestCase()
{
}
void test_Base::cleanupTestCase()
{
}

void test_Base::init()
{
  m_test_object = new qig::qigRepositoryStub();
  m_repo=  createSimple();
  m_test_object->repositoryLoaded(m_repo);
}

void test_Base::cleanup()
{
  delete m_test_object;
}
void test_Base::test_valid()
{
  cleanup();

  qig::qigBase test_object(nullptr) ;
  QVERIFY2( test_object.isValid() == false ,  "uninitialised object is invalid");

  init();
  QVERIFY2( m_test_object->isValid() == true ,  "initialised object is valid");
  QCOMPARE(m_test_object->_m_repo.use_count() , 1l);

  m_test_object->repositoryUnloaded();
  QVERIFY2( m_test_object->isValid() == false ,  "uninitialised object is invalid");

  m_test_object->repositoryLoaded(createSimple());
  QVERIFY2( m_test_object->isValid() == true ,  "uninitialised object is invalid");
}

void test_Base::test_copy()
{
  qig::qigBase copy = *m_test_object;
  QCOMPARE(m_test_object->_m_repo.use_count() , 2l);
  QVERIFY2( copy.isValid() == true ,  "uninitialised object is invalid");

  qig::qigBase move = std::move(copy);
  QCOMPARE(m_test_object->_m_repo.use_count() , 2l);
  QVERIFY2( move.isValid() == true ,  "uninitialised object is invalid");

}

