#include <unistd.h>
#include <thread>

#include "test_RingBuffer.h"



test_RingBuffer::test_RingBuffer():
  buffer_size(10),
  m_buffer(buffer_size),
  test_again_single_threaded(1),
  test_again_threaded(1),
  reader_test_objects(15000*buffer_size), //< add 15000 times more objects then items can store
  reader_is_running(ATOMIC_FLAG_INIT),
  reader_finished_reading(ATOMIC_FLAG_INIT)
{
}

void test_RingBuffer::test_feed_and_read_single_threaded()
{

  const unsigned int mock_object_size = buffer_size + 3;
  int mock_objects[ mock_object_size ];

  // test on empty buffer
  int * popped_object;
  popped_object = m_buffer.pop();

  QVERIFY2(popped_object == 0, "pop on empty buffer should return 0");


  // try push more objects to the buffer than it can store;
  unsigned int pushed_objects=0;
  while(m_buffer.push( &(mock_objects[pushed_objects] ) ) && pushed_objects < mock_object_size)
  {
    ++pushed_objects;
  }

  // only as many objects should be pushed as the buffer can store
  // QCompare must take same type
  QCOMPARE( pushed_objects, (unsigned int)buffer_size);

  // pop them, they should be still the same as the mock_objects
  for(pushed_objects =0; pushed_objects<buffer_size; ++pushed_objects)
  {
    popped_object = m_buffer.pop();
    QCOMPARE(popped_object, &(mock_objects[pushed_objects]));
  }
  if(test_again_single_threaded--)
  {
    test_feed_and_read_single_threaded();
  }
}

void test_RingBuffer::test_feed_and_read_threaded()
{
  // do not delete the braces. mock_objcts should be freed before calling this function recursively
  {
    int mock_objects[reader_test_objects];

    popped_items.clear();
    popped_items.reserve(reader_test_objects);

    // make sure flag is set when the reader thread starts
    reader_is_running.test_and_set();
    start_reader_thread = &test_RingBuffer::simpleReader;
    // start reader thread;
    std::thread reader(test_RingBuffer::startThread, this);


    //reader.start();


    // wait till the reader thread clears the flag
    while(reader_is_running.test_and_set())
    {
      usleep(1);
    }
    reader_is_running.clear();

    // start to push all mock_objects
    for(unsigned int i=0; i<reader_test_objects; )
    {
      if(m_buffer.push(& mock_objects[i]))
      {
	++i;
      }
    }

    // wait till the reader thread finishes
    reader.join();
    // the reader thread should have pushed all objects to popped_items;
    QCOMPARE((unsigned int) reader_test_objects, (unsigned int) popped_items.size());

    for(size_t i = 0; i< popped_items.size(); ++i)
    {
      /*
      if(popped_items[i] != & mock_objects[i])
      {
	qDebug() << "comparing item "<< i << " items are: " << popped_items[i] <<" and " << & mock_objects[i];
	qDebug() <<" delta is " << (int) (&mock_objects[i] - popped_items[i]) <<" and item read before invalid is "<< popped_items[i-1];
	qDebug() << "start pointer is " << &mock_objects[0] << " last valid item is " << & mock_objects[reader_test_objects-1];
      }
      */
      QCOMPARE(popped_items[i], & mock_objects[i]);
    }
  }
  popped_items.clear();
  //if(test_again_threaded--)
  //  test_feed_and_read_threaded();
}

void test_RingBuffer::simpleReader()
{
  // clear the flag
  reader_is_running.clear();

  while(popped_items.size() != reader_test_objects)
  {
    int * popped_object = m_buffer.pop();
    if(popped_object)
    {
      popped_items.push_back(popped_object);
    }
  }
  reader_finished_reading.clear();
}

