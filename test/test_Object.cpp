#include <git2.h>

#include "stubs/ObjectStub.h"
#include "test_Object.h"
#include "stubs/repository.h"
#include "../src/StringFilter.h"
#include "../src/Oid.h"
#include "../src/Commit.h"
#include "../src/enums.h"
#include "../src/Tree.h"


  test_Object::test_Object()
  {
    m_object = 0;
    m_repo=0;
  }

  void test_Object::initTestCase()
  {
    git_libgit2_init();

    m_repo=  createSimple();

    if(objects_length <2)
      throw 1;

    m_object = getObjectByTypeAndIndex(qig::objectTypeCommit,1);
  }

  void test_Object::cleanupTestCase()
  {
    git_object_free(m_object);
    free_repository(m_repo);
  }


  void test_Object::test_ctor()
  {
    // test the default ctor;
    git_object* obj1; git_object_dup(&obj1, m_object);
    qigObjectStub test_object1(obj1);

    // create a new object from the same git_object 
    git_object* obj2; git_object_dup(&obj2, m_object);
    qigObjectStub test_object2Dum(obj1);
    qig::qigObject &test_object2=test_object2Dum;


    // objects constructed from the same objects should be the same
    QVERIFY(test_object1.isValid());
    QVERIFY(test_object1 == test_object2);
  }

  void test_Object::test_filter()
  {
    // create an commit
    git_object* obj1; git_object_dup(&obj1, m_object);
    qigObjectStub test_object1Dum(obj1);
    qig::qigObject &dum=test_object1Dum;
    qig::qigOid oid;
    dum.oid(oid);
    qig::qigCommit test_object1;
    test_object1.load(oid);

    // the commiter
    qig::qigStringFilter commiterfilter("Tobias", qig::logFilter::fieldCommiter);
    QVERIFY2(test_object1.applyFilter(commiterfilter), "filter should match");

    // the message
    qig::qigStringFilter messagefilter("i4", qig::logFilter::fieldCommitMessage);
    QVERIFY2(test_object1.applyFilter(messagefilter), "filter should match");

    // the header
    qig::qigStringFilter headerfilter("4->b", qig::logFilter::fieldCommitHeader);
    QVERIFY2(test_object1.applyFilter(headerfilter), "filter should match");

    // test negative
    qig::qigStringFilter headerfilter2("this text does not match", qig::logFilter::fieldCommitHeader);
    QVERIFY2(!test_object1.applyFilter(headerfilter2), "filter not should match");
  }

  void test_Object::test_treeObject()
  {
    QVERIFY2(git_object_type(m_object) == GIT_OBJ_COMMIT, "test requires commit object");

    const git_oid* id = git_object_id(m_object);

    qigOidStub test_oid(id, m_repo);
    qig::qigTree tree(test_oid);
    QVERIFY2(tree.entryCount() > 0, "expected a tree with entries");

    QStringList entry_name;
    entry_name <<"br_1"<<"mas";
    QCOMPARE((unsigned long)entry_name.size() , tree.entryCount());
    for(auto i=0UL ; i< tree.entryCount(); ++i)
    {
      QCOMPARE( QString(tree.name(i)), entry_name[i]);
    }

    qig::qigTree empty_tree;
    QCOMPARE((unsigned long)0 , empty_tree.entryCount());

  }
