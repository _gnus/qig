
#pragma once
#include <framework.h>
#include "../src/KeyEventHandler.h"

using namespace qig;
/** test for qig::qigKeyEventHandler
 */
class test_KeyEventHandler:public QObject
{
  Q_OBJECT
    // the tests
  private Q_SLOTS:
    void test_single_key();
    void test_single_key_with_modifer();
    void test_single_key_with_modifers();
    void test_double_key();
    void test_double_key_with_modifer();
    // end tests

  private Q_SLOTS:
    // global cleanup
    //void initTestCase();
    //void cleanupTestCase();

    //init for each test
    void init();
    void cleanup();

  protected Q_SLOTS:
    // not a real test case. do not declare as private slot
    void onKeyPress(){m_event_triggered=true;}

  // helper function
  public:
    test_KeyEventHandler();

  private:
    bool m_event_triggered;
    qig::qigKeyEventHandlerWorker *m_test_object_worker;
    qig::qigKeyEventHandler        m_test_object;
};

DECLARE_TEST(test_KeyEventHandler);
