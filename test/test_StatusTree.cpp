#include "../src/StatusTree.h"

#include <cassert>
#include <QDebug>
#include <git2.h>

#include "test_StatusTree.h"
#include "stubs/ObjectStub.h"
#include "stubs/repository.h"

#include "../src/Tree.h"
#include "../src/DiffViewerBuiltinDiffer.h"

using namespace qig;

test_StatusTree::test_StatusTree()
{
  m_repo=0;
}

void test_StatusTree::initTestCase()
{
  m_repo = createSimple();

  m_repo_wrap.init(m_repo);
}

void test_StatusTree::cleanupTestCase()
{
  m_repo_wrap.cleanUp();
}

void test_StatusTree::test_status()
{
  git_commit* ci = (git_commit*) getObjectByTypeAndIndex(GIT_OBJ_COMMIT, 6);
  QVERIFY(ci);
  qigObjectStub w((git_object*) ci);
  QVERIFY( git_commit_parentcount(ci) );
  const git_oid* parent = git_commit_parent_id(ci, 0);

  qigOid s;
  w.oid(s);
  qigOidStub t(parent, git_commit_owner(ci));

  qigStatusTree test_object;
  qigDifferHelperBuiltinTreeToTree filler(test_object);
  filler.setSource(s, qigDifferHelper::sourceTypeNewSource);
  filler.setSource(t, qigDifferHelper::sourceTypeOldSource);

  QVERIFY(filler.diff()==::GIT_OK);
  unsigned int flag;
  test_object.statusFile(flag, QString("br_4"));

  QCOMPARE(uint32_t(flag), uint32_t(fileStateWDNew));
  //TODO test delete and modified
}
