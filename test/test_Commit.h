#pragma once
#include <framework.h>
#include <../src/Commit.h>

/** test for qig::qigCommit
 * @class test_Commit
 */
class test_Commit:public QObject
{
  Q_OBJECT

    // the tests
  private Q_SLOTS:
    void test_ctor();
    void test_filter();
    void test_commit();
    // end tests

  private Q_SLOTS:
    // global cleanup
    void initTestCase();
    void cleanupTestCase();

    //init for each test
    //void init(){};
    //void cleanup();

  // helper function
  public:
    test_Commit();

  private:
    git_object* m_object;
    git_repository *m_repo;
};

DECLARE_TEST(test_Commit);
