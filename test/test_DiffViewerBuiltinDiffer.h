#include <framework.h>

#include "../src/LogModelHistoryWorker.h"
#include "stubs/Repository.h"

class test_DiffViewerBuiltinDiffer: public QObject
{
  Q_OBJECT
  private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void test_diff();

  public: 
    test_DiffViewerBuiltinDiffer();
  private:
    git_repository * m_repo;
    git_object * m_object;
    qig::qigRepositoryStub m_repo_wrap;
};
DECLARE_TEST(test_DiffViewerBuiltinDiffer);
