#include <QtTest/QTest>
#include <git2.h>
#include "test_GitOidHolder.h"
#include "stubs/GitOidHolderStub.h"
#include <QDebug>
//#include "test.h"


typedef void(*registerTest_f)();
extern void registerTest(registerTest_f);
static test_GitOidHolder TEST_OBJECT;
static void _run_test_GitOidHolder() { QTest::qExec(&TEST_OBJECT);}
registerTest_f x = _run_test_GitOidHolder;
//registerTest(0);


test_GitOidHolder::test_GitOidHolder(): reload_called(0), reset_called(0)
{
}

void test_GitOidHolder::test_ctor()
{

  git_oid id;
  memset(id.id,3,20);


  // p shouldn't be used. any value is ok
  git_repository* p = (git_repository*)567;


  // test nullptr
  //qigGitOidHolderStub  test_objectmock0(0, 0);

  //test ctor
  qigGitOidHolderStub  test_objectmock1(&id, p);
  qigGitOidHolder& test_object1 = test_objectmock1;

  // oid should be the same
  const git_oid * p_oid = test_object1.oid();
  QCOMPARE( memcmp(p_oid->id, id.id, 20) , 0);

  // copy ctor
  qigGitOidHolderStub test_objectmock2(&id, p);
  qigGitOidHolder& test_object2 = test_objectmock2;
  QCOMPARE( test_object2 , test_object1);

  // copy ctor for anonymous object and move ctor for test_object3
  qigGitOidHolderStub test_object3(qigGitOidHolderStub(&id,p));
  QVERIFY( test_object2 == test_object3);

  memset(id.id,1,20);
  qigGitOidHolderStub  test_objectmock_diff(&id, p);
  qigGitOidHolder& test_object_diff = test_objectmock_diff;
  QCOMPARE(test_object_diff == test_object1, false);//< should differ

}

void test_GitOidHolder::test_operator()
{
  git_oid id;
  qigGitOidHolderStub stub1(&id, nullptr);
  qigGitOidHolderStub stub2(&id, nullptr);

  const git_oid* stub1_oid = stub1.oid();
  const git_oid* stub2_oid = stub2.oid();

  stub1 = std::move(stub2);

  QVERIFY(stub1_oid != stub2_oid);
  // stub1 should now contain stub2s oid;
  QCOMPARE(stub2_oid, stub1.oid());
  

}

void test_GitOidHolder::test_assign()
{
  git_oid id;
  memset(id.id,3,20);
  // p shouldn't be used. any value is ok
  git_repository* p = (git_repository*)567;

  //test ctor
  //qigGitOidHolderStub  test_objectmock1(&id, p);

  int reload_called_cached = reload_called;
  int reset_called_cached = reset_called;
  reset();
  reloadFromId();
  if( ++reload_called_cached != reload_called || ++reset_called_cached != reset_called)
  {
    QFAIL("check the test impl");
  }

  // should not reload because they are the same object
  //load(&id, p);

  // maybe remove. because of performance. in most cases the shouldn't be the case
  if( reload_called_cached != reload_called || reset_called_cached != reset_called)
  {
    //QFAIL("oid reloaded the same object");
  }

  // load another oid
  git_oid id_another;
  memset(id.id,1,20);
  load(&id_another, p);

  QCOMPARE( reload_called , ++reload_called_cached);
  QCOMPARE( reset_called , ++reset_called_cached);


  qigGitOidHolderStub test_object1 (&id, p);
  load(test_object1);
  QCOMPARE( reload_called , ++reload_called_cached);
  QCOMPARE( reset_called , ++reset_called_cached);

  QVERIFY2(test_object1==*this, "copy differ from source");
}

