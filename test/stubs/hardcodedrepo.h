#pragma once
#include <inttypes.h>


struct _hardecoded_objects {
  int8_t type;
  uint8_t oid[20];
  uint16_t len;
  uint8_t data[400];
};

typedef struct _hardecoded_objects hardcoded_object;

extern const hardcoded_object objects[];
