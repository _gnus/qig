#include "../../src/gitTypes.h"

#ifdef __cplusplus
extern "C"
{
#endif

extern git_oid empty_blob;
extern git_oid empty_tree;

// creates a repository without filesystem backend but with some commits
git_repository* createSimple();

// load local repository
git_repository* loadLocal();


void free_repository(git_repository*);

extern const int objects_length;

struct git_oid;
struct git_object;

bool fillOidByIndex(git_oid* oid, int index);
// return a new object that should be freed by git_object_free();
git_object* getObjectByIndex(int index);

// returns the n-th occurrence an object of type type
git_object *getObjectByTypeAndIndex(int type, int n);

extern const int commit_length;


#ifdef __cplusplus
}
#endif
