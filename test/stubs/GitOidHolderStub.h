#pragma once

#include "../../src/gitTypes.h"
#include "../../src/GitOidHolder.h"

class qigGitOidHolderStub: public qigGitOidHolder
{
  public:
    qigGitOidHolderStub(const git_oid* id, git_repository*p): qigGitOidHolder(id, p){}
    void reset(){};
    void reloadFromId(){};
};
