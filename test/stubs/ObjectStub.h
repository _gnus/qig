
#pragma once


#include "../../src/gitTypes.h"
#include "../../src/Object.h"
#include "../../src/Oid.h"

class qigObjectStub: public qig::qigObject
{
  public:
    qigObjectStub(){}
    qigObjectStub(git_object*p):
      qig::qigObject(p)
    {}
};

class qigOidStub: public qig::qigOid
{
  public:
    qigOidStub(){}
    qigOidStub(const git_oid*p, git_repository* rep)
    {
      load(p,rep);
    }
};
