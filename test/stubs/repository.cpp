#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include <git2/sys/odb_backend.h>
#include <git2.h>
#include "hardcodedrepo.c"
#include "repository.h"

#define ADDITIONAL 5
struct BackEnd {
   git_odb_backend backend;

   int cnt;

   hardcoded_object additional_objects[5];
};

const int objects_length = sizeof(objects)/sizeof(objects[0]);

void m_free(git_odb_backend* u)
{
  free((BackEnd *)u);
}

static git_repository * static_repo;

int m_write( git_odb_backend * b, const git_oid * id, const void *data, size_t len, git_otype t)
{
  BackEnd*be =(BackEnd*) b;
  if(be->cnt<ADDITIONAL)
  {
    hardcoded_object * current = & be->additional_objects[be->cnt];
    current->type=t;
    current->len=len;
    memcpy(&current->oid, &id->id,20);
    memcpy(&current->data, data, len);
    ++be->cnt;
  }
  return GIT_EUSER;
}
int m_read(void ** data, size_t * s, git_otype * t, git_odb_backend* b, const git_oid* oid)
{
  *s=0;
  int size = sizeof(objects)/sizeof(objects[0]);
  for (int i = 0 ; i< size; ++i)
  {
    const hardcoded_object*cur = &objects[i];
    if( !memcmp(cur->oid, oid->id, 20))
    {
      *s=cur->len;
      *data=git_odb_backend_malloc(b, *s);
      memcpy(*data, cur->data, *s);
      *t =(git_otype) cur->type;
      return 0;
    }
  }
  BackEnd * be = (BackEnd*) b;
  for(int i =0; i<be->cnt; ++i)
  {
    const hardcoded_object*cur = &be->additional_objects[i];
    if( !memcmp(cur->oid, oid->id, 20))
    {
      *s=cur->len;
      *data=git_odb_backend_malloc(b, *s);
      memcpy(*data, cur->data, *s);
      *t =(git_otype) cur->type;
      return 0;
    }
  }
  return -2;
}

int m_exists(git_odb_backend* o, const git_oid* oid)
{
  int size = sizeof(objects)/sizeof(objects[0]);
  for (int i = 0 ; i< size; ++i)
  {
    if( !memcmp(objects[i].oid, oid->id, 20))
      return true;
  }
  BackEnd * be = reinterpret_cast<BackEnd*>(o);

  size = be->cnt;
  for (int i = 0 ; i< size; ++i)
  {
    if( !memcmp(be->additional_objects[i].oid, oid->id, 20))
      return true;
  }
  return false;
}

static BackEnd* initbackend()
{
  BackEnd* backend = (BackEnd*) calloc(1,sizeof(BackEnd));

  git_odb_backend* odb = &backend->backend;

  git_odb_init_backend(odb, GIT_ODB_BACKEND_VERSION);
  odb->read = m_read;
  odb->exists = m_exists;
  odb->free = m_free;
  odb->write = m_write;
  return backend;
}
static git_odb* createOdb()
{
  BackEnd * b=initbackend();
  git_odb*odb;
  git_odb_new(&odb);
  int i = git_odb_add_backend(odb, &b->backend, 0);
  if(i)
    odb=0;

  return odb;
}

void free_repository(git_repository* p)
{
  git_repository_free(p);
}

git_oid empty_blob = {{ 0xe6, 0x9d, 0xe2, 0x9b, 0xb2, 0xd1, 0xd6, 0x43, 0x4b, 0x8b,
			       0x29, 0xae, 0x77, 0x5a, 0xd8, 0xc2, 0xe4, 0x8c, 0x53, 0x91 }};
git_oid empty_tree = {{ 0x4b, 0x82, 0x5d, 0xc6, 0x42, 0xcb, 0x6e, 0xb9, 0xa0, 0x60,
			       0xe5, 0x4b, 0xf8, 0xd6, 0x92, 0x88, 0xfb, 0xee, 0x49, 0x04 }};
git_repository* createSimple()
{
  git_odb* p_odb = createOdb();
  git_repository* p_repo = 0;
  if(p_odb!=0)
  {
    if(git_repository_wrap_odb(&p_repo, p_odb))
    {
      p_repo = NULL;
    }
    git_odb_free(p_odb);
  }
  static_repo = p_repo;
  return p_repo;
}

git_repository* loadLocal()
{
  git_repository *p_repo;
  char pwd[100];
  getcwd(pwd,100);
  // ensure 0 termination
  pwd[99]=0;

  if(!git_repository_open(&p_repo, pwd))
  {
    return p_repo;
  }
  return 0;
}


bool fillOidByIndex(git_oid *o, int index )
{
  if (index < objects_length)
  {
    memcpy(o->id, objects[index].oid, GIT_OID_RAWSZ);
    return true;
  }
  return false;
}

bool fillOidByTypeAndIndex(git_oid*o, int type, int index)
{
  ++index;
  for(unsigned int i =0; i<objects_length; ++i)
  {
    if(objects[i].type == type && --index==0)
    {
      return fillOidByIndex(o, i);
    }
  }
  return false;
}



git_object *getObjectByIndex(int index)
{
  git_object* p_obj;
  git_oid id;
  fillOidByIndex(&id, index);
  if(git_object_lookup(&p_obj, static_repo, &id, GIT_OBJ_ANY))
  {
    return 0;
  }
  return p_obj;
}


git_object *getObjectByTypeAndIndex(int type, int index)
{
  git_object* p_obj;
  git_oid id;
  if(fillOidByTypeAndIndex(&id, type, index))
  {
    if(git_object_lookup(&p_obj, static_repo, &id, GIT_OBJ_ANY)==0)
    {
      return p_obj;
    }
  }
  return 0;
}



















#if exe



int main(int argc, char** argv)
{
  git_libgit2_init();
  git_repository* p= createSimple();
  git_commit* ci;
  git_oid id;
  memcpy(id.id, objects[0].oid, 20);
  if(!git_commit_lookup(&ci, p, &id))
    printf("found commit");
  else
    printf("no commit found");
  git_repository_free(p);
  git_libgit2_shutdown();
  return 0;
};

#endif
