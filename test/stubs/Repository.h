#pragma once
#include "../../src/Repository.h"
namespace qig
{
class qigRepositoryStub:public qigBase 
{
  public:
    inline qigRepositoryStub();
    qigRepositoryStub(qigBase&& b):qigBase(std::move(b)){};
    
    qigRepositoryStub(const qigBase&b):qigBase(b){};
    ~qigRepositoryStub() {
    }
    void init(git_repository*p) {
      repositoryLoaded(p);
    }
    void cleanUp(){
      repositoryUnloaded();
    }

  private:
    //inline static qigBase stub() {int err; return qigBase::load("",1,err);}
};

static int i;
inline qigRepositoryStub::qigRepositoryStub():
  qigBase(qigBase::load("",true, i) ) {
}
}
