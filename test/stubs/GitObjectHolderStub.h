#pragma once


#include "../../src/gitTypes.h"
#include "../../src/GitObjectHolder.h"

class qigGitObjectHolderStub: public qig::qigGitObjectHolder
{
  public:
    qigGitObjectHolderStub(){}
    qigGitObjectHolderStub(git_object*p): qigGitObjectHolder(p){}
    void reset(){};
    void reload(){};
};
