#pragma once

#include <framework.h>

#include <unordered_map>
#include <memory>

#include <../src/treeEntry.h>

class treeEntryMock;
// use same baseclass as treeEntry
/** test for qig::treeEntry
 * @class test_GitObjectHolder
 */
class test_treeEntry:public QObject
{
  Q_OBJECT
  typedef std::shared_ptr<treeEntryMock> treeEntryPtr;
  typedef std::weak_ptr<treeEntryMock> treeEntryWeakPtr;
  // treeEntry-> entries of its children
  typedef std::unordered_multimap<treeEntryPtr, treeEntryPtr> treeEntryList;
  typedef void (test_treeEntry::*onStep)(treeEntryPtr&, void *p);

  // the tests
  private Q_SLOTS:
    void test_ctor();
    void test_createTree();
    void test_delete();
    void test_setFlag();
    void test_parentChildRelation();
    void test_checkState();
    // end tests

  private Q_SLOTS:
    // global cleanup
    //void initTestCase();
    //void cleanupTestCase();

    //init for each test
    void init();
    void cleanup();

  // helper function
  public:
    test_treeEntry();

  private:
    onStep on_step;

    void checkChildrenCount(treeEntryPtr& parent, void* p);
    void checkDelete(treeEntryPtr& parent, void*);
    void createTree(treeEntryList &to_be_filled) const;
    /// returns the root of our tree created by the treeEntries.
    /// to obtain the root of the treeEntryList use find treeEntryPtr();
    treeEntryPtr& root();
    // traverse tree and call on_step on each node except of root
    // returns number of nodes visited
    // @param parent; the root to start at
    // @param p a payload that can be used in the callback;
    int traverse(const treeEntryPtr& parent,  void*p);


  private:
    // build independant tree structure using the unordered_multimap
    treeEntryList m_tree_entries;
    
};

DECLARE_TEST(test_treeEntry);
