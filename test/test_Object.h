#pragma once
#include <framework.h>
#include <../src/Object.h>

//#include "repo_loader.h"

/** test for qig::qigGitObjectHolder
 * @class test_GitObjectHolder
 */
class test_Object:public QObject
{
  Q_OBJECT

    // the tests
  private Q_SLOTS:
    void test_ctor();
    void test_filter();
    void test_treeObject();
    // end tests

  private Q_SLOTS:
    // global cleanup
    void initTestCase();
    void cleanupTestCase();

    //init for each test
    //void init(){};
    //void cleanup();

  // helper function
  public:
    test_Object();

  private:
    git_object* m_object;
    git_repository *m_repo;
};

DECLARE_TEST(test_Object);
