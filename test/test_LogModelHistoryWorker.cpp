#include <thread>
#include <git2.h>

#include "stubs/ObjectStub.h"
#include "test_LogModelHistoryWorker.h"
#include "stubs/repository.h"
#include "../src/LogModelHistoryWorker.h"


namespace qig
{
class LogModelHistoryWorkerMock: public qig::qigLogModelHistoryWorker
{
  public:
    LogModelHistoryWorkerMock(const qig::qigBase* base):
      qigLogModelHistoryWorker(base),
      object_cnt(0),
      sleep(0){}
    ~LogModelHistoryWorkerMock(){}

    virtual int addObject(const qig::qigOid& oid) { 
      std::this_thread::sleep_for(std::chrono::milliseconds(sleep));
      return qigLogModelHistoryWorker::addObject(oid);
    }

    void setMSleep(int s) { sleep = s;}

    int object_cnt;

  private:
    int sleep;

};
}

test_LogModelHistoryWorker::test_LogModelHistoryWorker()
{
  m_object = 0;
  m_repo=0;
}

void test_LogModelHistoryWorker::initTestCase()
{
  git_libgit2_init();
  m_repo=  createSimple();

  if(objects_length <2)
    throw 1;

  m_object = getObjectByTypeAndIndex(qig::objectTypeCommit, 12);
  m_repo_wrap.init(m_repo);
}

void test_LogModelHistoryWorker::cleanupTestCase()
{
  git_object_free(m_object);
  m_repo_wrap.cleanUp();
  git_libgit2_shutdown();
}


void test_LogModelHistoryWorker::test_traverseRepoWithoutHide()
{
  // test the default ctor;
  git_object* obj1; git_object_dup(&obj1, m_object);
  qigObjectStub head(obj1);
  
  qig::qigLogModelHistoryWorker test_object(&m_repo_wrap);
  qig::qigOid oid;
  head.oid(oid);
  test_object.pushStart(oid);

  test_object.start();

  //std::this_thread::sleep_for(std::chrono::milliseconds(1000));

  while ( !test_object.hasFinished() )
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }

  // test the result;

  qig::qigLogModelDataMap commits_and_connection = test_object.result();
  int branch_cnt = commits_and_connection.branchCount();
  int commits_and_connection_size = commits_and_connection.storedObjectsCounter();
  QCOMPARE(commits_and_connection_size, 12);
  QCOMPARE(branch_cnt, 5);
  {
    typedef qig::qigLogModelHistoryWorker::state state;
    const unsigned int commit = state::commit;
    const unsigned int fork = state::fork;
    const unsigned int forkStart = state::forkStart;
    const unsigned int forkEnd = state::forkEnd;
    //const unsigned int merge = state::merge;
    const unsigned int mergeStart = state::mergeStart;
    const unsigned int mergeEnd = state::mergeEnd;
    const unsigned int connection = state::connection;
    //const unsigned int shiftOut = state::shiftOut;
    const unsigned int shiftOutStart = state::shiftOutStart;
    const unsigned int shiftOutEnd = state::shiftOutEnd;
    //const unsigned int shiftIn = state::shiftIn;
    const unsigned int shiftInStart = state::shiftInStart;
    const unsigned int shiftInEnd = state::shiftInEnd;

    unsigned int flags_mask = 0x3fff;
    {
      auto iter = commits_and_connection.find(0,0);
      QVERIFY(iter != commits_and_connection.end());
      QVERIFY(head==*iter);
      // head is merge commit
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), commit|mergeStart|connection );
    }

    {
      //dummy entry to show parent child relation
      auto iter = commits_and_connection.find(0,1);
      QVERIFY(iter != commits_and_connection.end());
      //QVERIFY(head==*iter);
      // no real commit
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), mergeEnd);
    }
    {
      auto iter = commits_and_connection.find(1,1);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), connection|commit);
    }

    {
      auto iter = commits_and_connection.find(2,1);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), shiftOutStart | mergeEnd);
    }
    {
      auto iter = commits_and_connection.find(5,0);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), connection);
    }
    {
      auto iter = commits_and_connection.find(5,1);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), connection|commit);
    }
    {
      auto iter = commits_and_connection.find(6,0);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), connection|commit|mergeStart);
    }
    {
      auto iter = commits_and_connection.find(6,1);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), shiftOutStart | mergeEnd);
    }
    {
      auto iter = commits_and_connection.find(6,2);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), shiftOutStart | shiftOutEnd);
    }
    {
      auto iter = commits_and_connection.find(6,3);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), shiftOutStart | shiftOutEnd);
    }
    {
      auto iter = commits_and_connection.find(6,4);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), shiftOutEnd);
    }
    {
      auto iter = commits_and_connection.find(6,5);
      QVERIFY(iter == commits_and_connection.end());
    }
    {
      auto iter = commits_and_connection.find(8,0);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), connection);
    }
    {
      auto iter = commits_and_connection.find(8,1);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), connection|commit |forkStart | mergeStart);
    }
    {
      auto iter = commits_and_connection.find(8,2);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), mergeEnd | forkEnd| fork);
    }
    {
      auto iter = commits_and_connection.find(8,3);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), forkEnd | shiftInEnd);
    }
    {
      auto iter = commits_and_connection.find(8,4);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), shiftInStart);
    }
    {
      auto iter = commits_and_connection.find(8,5);
      QVERIFY(iter == commits_and_connection.end());
    }
    {
      auto iter = commits_and_connection.find(9,3);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), connection);
    }
    {
      auto iter = commits_and_connection.find(9,4);
      QVERIFY(iter == commits_and_connection.end());
    }
    {
      auto iter = commits_and_connection.find(9,5);
      QVERIFY(iter == commits_and_connection.end());
    }
    {
      auto iter = commits_and_connection.find(10,2);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), forkEnd | shiftInEnd);
    }
    {
      auto iter = commits_and_connection.find(10,3);
      QVERIFY(iter != commits_and_connection.end());
      QCOMPARE(iter->hasRepresentationFlag(flags_mask), shiftInStart);
    }
    {
      auto iter = commits_and_connection.find(10,4);
      QVERIFY(iter == commits_and_connection.end());
    }
  }
  for(auto iter = commits_and_connection.begin(); iter != commits_and_connection.end() ;++iter)
  {
    if(iter->hasRepresentationFlag(qig::qigLogModelHistoryWorker::commit))
    {
      QByteArray raw_oid;
      iter->rawData(raw_oid);
      QVERIFY(raw_oid.size()==OID_RAWSZ);
      const qig::qigOid& oid=*iter;
      QVERIFY(oid.isValid());
    }
  }
}

void test_LogModelHistoryWorker::test_abort()
{
  // test the default ctor;
  git_object* obj1; git_object_dup(&obj1, m_object);
  qigObjectStub head(obj1);
  
  qig::LogModelHistoryWorkerMock test_object(&m_repo_wrap);
  qig::qigOid oid;
  head.oid(oid);
  test_object.pushStart(oid);

  test_object.setMSleep(100);
  test_object.setMSleep(000);
  test_object.start();
  test_object.abort();
  QVERIFY(test_object.object_cnt < 3);
}
