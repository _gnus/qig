
#pragma once
#include <framework.h>
#include "../src/GitObjectHolder.h"

//#include "repo_loader.h"

using namespace qig;
/** test for qig::qigGitObjectHolder
 * @class test_GitObjectHolder
 */
class test_GitObjectHolder:public QObject,private qigGitObjectHolder//, public repo_loader
{
  Q_OBJECT

    // the tests
  private Q_SLOTS:
    void test_ctor();
    void test_assign();
    // end tests

  private Q_SLOTS:
    // global cleanup
    void initTestCase();
    void cleanupTestCase();

    //init for each test
    //void init(){};
    //void cleanup();

  // helper function
  public:
    test_GitObjectHolder();

  private:
    virtual void reload(){++reload_called;}
    virtual void reset(){++reset_called;}
    git_object* m_object;
    git_object* m_object_other;
    git_repository *m_repo;


  private:
    int reload_called;
    int reset_called;
};

DECLARE_TEST(test_GitObjectHolder);
