#include <git2.h>

#include "stubs/GitObjectHolderStub.h"
#include "test_GitObjectHolder.h"
#include "stubs/repository.h"

test_GitObjectHolder::test_GitObjectHolder()
{
  reload_called=0;
  reset_called=0;
  m_object_other= m_object = 0;
  m_repo=0;
}

void test_GitObjectHolder::initTestCase()
{
  git_libgit2_init();

  m_repo=  createSimple();

  if(objects_length <3)
    throw 1;

  m_object       = getObjectByTypeAndIndex(GIT_OBJ_TREE, 0);
  m_object_other = getObjectByTypeAndIndex(GIT_OBJ_TREE, 2);
}

void test_GitObjectHolder::cleanupTestCase()
{
  git_object_free(m_object);
  git_object_free(m_object_other);
  free_repository(m_repo);
  git_libgit2_shutdown();
}


void test_GitObjectHolder::test_ctor()
{
  // test the default ctor;
  git_object* obj1; git_object_dup(&obj1, m_object);
  qigGitObjectHolderStub test_object1(obj1);

  // create a new object from the same git_object 
  git_object* obj2; git_object_dup(&obj2, m_object);
  qigGitObjectHolderStub test_object2(obj2);

  // test cop ctor
  git_object* obj3; git_object_dup(&obj3, m_object);
  qigGitObjectHolderStub test_object3(obj3);
  //
  // test move ctor
  git_object* obj4; git_object_dup(&obj4, m_object);
  qigGitObjectHolderStub test_object4((qigGitObjectHolderStub(obj4)));
  //qigGitObjectHolderStub test_object4(m_object);

  // should be all the same
  QCOMPARE(test_object1, test_object2);
  QCOMPARE(test_object1, test_object3);
  //qig::qigGitObjectHolder& ob4 = test_object4;

  test_object4.object(objectTypeAny);
  QCOMPARE(test_object1, test_object4);

  QCOMPARE( test_object1.object(objectTypeAny), obj1);


  // create another object
  git_object* obj_other; git_object_dup(&obj_other, m_object_other);
  qigGitObjectHolderStub test_object_other(obj_other);
  QVERIFY2( !(test_object1 == test_object_other),  "objects should differ");
}

void test_GitObjectHolder::test_assign()
{
  // create an empty object


  int reload_called_cached = reload_called;
  int reset_called_cached = reset_called;

  // load another object. loading an object emits signals.
  git_object* obj1; git_object_dup(&obj1, m_object_other);
  load(obj1);
  QCOMPARE(++reload_called_cached, reload_called);
  QCOMPARE(++reset_called_cached, reset_called);

  QCOMPARE( object(objectTypeAny), m_object_other );

  // load another object by its id; 
  const git_oid* id=git_object_id(m_object);
  qigGitOidHolder::load(id, m_repo);
  QCOMPARE( object(objectTypeAny), m_object);
}
