cmake_minimum_required(VERSION 2.8.10)

#cmake_policy(CMP0043 NEW)

PROJECT(qig)

macro(process_config)
  include(config.cmake)

  if(NOT qig_BUILD_DIRECTORY)
    set(qig_BUILD_DIRECTORY "./build")
    MESSAGE(WARNING "insource build not supported. building in ${qig_BUILD_DIRECTORY}")
  endif()
  set(BUILD_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/${qig_BUILD_DIRECTORY}")
  set(CMAKE_BINARY_DIR ${BUILD_DIRECTORY})

  if(qig_RUNTIME_OUTPUT_DIRECTORY)
    if(IS_ABSOLUTE "${qig_RUNTIME_OUTPUT_DIRECTORY}")
      set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${qig_RUNTIME_OUTPUT_DIRECTORY}")
    else()
      set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/${qig_RUNTIME_OUTPUT_DIRECTORY}")
    endif()
  else()
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}")
  endif()
endmacro(process_config)


process_config()

MESSAGE("building in ${CMAKE_BINARY_DIR}")
MESSAGE("the binary will be placed in ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")

if(${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
  add_definitions("-DUNIX_BUILD")
elseif(${CMAKE_SYSTEM_NAME} STREQUAL "WIN32")
  add_definitions("-DWIN32")
endif()

set(VERSION ${VERSION_MAJOR}.${VERSION_MINOR})


#include_directories("${CMAKE_CURRENT_SOURCE_DIR}/src")
find_package(Qt5Widgets REQUIRED)


add_subdirectory(src "${CMAKE_BINARY_DIR}/lib")
add_subdirectory(test "${CMAKE_BINARY_DIR}/test")

add_executable(${PROJECT_NAME} qig.cpp)
target_compile_definitions(${PROJECT_NAME} PRIVATE VERSION=${VERSION};)

target_link_libraries(${PROJECT_NAME} ${PROJECT_NAME}_LIB Qt5::Widgets git2)


install(TARGETS ${PROJECT_NAME}
  RUNTIME DESTINATION ${BIN_INSTALL_DIRECTORY}
  LIBRARY DESTINATION ${LIN_INSTALL_DIRECTORY}
  ARCHIVE DESTINATION ${LIN_INSTALL_DIRECTORY}
  )
